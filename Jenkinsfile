node (label: 'docker') {
    def app
    def registry = "cp-registry.office.comscore.com"
    def label = "airflow-dags-tests"
    def tag = "${env.BUILD_NUMBER}"

    def image_name = "${registry}/${label}:${tag}"
    def CONT_AIRFLOW_HOME = "/usr/local/airflow"
    def test_results = "$WORKSPACE/results"

    stage('Clone repository') {
        /* Let's make sure we have the repository cloned to our workspace */
        checkout scm
    }
	
    stage('Build & publish test image') {
        app = docker.build(image_name, ".")
        app.push()
    }

    stage('Run tests & report') {
        sh "mkdir -p $test_results && chmod -R 771 $test_results && \
            docker run -d $image_name && \
            id=\$(docker ps -alq) && \
            exitcode=\$(docker wait \$id) && \
            name=\$(docker inspect \${id} --format='{{.Name}}') && \
            name=\${name:1} && \
            docker logs \${name} && \
            docker cp \$id:$CONT_AIRFLOW_HOME/results/test_results.xml $test_results && \
			exit \$exitcode"

        junit 'results/*.xml'
    }

    stage('Deliver dags to staging') {

         script {
            sshPublisher(
                continueOnError: false, failOnError: true,
                publishers: [
                sshPublisherDesc(
                    configName: "Airflow ETV",
                    verbose: true,
                    transfers: [
                sshTransfer(
                    sourceFiles:"dags/*.py",
                    remoteDirectory: "dags/tested",
                )
             ])
           ])
         }

    }

}
