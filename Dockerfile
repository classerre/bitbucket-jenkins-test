FROM python:2.7-slim

# Never prompts the user for choices on installation/configuration of packages
ENV DEBIAN_FRONTEND noninteractive
ENV TERM linux

# Airflow  $AIRFLOW__{SECTION}__{KEY}
ARG AIRFLOW_VERSION=1.10.2
ARG AIRFLOW_HOME=/usr/local/airflow
ARG DAGS_FOLDER=dags
ARG TESTS_FOLDER=test
ENV AIRFLOW_GPL_UNIDECODE yes

# Define en_US.
ENV LANGUAGE en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LC_ALL en_US.UTF-8
ENV LC_CTYPE en_US.UTF-8
ENV LC_MESSAGES en_US.UTF-8

RUN set -ex \
    && buildDeps=' \
        freetds-dev \
        libkrb5-dev \
        libsasl2-dev \
        libssl-dev \
        libffi-dev \
        libpq-dev \
        git \
    ' \
    && apt-get update -yqq \
    && apt-get upgrade -yqq \
    && apt-get install -yqq --no-install-recommends \
        $buildDeps \
        freetds-bin \
        build-essential \
        default-libmysqlclient-dev \
        apt-utils \
        curl \
        rsync \
        netcat \
        locales \
    && sed -i 's/^# en_US.UTF-8 UTF-8$/en_US.UTF-8 UTF-8/g' /etc/locale.gen \
    && locale-gen \
    && update-locale LANG=en_US.UTF-8 LC_ALL=en_US.UTF-8 \
    && useradd -ms /bin/bash -d ${AIRFLOW_HOME} airflow \
    && pip install -U pip setuptools wheel \
    && pip install pytz \
    && pip install pyOpenSSL \
    && pip install ndg-httpsclient \
    && pip install pyasn1 \
    && pip install pywinrm \
    && pip install apache-airflow[all]==${AIRFLOW_VERSION} \
    && pip install 'redis>=2.10.5,<3' \
    && apt-get purge --auto-remove -yqq $buildDeps \
    && apt-get autoremove -yqq --purge \
    && apt-get clean \
    && rm -rf \
        /var/lib/apt/lists/* \
        /tmp/* \
        /var/tmp/* \
        /usr/share/man \
        /usr/share/doc \
        /usr/share/doc-base \
    && pip install pytest \
    && pip install pycodestyle \
    && pip install pytest-html

RUN mkdir ${AIRFLOW_HOME}/results

COPY airflow.cfg ${AIRFLOW_HOME}/airflow.cfg
COPY entrypoint.sh ${AIRFLOW_HOME}/entrypoint.sh
COPY pytest_config.cfg ${AIRFLOW_HOME}/pytest_config.cfg

ENV AIRFLOW_HOME ${AIRFLOW_HOME}
RUN chown -R airflow ${AIRFLOW_HOME}
USER airflow
WORKDIR ${AIRFLOW_HOME}


ADD $DAGS_FOLDER ${AIRFLOW_HOME}/dags
ADD $TESTS_FOLDER ${AIRFLOW_HOME}/test
ENTRYPOINT ["./entrypoint.sh"]

