import imp
import runpy
import os
import pytest
from airflow import models as af_models

DAG_PATH = 'dags/'

DAG_FILES = [f for f in os.listdir(DAG_PATH) if f.endswith('.py')]


@pytest.mark.parametrize('dag_file', DAG_FILES)
def test_integrity(dag_file):
    module_name, _ = os.path.splitext(dag_file)
    module_path = os.path.join(DAG_PATH, dag_file)
    try:
        module = imp.load_source(module_name, module_path)
        runpy.run_path(module_path)
        assert any(
            isinstance(var, af_models.DAG)
            for var in vars(module).values()), "must be DAGs present in file " + dag_file
    except Exception as e:
        assert False, str(e.__class__.__name__) + " " + e.message
