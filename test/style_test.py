import os
import pycodestyle
import pytest

DAG_PATH = 'dags/'

DAG_FILES = [f for f in os.listdir(DAG_PATH) if f.endswith('.py')]


@pytest.mark.parametrize('dag_file', DAG_FILES)
def test_style(dag_file):
    pathfile = DAG_PATH+dag_file
    checker = pycodestyle.Checker(pathfile)
    style_issues = checker.check_all()
    assert style_issues == 0, pathfile+" must have 0 style issues, currently has " + str(style_issues)
