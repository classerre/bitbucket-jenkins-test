import imp
import os
import pytest
from airflow import models as af_models
from airflow.exceptions import AirflowDagCycleException

DAG_PATH = 'dags/'

DAG_FILES = [f for f in os.listdir(DAG_PATH) if f.endswith('.py')]


@pytest.mark.parametrize('dag_file', DAG_FILES)
def test_acyclicity(dag_file):
    module_name, _ = os.path.splitext(dag_file)
    module_path = os.path.join(DAG_PATH, dag_file)
    try:
        module = imp.load_source(module_name, module_path)
        for var in vars(module).values():
            if isinstance(var, af_models.DAG):
                var.test_cycle()
    except AirflowDagCycleException:
        pytest.fail("there is a cycle in the dag definition for " + var.dag_id)
