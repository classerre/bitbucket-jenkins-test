from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from datetime import datetime, timedelta  
from airflow.models import Variable

default_args = {
    'owner': 'vkomuravelli',
    'depends_on_past': False,
    'start_date': datetime(2019, 1, 31),
    'email': ['sgude@comscore.com, vkomuravelli@comscore.com, skamidi@comscore.com, rbhallamudi@comscore.com, gmuthyala@comscore.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=1)
}	
   
def etv_tv_data_loads(dag_name):
  dag = DAG(dag_name, default_args=default_args, schedule_interval=None)
  
  
  DataQuality_Check_Bounding	= BashOperator(task_id='Data_Loading_S3_to_Snowflake',
  												bash_command='/etv/bin/data_loads/datacopy.sh public dataloadtest 0 989', 
  												dag=dag										
  												)
                                                                        
  return dag

etv_tv_data_loads_dag = etv_tv_data_loads('etv_tv_data_loads')