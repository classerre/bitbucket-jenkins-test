from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.models import Variable
from datetime import timedelta,datetime
from operators.dfm import DFMOperator
from comscoreEmail import notify_email
from csdatetools import csdatetools

default_args = {
  'owner': 'etv',
  'depends_on_past': False,
  'start_date': datetime(2019, 1, 31),
  'email': [
      'sgude@comscore.com, vkomuravelli@comscore.com, skamidi@comscore.com, rbhallamudi@comscore.com, gmuthyala@comscore.com'],
  'email_on_failure': False,
  'email_on_retry': False,
  'retries': 0, 
  'on_failure_callback': notify_email,
  'retry_delay': timedelta(minutes=1)
} 

def etv_daily_duration_tvd_apply(dag_name):


  dag = DAG(dag_name, default_args=default_args, schedule_interval=None)

  # DAG Specific constants
  PRODUCT = 'ETV'
  PLATFORM = 'DIGITAL'
  process_system = 'Digital_TVD'
  process_subsystem = 'tvd_apply'

  # Variables
  BASE_SQL_PATH = Variable.get("base_sql_path") + 'etv_daily_duration_tvd_apply'
  WEEK_ID = Variable.get("week_id")
  CONFIG_PATH = Variable.get("config_path")
  BASE_s3_PATH = Variable.get("base_s3_loc") + '/'
  BASE_s3_PATH_WEEKLY = BASE_s3_PATH + WEEK_ID + 'w'

  # Derived Variables
  Week = 'W' + WEEK_ID
  WEEK_1 = str(int(WEEK_ID) - 5)
  WEEK_2 = str(int(WEEK_ID) - 4)
  WEEK_3 = str(int(WEEK_ID) - 3)
  WEEK_4 = str(int(WEEK_ID) - 2)
  WEEK_5 = str(int(WEEK_ID) - 1)

  HDFS_STAGE_S3 = Variable.get("base_s3_path")
  DAILY_DURATION_HDFS_PATH = '/mapr/ri1.comscore.com/test_data/etv/daily_duration_tvd/' + WEEK_ID + 'w/finalclose'
  SCHEDULES_HDFS_PATH = '/mapr/ri1.comscore.com/test_data/etv/tv_dictionary'
  #NOTE: BOTH THE BELOW PATHS SHOULD BE SAME
  DAILY_DURATION_S3_PATH = BASE_s3_PATH_WEEKLY + '/daily_duration_tvd_apply/finalclose'
  DAILY_DURATION_S3_STAGE = HDFS_STAGE_S3 + '/' + WEEK_ID + 'w/daily_duration_tvd_apply/finalclose'
  DIGITAL_TVD_MAPPING_S3_PATH = BASE_s3_PATH_WEEKLY + '/digital_tvd_mapping/'
  DIGITAL_TVD_MAPPING_HDFS_PATH = '/mapr/ri1.comscore.com/test_data/etv/' + WEEK_ID + 'w/digital_tvd_mapping/'

  START_TIME_ID = str(csdatetools.get_first_time_id_in_week_id(int(WEEK_ID)))
  END_TIME_ID = str(csdatetools.get_last_time_id_in_week_id(int(WEEK_ID)))
  RUN_AGG = Variable.get("RUN_AGG")
  RUN_BATCH = Variable.get("RUN_BATCH")
  MONTH_ID = str(csdatetools.week_id_to_broadcast_month(int(WEEK_ID)))

  if RUN_BATCH == "TRUE":
    Prep_the_TV_Schedule = BashOperator(task_id='Prep_the_TV_Schedule',bash_command='snowsql --config ' + CONFIG_PATH + ''
                                              ' -f ' + BASE_SQL_PATH + '/prep_tv_scheduling.sql'
                                              ' -D  WEEK_SCHEMA_0=' + Week + ''
                                              ' -D  startTimeID=' + START_TIME_ID + ''
                                              ' -D  endTimeID=' + END_TIME_ID + ''
                                              ' -D  S3_STAGE=' + HDFS_STAGE_S3 + ''
                                              ' -D  WEEK_ID=' + WEEK_ID + '',
                                              dag=dag)

    # IF NEED TO CHANGE BELOW S3 PATHS, MAKE SURE TO CHANGE IT IN ABOVE SQL FILE "prep_tv_scheduling.sql" AT BOTTOM OF THE SCRIPT.                    
    Copy_Files_To_HDFS = BashOperator(task_id='copy_files_to_hdfs',
                                     bash_command='aws s3 cp ' + BASE_s3_PATH + WEEK_ID +'w/tv_dictionary/schedule_'+ WEEK_ID +'w.txt ' + SCHEDULES_HDFS_PATH + ' && '
                                     'aws s3 sync ' + DIGITAL_TVD_MAPPING_S3_PATH + ' ' + DIGITAL_TVD_MAPPING_HDFS_PATH + ' && '
                                     'aws s3 rm ' + DAILY_DURATION_S3_PATH + ' --recursive',
                                     dag=dag)

    Daily_Duaration_TVD_Apply_DFM = DFMOperator(
        task_id = "Daily_Duaration_TVD_Apply_DFM",
        submitter_id = 3009, # Borg Adhoc submitter id from DFM, example 3009 = etv_daily_duration_tvd_apply_pig_task_test
        overwrite_tags = { "WEEK_ID": WEEK_ID, "BASE_OUTPUT_PATH": DAILY_DURATION_HDFS_PATH, "SCHEDULES_FINAL_OUTPUT": SCHEDULES_HDFS_PATH, "S3_OUTPUT_PATH": DAILY_DURATION_S3_PATH, "DIGITAL_TVD_MAPPING": DIGITAL_TVD_MAPPING_HDFS_PATH }, # No tags = {}
        timeout = 36000, # 10 hours
        dag=dag
     )
     
    '''Move_Daily_Duaration_Output_To_S3 = BashOperator(task_id='move_daily_duaration_output_to_s3',
                                     bash_command='aws s3 sync ' + DAILY_DURATION_HDFS_PATH + ' ' + DAILY_DURATION_S3_PATH,
                                     dag=dag)'''


    if RUN_AGG == "TRUE":                                                            
      Daily_Duration_TVD_Agg = BashOperator(task_id='Daily_Duration_TVD_Agg',bash_command='snowsql --config ' + CONFIG_PATH + ''
                                                              ' -f ' + BASE_SQL_PATH + '/tvd_dd_qa_agg.sql'
                                                              ' -D  WEEK_SCHEMA_0=' + Week + ''
                                                              ' -D  DAILY_DURATION_S3_STAGE=' + DAILY_DURATION_S3_STAGE + ''
                                                              ' -D  WEEK_ID=' + WEEK_ID + '',
                                                              dag=dag)
                                                                
      TVD_DD_Summary_Report = BashOperator(task_id='TVD_DD_Summary_Report',bash_command='snowsql --config ' + CONFIG_PATH + ''
                                                                ' -f ' + BASE_SQL_PATH + '/etv_tvd_apply_summary_psql.sql'
                                                                ' -D  WEEK_0=' + WEEK_ID + ''
                                                                ' -D  WEEK_SCHEMA_0=' + Week + ''
                                                                ' -D  MONTH_ID=' + MONTH_ID + '',
                                                                dag=dag)


  if RUN_BATCH == "TRUE":
    Prep_the_TV_Schedule >> Copy_Files_To_HDFS >> Daily_Duaration_TVD_Apply_DFM

    if RUN_AGG == "TRUE":
      #Move_Daily_Duaration_Output_To_S3.set_upstream(Daily_Duaration_TVD_Apply_DFM)
      Daily_Duration_TVD_Agg.set_upstream(Daily_Duaration_TVD_Apply_DFM)
      TVD_DD_Summary_Report.set_upstream(Daily_Duration_TVD_Agg)
      
  return dag

etv_daily_duration_tvd_apply_dag = etv_daily_duration_tvd_apply('etv_daily_duration_tvd_apply')
