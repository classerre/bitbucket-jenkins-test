__author__ = " Vidyan Komuravelli "

"""
       importing dependencies for etv_batch0
"""
from etv_tv_shared_pre_batch import etv_tv_shared_pre_batch
from etv_tv_data_loads import etv_tv_data_loads

from airflow.models import DAG
from airflow.models import Variable
from airflow.operators.subdag_operator import SubDagOperator
from datetime import datetime, timedelta
from airflow.executors.celery_executor import CeleryExecutor
from comscoreEmail import returnEmailOperator

BATCH_NAME = 'etv_batch0'
WEEK_ID = Variable.get("week_id")

args = {
    'owner': 'etv',
    'depends_on_past': False,
    'start_date': datetime(2019, 1, 24),
    'email': ['sgude@comscore.com', 'vkomuravelli@comscore.com', 'skamidi@comscore.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=1)
}

def etv_batch0(dag_name):
    dag = DAG(dag_name, default_args=args, schedule_interval=None)

    etv_tv_data_loads_dag = SubDagOperator(
        executor=CeleryExecutor(),
        task_id='etv_tv_data_loads',
        subdag=etv_tv_data_loads(dag_name + '.etv_tv_data_loads'),
        default_args=args,
        dag=dag)

    etv_tv_shared_pre_batch_dag = SubDagOperator(
        executor=CeleryExecutor(),
        task_id='etv_tv_shared_pre_batch',
        subdag=etv_tv_shared_pre_batch(dag_name + '.etv_tv_shared_pre_batch'),
        default_args=args,
        dag=dag)

    # Dependencies:
    etv_tv_shared_pre_batch_dag.set_upstream(etv_tv_data_loads_dag)

    notify_success = returnEmailOperator(dag, BATCH_NAME, WEEK_ID)
    notify_success.set_upstream(etv_tv_shared_pre_batch_dag)
    return dag

etv_batch0_dag = etv_batch0(BATCH_NAME)