__author__ = " Srinivasa Gude "

import airflow
"""
       importing dependencies for etv_batch2
"""
from etv_tv_dvr_batch import etv_tv_dvr_batch
from etv_tv_live_smalls_monthly_spark_alpha import etv_tv_live_smalls_monthly_spark_alpha
from etv_tv_overlap_batch import etv_tv_overlap_batch
from etv_tv_vod_batch import etv_tv_vod_batch
from etv_tv_live_inc_bounding import etv_tv_live_inc_bounding
from etv_tv_live_smalls_weekly_spark_alpha import etv_tv_live_smalls_weekly_spark_alpha

from airflow.models import DAG
from airflow.models import Variable
from airflow.operators.subdag_operator import SubDagOperator
from datetime import datetime, timedelta
from airflow.executors.celery_executor import CeleryExecutor
from comscoreEmail import returnEmailOperator


BATCH_NAME = 'etv_batch2'
WEEK_ID = Variable.get("week_id")

args = {
    'owner': 'etv',
    'depends_on_past': False,
    'start_date': datetime(2019, 1, 29),
    'email': ['sgude@comscore.com', 'vkomuravelli@comscore.com', 'skamidi@comscore.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 3,
    'retry_delay': timedelta(minutes=1)
}

def etv_batch2(dag_name):
    dag = DAG(dag_name, default_args=args, schedule_interval=None, concurrency=10)

    etv_tv_dvr_batch_dag = SubDagOperator(
        executor=CeleryExecutor(),
        task_id='etv_tv_dvr_batch',
        subdag=etv_tv_dvr_batch(dag_name +'.etv_tv_dvr_batch'),
        default_args=args,
        dag=dag)

    etv_tv_live_smalls_weekly_spark_alpha_dag = SubDagOperator(
        executor=CeleryExecutor(),
        task_id='etv_tv_live_smalls_weekly_spark_alpha',
        subdag=etv_tv_live_smalls_weekly_spark_alpha(dag_name +'.etv_tv_live_smalls_weekly_spark_alpha'),
        default_args=args,
        dag=dag)

    etv_tv_live_smalls_monthly_spark_alpha_dag = SubDagOperator(
        executor=CeleryExecutor(),
        task_id='etv_tv_live_smalls_monthly_spark_alpha',
        subdag=etv_tv_live_smalls_monthly_spark_alpha(dag_name +'.etv_tv_live_smalls_monthly_spark_alpha'),
        default_args=args,
        dag=dag)

    etv_tv_overlap_batch_dag= SubDagOperator(
        executor=CeleryExecutor(),
        task_id='etv_tv_overlap_batch',
        subdag=etv_tv_overlap_batch(dag_name +'.etv_tv_overlap_batch'),
        default_args=args,
        dag=dag)

    etv_tv_vod_batch_dag = SubDagOperator(
        executor=CeleryExecutor(),
        task_id='etv_tv_vod_batch',
        subdag=etv_tv_vod_batch(dag_name +'.etv_tv_vod_batch'),
        default_args=args,
        dag=dag)

    etv_tv_live_inc_bounding_dag = SubDagOperator(
        executor=CeleryExecutor(),
        task_id='etv_tv_live_inc_bounding',
        subdag=etv_tv_live_inc_bounding(dag_name +'.etv_tv_live_inc_bounding'),
        default_args=args,
        dag=dag)

    # Dependencies :
    etv_tv_live_smalls_monthly_spark_alpha_dag.set_upstream(etv_tv_live_smalls_weekly_spark_alpha_dag)
    etv_tv_live_inc_bounding_dag.set_upstream(etv_tv_live_smalls_monthly_spark_alpha_dag)
    etv_tv_overlap_batch_dag.set_upstream(etv_tv_live_inc_bounding_dag)
    etv_tv_overlap_batch_dag.set_upstream(etv_tv_dvr_batch_dag)
    etv_tv_overlap_batch_dag.set_upstream(etv_tv_vod_batch_dag)

    notify_success = returnEmailOperator(dag, BATCH_NAME, WEEK_ID)
    notify_success.set_upstream(etv_tv_overlap_batch_dag)
    return dag

etv_batch2_dag = etv_batch2(BATCH_NAME)