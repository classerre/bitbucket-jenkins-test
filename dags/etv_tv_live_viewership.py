from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.contrib.operators.ssh_operator import SSHOperator
from airflow.models import Variable
from airflow.contrib.hooks.ssh_hook import SSHHook
from datetime import datetime, timedelta
from emr_operations import emr_tasks
from comscoreEmail import notify_email
from csdatetools.csdatetools import get_first_time_id_in_week_id, get_last_time_id_in_week_id, time_id_to_yyyymmdd, \
    time_id_to_broadcast_month, datetime_to_timestamp, datetime_to_time_id

default_args = {
    'owner': 'etv',
    'depends_on_past': False,
    'start_date': datetime(2019, 1, 31),
    'email': ['sgude@comscore.com, vkomuravelli@comscore.com, skamidi@comscore.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=1),
    'on_failure_callback': notify_email
}


def etv_tv_live_viewership(dag_name):
    dag = DAG(dag_name, default_args=default_args, schedule_interval=None)

    emr = emr_tasks(dag)
    sshHook = emr.get_ssh_hook("live_viewership")

    CONFIG_PATH = Variable.get("config_path")
    BASE_SQL_PATH = Variable.get("base_sql_path") + 'etv_tv_live_viewership'
    DATA_EXPORT_SQL_PATH = Variable.get("base_sql_path")
    WEEK_ID = Variable.get("week_id")
    Tasks = Variable.get("etv_tv_live_viewership_TaskList")
    S3_STAGE_PATH = Variable.get("base_s3_path")
    S3_BASE_PATH = Variable.get("base_s3_loc")
    UTILITY_PATH = Variable.get("base_sql_path") + 'utilities' 

    # Derived Variables
    WEEK_SCHEMA_0 = 'W' + WEEK_ID
    PREVIOUS_WEEK_ID_1 = str(int(WEEK_ID) - 1)
    PREVIOUS_WEEK_ID_5 = str(int(WEEK_ID) - 5)
    WEEK_SCHEMA_1 = 'W' + PREVIOUS_WEEK_ID_1
    TaskList = Tasks.split(",")

    FIRST_TIME_ID_THRESHOLD = Variable.get("first_time_id_threshold")
    # "{{ dag_run.conf['first_time_id_threshold'] }}"
    FIRST_TIME_ID_IN_RANGE = str(get_first_time_id_in_week_id(int(WEEK_ID)))
    LAST_TIME_ID_IN_RANGE = str(get_last_time_id_in_week_id(int(WEEK_ID)))
    START_DATE = str(time_id_to_yyyymmdd(int(FIRST_TIME_ID_IN_RANGE) - int(FIRST_TIME_ID_THRESHOLD)))
    END_DATE = str(time_id_to_yyyymmdd(int(LAST_TIME_ID_IN_RANGE)))
    BC_MONTH_ID = str(time_id_to_broadcast_month(int(LAST_TIME_ID_IN_RANGE)))
    DATE_TIME = str(datetime_to_timestamp(datetime.now()))
    CURRENT_TIMEID = str(datetime_to_time_id(datetime.now()))

    if "LIVEVIEWXREF" in TaskList:
        truncate_dictionary_live_viewership = BashOperator(
            task_id='truncate_dictionary_live_viewership',
            bash_command='snowsql --config {}'.format(CONFIG_PATH) +
                         ' -f {}/etv_live_viewership_truncate.sql'.format(BASE_SQL_PATH) +
                         ' -D WEEK_SCHEMA_0={} '.format(WEEK_SCHEMA_0)
            , dag=dag
        )

        live_viewership_xref = BashOperator(
            task_id='live_viewership_xref',
            bash_command='snowsql --config {}'.format(CONFIG_PATH) +
                         ' -f {}/live_viewership_xref.sql'.format(BASE_SQL_PATH) +
                         ' -D WEEK_SCHEMA_0={} '.format(WEEK_SCHEMA_0) +
                         ' -D WEEK_0={} '.format(WEEK_ID) +
                         ' -D WEEK_5={} '.format(PREVIOUS_WEEK_ID_5)
            , dag=dag
        )

    if "LIVEVIEW" in TaskList:
        process_manual_program_telecast_xref_step = BashOperator(
            task_id='process_manual_program_telecast_xref',
            bash_command='snowsql --config {}'.format(CONFIG_PATH) +
                         ' -f {}/process_manual_program_telecast_xref.sql'.format(BASE_SQL_PATH) +
                         ' -D WEEK_SCHEMA_0={} '.format(WEEK_SCHEMA_0) +
                         ' -D firstTimeIdInRange={} '.format(FIRST_TIME_ID_IN_RANGE) +
                         ' -D endTimeIdInRange={} '.format(LAST_TIME_ID_IN_RANGE)
            , dag=dag
        )

        compress_program_telecast_xref_step = BashOperator(
            task_id='compress_program_telecast_xref',
            bash_command='snowsql --config {}'.format(CONFIG_PATH) +
                         ' -f {}/compress_program_telecast_xref.sql'.format(BASE_SQL_PATH) +
                         ' -D WEEK_SCHEMA_0={} '.format(WEEK_SCHEMA_0)
            , dag=dag
        )

        filter_views_by_market_station_day_step = BashOperator(
            task_id='filter_view_by_market_station_day',
            bash_command='snowsql --config {}'.format(CONFIG_PATH) +
                         ' -f {}/filter_views_by_market_station_day.sql'.format(BASE_SQL_PATH) +
                         ' -D WEEK_SCHEMA_0={} '.format(WEEK_SCHEMA_0) +
                         ' -D firstTimeIdInRange={} '.format(FIRST_TIME_ID_IN_RANGE) +
                         ' -D endTimeIdInRange={} '.format(LAST_TIME_ID_IN_RANGE)
            , dag=dag
        )

        build_grouped_live_viewership_step = BashOperator(
            task_id='build_grouped_live_viewership',
            bash_command='snowsql --config {}'.format(CONFIG_PATH) +
                         ' -f {}/build_grouped_live_viewership.sql'.format(BASE_SQL_PATH) +
                         ' -D WEEK_SCHEMA_0={} '.format(WEEK_SCHEMA_0) +
                         ' -D START_DATE={} '.format(START_DATE) +
                         ' -D END_DATE={} '.format(END_DATE)
            , dag=dag
        )

        prepare_projected_hours_by_series_station_step = BashOperator(
            task_id='prepare_projected_hours_by_series_station',
            bash_command='snowsql --config {}'.format(CONFIG_PATH) +
                         ' -f {}/prepare_projected_hours_by_series_station.sql'.format(BASE_SQL_PATH) +
                         ' -D WEEK_SCHEMA_0={} '.format(WEEK_SCHEMA_0)
            , dag=dag
        )


        delete_export_pre_projected_hours_by_series_station=BashOperator(
            task_id='delete_export_pre_projected_hours_by_series_station'
            , bash_command='{}/deleteS3Object.sh '.format(UTILITY_PATH) +
                           '--s3Path {0}/{1}w/projected_viewership/pre_projected_hours_by_series_station/'.format(
                               S3_BASE_PATH, WEEK_ID)
            , dag=dag
        )

        delete_export_grouped_live_viewership = BashOperator(
            task_id='delete_export_grouped_live_viewership'
            , bash_command='{}/deleteS3Object.sh '.format(UTILITY_PATH) +
                           '--s3Path {0}/{1}w/projected_viewership/grouped_live_viewership/'.format(
                               S3_BASE_PATH, WEEK_ID)
            , dag=dag
        )

        delete_output_build_projected_hours_by_series_station = BashOperator(
            task_id='delete_output_build_projected_hours_by_series_station'
            , bash_command='{}/deleteS3Object.sh '.format(UTILITY_PATH) +
                           '--s3Path {0}/{1}w/projected_viewership/projected_hours_by_series_station/ '.format(S3_BASE_PATH, WEEK_ID)
            , dag=dag
        )

        delete_output_build_unprojected_live_viewership_agg = BashOperator(
            task_id='delete_output_build_unprojected_live_viewership_agg'
            , bash_command='{}/deleteS3Object.sh '.format(UTILITY_PATH) +
                           '--s3Path {0}/{1}w/projected_viewership/unprojected_live_viewership_agg/ '.format(S3_BASE_PATH, WEEK_ID)
            , dag=dag
        )


        # Input preparation for EMR jobs
        export_pre_projected_hours_by_series_station = BashOperator(
            task_id='export_pre_projected_hours_by_series_station',
            bash_command='snowsql --config {} '.format(CONFIG_PATH) +
                         ' -f {}exportData.sql '.format(DATA_EXPORT_SQL_PATH) +
                         ' -D  s3Path={0}/{1}w/projected_viewership/pre_projected_hours_by_series_station/part_r '.format(
                             S3_STAGE_PATH, WEEK_ID) +
                         ' -D  sourceTable={} '.format(WEEK_SCHEMA_0 + '.pre_projected_hours_by_series_station') +
                         ' -D  outputFormat={} '.format('MY_CSV_FORMAT_UNCOMPRESSED') +
                         ' -D  exportType={} '.format('false') +
                         ' -D  withHeader={} '.format('false')
            , dag=dag
        )

        export_grouped_live_viewership = BashOperator(
            task_id='export_grouped_live_viewership',
            bash_command='snowsql --config {} '.format(CONFIG_PATH) +
                         ' -f {}exportData.sql '.format(DATA_EXPORT_SQL_PATH) +
                         ' -D  s3Path={0}/{1}w/projected_viewership/grouped_live_viewership/part_r '.format(
                             S3_STAGE_PATH,
                             WEEK_ID) +
                         ' -D  sourceTable={} '.format(WEEK_SCHEMA_0 + '.grouped_live_viewership') +
                         ' -D  outputFormat={} '.format('MY_CSV_FORMAT_UNCOMPRESSED') +
                         ' -D  exportType={} '.format('false') +
                         ' -D  withHeader={} '.format('false')
            , dag=dag
        )

        build_projected_hours_by_series_station = SSHOperator(
            task_id='build_projected_hours_by_series_station',
            command='hadoop jar '
                    ' /home/hadoop/xms-projected-viewership-2.3.0.jar '
                    ' com.comscore.crm.projectedviewership.hadoop.projectedhoursbyseriesstation.ProjectedHoursBySeriesStationDriver '
                    ' -Dmapred.job.name=crm_xms_projected_viewership.build_projected_hours_by_series_station.205m '
                    ' -Dmapred.reduce.slowstart.completed.maps=1'
                    ' -Dmapred.job.shuffle.input.buffer.percent=0.7'
                    ' -Dio.sort.mb=1000 '
                    ' -Dio.sort.factor=100 '
                    ' -Dio.sort.record.percent=0.34 '
                    ' -Dmapred.reduce.tasks=1000 '
                    ' -Dmapred.max.split.size=134217728 '
                    ' {0}/{1}w/projected_viewership/pre_projected_hours_by_series_station '.format(S3_BASE_PATH,
                                                                                                   WEEK_ID) +
                    ' {0}/{1}w/projected_viewership/projected_hours_by_series_station/ '.format(S3_BASE_PATH, WEEK_ID) +
                    ' ' + FIRST_TIME_ID_IN_RANGE + '' +
                    ' ' + LAST_TIME_ID_IN_RANGE + '' +
                    ' ' + FIRST_TIME_ID_THRESHOLD + ''
            , dag=dag,
            ssh_hook=sshHook
        )

        build_unprojected_live_viewership_agg = SSHOperator(
            task_id='build_unprojected_live_viewership_agg',
            command='hadoop jar '
                    '/home/hadoop/xms-projected-viewership-2.3.0.jar '
                    ' com.comscore.crm.projectedviewership.hadoop.unprojectedliveviewershipagg.UnprojectedLiveViewershipAggDriver '
                    ' -Dmapred.job.name=crm_xms_projected_viewership.build_projected_hours_by_series_station.205m '
                    ' -Dmapred.reduce.slowstart.completed.maps=1'
                    ' -Dmapred.job.shuffle.input.buffer.percent=0.7'
                    ' -Dio.sort.mb=1000 '
                    ' -Dio.sort.factor=100 '
                    ' -Dio.sort.record.percent=0.25 '
                    ' -Dmapred.reduce.tasks=1000 '
                    ' -Dmapred.max.split.size=134217728 '
                    ' {0}/{1}w/projected_viewership/grouped_live_viewership '.format(S3_BASE_PATH, WEEK_ID) +
                    ' {0}/{1}w/projected_viewership/unprojected_live_viewership_agg/ '.format(S3_BASE_PATH, WEEK_ID) +
                    ' ' + FIRST_TIME_ID_IN_RANGE + '' +
                    ' ' + LAST_TIME_ID_IN_RANGE + '' +
                    ' ' + FIRST_TIME_ID_THRESHOLD + ''
            , dag=dag,
            ssh_hook=sshHook
        )

        copy_binaries = emr.copy_binaries(sshHook)
        create_cluster = emr.create_emr_cluster("Live_Viewership_MR", "30", "200")
        delete_cluster = emr.delete_cluster("Live_Viewership_MR")
        export_grouped_live_viewership >> create_cluster
        export_pre_projected_hours_by_series_station >> create_cluster
        create_cluster >> emr.sleep_operation() >> emr.update_airflow_emr_connection("Live_Viewership_MR",
                                                                                     "live_viewership") >> copy_binaries
        copy_binaries >> build_projected_hours_by_series_station
        copy_binaries >> build_unprojected_live_viewership_agg
        build_projected_hours_by_series_station >> delete_cluster
        build_unprojected_live_viewership_agg >> delete_cluster

        # 1 MR job
        import_projected_hours_by_series_station = BashOperator(
            task_id='import_projected_hours_by_series_station',
            bash_command='snowsql --config {} '.format(CONFIG_PATH) +
                         ' -f {}/load_projected_hours_by_series_station.sql '.format(BASE_SQL_PATH) +
                         ' -D WEEK_SCHEMA_0={} '.format(WEEK_SCHEMA_0) +
                         ' -D WEEK_ID={} '.format(WEEK_ID)
            , dag=dag)

        # 2 MR job
        import_unprojected_viewership_by_market = BashOperator(
            task_id='import_unprojected_viewership_by_market',
            bash_command='snowsql --config {} '.format(CONFIG_PATH) +
                         ' -f {}/load_unprojected_viewership_by_market.sql '.format(BASE_SQL_PATH) +
                         ' -D WEEK_SCHEMA_0={} '.format(WEEK_SCHEMA_0) +
                         ' -D WEEK_ID={} '.format(WEEK_ID)
            , dag=dag)

        import_unprojected_viewership_by_market_station_day = BashOperator(
            task_id='import_unprojected_viewership_by_market_station_day',
            bash_command='snowsql --config {} '.format(CONFIG_PATH) +
                         ' -f {}/load_unprojected_viewership_by_market_station_day.sql '.format(
                             BASE_SQL_PATH) +
                         ' -D WEEK_SCHEMA_0={} '.format(WEEK_SCHEMA_0) +
                         ' -D WEEK_ID={} '.format(WEEK_ID)
            , dag=dag)

        import_unprojected_viewership_by_market_day = BashOperator(
            task_id='import_unprojected_viewership_by_market_day',
            bash_command='snowsql --config {} '.format(CONFIG_PATH) +
                         ' -f {}/load_unprojected_viewership_by_market_day.sql '.format(BASE_SQL_PATH) +
                         ' -D WEEK_SCHEMA_0={} '.format(WEEK_SCHEMA_0) +
                         ' -D WEEK_ID={} '.format(WEEK_ID)
            , dag=dag)

        build_live_projection_factors_by_market_step = BashOperator(
            task_id='build_live_projection_factors_by_market',
            bash_command='snowsql --config {} '.format(CONFIG_PATH) +
                         ' -f {}/build_live_projection_factors_by_market.sql'.format(BASE_SQL_PATH) +
                         ' -D WEEK_SCHEMA_0={} '.format(WEEK_SCHEMA_0)
            , dag=dag
        )

        build_total_duration_by_series_step = BashOperator(
            task_id='build_total_duration_by_series',
            bash_command='snowsql --config {} '.format(CONFIG_PATH) +
                         ' -f {}/build_total_duration_by_series.sql'.format(BASE_SQL_PATH) +
                         ' -D WEEK_SCHEMA_0={} '.format(WEEK_SCHEMA_0)
            , dag=dag
        )

        build_hhs_factor_blend_by_series_market_step = BashOperator(
            task_id='build_hhs_factor_blend_by_series_market',
            bash_command='snowsql --config {} '.format(CONFIG_PATH) +
                         ' -f {}/build_hhs_factor_blend_by_series_market.sql'.format(BASE_SQL_PATH) +
                         ' -D WEEK_SCHEMA_0={} '.format(WEEK_SCHEMA_0)
            , dag=dag
        )

        build_series_live_hours_and_hhs_by_market_step = BashOperator(
            task_id='build_series_live_hours_and_hhs_by_market',
            bash_command='snowsql --config {} '.format(CONFIG_PATH) +
                         ' -f {}/build_series_live_hours_and_hhs_by_market.sql'.format(BASE_SQL_PATH) +
                         ' -D WEEK_SCHEMA_0={} '.format(WEEK_SCHEMA_0)
            , dag=dag
        )

        series_viewership_results_handoff_step = BashOperator(
            task_id='series_viewership_results_handoff',
            bash_command='snowsql --config {} '.format(CONFIG_PATH) +
                         ' -f {}/series_viewership_results_handoff.sql'.format(BASE_SQL_PATH) +
                         ' -D WEEK_SCHEMA_0={} '.format(WEEK_SCHEMA_0)
            , dag=dag
        )

    if "LIVEVIEWXREF" in TaskList:
        live_viewership_xref.set_upstream(truncate_dictionary_live_viewership)

    if "LIVEVIEW" in TaskList:
        if "LIVEVIEWXREF" in TaskList:
            process_manual_program_telecast_xref_step.set_upstream(live_viewership_xref)
            filter_views_by_market_station_day_step.set_upstream(live_viewership_xref)

        compress_program_telecast_xref_step.set_upstream(process_manual_program_telecast_xref_step)
        build_grouped_live_viewership_step.set_upstream(compress_program_telecast_xref_step)
        prepare_projected_hours_by_series_station_step.set_upstream(
            [filter_views_by_market_station_day_step, build_grouped_live_viewership_step])

        # MR 1
        export_pre_projected_hours_by_series_station.set_upstream(prepare_projected_hours_by_series_station_step)
        export_pre_projected_hours_by_series_station.set_upstream(delete_export_pre_projected_hours_by_series_station)
        # MR 2
        export_grouped_live_viewership.set_upstream(build_grouped_live_viewership_step)
        export_grouped_live_viewership.set_upstream(delete_export_grouped_live_viewership)

        # MR outputs
        # MR 1
        build_projected_hours_by_series_station.set_upstream(delete_output_build_projected_hours_by_series_station)
        import_projected_hours_by_series_station.set_upstream(build_projected_hours_by_series_station)
        # MR 2
        build_unprojected_live_viewership_agg.set_upstream(delete_output_build_unprojected_live_viewership_agg)
        import_unprojected_viewership_by_market.set_upstream(build_unprojected_live_viewership_agg)
        import_unprojected_viewership_by_market_station_day.set_upstream(build_unprojected_live_viewership_agg)
        import_unprojected_viewership_by_market_day.set_upstream(build_unprojected_live_viewership_agg)

        build_live_projection_factors_by_market_step.set_upstream(
            [filter_views_by_market_station_day_step, import_unprojected_viewership_by_market,
             import_unprojected_viewership_by_market_station_day,
             import_unprojected_viewership_by_market_day])
        build_total_duration_by_series_step.set_upstream(import_projected_hours_by_series_station)
        build_hhs_factor_blend_by_series_market_step.set_upstream(
            [build_total_duration_by_series_step, import_unprojected_viewership_by_market,
             import_unprojected_viewership_by_market_station_day,
             import_unprojected_viewership_by_market_day])
        build_series_live_hours_and_hhs_by_market_step.set_upstream(
            [build_live_projection_factors_by_market_step, build_hhs_factor_blend_by_series_market_step])
        series_viewership_results_handoff_step.set_upstream(
            [build_series_live_hours_and_hhs_by_market_step, build_total_duration_by_series_step])

    return dag


etv_tv_live_viewership_dag = etv_tv_live_viewership('etv_tv_live_viewership')
