from airflow.operators.sensors import BaseSensorOperator
from airflow.utils.decorators import apply_defaults
from datetime import datetime
from time import sleep
import requests
import json

class BorgStatus():
    BORG_PROCESSED = 'borg_processed'
    BORG_ERROR = 'borg_error'
    SUBMITTED = 'submitted'
    BORG_SUBMITTED = 'borg_submitted'
    BORG_IN_PROGRESS = 'borg_in progress'
    BORG_QUEUED = 'borg_queued'

class DFMData():
    URL_BASE = "http://csiadsrp02/DFMRestService/dfm.svc/job"
    HEADER_POST = {'Content-Type': 'application/json'}
    HEADER_GET = {'Accept': 'application/json'}
    BORG_JOB_INFO_BASE = "http://borg/JobDetails.aspx?JobID="

class DFMOperator(BaseSensorOperator): 

    ui_color = '#000000'
    @apply_defaults
    def __init__( self, submitter_id, overwrite_tags, *args, **kwargs):
        super(DFMOperator, self).__init__(*args, **kwargs)
        self.submitter_id = submitter_id
        self.overwrite_tags = overwrite_tags
        self.poke_interval = 15

    def createLinkBorg(self, oldBorgLink):
        jobID = oldBorgLink.split('=')[1]
        newLink = DFMData.BORG_JOB_INFO_BASE + str(jobID)
        return newLink

    def buildDataJsonBorg(self, templateNumber, customTags):
        tagsCounter = len(customTags)
        data = '{"TemplateID":"' + str(templateNumber) + '","CustomTags":['
        for key, value in customTags.items():
            data += '{"Name":"' + str(key) + '","Value":"' + str(value) + '"}'
            tagsCounter -= 1
            if(tagsCounter):
                data += ','
        data += ']}'
        return json.loads(data)

    def executeBorg(self, templateId, customTags):
        jsonData =  self.buildDataJsonBorg(templateId, customTags)
        r = requests.post(  DFMData.URL_BASE,
                            headers = DFMData.HEADER_POST,
                            json = jsonData)
        dfmId = -1
        if r.status_code == requests.codes.ok:
            dfmId = r.json()["DFMID"]
        return dfmId

    def poke(self, context):
        r = requests.get(   DFMData.URL_BASE + '/' + str(self.DFMID),
                            headers = DFMData.HEADER_GET)
        rJson = r.json()      
        if(self.poke_info == True):
            borgStatusPage = rJson["BorgStatusPage"]
            if(borgStatusPage is not None):
                self.log.info("Link to Borg: " + self.createLinkBorg(borgStatusPage))
                self.poke_info = False
        return rJson["Status"]

    def execute(self, context):
        self.DFMID = self.executeBorg(self.submitter_id, self.overwrite_tags)
        if(self.DFMID == -1):
            raise AirflowSkipException("Some error has occurred when calling DFM. Returned DFMID -1.")
        started_at = datetime.utcnow()
        self.poke_info = True
        poke_status = self.poke(context)
        while poke_status != BorgStatus.BORG_PROCESSED:
            self.log.info("poke status: " + str(poke_status))
            if(poke_status == BorgStatus.BORG_ERROR): 
                raise AirflowSkipException("Exporting {0}/{1} ended with status borg error.".format(self.project, self.instance))
            if (datetime.utcnow() - started_at).total_seconds() > self.timeout:
                if self.soft_fail:
                    raise AirflowSkipException("Exporting {0}/{1} took to long.".format(self.project, self.instance))
                else:
                    raise AirflowSensorTimeout("Exporting {0}/{1} took to long.".format(self.project, self.instance))
            sleep(self.poke_interval)
            poke_status = self.poke(context)
        self.log.info("Borg DFMID " + str(self.DFMID) + " ended with status borg_processed. Exiting.")