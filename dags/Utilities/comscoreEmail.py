from airflow.utils.email import send_email
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from datetime import datetime, timedelta  
from airflow.models import Variable

def notify_email(contextDict, **kwargs):
    """Send custom email alerts."""
    # Email Distibution List
    to_email = 'comscoreetvmigration@ggktech.com'
    # email title.
    task = "{task_instance_key_str}".format(**contextDict)
    title = "ETV Data Loads: Failure in ETL processing at " + task
    # email contents
    body = """
    Hello Everyone, <br>
    <br>
    ETV Data loads encountered a failure at """ + task + """<br>
    <br>
    Please take necessary actions. More Details at http://cviadpetl01:8080/admin/<br>
    <br>
    Thanks,<br>
    ETV Operations <br>
    """.format(**contextDict)
    send_email(to_email, title, body)
