#!/bin/sh 

jobName="${1}"

cid=$(aws emr list-clusters --query  "Clusters[?Name==\`$jobName\`].Id" --active | head -2 | tail -1 | tr -d '"'| tr -d ',')
echo $cid
aws emr terminate-clusters --cluster-ids $cid
