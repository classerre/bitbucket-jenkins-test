#!/bin/bash
#AUTHOR:Vidyan Komuravelli
#CREATED DATE:19-03-2019
#MODIFIED DATE:19-03-2019



#Reading arguments and declaring required variables
processSystem="${1}"
processSubSystem="${2}"
weekid="${3}"
platform="${4}"
parameters="${5}"
dqcPath="${6}"
configPath="${7}"
declare -a Params=($parameters)
declare -A paramMatrix
paramCount=${#Params[@]}
jobStatus="START"
pythonFilePath="$dqcPath/sendDataQualityResultEmail.py"
propFilePath="$dqcPath/dataqualitycheck.prop"

source $propFilePath

#Checking Snowflake connection
isSnowflakeAvailable=$(snowsql --config $configPath -q "Select 1;")
if ! [[ -n "$isSnowflakeAvailable" ]]; then
echo "There is an issue while connecting snowflake"
exit 1
fi

processsystem=$(snowsql --config $configPath --option friendly=false --option timing=false --option header=false --option output_format=tsv -q "select distinct process_system from xmedia_qa_script_catalog a join xmedia_qa_process_systems b on a.qa_id = b.qa_id where b.process_system ='$processSystem' and b.process_subsystem = '$processSubSystem' and platform='$platform' and b.lower_boundary <> 0 and upper_boundary <> 0 and IsActive=1 limit 1;")
product=$(snowsql --config $configPath --option friendly=false --option timing=false --option header=false --option output_format=tsv -q "select distinct productp from xmedia_qa_script_catalog a join xmedia_qa_process_systems b on a.qa_id = b.qa_id where b.process_system ='$processSystem' and b.process_subsystem = '$processSubSystem' and platform='$platform' and IsActive=1 and b.lower_boundary <> 0 and upper_boundary <> 0 limit 1;")

#Splitting parameter name and values
for(( i=0; i<${#Params[@]}; i++ ))
do
paramname=${Params[$i]%=*}
paramvalue=${Params[$i]#*=}

paramMatrix[$i,0]=$paramname
paramMatrix[$i,1]=$paramvalue
done



#Getting all the required queries and their boundaries from postgres based on processSubSystem
OIFS=$IFS;
IFS=";";

declare -a Queries=($(snowsql --config $configPath --option friendly=false --option timing=false --option header=false --option output_format=tsv -q "select a.process_query from xmedia_qa_script_catalog a join xmedia_qa_process_systems b on a.qa_id = b.qa_id where b.process_system ='$processSystem' and b.process_subsystem = '$processSubSystem' and platform='$platform' and IsActive=1 and b.lower_boundary <> 0 and upper_boundary <> 0;"));

declare -a queryCheckProp=($(snowsql --config $configPath --option friendly=false --option timing=false --option header=false --option output_format=tsv -q "select concat(concat(concat(concat(concat(concat(concat(concat(concat(lower_boundary,'|'),upper_boundary),'|'),fail),'|'),a.qa_id),'|'),process_check),';') from xmedia_qa_script_catalog a join xmedia_qa_process_systems b on a.qa_id = b.qa_id where b.process_system ='$processSystem' and b.process_subsystem = '$processSubSystem' and platform='$platform' and IsActive=1 and b.lower_boundary <> 0 and upper_boundary <> 0;"));

IFS=$OIFS;

#Generating LogID
LogID=$(snowsql --config $configPath --option friendly=false --option timing=false --option header=false --option output_format=tsv -q "select COALESCE(max(LogID),0)+1 from public.data_quality_check_log;");


for(( k=0; k < ${#Queries[@]}; k++ ))
do
echo ${Queries[$k]}
updatedQuery=${Queries[$k]}

#Replacing Param names with param values
for(( j=0; j < $paramCount; j++ ))
do
updatedQuery=$( echo "$updatedQuery" | sed "s/${paramMatrix[$j,0]}/${paramMatrix[$j,1]}/g")
if [[ "${paramMatrix[$j,0]}" == "@table_name" ]]; then
table_name="${paramMatrix[$j,1]}"
fi
done

updatedQuery=$( echo "$updatedQuery" | sed "s/@week_id/$weekid/g")
updatedQuery=$( echo "$updatedQuery" | sed "s/@platform/$platform/g")

lowerBoundary=$( echo ${queryCheckProp[$k]} | cut -f 1 -d "|")
upperBoundary=$( echo ${queryCheckProp[$k]} | cut -f 2 -d "|")
failFlag=$( echo ${queryCheckProp[$k]} | cut -f 3 -d "|")
queryid=$( echo ${queryCheckProp[$k]} | cut -f 4 -d "|")
processcheck=$( echo ${queryCheckProp[$k]} | cut -f 5 -d "|")

result=$(snowsql --config $configPath --option friendly=false --option timing=false --option header=false --option output_format=tsv -q "$updatedQuery")

#Comparing result with the boundaries
#if echo "$result" | grep -q "False" || echo "$result" | grep -q "NULL" ; then
resultinInt=$( echo "$result/1" | bc)
echo "Result is $resultinInt. Lower boundary is $lowerBoundary and upper boundary is $upperBoundary"

if echo "$result" | grep -q "error"; then
statusflag="Query Error"
jobStatus="STOP"
else
if [[ $resultinInt -ge $lowerBoundary ]] && [[ $resultinInt -le $upperBoundary ]]; then 
statusflag="Pass"
else
if [[ $failFlag == "y" ]]; then
jobStatus="STOP"
statusflag="Fail"
else
statusflag="Pass"
fi
fi
fi

updatedQuery=$( echo $updatedQuery | sed "s/'//g")

#inserting data into Log table
snowsql --config $configPath --option friendly=false --option timing=false --option header=false --option output_format=tsv -q "insert into public.data_quality_check_log values($LogID,$weekid,'$processSubSystem','$platform',$queryid,'$processcheck','$updatedQuery','$result',$lowerBoundary,$upperBoundary,'$failFlag','$statusflag',current_timestamp())"

done

DataProfileResults=$(snowsql --config $configPath --option friendly=false --option timing=false --option output_format=html -q "Select queryid as query_id,processcheck as process_check,result,statusflag as Status from public.data_quality_check_log where logid='$LogID'")


if [ $jobStatus == "STOP" ]; then
echo "job is failed"
python $pythonFilePath "$DataProfileResults" "$LogID" "$platform" "$processSubSystem" "$table_name" "$processsystem" "Failed" "$weekid" "$sender" "$recipients" "$SMTPServer" "$product"
exit 1
else
python $pythonFilePath "$DataProfileResults" "$LogID" "$platform" "$processSubSystem" "$table_name" "$processsystem" "Passed" "$weekid" "$sender" "$recipients" "$SMTPServer" "$product"

fi



