#Description : This file send Email of Data profile Results
#Author : Vidyan Komuravelli
#Created Date: 26/03/2018
#Modified Date: 27/03/2018



import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import sys
import datetime
HTMLTable=sys.argv[1]
runid=sys.argv[2]
platform=sys.argv[3]
processsubsystem=sys.argv[4]
tablename=sys.argv[5]
processsystem=sys.argv[6]
jobstatus=sys.argv[7]
weekid=sys.argv[8]
sender=sys.argv[9]
recipients=sys.argv[10]
SMTPServer=sys.argv[11]
product=sys.argv[12]

RecipientsList=recipients.split(",")
pointer = datetime.datetime.now().date()
today=str(pointer)
msg = MIMEMultipart('alternative')


msg['Subject'] = "Data Profile "+jobstatus+ " for "+weekid+"w - "+platform+" - "+processsystem+" - "+processsubsystem
msg['From'] = sender
msg['To'] = recipients

HTMLFirst = """\
<html>
<head>
<title>E-Mail From Production</title>
<style>
table {
    width:auto;
    font-family:calibri;
}
table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
th, td {
    padding: 5px;
    text-align: left;
}
th {
background-color: #4F81BD;
}
</style>
</head>
<body style=\"font-family:calibri;\">
<p><b><br><br>Profile MetaData:</b><br>RunID= <b>"""+runid+"""</b><br>Product= <b>"""+product+"""</b><br>Platform= <b>"""+platform+"""</b><br>process_system= <b>"""+processsystem+"""</b><br>process_subsystem= <b>"""+processsubsystem+"""</b><br>table_name= <b>"""+tablename+"""</b><br><br><br><b>Profile Results:</b><br>
"""
HTMLEnd="""\
<p><br><br>Thanks,<br>Comscore Migration Team</p>
<br>
</body>
</html>
"""

HTML= HTMLFirst+HTMLTable+HTMLEnd
#print(HTML)
part1 = MIMEText(HTML, 'html')
msg.attach(part1)
s = smtplib.SMTP(SMTPServer)
s.sendmail(sender,RecipientsList, msg.as_string())
s.quit()
