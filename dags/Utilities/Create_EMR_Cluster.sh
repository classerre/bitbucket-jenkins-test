#!/bin/sh
#pwd

#~/saml2aws login -p r-csxpdev-xp-galactus

# export AWS_DEFAULT_PROFILE=r-csxpdev-xp-galactus AWS_PROFILE=r-csxpdev-xp-galactus 

jobName="${1}"
instanceCount="${2}"
ebsSizeInGB="${3}"

aws emr create-cluster \
--applications Name=Spark Name=Pig Name=Tez Name=Ganglia \
--tags 'owner=Travis Naganuma' 'project=ETV-Pipeline' 'product=XP' 'department=XP' 'approval=Travis Naganuma' 'Name=etv-staging' 'environment=staging' \
--ebs-root-volume-size 10 --ec2-attributes '{"KeyName":"csxpdev_xp_helios_20171122","AdditionalSlaveSecurityGroups":["sg-abb259de"],"InstanceProfile":"r-csxpdev-xp-galactus","ServiceAccessSecurityGroup":"sg-a8d28add","SubnetId":"subnet-38e61165","EmrManagedSlaveSecurityGroup":"sg-42cf9737","EmrManagedMasterSecurityGroup":"sg-76c99103","AdditionalMasterSecurityGroups":["sg-abb259de"]}' \
--service-role EMR_DefaultRole \
--release-label emr-5.5.0 --log-uri 's3n://csxpdev-xp-east/etv-pipeline/poc/espresso_agg_logs/' \
--name $jobName \
--instance-groups "[{\"InstanceCount\":1,\"EbsConfiguration\":{\"EbsBlockDeviceConfigs\":[{\"VolumeSpecification\":{\"SizeInGB\":500,\"VolumeType\":\"gp2\"},\"VolumesPerInstance\":1}],\"EbsOptimized\":true},\"InstanceGroupType\":\"MASTER\",\"InstanceType\":\"m4.2xlarge\",\"Name\":\"MASTER\"},{\"InstanceCount\":$instanceCount,\"EbsConfiguration\":{\"EbsBlockDeviceConfigs\":[{\"VolumeSpecification\":{\"SizeInGB\":$ebsSizeInGB,\"VolumeType\":\"gp2\"},\"VolumesPerInstance\":1}],\"EbsOptimized\":true},\"InstanceGroupType\":\"CORE\",\"InstanceType\":\"m4.16xlarge\",\"Name\":\"CORE\"},{\"InstanceCount\":30,\"BidPrice\":\"OnDemandPrice\",\"EbsConfiguration\":{\"EbsBlockDeviceConfigs\":[{\"VolumeSpecification\":{\"SizeInGB\":200,\"VolumeType\":\"gp2\"},\"VolumesPerInstance\":1}],\"EbsOptimized\":true},\"InstanceGroupType\":\"TASK\",\"InstanceType\":\"m4.16xlarge\",\"Name\":\"Task - 3\"}]" \
--scale-down-behavior TERMINATE_AT_INSTANCE_HOUR \
--region us-east-1
