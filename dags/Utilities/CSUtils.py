from subprocess import Popen, PIPE
import shlex

def runCSUtilsMethod(*argv):
    methodWithArguments=""
    for arg in argv:  
       methodWithArguments = methodWithArguments+str(arg)+" "       
    p= Popen(shlex.split("java -jar /home/sgude/airflow/dags/csutils-1.3.0-jar-with-dependencies.jar "+methodWithArguments),stdout=PIPE,stderr=PIPE)
    stdout,stderr = p.communicate()
    return stdout.decode("utf-8").strip()