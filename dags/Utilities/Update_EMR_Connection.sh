#!/bin/sh 

jobName="${1}"
connId="${2}"

cid=$(aws emr list-clusters --query  "Clusters[?Name==\`$jobName\`].Id" --active | head -2 | tail -1 | tr -d '"'| tr -d ',')
dns=$(aws emr  describe-cluster --cluster-id $cid |  grep MasterPublic | cut -d "\"" -f 4)
ip=$(aws emr  describe-cluster --cluster-id $cid |  grep MasterPublic | cut -d "\"" -f 4 | cut -c4-50 | cut -d '.' -f1 | tr - .) 
# Delete existing airflow connection
airflow connections  -d --conn_id $connId

airflow connections -a --conn_id $connId --conn_type 'SSH' --conn_host $ip --conn_login 'hadoop' --conn_port 22  --conn_extra '{"key_file": "/home/airflow/csxpdev_xp_helios_20171122.pem", "no_host_key_check": true}'
echo "masternode public dns is: $dns"

echo "connection created successfully and following are the details"
#airflow connections -l | grep $connId
