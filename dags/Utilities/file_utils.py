import os
import glob


def get_latest(expression):
    '''Gets the highest numbered file from an expression such as
    "/path/file_*.ext", where the asterisk represents the numbering.'''
    # separate filename into filename, extension
    basefile = expression.split('*')[0]      # filename_input_demo_
    # obtain file list
    files = glob.glob(basefile + '*')
    # find newest filename
    newest_file = ''
    newest = 0
    for thisfile in files:
        timestamp = os.path.splitext(thisfile.replace(basefile,""))[0]
        if int(timestamp) > int(newest): 
            newest = timestamp
            newest_file = thisfile

    # get highest timestamp
    if len(files) == 0:
        return("ERROR! Expression " + expression + " does not resolve to any file.")
    else:
        return(newest_file)

