while [[ $# > 0 ]] ; do
        key="$1"
        case $key in
                --exportType)
                exportType="$2"
                shift # skip exportType value
                ;;
                --HDFSPath)
                HDFSPath="$2"
                shift # skip HDFSPath value
                ;;
		--s3Path)
                s3Path="$2"
                shift # skip s3Path value
                ;;
		--isS3Source)
                isS3Source="$2"
                shift # skip isS3Source value
                ;;
	        --propFile)
                propFile="$2"
                shift
        esac
        shift # past argument or value
done

source $propFile

aws s3 ls $S3_BASE_PATH | wc -l

if [ $? -ne 0 ];
then
	echo "[ERROR] unable to connect to s3. Please check if access key and access secret values are set properly in /tmp/filecopy.prop"
	exit 0
fi

if [ -z $isS3Source ]; then
isS3Source=false
fi

#check if source path exists
if  $isS3Source ;
then
	sourcePath=$s3Path
	targetPath=$HDFSPath
	if [ $exportType == "D" ];
	then
		if [ ! "${s3Path: -1}" == "/" ];
		then
			s3Path=$s3Path/
			sourcePath=$s3Path
		fi
		count=`aws s3 ls $s3Path | wc -l`
		if [[ ! ( $? -eq 0 ) && ( $count -ge 0 ) ]];
		then
			echo "[ERROR] Source path $s3Path is not a directory or doesn't exist"
			exit 0
		fi
	elif [ $exportType == "F" ];
	then
		count=`aws s3 ls $s3Path | wc -l`
		if [[ ! ( $? -eq 0 ) && ( $count -eq 1 ) ]];
		then
			echo "[ERROR] Source path $s3Path is not a file or doesn't exist"
			exit 0
		fi
	else
		echo "[ERROR] Invalid value given for exportType. Supported values are D for directory and F for single File"
		exit 0
	fi
else
	sourcePath=$HDFSPath 
	targetPath=$s3Path
	if [ $exportType == "D" ];
	then
		if [ ! -d $HDFSPath ];
		then
			echo "[ERROR] Source path $HDFSPath is not a directory or doesn't exist"
			exit 0
		fi
	elif [ $exportType == "F" ];
	then
		if [ ! -f $HDFSPath ];
		then
			echo "[ERROR] Source path $HDFSPath is not a file or doesn't exist"
			exit 0
		fi
	else
		echo "[ERROR] Invalid value given for exportType. Supported values are D for directory and F for single File"
		exit 0
	fi
fi

if [ $exportType == "D" ];
then
	echo "[INFO] copy of directory $sourcePath to $targetPath initiated"
	aws s3 sync $sourcePath $targetPath
	
	if [ $? -eq 0 ];
	then
		echo "[INFO] successfully copied from $sourcePath to $targetPath"
		
	else
		echo "[ERROR] Data Transfer between $sourcePath to $targetPath failed. Check if the user has permissions to create dir/files in target location"
		exit 0
	fi
elif [ $exportType == "F" ];
then
	echo "[INFO] copy of file $sourcePath to $targetPath initiated"
	aws s3 cp $sourcePath $targetPath
	if [ $? -eq 0 ];
	then
		echo "[INFO] successfully copied from $sourcePath to $targetPath"
		
	else
		echo "[ERROR] Data Transfer between $sourcePath to $targetPath failed. Check if the user has permissions to create dir/files in target location"
		exit 0	
	fi
	
fi 

