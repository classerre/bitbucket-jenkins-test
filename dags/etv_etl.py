from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from datetime import datetime, timedelta
from airflow.models import Variable
from csdatetools import csdatetools
from comscoreEmail import notify_email

default_args = {
    'owner': 'etv',
    'depends_on_past': False,
    'start_date': datetime(2019, 1, 31),
    'email': [
        'sgude@comscore.com, vkomuravelli@comscore.com, skamidi@comscore.com, rbhallamudi@comscore.com, gmuthyala@comscore.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=1),
    'on_failure_callback': notify_email
}
def etv_etl(dag_name):
  dag = DAG(dag_name, default_args=default_args, schedule_interval=None)

  # Variables
  BASE_SQL_PATH = Variable.get("base_sql_path") + 'etv_etl'
  WEEK_ID = Variable.get("week_id")
  # "{{ dag_run.conf['week_id'] }}"
  VENTI_TABLE_LOOP = Variable.get("venti_table_loop").split(",")
  CONFIG_PATH = Variable.get("config_path")

  # Derived Variables
  Week = 'W' + WEEK_ID
  PREVIOUS_WEEK_ID_1 = str(int(WEEK_ID) - 1)
  WEEK_SCHEMA_1 = 'W' + PREVIOUS_WEEK_ID_1

  MONTH_ID = str(csdatetools.time_id_to_broadcast_month(csdatetools.get_first_time_id_in_week_id(int(WEEK_ID))))

  RUNTIME_HANDOFF = BashOperator(task_id='RUNTIME_HANDOFF',
                                 bash_command='snowsql --config ' + CONFIG_PATH +
                                 ' -f ' + BASE_SQL_PATH + '/runtime_handoff.sql'
                                 ' -D  WEEK_SCHEMA_0=' + Week + ''
                                 ' -D  WEEK_ID=' + WEEK_ID + '',
                                 dag=dag
                                 )

  PLATFORM = 'live'
  Live_TV_ETL = BashOperator(task_id='Live_TV_ETL',
                             bash_command='snowsql --config ' + CONFIG_PATH +
                             ' -f ' + BASE_SQL_PATH + '/venti_singles_live.sql'
                             ' -D  WEEK_SCHEMA_0=' + Week + ''
                             ' -D  WEEK_ID=' + WEEK_ID + ''
                             ' -D  PLATFORM=' + PLATFORM + '',
                             dag=dag
                             )
  PLATFORM = 'dvr'
  DVR_ETL = BashOperator(task_id='DVR_ETL',
                         bash_command='snowsql --config ' + CONFIG_PATH +
                         ' -f ' + BASE_SQL_PATH + '/venti_singles_tv.sql'
                         ' -D  WEEK_SCHEMA_0=' + Week + ''
                         ' -D  WEEK_ID=' + WEEK_ID + ''
                         ' -D  PLATFORM=' + PLATFORM + '',
                         dag=dag
                         )

  PLATFORM = 'vod'
  VOD_ETL = BashOperator(task_id='VOD_ETL',
                         bash_command='snowsql --config ' + CONFIG_PATH +
                         ' -f ' + BASE_SQL_PATH + '/venti_singles_tv.sql'
                         ' -D  WEEK_SCHEMA_0=' + Week + ''
                         ' -D  WEEK_ID=' + WEEK_ID + ''
                         ' -D  PLATFORM=' + PLATFORM + '',
                         dag=dag
                         )
  Total_TV_ETL = BashOperator(task_id='Total_TV_ETL',
                              bash_command='snowsql --config ' + CONFIG_PATH +
                              ' -f ' + BASE_SQL_PATH + '/venti_overlap_tv.sql'
                              ' -D  WEEK_SCHEMA_0=' + Week + ''
                              ' -D  WEEK_ID=' + WEEK_ID + '',
                              dag=dag
                              )
  PC_Mobile_ETL = BashOperator(task_id='PC_Mobile_ETL',
                               bash_command='snowsql --config ' + CONFIG_PATH +
                               ' -f ' + BASE_SQL_PATH + '/digital_jz_venti.sql'
                               ' -D  WEEK_SCHEMA_0=' + Week + ''
                               ' -D  WEEK_ID=' + WEEK_ID + ''
                               ' -D MONTH_ID=' + MONTH_ID + '',
                               dag=dag
                               )
  OTT_ETL = BashOperator(task_id='OTT_ETL',
                         bash_command='snowsql --config ' + CONFIG_PATH +
                         ' -f ' + BASE_SQL_PATH + '/ott_jz_venti.sql'
                         ' -D  WEEK_SCHEMA_0=' + Week + ''
                         ' -D  WEEK_ID=' + WEEK_ID + ''
                         ' -D MONTH_ID=' + MONTH_ID + '',
                         dag=dag
                         )
  Total_Digital_ETL = BashOperator(task_id='Total_Digital_ETL',
                                   bash_command='snowsql --config ' + CONFIG_PATH +
                                   ' -f ' + BASE_SQL_PATH + '/total_digital_venti_etl.sql'
                                   ' -D  WEEK_SCHEMA_0=' + Week + ''
                                   ' -D  WEEK_0=' + WEEK_ID + '',
                                   dag=dag
                                   )
  Total_Audience_ETL = BashOperator(task_id='Total_Audience_ETL',
                                    bash_command='snowsql --config ' + CONFIG_PATH +
                                    ' -f ' + BASE_SQL_PATH + '/total_platform_venti_etl.sql'
                                    ' -D  WEEK_SCHEMA_0=' + Week + ''
                                    ' -D  WEEK_0=' + WEEK_ID + '',
                                    dag=dag
                                    )

  POST_ETL_Remove_networks_without_Digital_Content = BashOperator(
      task_id='POST_ETL_Remove_networks_without_Digital_Content_for_' + WEEK_ID + 'w',
      bash_command='snowsql --config ' + CONFIG_PATH +
      ' -f ' + BASE_SQL_PATH + '/post_ETL_remove_nondigital_networks.sql'
      ' -D  WEEK_SCHEMA_0=' + Week + ''
      ' -D  WEEK_0=' + WEEK_ID + '',
      dag=dag
  )

  #BOL_Generation_for = BashOperator(task_id='BOL_Generation_for_' + WEEK_ID + 'w',
  #                                  bash_command='snowsql --config ' + CONFIG_PATH + ''
  #                                                                                   ' -f ' + BASE_SQL_PATH + '/etv_bol.sql'
  #                                                                                                            ' -D  WEEK_SCHEMA_0=' + Week + ''
  #                                                                                                                                           ' -D  WEEK_SCHEMA_1=' + WEEK_SCHEMA_1 + ''
  #                                                                                                                                                                                   ' -D  WEEK_0=' + WEEK_ID + '',
  #                                  dag=dag
  #                                  )
  for VENTI_TABLE in VENTI_TABLE_LOOP:
      POST_ETL_UV_Capping = """POST_ETL_UV_Capping_for_{0} = BashOperator(
  	task_id= 'POST_ETL_UV_Capping_for_{0}',
  	bash_command='snowsql --config ' +CONFIG_PATH +''
  	' -f '+BASE_SQL_PATH + '/post_etl_uv_capping.sql' +''
  	' -D WEEK_SCHEMA_0={2}'
  	' -D VENTI_TABLE_NAME={1}'
  	,dag=dag)""".format(VENTI_TABLE, VENTI_TABLE, Week)

      exec(POST_ETL_UV_Capping)

  Live_TV_ETL.set_upstream(RUNTIME_HANDOFF)
  DVR_ETL.set_upstream(RUNTIME_HANDOFF)
  Total_TV_ETL.set_upstream(RUNTIME_HANDOFF)
  VOD_ETL.set_upstream(RUNTIME_HANDOFF)
  PC_Mobile_ETL.set_upstream(RUNTIME_HANDOFF)
  OTT_ETL.set_upstream(RUNTIME_HANDOFF)
  Total_Digital_ETL.set_upstream(RUNTIME_HANDOFF)
  Total_Audience_ETL.set_upstream(RUNTIME_HANDOFF)
  POST_ETL_Remove_networks_without_Digital_Content.set_upstream(
      [Live_TV_ETL,DVR_ETL, VOD_ETL, PC_Mobile_ETL, OTT_ETL, Total_Digital_ETL, Total_Audience_ETL])
  #for VENTI_TABLE in VENTI_TABLE_LOOP:
  #    BOL_Generation_for_dependency = """BOL_Generation_for.set_upstream(POST_ETL_UV_Capping_for_{0})""".format(
  #        VENTI_TABLE)
  #    exec(BOL_Generation_for_dependency)
  for VENTI_TABLE in VENTI_TABLE_LOOP:
      POST_ETL_UV_Capping_dependency = """POST_ETL_UV_Capping_for_{0}.set_upstream(POST_ETL_Remove_networks_without_Digital_Content)""".format(
          VENTI_TABLE)
      exec(POST_ETL_UV_Capping_dependency)
      
  return dag    

etv_etl_dag = etv_etl('etv_etl')
