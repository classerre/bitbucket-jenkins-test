from datetime import datetime, timedelta
from airflow import DAG
from airflow.contrib.operators.ssh_operator import SSHOperator
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator
from airflow.contrib.hooks.ssh_hook import SSHHook
from airflow.models import Variable
from comscoreEmail import notify_email
from csdatetools import csdatetools

default_args = {
    'owner': 'etv',
    'depends_on_past': False,
    'start_date': datetime(2019, 1, 31),
    'email': ['sgude@comscore.com, vkomuravelli@comscore.com, skamidi@comscore.com, rbhallamudi@comscore.com, gmuthyala@comscore.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=1),
    'on_failure_callback': notify_email
}

def etv_total_digital_ott_tld_overlap(dag_name):
    WEEK_ID = Variable.get("week_id")
    MONTH_ID = str(csdatetools.time_id_to_broadcast_month(csdatetools.get_first_time_id_in_week_id(int(WEEK_ID))))
    POPULATION_ID = Variable.get("etv_total_digital_population_id")
    WeekID_0 = WEEK_ID
    WeekID_1 = str(int(WEEK_ID) - 1)
    WeekID_2 = str(int(WEEK_ID) - 2)
    WeekID_3 = str(int(WEEK_ID) - 3)
    WeekID_4 = str(int(WEEK_ID) - 4)
    WeekID_5 = str(int(WEEK_ID) - 5)

    CONFIG_PATH = Variable.get("config_path")
    BASE_SQL_PATH = Variable.get("base_sql_path") + \
        'etv_total_digital_ott_tld_overlap/'


    dag = DAG(
        dag_name,
        default_args=default_args,
        schedule_interval=None)

    Run_Python_to_call_ADW_psql = BashOperator(
        task_id='Run_Python_to_call_ADW_psql',
        bash_command='snowsql --config ' + CONFIG_PATH + ''
        ' -f ' + BASE_SQL_PATH + 'step1_tld_ott_coef_apply.sql' + ''
        ' -D POPULATION_ID=' + POPULATION_ID + ''
        ' -D  WEEK_SCHEMA_0=w' + WeekID_0 + ''
        ' -D  WEEK_SCHEMA_1=w' + WeekID_1 + ''
        ' -D MONTH_ID=' + MONTH_ID + ''
        ' -D WeekID_0=' + WeekID_0 + ''
        ' -D WeekID_1=' + WeekID_1 + '',
        dag=dag)

    j = 0
    list = [WeekID_1, WeekID_2, WeekID_3, WeekID_4, WeekID_5]
    for i in list:
        temp = """Run_Python_to_call_ADW_psql_{0}=BashOperator(
    		task_id= 'Run_Python_to_call_ADW_psql_{0}',
    		bash_command='snowsql --config ' + CONFIG_PATH +''
    				' -f '+BASE_SQL_PATH + 'step2_tld_ott_coef_apply.sql' +'' 
    				#\\csiadcrm05\ddrive\production\etv\etv_ttd_ott\step2_tld_ott_coef_apply.py
    				' -D MONTH_ID={1}'
    				' -D WeekID_0={3}'
    				' -D POPULATION_ID={2}'
    				' -D  WEEK_SCHEMA_0=w{3}'
    				,dag=dag)""".format(j, MONTH_ID, POPULATION_ID, i)
        j += 1

        exec(temp)

    Run_Python_to_call_ADW_psql_last = BashOperator(
        task_id='Run_Python_to_call_ADW_psql_last',
        bash_command='snowsql --config ' + CONFIG_PATH + ''
        ' -f ' + BASE_SQL_PATH + 'step3_tld_ott_coef_apply.sql' + ''
        #csiadcrm05\ddrive\production\etv\etv_ttd_ott\step3_tld_ott_coef_apply.py
        ' -D POPULATION_ID=' + POPULATION_ID + ''
        ' -D MONTH_ID=' + MONTH_ID + ''
        ' -D WeekID_0=' + WeekID_0 + ''
        ' -D  WEEK_SCHEMA_0=w' + WeekID_0 + '',
        dag=dag)

    i = 0
    while i < 5:
        temp_2 = """Run_Python_to_call_ADW_psql_{0}.set_upstream(Run_Python_to_call_ADW_psql)""".format(i)
        exec(temp_2)

        temp_1 = """Run_Python_to_call_ADW_psql_last.set_upstream(Run_Python_to_call_ADW_psql_{0})""".format(i)
        exec(temp_1)
    	
        i += 1
    return dag

etv_total_digital_ott_tld_overlap_dag = etv_total_digital_ott_tld_overlap('etv_total_digital_ott_tld_overlap')