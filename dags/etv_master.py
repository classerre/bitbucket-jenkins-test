__author__ = " Vidyan Komuravelli "

"""
       master dag to trigger all batches
"""
from etv_batch0 import etv_batch0
from etv_batch1 import etv_batch1
from etv_batch2 import etv_batch2
from etv_batch3 import etv_batch3
from etv_batch4 import etv_batch4
from etv_batch5 import etv_batch5

from airflow.models import DAG
from airflow.models import Variable
from airflow.operators.subdag_operator import SubDagOperator
from datetime import datetime, timedelta
from comscoreEmail import returnEmailOperator

DAG_NAME = 'etv_master'
WEEK_ID = Variable.get("week_id")

args = {
    'owner': 'etv',
    'depends_on_past': False,
    'start_date': datetime(2019, 1, 24),
    'email': ['sgude@comscore.com', 'vkomuravelli@comscore.com', 'skamidi@comscore.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=1)
}

dag = DAG(DAG_NAME, default_args=args, schedule_interval=None)

etv_batch0_dag = SubDagOperator(
    task_id='etv_batch0',
    subdag=etv_batch0('etv_master.etv_batch0'),
    default_args=args,
    dag=dag)

etv_batch1_dag = SubDagOperator(
    task_id='etv_batch1',
    subdag=etv_batch1('etv_master.etv_batch1'),
    default_args=args,
    dag=dag)

etv_batch2_dag = SubDagOperator(
    task_id='etv_batch2',
    subdag=etv_batch2('etv_master.etv_batch2'),
    default_args=args,
    dag=dag)

etv_batch3_dag = SubDagOperator(
    task_id='etv_batch3',
    subdag=etv_batch3('etv_master.etv_batch3'),
    default_args=args,
    dag=dag)

etv_batch4_dag = SubDagOperator(
    task_id='etv_batch4',
    subdag=etv_batch4('etv_master.etv_batch4'),
    default_args=args,
    dag=dag)

etv_batch5_dag = SubDagOperator(
    task_id='etv_batch5',
    subdag=etv_batch5('etv_master.etv_batch5'),
    default_args=args,
    dag=dag)

notify_success = returnEmailOperator(dag, DAG_NAME, WEEK_ID)

# Dependencies :
etv_batch1_dag.set_upstream(etv_batch0_dag)
etv_batch3_dag.set_upstream(etv_batch0_dag)
etv_batch2_dag.set_upstream(etv_batch1_dag)
etv_batch4_dag.set_upstream(etv_batch3_dag)
etv_batch5_dag.set_upstream([etv_batch2_dag, etv_batch4_dag])
notify_success.set_upstream(etv_batch5_dag)
