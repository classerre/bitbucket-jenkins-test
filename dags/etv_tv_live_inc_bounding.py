from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from datetime import datetime, timedelta
from airflow.models import Variable
from csdatetools import csdatetools

default_args = {
    'owner': 'etv',
    'depends_on_past': False,
    'start_date': datetime(2019, 3, 11),
    'email': ['sgude@comscore.com, vkomuravelli@comscore.com, skamidi@comscore.com, rbhallamudi@comscore.com, gmuthyala@comscore.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=1)
}

# DAG Specific constants
PLATFORM = 'live'

# Global Variables
CONFIG_PATH = Variable.get("config_path")
BASE_SQL_PATH = Variable.get("base_sql_path") + 'etv_tv_live_inc_bounding/'

# Local Variables
WEEK_ID = Variable.get("week_id")
MONTH_ID=str(csdatetools.time_id_to_month_id(csdatetools.get_last_time_id_in_week_id(int(WEEK_ID))))
Tasks = Variable.get("ETV_TV_Incremental_Bounding_TaskList")
Sequence = [1, 2, 3, 4, 5, 6]
FIRST_TIME_ID_IN_RANGE = str(csdatetools.get_first_time_id_in_week_id(int(WEEK_ID)))
LAST_TIME_ID_IN_RANGE = str(csdatetools.get_last_time_id_in_week_id(int(WEEK_ID)))
DQC_PATH = Variable.get("data_quality_check_path")
DATA_QUALITY_CHECK = Variable.get("data_quality_check")
#incrementalMLE_Live_Part4_tasks=[]

#Derived Variables
WEEK_SCHEMA = 'W' + WEEK_ID;
TaskList = Tasks.split(",")
Prev_WEEK_ID = str(int(WEEK_ID)-1)
Prev_WEEK_SCHEMA = 'W' + Prev_WEEK_ID;

def etv_tv_live_inc_bounding(dag_name):
    dag = DAG(dag_name, default_args=default_args, schedule_interval=None)

    if "LIVEINC" in TaskList:
            incrementalMLE_Live_Part0 = BashOperator(task_id='incrementalMLE_Live_Part0',bash_command='snowsql --config ' + CONFIG_PATH + ''
                                                                        ' -f '+ BASE_SQL_PATH + 'incremental_pt0.sql'
                                                                        ' -D WEEK_SCHEMA_0='+WEEK_SCHEMA +'',
                                                                        dag=dag)

            incrementalMLE_Live_Part1 = BashOperator(task_id='incrementalMLE_Live_Part1',bash_command='snowsql --config ' + CONFIG_PATH + ''
                                                                        ' -f '+ BASE_SQL_PATH + 'incremental_pt1.sql'
                                                                        ' -D WEEK_SCHEMA_0='+WEEK_SCHEMA +''
                                                                        ' -D WEEK_SCHEMA_1='+Prev_WEEK_SCHEMA+''
                                                                        ' -D WEEK_0='+WEEK_ID+'',
                                                                        dag=dag)

            incrementalMLE_Live_Part2 = BashOperator(task_id='incrementalMLE_Live_Part2',bash_command='snowsql --config ' + CONFIG_PATH + ''
                                                                        ' -f '+ BASE_SQL_PATH + 'incremental_pt2.sql'
                                                                        ' -D WEEK_SCHEMA_0='+WEEK_SCHEMA +''
                                                                        ' -D WEEK_SCHEMA_1='+Prev_WEEK_SCHEMA+'',
                                                                        dag=dag)

            incrementalMLE_Live_Part3 = BashOperator(task_id='incrementalMLE_Live_Part3',bash_command='snowsql --config ' + CONFIG_PATH + ''
                                                                        ' -f '+ BASE_SQL_PATH + 'incremental_pt3.sql'
                                                                        ' -D WEEK_SCHEMA_0='+WEEK_SCHEMA +''
                                                                        ' -D WEEK_0='+WEEK_ID+'',
                                                                        dag=dag)
            previous_part = incrementalMLE_Live_Part3
            for value in Sequence:
                incrementalMLE_Live_Part4 = BashOperator(task_id='incrementalMLE_Live_Part4'+"_"+str(value),bash_command='snowsql --config ' + CONFIG_PATH + ''
                                                                        ' -f '+ BASE_SQL_PATH + 'incremental_pt4.sql'
                                                                        ' -D WEEK_SCHEMA_0='+WEEK_SCHEMA +''
                                                                        ' -D CURRENT='+str(value) +'',
                                                                        dag=dag)
                #incrementalMLE_Live_Part4_tasks.append(incrementalMLE_Live_Part4)
                incrementalMLE_Live_Part4.set_upstream(previous_part)
                previous_part = incrementalMLE_Live_Part4

            incrementalMLE_Live_Part5 = BashOperator(task_id='incrementalMLE_Live_Part5',bash_command='snowsql --config ' + CONFIG_PATH + ''
                                                                        ' -f '+ BASE_SQL_PATH + 'incremental_pt5.sql'
                                                                        ' -D WEEK_SCHEMA_0='+WEEK_SCHEMA +''
                                                                        ' -D WEEK_0='+WEEK_ID+'',
                                                                        dag=dag)

            incrementalMLE_Live_Part6 = BashOperator(task_id='incrementalMLE_Live_Part6',bash_command='snowsql --config ' + CONFIG_PATH + ''
                                                                        ' -f '+ BASE_SQL_PATH + 'incremental_pt6.sql'
                                                                        ' -D WEEK_SCHEMA_0='+WEEK_SCHEMA +''
                                                                        ' -D platform='+PLATFORM +'',
                                                                        dag=dag)
            if DATA_QUALITY_CHECK == "TRUE":
                DQC_for_Live_Inc_Latte    = BashOperator(task_id='DQC_for_Live_Inc_Latte',
                    bash_command = DQC_PATH+'/dataqualitycheck.sh Incremental incremental '+WEEK_ID+' LIVE \"@table_name='+WEEK_SCHEMA
                    +'.etv_live_latte_inclusive @table_previous='+Prev_WEEK_SCHEMA+'.etv_live_latte_inclusive\"'+DQC_PATH+' '+CONFIG_PATH,
                    dag=dag
                    )
    if "LIVEBOUNDING" in TaskList:
            etv_tv_bounding_Live = BashOperator(task_id='etv_tv_bounding_for_Live',bash_command=BASE_SQL_PATH + 'Execute_tv_bounding_step.sh'
                                                                            ' '+ BASE_SQL_PATH +''
                                                                            ' '+CONFIG_PATH +''
                                                                            ' '+ WEEK_SCHEMA +''
                                                                            ' '+ Prev_WEEK_SCHEMA +''
                                                                            ' '+ WEEK_ID +''
                                                                            ' '+ Prev_WEEK_ID +''
                                                                            ' '+ PLATFORM +''
                                                                            ' '+ FIRST_TIME_ID_IN_RANGE +''
                                                                            ' '+ LAST_TIME_ID_IN_RANGE +'',
                                                               dag=dag)

            if DATA_QUALITY_CHECK == "TRUE":
                DQC_for_LIVE_Bounding    = BashOperator(task_id='DQC_for_LIVE_Bounding',
                    bash_command = DQC_PATH+'/dataqualitycheck.sh Bounding bounding '+WEEK_ID+' LIVE \"@table_name='+WEEK_SCHEMA
                    +'.etv_live_latte_inclusive_bounded @table_previous='+Prev_WEEK_SCHEMA+'.etv_live_latte_inclusive_bounded\"'+DQC_PATH+' '+CONFIG_PATH,
                    dag=dag
                    )

    #check FIRST_WEEKID_OF_BROADCAST_MONTH
    if  WEEK_ID > str(csdatetools.time_id_to_week_id(csdatetools.broadcast_month_to_first_time_id(csdatetools.time_id_to_broadcast_month(csdatetools.get_last_time_id_in_week_id(int(WEEK_ID)))))):
        if "MONTHLY" in TaskList:           
            if WEEK_ID == str(int(csdatetools.get_first_week_id_in_broadcast_month(csdatetools.week_id_to_broadcast_month(int(WEEK_ID)))) + 1):
                single_PLATFORM_LIVE_overlap_w_final_format_if_last_week=BashOperator(
                    task_id='single_PLATFORM_Live_overlap_w_final_format_if_last_week',
                    bash_command='snowsql --config ' +CONFIG_PATH +''
                            ' -f '+BASE_SQL_PATH + '03_single_platform_overlap_2nd_week.sql' +''
                            ' -D Week_0=' +WEEK_ID  +''
                                                    ' -D Week_1=' +Prev_WEEK_ID  +''
                            ' -D  WEEK_SCHEMA_0=w'+WEEK_ID  +''
                            ' -D  WEEK_SCHEMA_1=w'+Prev_WEEK_ID  +''
                            ,dag=dag)
            else :
                single_PLATFORM_LIVE_overlap_w_final_format_if_last_week=BashOperator(
                    task_id='single_PLATFORM_Live_overlap_w_final_format_if_last_week',
                    bash_command='snowsql --config ' +CONFIG_PATH +''
                            ' -f '+BASE_SQL_PATH + '03_single_platform_overlap.sql' +''
                            ' -D Week_0=' +WEEK_ID  +''
                            ' -D  WEEK_SCHEMA_0=w'+WEEK_ID  +''
                            ' -D  WEEK_SCHEMA_1=w'+Prev_WEEK_ID  +''
                            ,dag=dag)

            if WEEK_ID == str(csdatetools.time_id_to_week_id(csdatetools.broadcast_month_to_last_time_id(csdatetools.week_id_to_broadcast_month(int(WEEK_ID))))):
                single_PLATFORM_LIVE_overlap_w_final_format_if_last_week_1=BashOperator(
                    task_id='single_PLATFORM_Live_overlap_w_final_format_if_last_week_1',
                    bash_command='snowsql --config ' +CONFIG_PATH +''
                            ' -f '+BASE_SQL_PATH + '04_single_platform_final_format.sql' +''
                            ' -D Week_0=' +WEEK_ID  +''
                            ' -D  WEEK_SCHEMA_0=w'+WEEK_ID  +''
                            ' -D  WEEK_SCHEMA_1=w'+Prev_WEEK_ID  +''
                            ' -D  MONTH_ID='+MONTH_ID  +''
                            ,dag=dag)

    if "LIVEINC" in TaskList:
        incrementalMLE_Live_Part1.set_upstream(incrementalMLE_Live_Part0)
        incrementalMLE_Live_Part2.set_upstream(incrementalMLE_Live_Part0)
        incrementalMLE_Live_Part3.set_upstream(incrementalMLE_Live_Part1)
        incrementalMLE_Live_Part5.set_upstream(incrementalMLE_Live_Part4)
        incrementalMLE_Live_Part5.set_upstream(incrementalMLE_Live_Part2)
        incrementalMLE_Live_Part6.set_upstream(incrementalMLE_Live_Part5)
        if DATA_QUALITY_CHECK == "TRUE":
            DQC_for_Live_Inc_Latte.set_upstream(incrementalMLE_Live_Part6)

    if "LIVEBOUNDING" in TaskList:
        if "LIVEINC" in TaskList:
            if DATA_QUALITY_CHECK == "TRUE":
                etv_tv_bounding_Live.set_upstream(DQC_for_Live_Inc_Latte)
            else:
                etv_tv_bounding_Live.set_upstream(incrementalMLE_Live_Part6)
    if  WEEK_ID > str(csdatetools.time_id_to_week_id(csdatetools.broadcast_month_to_first_time_id(csdatetools.time_id_to_broadcast_month(csdatetools.get_last_time_id_in_week_id(int(WEEK_ID)))))):
        if "MONTHLY" in TaskList:
            if "LIVEBOUNDING" in TaskList:
                if WEEK_ID == str(int(csdatetools.get_first_week_id_in_broadcast_month(csdatetools.week_id_to_broadcast_month(int(WEEK_ID)))) + 1):
                    if DATA_QUALITY_CHECK == "TRUE":
                        DQC_for_LIVE_Bounding.set_upstream(etv_tv_bounding_Live)
                        single_PLATFORM_LIVE_overlap_w_final_format_if_last_week.set_upstream(DQC_for_LIVE_Bounding)
                    else:
                        single_PLATFORM_LIVE_overlap_w_final_format_if_last_week.set_upstream(etv_tv_bounding_Live)
                else :
                    if DATA_QUALITY_CHECK == "TRUE":
                        DQC_for_LIVE_Bounding.set_upstream(etv_tv_bounding_Live)
                        single_PLATFORM_LIVE_overlap_w_final_format_if_last_week.set_upstream(DQC_for_LIVE_Bounding)
                    else:
                        single_PLATFORM_LIVE_overlap_w_final_format_if_last_week.set_upstream(etv_tv_bounding_Live)
    
                if WEEK_ID == str(csdatetools.time_id_to_week_id(csdatetools.broadcast_month_to_last_time_id(csdatetools.week_id_to_broadcast_month(int(WEEK_ID))))):
                    if DATA_QUALITY_CHECK == "TRUE":
                        single_PLATFORM_LIVE_overlap_w_final_format_if_last_week_1.set_upstream(DQC_for_LIVE_Bounding)
                    else:
                        single_PLATFORM_LIVE_overlap_w_final_format_if_last_week_1.set_upstream(etv_tv_bounding_Live)

    return dag

etv_tv_live_inc_bounding_dag = etv_tv_live_inc_bounding('etv_tv_live_inc_bounding')
