from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from datetime import datetime, timedelta
from airflow.models import Variable
from comscoreEmail import notify_email
from Utilities.file_utils import get_latest


default_args = {
    'owner': 'etv',
    'depends_on_past': False,
    'start_date': datetime(2019, 1, 31),
    'email': ['sgude@comscore.com, vkomuravelli@comscore.com, skamidi@comscore.com, rbhallamudi@comscore.com, gmuthyala@comscore.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=1),
    'on_failure_callback': notify_email
}

def etv_digital_tvd_mapping_parts(dag_name):

    dag = DAG(
        dag_name,
        default_args=default_args,
        schedule_interval=None)

    # DAG Specific constants
    PRODUCT = 'tv'
    PLATFORM_COMMON = 'common'
    PLATFORM_LIVE = 'live'
    PLATFORM_LOOKUPS = 'lookups'

    # Variables
    RUN_BATCH = Variable.get("RUN_BATCH")
    DATA_TRANSFER_SCRIPT_PATH = Variable.get('data_transfer_script_path')
    CONFIG_PATH = Variable.get("config_path")
    BASE_SQL_PATH = Variable.get("base_sql_path") + \
        "etv_digital_tvd_mapping_parts/"
    WEEK_ID = Variable.get("week_id")
    HDFS_STAGE_S3 = Variable.get("base_s3_path")
    BASE_s3_PATH = Variable.get("base_s3_loc") + '/'
    BASE_s3_PATH_WEEKLY = BASE_s3_PATH + WEEK_ID + 'w'
    DIGITAL_TVD_MAPPING_S3_PATH = BASE_s3_PATH_WEEKLY + '/digital_tvd_mapping/'
    DQC_PATH = Variable.get("data_quality_check_path")
    DATA_QUALITY_CHECK = Variable.get("data_quality_check")


    # Derived Variables
    WEEK_ID_0 = WEEK_ID
    WEEK_ID_1 = str(int(WEEK_ID) - 1)
    WEEK_ID_2 = str(int(WEEK_ID) - 2)
    WEEK_ID_3 = str(int(WEEK_ID) - 3)
    WEEK_ID_4 = str(int(WEEK_ID) - 4)
    WEEK_ID_5 = str(int(WEEK_ID) - 5)
    WEEK_SCHEMA_0  = 'W'+ WEEK_ID
    WEEK_SCHEMA_1  = 'W'+ str(int(WEEK_ID)-1)

    if RUN_BATCH == "TRUE":
        load_voltron_metadata_to_s3 = BashOperator(
            task_id='load_voltron_metadata_to_s3',
            bash_command='' + DATA_TRANSFER_SCRIPT_PATH +
                         ' --exportType F' +
                         ' --HDFSPath ' + get_latest('/mnt/smb/corpftp/rentrak/SeptConnectData/SeptCabRun/voltron_metadata_*.txt') +

                         ' --s3Path s3://csxpdev-xp-east/etv/HDFS/public/voltron_metadata.txt' +
                         ' --propFile /mapr/ri1.comscore.com/test_data/census/dags/SnowflakeSQL/DataCopy/fileCopy.prop',
            dag=dag
        )

        load_voltron_owners_xref_to_s3 = BashOperator(
            task_id='load_voltron_owners_xref_to_s3',
            bash_command='' + DATA_TRANSFER_SCRIPT_PATH +
                         ' --exportType F' +
                         ' --HDFSPath ' + get_latest('/mnt/smb/corpftp/rentrak/SeptConnectData/SeptCabRun/voltron_owners_xref_*.txt') +
                         ' --s3Path s3://csxpdev-xp-east/etv/HDFS/public/voltron_owners_xref.txt' +
                         ' --propFile /mapr/ri1.comscore.com/test_data/census/dags/SnowflakeSQL/DataCopy/fileCopy.prop',
            dag=dag
        )

        load_voltron_series_xref_to_s3 = BashOperator(
            task_id='load_voltron_series_xref_to_s3',
            bash_command='' + DATA_TRANSFER_SCRIPT_PATH +
                         ' --exportType F' +
                         ' --HDFSPath ' + get_latest('/mnt/smb/corpftp/rentrak/SeptConnectData/SeptCabRun/voltron_series_xref_*.txt') +
                         ' --s3Path s3://csxpdev-xp-east/etv/HDFS/public/voltron_series_xref.txt' +
                         ' --propFile /mapr/ri1.comscore.com/test_data/census/dags/SnowflakeSQL/DataCopy/fileCopy.prop',
            dag=dag
        )

        load_voltron_titles_xref_to_s3 = BashOperator(
            task_id='load_voltron_titles_xref_to_s3',
            bash_command='' + DATA_TRANSFER_SCRIPT_PATH +
                         ' --exportType F' +
                         ' --HDFSPath ' + get_latest('/mnt/smb/corpftp/rentrak/SeptConnectData/SeptCabRun/voltron_titles_xref_*.txt') +
                         ' --s3Path s3://csxpdev-xp-east/etv/HDFS/public/voltron_titles_xref.txt' +
                         ' --propFile /mapr/ri1.comscore.com/test_data/census/dags/SnowflakeSQL/DataCopy/fileCopy.prop',
            dag=dag
        )

        build_six_weeks_tv_dictionary = BashOperator(
            task_id='build_six_weeks_tv_dictionary',
            bash_command='snowsql --config ' + CONFIG_PATH +
                         ' -f ' + BASE_SQL_PATH + 'build_6_weeks_tv_dictionary.sql' +
                         ' -D  WEEK_SCHEMA_0=w' + WEEK_ID_0 +
                         ' -D  WEEK_0=' + WEEK_ID_0 +
                         ' -D  WEEK_SCHEMA_1=w' + WEEK_ID_1 +
                         ' -D  WEEK_1=' + WEEK_ID_1 +
                         ' -D  WEEK_SCHEMA_2=w' + WEEK_ID_2 +
                         ' -D  WEEK_2=' + WEEK_ID_2 +
                         ' -D  WEEK_SCHEMA_3=w' + WEEK_ID_3 +
                         ' -D  WEEK_3=' + WEEK_ID_3 +
                         ' -D  WEEK_SCHEMA_4=w' + WEEK_ID_4 +
                         ' -D  WEEK_4=' + WEEK_ID_4 +
                         ' -D  WEEK_SCHEMA_5=w' + WEEK_ID_5 +
                         ' -D  WEEK_5=' + WEEK_ID_5,
            dag=dag
        )

        # TODO: the s3 files need to be loaded into tables in build_digital_tvd_mapping_prep.sql or add tasks to load them.
    	# Created tables for the above static files in digital_tvd_mapping_handoff.sql
        create_tvd_linear_mapping_full_intermediate_table = BashOperator(
            task_id='create_tvd_linear_mapping_full_intermediate_table',
            bash_command='snowsql --config ' + CONFIG_PATH +
                         ' -f ' + BASE_SQL_PATH + 'build_digital_tvd_mapping_prep.sql' +
                         ' -D  WEEK_SCHEMA_0=w' + WEEK_ID_0 +
                         ' -D  WEEK_0=' + WEEK_ID_0,
            dag=dag
        )

        create_digital_tvd_mapping_table = BashOperator(
            task_id='create_digital_tvd_mapping_table',
            bash_command='snowsql --config ' + CONFIG_PATH +
                         ' -f ' + BASE_SQL_PATH + 'digital_tvd_mapping_handoff.sql' +
                         ' -D  WEEK_SCHEMA_0=w' + WEEK_ID_0 +
                         ' -D  WEEK_0=' + WEEK_ID_0 +
    					 ' -D  S3_STAGE=' + HDFS_STAGE_S3,
            dag=dag
        )
    	
        remove_s3_tvd_mapping_path = BashOperator(
            task_id='remove_s3_tvd_mapping_path',
            bash_command='aws s3 rm ' + DIGITAL_TVD_MAPPING_S3_PATH + ' --recursive',
            dag=dag
    	)

        if DATA_QUALITY_CHECK == "TRUE":
            dqc_digital_tvd_mapping = BashOperator(
                task_id='dqc_digital_tvd_mapping',
                bash_command='{0}/dataqualitycheck.sh Digital_TVD digital_tvd_mapping {1} Digital \"@table_name={2}.digital_tvd_mapping @table_previous={3}.digital_tvd_mapping\" {0} {4}'.format(DQC_PATH, WEEK_ID, WEEK_SCHEMA_0, WEEK_SCHEMA_1, CONFIG_PATH)
            , dag=dag)


        create_tvd_linear_mapping_full_intermediate_table.set_upstream(load_voltron_metadata_to_s3)
        create_tvd_linear_mapping_full_intermediate_table.set_upstream(load_voltron_owners_xref_to_s3)
        create_tvd_linear_mapping_full_intermediate_table.set_upstream(load_voltron_series_xref_to_s3)
        create_tvd_linear_mapping_full_intermediate_table.set_upstream(load_voltron_titles_xref_to_s3)
        create_tvd_linear_mapping_full_intermediate_table.set_upstream(build_six_weeks_tv_dictionary)
        remove_s3_tvd_mapping_path.set_upstream(create_tvd_linear_mapping_full_intermediate_table)
        create_digital_tvd_mapping_table.set_upstream(remove_s3_tvd_mapping_path)
        if DATA_QUALITY_CHECK == "TRUE":
            dqc_digital_tvd_mapping.set_upstream(create_digital_tvd_mapping_table)

    return dag

etv_digital_tvd_mapping_parts_dag = etv_digital_tvd_mapping_parts('etv_digital_tvd_mapping_parts')
