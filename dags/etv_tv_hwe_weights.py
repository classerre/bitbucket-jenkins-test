from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from datetime import datetime, timedelta
from airflow.models import Variable
from operators.dfm import DFMOperator
from ecs_operator import ECSOperator
from csdatetools.csdatetools import get_month_id_from_week_id

default_args = {
    'owner': 'etv',
    'depends_on_past': False,
    'start_date': datetime(2019, 1, 31),
    'email': ['sgude@comscore.com, vkomuravelli@comscore.com, skamidi@comscore.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=1)
}

# Global Variables
CONFIG_PATH = Variable.get("config_path")
BASE_SQL_PATH = Variable.get("base_sql_path") + 'etv_tv_hwe_weights/'
DATA_EXPORT_SQL_PATH = Variable.get("base_sql_path")
S3_STAGE_PATH = Variable.get("base_s3_path")
S3_BASE_PATH = Variable.get("base_s3_loc")
UTILITY_PATH = Variable.get("base_sql_path") + 'utilities'
DQC_PATH = Variable.get("data_quality_check_path")
DATA_QUALITY_CHECK = Variable.get("data_quality_check")

# Local Variables
WEEK_ID = Variable.get("week_id")
PREVIOUS_WEEK_1 = 'W' + str(int(WEEK_ID) - 1)
Tasks = Variable.get("ETV_TV_HWE_TaskList")
DMA = Variable.get("ETV_TV_HWE_DMAList")
VCEXP_RUN_PARAM = Variable.get("VCEXP_RUN")
S3_DATA_PATH = Variable.get("base_s3_loc") + '/'
S3_DATA_PATH_WEEK = S3_DATA_PATH + WEEK_ID + 'w/'
MONTH_ID = str(get_month_id_from_week_id(int(WEEK_ID)))

# Derived Variables
WEEK_SCHEMA_0 = 'W' + WEEK_ID
TaskList = Tasks.split(",")
DMAList = DMA.split(",")
threshold_cap_HWE_WEIGHTS = 's3://csxpdev-xp-east/etv/etv-ecs/weights/threshold_cap_HWE_WEIGHTS.csv'

def etv_tv_hwe_weights(dag_name):
    dag = DAG(dag_name, default_args=default_args, schedule_interval=None)

    if "WEIGHTS" in TaskList:
        etv_expresso_hwe_by_dma = BashOperator(
            task_id='etv_expresso_hwe_by_dma',
            bash_command='snowsql --config ' + CONFIG_PATH + '' +
                         ' -f ' + BASE_SQL_PATH + 'hwe_person_hh_weights_by_dma.sql' +
                         ' -D WEEK_SCHEMA_0=' + WEEK_SCHEMA_0 + '' +
                         ' -D week_0=' + WEEK_ID + '' +
                         ' -D month_Id=' + MONTH_ID + ' -D VCEXP_RUN=' + VCEXP_RUN_PARAM + '',
            dag=dag)

        # Data Movement Person from Snowflake to S3
        export_live_hwe_weights_strata_info_hisp_top30 = BashOperator(
            task_id='export_live_hwe_weights_strata_info_hisp_top30',
            bash_command='snowsql --config {} '.format(CONFIG_PATH) +
                         ' -f {}exportDataWithCustomHeader.sql '.format(DATA_EXPORT_SQL_PATH) +
                         ' -D  s3Path={0}/{1}w/live_hwe_weights_strata_info_hisp_top30.csv '.format(
                             S3_STAGE_PATH, WEEK_ID) +
                         ' -D  sourceTable={} '.format(WEEK_SCHEMA_0 + '.live_hwe_weights_strata_info_hisp_top30') +
                         ' -D  customHeader="{}" '.format('DMA_ID as country_num, LOCATION as location, VARNAME as varname, VARVAL_EFFECTIVE as varval_effective, TARGPCT_EFFECTIVE as targpct_effective, STRATA_ORDER as strata_order') +
                         ' -D  outputFormat={} '.format('csv_format_comma_separated_uncompressed') +
                         ' -D  exportType={} '.format('true')
            , dag=dag)

        export_live_hwe_weights_sample_roster_hisp_top30 = BashOperator(
            task_id='export_live_hwe_weights_sample_roster_hisp_top30',
            bash_command='snowsql --config {} '.format(CONFIG_PATH) +
                         ' -f {}exportDataWithCustomHeader.sql '.format(DATA_EXPORT_SQL_PATH) +
                         ' -D  s3Path={0}/{1}w/live_hwe_weights_sample_roster_hisp_top30.csv '.format(
                             S3_STAGE_PATH, WEEK_ID) +
                         ' -D  sourceTable={} '.format(WEEK_SCHEMA_0 + '.live_hwe_weights_sample_roster_hisp_top30') +
                         ' -D  customHeader="{}" '.format('DMA_ID as country_num, LOCATION as location, PERSON_ID as person_id, GENDER_AGE_INFO as gender_age_info, ETHNICITY as ethnicity, CHILD_PRESENCE as child_presence, HH_SIZE as hh_size, TOTAL_USAGE_INTENSITY as total_usage_intensity') +
                         ' -D  outputFormat={} '.format('csv_format_comma_separated_uncompressed') +
                         ' -D  exportType={} '.format('true')
            , dag=dag)

        export_live_hwe_weights_pop_estimate_hisp_top30 = BashOperator(
            task_id='export_live_hwe_weights_pop_estimate_hisp_top30',
            bash_command='snowsql --config {} '.format(CONFIG_PATH) +
                         ' -f {}exportDataWithCustomHeader.sql '.format(DATA_EXPORT_SQL_PATH) +
                         ' -D  s3Path={0}/{1}w/live_hwe_weights_pop_estimate_hisp_top30.csv '.format(
                             S3_STAGE_PATH, WEEK_ID) +
                         ' -D  sourceTable={} '.format(WEEK_SCHEMA_0 + '.live_hwe_weights_pop_estimate_hisp_top30') +
                         ' -D  customHeader="{}" '.format('DMA_ID as country_num, LOCATION as location, POP_SIZE as pop_size') +
                         ' -D  outputFormat={} '.format('csv_format_comma_separated_uncompressed') +
                         ' -D  exportType={} '.format('true')
            , dag=dag)

        pre_espresso_weights_person_delete_S3_object = BashOperator(
            task_id='pre_espresso_weights_person_delete_S3_object'
            , bash_command='{}/deleteS3Object.sh '.format(UTILITY_PATH) +
                           '--s3Path {0}person/ '.format(S3_DATA_PATH_WEEK)
            , dag=dag
        )

        export_live_hwe_weights_strata_info_hisp_top30.set_upstream(etv_expresso_hwe_by_dma)
        export_live_hwe_weights_sample_roster_hisp_top30.set_upstream(etv_expresso_hwe_by_dma)
        export_live_hwe_weights_pop_estimate_hisp_top30.set_upstream(etv_expresso_hwe_by_dma)
        pre_espresso_weights_person_delete_S3_object.set_upstream(export_live_hwe_weights_strata_info_hisp_top30)
        pre_espresso_weights_person_delete_S3_object.set_upstream(export_live_hwe_weights_sample_roster_hisp_top30)
        pre_espresso_weights_person_delete_S3_object.set_upstream(export_live_hwe_weights_pop_estimate_hisp_top30)

        weight_tasks_person = []
        for dmavalue in DMAList:
            pre_espresso_weights_person = ECSOperator(
                task_id="pre_espresso_weights_person" + dmavalue,
                task_definition="weightssystem-linux_task",
                launch_type="FARGATE",
                cluster="Fargate-Cluster",
                network_configuration={
                    'awsvpcConfiguration': {
                        'subnets': [
                            'subnet-0d6d29c0d1dfe14f1',
                        ],
                        'securityGroups': [
                            'sg-02b1cb2a987877ff1',
                        ],
                        'assignPublicIp': 'ENABLED'
                    }
                },
                overrides={
                    'containerOverrides': [
                        {
                            'name': 'weightssystem-linux',
                            'command': [
                                '-s3_sample_path', S3_DATA_PATH_WEEK + 'live_hwe_weights_sample_roster_hisp_top30.csv',
                                '-s3_strata_info_path',
                                                   S3_DATA_PATH_WEEK + 'live_hwe_weights_strata_info_hisp_top30.csv',
                                '-s3_population_estimates_path',
                                                   S3_DATA_PATH_WEEK + 'live_hwe_weights_pop_estimate_hisp_top30.csv',
                                '-s3_threshold_cap_path',
                                '' + threshold_cap_HWE_WEIGHTS,  # STATIC FILE
                                '-s3_output_folder', S3_DATA_PATH_WEEK + 'person/' + dmavalue,
                                '-country_num', str(dmavalue),
                                '-location', 'All_loc',  # STATIC VAR
                                '-email_address', 'jdaly@comscore.com,ETVProduction@comscore.com',
                                '-max_iterations', '50',
                                '-level', 'person',
                                '-max_threads', '100',
                                '-weights_output_name', 'live_hwe_weights',
                                '-weights_distribution_output_name', 'live_hwe_weights_distribution',
                                '-weights_info_name', 'live_hwe_weights_info',
                                '-weights_log_name', 'live_hwe_weights_log'
                            ]
                        }
                    ]
                },
                dag=dag)

            pre_espresso_weights_person.set_upstream(pre_espresso_weights_person_delete_S3_object)
            weight_tasks_person.append(pre_espresso_weights_person)

        import_live_etv_hwe_weights = BashOperator(
            task_id='import_live_etv_hwe_weights',
            bash_command='snowsql --config {} '.format(CONFIG_PATH) +
                         ' -f {}/live_etv_hwe_weights.sql '.format(BASE_SQL_PATH) +
                         ' -D WEEK_SCHEMA_0={} '.format(WEEK_SCHEMA_0) +
                         ' -D WEEK_0={} '.format(WEEK_ID)
            , dag=dag)

        import_live_etv_hwe_weights.set_upstream(weight_tasks_person)

        if DATA_QUALITY_CHECK == "TRUE":
            dqc_person_weights = BashOperator(
                task_id='dqc_person_weights',
                bash_command='{0}/dataqualitycheck.sh HWE_Person_Weight hwe_person_weight {1} LIVE \"@table_name={2}.live_etv_hwe_weights @table_previous={3}.live_etv_hwe_weights\" {0} {4}'.format(DQC_PATH, WEEK_ID, WEEK_SCHEMA_0, PREVIOUS_WEEK_1, CONFIG_PATH)
            , dag=dag)

            dqc_person_weights.set_upstream(import_live_etv_hwe_weights)

        # Data Movement Household from Snowflake to S3
        export_live_hwe_weights_strata_info_H_hisp_top30 = BashOperator(
            task_id='export_live_hwe_weights_strata_info_H_hisp_top30',
            bash_command='snowsql --config {} '.format(CONFIG_PATH) +
                         ' -f {}exportDataWithCustomHeader.sql '.format(DATA_EXPORT_SQL_PATH) +
                         ' -D  s3Path={0}/{1}w/live_hwe_weights_strata_info_H_hisp_top30.csv '.format(
                             S3_STAGE_PATH, WEEK_ID) +
                         ' -D  sourceTable={} '.format(WEEK_SCHEMA_0 + '.live_hwe_weights_strata_info_H_hisp_top30') +
                         ' -D  customHeader="{}" '.format('DMA_ID as country_num, LOCATION as location, VARNAME as varname, VARVAL_EFFECTIVE as varval_effective, TARGPCT_EFFECTIVE as targpct_effective, STRATA_ORDER as strata_order') +
                         ' -D  outputFormat={} '.format('csv_format_comma_separated_uncompressed') +
                         ' -D  exportType={} '.format('true')
            , dag=dag)

        export_live_hwe_weights_sample_roster_H_hisp_top30 = BashOperator(
            task_id='export_live_hwe_weights_sample_roster_H_hisp_top30',
            bash_command='snowsql --config {} '.format(CONFIG_PATH) +
                         ' -f {}exportDataWithCustomHeader.sql '.format(DATA_EXPORT_SQL_PATH) +
                         ' -D  s3Path={0}/{1}w/live_hwe_weights_sample_roster_H_hisp_top30.csv '.format(
                             S3_STAGE_PATH, WEEK_ID) +
                         ' -D  sourceTable={} '.format(WEEK_SCHEMA_0 + '.live_hwe_weights_sample_roster_H_hisp_top30') +
                         ' -D  customHeader="{}" '.format('DMA_ID as country_num, LOCATION as location, HOUSEHOLD_ID as person_id, ETHNICITY as ethnicity, CHILD_PRESENCE as child_presence, HH_SIZE as hh_size, TOTAL_USAGE_INTENSITY as total_usage_intensity') +
                         ' -D  outputFormat={} '.format('csv_format_comma_separated_uncompressed') +
                         ' -D  exportType={} '.format('true')
            , dag=dag)

        export_live_hwe_weights_pop_estimate_H_hisp_top30 = BashOperator(
            task_id='export_live_hwe_weights_pop_estimate_H_hisp_top30',
            bash_command='snowsql --config {} '.format(CONFIG_PATH) +
                         ' -f {}exportDataWithCustomHeader.sql '.format(DATA_EXPORT_SQL_PATH) +
                         ' -D  s3Path={0}/{1}w/live_hwe_weights_pop_estimate_H_hisp_top30.csv '.format(
                             S3_STAGE_PATH, WEEK_ID) +
                         ' -D  sourceTable={} '.format(WEEK_SCHEMA_0 + '.live_hwe_weights_pop_estimate_H_hisp_top30') +
                         ' -D  customHeader="{}" '.format('DMA_ID as country_num, LOCATION as location, POP_SIZE as pop_size') +
                         ' -D  outputFormat={} '.format('csv_format_comma_separated_uncompressed') +
                         ' -D  exportType={} '.format('true')
            , dag=dag)

        pre_espresso_weights_household_delete_S3_object = BashOperator(
            task_id='pre_espresso_weights_household_delete_S3_object'
            , bash_command='{}/deleteS3Object.sh '.format(UTILITY_PATH) +
                           '--s3Path {0}household/ '.format(S3_DATA_PATH_WEEK)
            , dag=dag
        )

        export_live_hwe_weights_strata_info_H_hisp_top30.set_upstream(etv_expresso_hwe_by_dma)
        export_live_hwe_weights_sample_roster_H_hisp_top30.set_upstream(etv_expresso_hwe_by_dma)
        export_live_hwe_weights_pop_estimate_H_hisp_top30.set_upstream(etv_expresso_hwe_by_dma)
        pre_espresso_weights_household_delete_S3_object.set_upstream(export_live_hwe_weights_strata_info_H_hisp_top30)
        pre_espresso_weights_household_delete_S3_object.set_upstream(export_live_hwe_weights_sample_roster_H_hisp_top30)
        pre_espresso_weights_household_delete_S3_object.set_upstream(export_live_hwe_weights_pop_estimate_H_hisp_top30)

        weight_tasks_household = []
        for dmavalue in DMAList:
            pre_espresso_weights_household = ECSOperator(
                task_id="pre_espresso_weights_household" + dmavalue,
                task_definition="weightssystem-linux_task",
                launch_type="FARGATE",
                cluster="Fargate-Cluster",
                network_configuration={
                    'awsvpcConfiguration': {
                        'subnets': [
                            'subnet-0d6d29c0d1dfe14f1',
                        ],
                        'securityGroups': [
                            'sg-02b1cb2a987877ff1',
                        ],
                        'assignPublicIp': 'ENABLED'
                    }
                },
                overrides={
                    'containerOverrides': [
                        {
                            'name': 'weightssystem-linux',
                            'command': [
                                '-s3_sample_path',
                                S3_DATA_PATH_WEEK + 'live_hwe_weights_sample_roster_H_hisp_top30.csv',
                                '-s3_strata_info_path',
                                S3_DATA_PATH_WEEK + 'live_hwe_weights_strata_info_H_hisp_top30.csv',
                                '-s3_population_estimates_path',
                                S3_DATA_PATH_WEEK + 'live_hwe_weights_pop_estimate_H_hisp_top30.csv',
                                '-s3_threshold_cap_path',
                                '' + threshold_cap_HWE_WEIGHTS,  # STATIC FILE
                                '-s3_output_folder', S3_DATA_PATH_WEEK + 'household/' + dmavalue,
                                '-country_num', str(dmavalue),
                                '-location', 'All_loc',  # STATIC VAR
                                '-email_address', 'jdaly@comscore.com,ETVProduction@comscore.com',
                                '-max_iterations', '50',
                                '-level', 'household',
                                '-max_threads', '100',
                                '-weights_output_name', 'live_hwe_hh_weights',
                                '-weights_distribution_output_name', 'live_hwe_hh_weights_distribution',
                                '-weights_info_name', 'live_hwe_hh_weights_info',
                                '-weights_log_name', 'live_hwe_hh_weights_log'
                            ]
                        }
                    ]
                },
                dag=dag
            )

            
            pre_espresso_weights_household.set_upstream(pre_espresso_weights_household_delete_S3_object)
            weight_tasks_household.append(pre_espresso_weights_household)

        import_live_etv_hwe_hh_weights = BashOperator(
            task_id='import_live_etv_hwe_hh_weights',
            bash_command='snowsql --config {} '.format(CONFIG_PATH) +
                         ' -f {}/live_etv_hwe_hh_weights.sql '.format(BASE_SQL_PATH) +
                         ' -D WEEK_SCHEMA_0={} '.format(WEEK_SCHEMA_0) +
                         ' -D WEEK_0={} '.format(WEEK_ID)
            , dag=dag)

        import_live_etv_hwe_hh_weights.set_upstream(weight_tasks_household)

        if DATA_QUALITY_CHECK == "TRUE":
            dqc_hh_weights = BashOperator(
                task_id='dqc_hh_weights',
                bash_command='{0}/dataqualitycheck.sh HWE_HH_Weight hwe_hh_weight {1} LIVE \"@table_name={2}.live_etv_hwe_hh_weights @table_previous={3}.live_etv_hwe_hh_weights\" {0} {4}'.format(DQC_PATH, WEEK_ID, WEEK_SCHEMA_0, PREVIOUS_WEEK_1, CONFIG_PATH)
            , dag=dag)

            dqc_hh_weights.set_upstream(import_live_etv_hwe_hh_weights)

    return dag


etv_tv_hwe_weights_dag = etv_tv_hwe_weights('etv_tv_hwe_weights')
