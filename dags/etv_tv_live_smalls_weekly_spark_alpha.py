from airflow import DAG
from airflow.contrib.operators.ssh_operator import SSHOperator
from datetime import datetime, timedelta
from airflow.contrib.hooks.ssh_hook import SSHHook
from airflow.models import Variable
from emr_operations_spark import emr_tasks
from airflow.operators.bash_operator import BashOperator
from emr_operations_spark import emr_tasks
from airflow.utils.email import send_email
from comscoreEmail import notify_email
#sshHook = SSHHook(ssh_conn_id='emr_test')

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime.utcnow(),
    'email': ['sgude@comscore.com, vkomuravelli@comscore.com, skamidi@comscore.com, rbhallamudi@comscore.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=1),
    'on_failure_callback': notify_email
    #'pool': 'EMR_POOL'
}
PLATFORM = 'live'
#WEEK_ID = "{{var.value.week_id}}"
WEEK_ID = Variable.get("week_id")
BASE_PATH = "{{var.value.base_s3_loc}}"+'/'
BASE_OUTPUT_PATH=BASE_PATH+WEEK_ID+'w/'
ETV_TV_JAR = '/home/hadoop/etv-1.0.0.jar'
DATA_EXPORT_SCRIPT_PATH = '/mapr/ri1.comscore.com/test_data/census/dags/SnowflakeSQL/DataCopy/dataExport_aws.sh'
WEEK_SCHEMA_0 = 'W'+ WEEK_ID
PREVIOUS_WEEK_1 = 'W' + str(int(WEEK_ID) - 1)
CONFIG_PATH =  Variable.get("config_path_spark_weekly_monthly_batch")     # '/mapr/ri1.comscore.com/test_data/census/dags/snowsql/snowflake.config' #
BASE_SQL_PATH = Variable.get("base_sql_path")+'etv_tv_live_small_weekly_spark_alpha/'  # '/mapr/ri1.comscore.com/test_data/census/dags/SnowflakeSQL/' #
TEST_PREFIX = 'Test'
S3_STAGE_PATH= Variable.get("base_s3_path")
UTILITY_PATH = Variable.get("base_sql_path") + 'utilities'
DQC_PATH = Variable.get("data_quality_check_path")
DATA_QUALITY_CHECK = Variable.get("data_quality_check")
RUN_WITH_BLANK_FILES = Variable.get("run_with_blank_files")
def etv_tv_live_smalls_weekly_spark_alpha(dag_name):
    dag = DAG(dag_name, default_args=default_args, schedule_interval="@once", concurrency=10)

    emr=emr_tasks(dag)
    sshHook=emr.get_ssh_hook("emr_test")

    # GENERATE_EXPRIAN_ONLY_ROSTER_LOOKUP

    delete_S3_object_experian_only = BashOperator(
    task_id='DELETE_OBJECT_experian'
    , bash_command='{}/deletes3Object.sh '.format(UTILITY_PATH) +
    '--s3Path {0}/{1}w/live/Small/experian_only_roster_live_{1}w/'.format(BASE_PATH, WEEK_ID)
    , dag=dag
    )

    delete_S3_object_singular = BashOperator(
    task_id='DELETE_OBJECT_singular'
    , bash_command='{}/deletes3Object.sh '.format(UTILITY_PATH) +
    '--s3Path {0}/{1}w/live/Small/singular_key_demo_lookup_live_{1}w/'.format(BASE_PATH, WEEK_ID)
    , dag=dag
    )

    delete_S3_object_reporting_tsv = BashOperator(
    task_id='DELETE_OBJECT_reporting'
    , bash_command='{}/deletes3Object.sh '.format(UTILITY_PATH) +
    '--s3Path {0}/{1}w/live/Small/reporting_tsv_timeId_windows_live_{1}w.txt '.format(BASE_PATH, WEEK_ID)
    , dag=dag
    )


    delete_S3_object_espresso_agg = BashOperator(
    task_id='DELETE_OBJECT_espresso_agg'
    , bash_command='{}/deletes3Object.sh '.format(UTILITY_PATH) +
    '--s3Path {0}/{1}w/live/Espresso_Agg/Agg/espresso_agg_live_{1}w/'.format(BASE_PATH, WEEK_ID)
    , dag=dag
    )

    experian_only_roster_lookup_generation = BashOperator(task_id='Experian_roster_lookup',
                                                     bash_command='snowsql --config {}'.format(CONFIG_PATH) +
                                                                  ' -f {}experian_only_roster_lookup_generation.sql'.format(BASE_SQL_PATH) +
                                                                  ' -D  WEEK_SCHEMA_0={} '.format(WEEK_SCHEMA_0) +
                                                                  ' -D  WEEK_ID={} '.format(WEEK_ID)

                                                     ,dag=dag)



    Export_experian_only_incremental_small_SnowSQL_S3 = BashOperator(task_id='Export_experian_only_incremental_small_SnowSQL_S3',

                                                                     bash_command='snowsql --config {} '.format(CONFIG_PATH) +
                                                                                  ' -f {}exportData.sql '.format(BASE_SQL_PATH) +
                                                                                  ' -D  s3Path={0}/{1}w/live/Small/experian_only_roster_live_{1}w/incremental_small/part-r- '.format(S3_STAGE_PATH, WEEK_ID) +
                                                                                  ' -D  sourceTable={} '.format(WEEK_SCHEMA_0+'.experian_only_incremental_small') +
                                                                                  ' -D  outputFormat={} '.format('MY_CSV_FORMAT') +
                                                                                  ' -D  exportType={} '.format('false') +
                                                                                  ' -D  withHeader={} '.format('false') +
                                                                                  ' -D  overwrite={} '.format('true')
                                                                     , dag=dag)

    Export_experian_only_overlap_small_SnowSQL_S3 = BashOperator(task_id='Export_experian_only_overlap_small_SnowSQL_S3',
                                                                 bash_command='snowsql --config {} '.format(CONFIG_PATH) +
                                                                              ' -f {}exportData.sql '.format(BASE_SQL_PATH) +
                                                                              ' -D  s3Path={0}/{1}w/live/Small/experian_only_roster_live_{1}w/overlap_small/part-r- '.format(S3_STAGE_PATH, WEEK_ID) +
                                                                              ' -D  sourceTable={} '.format(WEEK_SCHEMA_0+'.experian_only_overlap_small') +
                                                                              ' -D  outputFormat={} '.format('MY_CSV_FORMAT') +
                                                                              ' -D  exportType={} '.format('false') +
                                                                              ' -D  withHeader={} '.format('false') +
                                                                              ' -D  overwrite={} '.format('true')
                                                                 , dag=dag)
    generate_singular_key = BashOperator(task_id='Generate_singular_key',
                                                     bash_command='snowsql --config {}'.format(CONFIG_PATH) +
                                                                  ' -f {}singular_key_id_demo_lookup.sql'.format(BASE_SQL_PATH) +
                                                                  ' -D  WEEK_SCHEMA_0={} '.format(WEEK_SCHEMA_0) +
                                                                  ' -D  WEEK_ID={} '.format(WEEK_ID)

                                                     ,dag=dag)


    Export_singular_key_demo_lookup_SnowSQL_S3 = BashOperator(task_id='Export_singular_key_demo_lookup_SnowSQL_S3',
                                                              bash_command='snowsql --config {} '.format(CONFIG_PATH) +
                                                                           ' -f {}exportData.sql '.format(BASE_SQL_PATH) +
                                                                           ' -D  s3Path={0}/{1}w/live/Small/singular_key_demo_lookup_live_{1}w/part-r '.format(S3_STAGE_PATH, WEEK_ID) +
                                                                           ' -D  sourceTable={} '.format(WEEK_SCHEMA_0+'.singular_key_demo_lookup') +
                                                                           ' -D  outputFormat={} '.format('my_csv_format_uncompressed') +
                                                                           ' -D  exportType={} '.format('false') +
                                                                           ' -D  withHeader={} '.format('false')+
                                                                           ' -D  overwrite={} '.format('true')
                                                              , dag=dag)



    Export_live_espresso_agg_weights_SnowSQL_S3 = BashOperator(task_id='Export_live_espresso_agg_weights_SnowSQL_S3',
                                                               bash_command='snowsql --config {} '.format(CONFIG_PATH) +
                                                                            ' -f {}exportData.sql '.format(BASE_SQL_PATH) +
                                                                            ' -D  s3Path={0}/{1}w/live/Espresso_Agg/Agg/espresso_agg_live_{1}w/part-r- '.format(S3_STAGE_PATH, WEEK_ID) +
                                                                            ' -D  sourceTable={} '.format(WEEK_SCHEMA_0+'.etv_live_espresso_agg_weights') +
                                                                            ' -D  outputFormat={} '.format('my_parquet_format') +
                                                                            ' -D  exportType={} '.format('false') +
                                                                            ' -D  withHeader={} '.format('true')
                                                               , dag=dag
                                                               )

    ## REPORTING_TSV_TIMEID_WINDOWS_LIVE
    reporting_tsv_timeId_windows_live = BashOperator(task_id='reporting_tsv_timeId_windows_live',
                                                     bash_command='snowsql --config {}'.format(CONFIG_PATH) +
                                                                  ' -f {}etv_tv_live_overlap_small_calculate_tsv_reporting_windows.sql'.format(BASE_SQL_PATH) +
                                                                  ' -D  WEEK_SCHEMA_0={} '.format(WEEK_SCHEMA_0) +
                                                                  ' -D  WEEK_ID={} '.format(WEEK_ID)

                                                     ,dag=dag)

    Export_reporting_tsv_timeId_windows_live_SnowSQL_S3 = BashOperator(task_id='Export_reporting_tsv_timeId_windows_live_SnowSQL_S3',
                                                                       bash_command='snowsql --config {} '.format(CONFIG_PATH) +
                                                                                    ' -f {}exportData.sql '.format(BASE_SQL_PATH) +
                                                                                    ' -D  s3Path={0}/{1}w/live/Small/reporting_tsv_timeId_windows_live_{1}w.txt '.format(S3_STAGE_PATH, WEEK_ID) +
                                                                                    ' -D  sourceTable={} '.format(WEEK_SCHEMA_0+'.reporting_tsv_timeId_windows_live') +
                                                                                    ' -D  outputFormat={} '.format('my_csv_format_uncompressed') +
                                                                                    ' -D  exportType={} '.format('true') +
                                                                                    ' -D  withHeader={} '.format('false')+
                                                                                    ' -D  overwrite={} '.format('true')
                                                                       , dag=dag
                                                                       )


    Export_experian_only_incremental_small_SnowSQL_S3.set_upstream(delete_S3_object_experian_only)
    Export_experian_only_incremental_small_SnowSQL_S3.set_upstream(delete_S3_object_singular)
    Export_experian_only_incremental_small_SnowSQL_S3.set_upstream(delete_S3_object_reporting_tsv)
    Export_experian_only_incremental_small_SnowSQL_S3.set_upstream(delete_S3_object_espresso_agg)
    Export_experian_only_incremental_small_SnowSQL_S3.set_upstream(experian_only_roster_lookup_generation)
    Export_experian_only_incremental_small_SnowSQL_S3.set_upstream(generate_singular_key)
    Export_experian_only_overlap_small_SnowSQL_S3.set_upstream(Export_experian_only_incremental_small_SnowSQL_S3)
    Export_singular_key_demo_lookup_SnowSQL_S3.set_upstream(Export_experian_only_incremental_small_SnowSQL_S3)
    Export_reporting_tsv_timeId_windows_live_SnowSQL_S3.set_upstream(Export_singular_key_demo_lookup_SnowSQL_S3)
    Export_live_espresso_agg_weights_SnowSQL_S3.set_upstream(Export_reporting_tsv_timeId_windows_live_SnowSQL_S3)

    emr.create_emr_cluster().set_upstream(Export_live_espresso_agg_weights_SnowSQL_S3)
    emr.sleep_operation().set_upstream(emr.create_emr_cluster())
    emr.update_airflow_emr_connection().set_upstream(emr.sleep_operation())
    emr.copy_binaries(sshHook).set_upstream(emr.update_airflow_emr_connection())

    Run_Mergepartition_On_Espresso_Agg = SSHOperator(task_id='Run_Mergepartition_On_Espresso_Agg',
                                                command='spark-submit --master yarn'
                                                        ' --conf spark.hadoop.mapreduce.fileoutputcommitter.algorithm.version=2'
                                                        ' --conf spark.executor.extraJavaOptions=-XX:+UseG1GC'
                                                        ' --driver-memory 2g'
                                                        ' --class com.comscore.xp.etv.drivers.Main'
                                                        ' --num-executors 40'
                                                        ' --executor-memory 25g'
                                                        ' --executor-cores 4 ' + ETV_TV_JAR +''
                                                        ' --sparkSerializer org.apache.spark.serializer.KryoSerializer'
                                                        ' --primaryStorageLevel MEMORY_AND_DISK_SER'
                                                        ' --secondaryStorageLevel DISK_ONLY'
                                                        ' --appName ETV_smallIncremental_partition'
                                                        ' --weekId '+WEEK_ID+''
                                                        ' --jobType mergepartition'
                                                        ' --espressoAggPath '+ BASE_OUTPUT_PATH + PLATFORM + '/Espresso_Agg/Agg/espresso_agg_live_'+WEEK_ID+'w'
                                                        ' --repartitionedEspressoAggPath '+ BASE_OUTPUT_PATH + PLATFORM + '/Espresso_Agg/Agg/espresso_agg_live_'+WEEK_ID+'w_staging',
                                                dag=dag,
                                                ssh_hook=sshHook
                                                )


    Run_Mergepartition_On_Espresso_Agg.set_upstream(emr.copy_binaries(sshHook))


    Run_partition_On_Espresso_Agg = SSHOperator(task_id='Run_partition_On_Espresso_Agg',
                                                command='spark-submit --master yarn'
                                                        ' --conf spark.hadoop.mapreduce.fileoutputcommitter.algorithm.version=2'
                                                        ' --conf spark.executor.extraJavaOptions=-XX:+UseG1GC'
                                                        ' --driver-memory 2g'
                                                        ' --class com.comscore.xp.etv.drivers.Main'
                                                        ' --num-executors 40'
                                                        ' --executor-memory 25g'
                                                        ' --executor-cores 4 ' + ETV_TV_JAR +''
                                                        ' --sparkSerializer org.apache.spark.serializer.KryoSerializer'
                                                        ' --primaryStorageLevel MEMORY_AND_DISK_SER'
                                                        ' --secondaryStorageLevel DISK_ONLY'
                                                        ' --appName ETV_smallIncremental_partition'
                                                        ' --weekId '+WEEK_ID+''
                                                        ' --jobType partition'
                                                        ' --repartitionedEspressoAggPath '+ BASE_OUTPUT_PATH + PLATFORM + '/Espresso_Agg/Agg/espresso_agg_live_'+WEEK_ID+'w_staging'
                                                        ' --numOfPartitions 25'
                                                        ' --partitionedEspressoAggPath '+ BASE_OUTPUT_PATH + PLATFORM + '/Small/Weekly/partitioned_espresso_Weekly',
                                                dag=dag,
                                                ssh_hook=sshHook
                                                )

    Run_partition_On_Espresso_Agg.set_upstream(Run_Mergepartition_On_Espresso_Agg)
    Run_Transpose_weekly_for_part =[]
    #Run_Transpose_Weekly for the given week for 0 to 25 partitions
    for i in range(25):
        temp = SSHOperator(task_id='Run_Transpose_weekly_for_part_'+str(i),
                           command='spark-submit --master yarn'
                                   ' --driver-memory 1g'
                                   ' --conf spark.yarn.executor.memoryOverhead=5g'
                                   ' --class com.comscore.xp.etv.drivers.Main'
                                   ' --num-executors 30'
                                   ' --executor-memory 20g'
                                   ' --executor-cores 6 '+ETV_TV_JAR+''
                                   ' --sparkSerializer org.apache.spark.serializer.KryoSerializer'
                                   ' --primaryStorageLevel MEMORY_AND_DISK_SER'
                                   ' --secondaryStorageLevel DISK_ONLY'
                                   ' --appName ETV_smallIncremental_weekly_transpose_live_'+str(WEEK_ID)+'w_partition_'+str(i)+' '
                                   ' --weekId '+WEEK_ID+''
                                   ' --jobType transpose'
                                   ' --espressoAggBasePath '+ BASE_OUTPUT_PATH + PLATFORM + '/Small/Weekly/partitioned_espresso_Weekly'
                                   ' --partitionedEspressoAggPath '+ BASE_OUTPUT_PATH + PLATFORM + '/Small/Weekly/partitioned_espresso_Weekly/partitionId='+str(i)+''
                                   ' --espressoAggTransposedPath ' + BASE_OUTPUT_PATH + PLATFORM + '/Small/Weekly/Transpose_live_'+str(WEEK_ID)+'w/part'+str(i)+' ',
                           dag=dag,
                           ssh_hook=sshHook
                           )
        temp.set_upstream(Run_partition_On_Espresso_Agg)
        Run_Transpose_weekly_for_part.append(temp)

    Experian_filter_1 =[]
    Experian_filter_2 =[]
    Experian_filter_3 =[]
    Experian_filter_4 =[]
    Experian_filter_5 =[]
    Experian_filter_6 =[]

    #Experian_Filter_on_Transpose_Weekly for the given week
    for i in range(25):
        temp1 = SSHOperator(task_id='LTT-Experian_filter_on_transpose_weekly_part'+str(i),
                            command='spark-submit --master yarn --driver-memory 1g'
                                    ' --conf spark.yarn.executor.memoryOverhead=5g'
                                    ' --class com.comscore.xp.etv.drivers.Main'
                                    ' --num-executors 30'
                                    ' --executor-memory 20g'
                                    ' --executor-cores 6 '+ETV_TV_JAR+''
                                    ' --sparkSerializer org.apache.spark.serializer.KryoSerializer'
                                    ' --primaryStorageLevel MEMORY_AND_DISK_SER'
                                    ' --secondaryStorageLevel DISK_ONLY '
                                    ' --appName ETV_smallIncremental_weekly_ltt_experian_filter_transpose_'+str(WEEK_ID)+'w_partition_'+str(i)+' '
                                    ' --weekId  '+WEEK_ID+''
                                    ' --jobType experianOnly '
                                    ' --espressoAggTransposedPath ' + BASE_OUTPUT_PATH + PLATFORM + '/Small/Weekly/Transpose_live_'+str(WEEK_ID)+'w/part'+str(i)+' '
                                    ' --experianLookUpPath ' + BASE_OUTPUT_PATH + PLATFORM + '/Small/experian_only_roster_live_'+WEEK_ID+'w/incremental_small '
                                    ' --expOnlyTransposedPath '+ BASE_OUTPUT_PATH + PLATFORM + '/Small/Weekly/experian_transpose_'+WEEK_ID+'w_weekly/part'+str(i)+' ',
                            dag=dag,
                            ssh_hook=sshHook
                            )
        Experian_filter_1.append(temp1)
        temp1.set_upstream(Run_Transpose_weekly_for_part[i])
    if RUN_WITH_BLANK_FILES == "FALSE":
        temparoty_counter = 1
        #Experian_filter_on_transpose_weekly from 955 to 959(0 to 24 partitions)
        for i in range(int(WEEK_ID)-5, int(WEEK_ID)):
            #for i in range(955, 960):
            temparoty_counter = temparoty_counter+ 1
            for j in range(25):
                Experian_filter =  SSHOperator(task_id='LTT-Experian_filter_on_transpose_weekly_'+str(i)+'w_for_part_'+str(j),
                                               command='spark-submit --master yarn '
                                                       ' --driver-memory 1g '
                                                       ' --conf spark.yarn.executor.memoryOverhead=5g'
                                                       ' --class com.comscore.xp.etv.drivers.Main'
                                                       ' --num-executors 30 '
                                                       ' --executor-memory 20g '
                                                       ' --executor-cores 6 '+ETV_TV_JAR+''
                                                       ' --sparkSerializer org.apache.spark.serializer.KryoSerializer'
                                                       ' --primaryStorageLevel MEMORY_AND_DISK_SER'
                                                       ' --secondaryStorageLevel DISK_ONLY'
                                                       ' --appName ETV_smallIncremental_weekly_ltt_experian_filter_transpose_'+str(i)+'w_partition_'+str(j)+'_for_'+str(WEEK_ID)+'w '
                                                       ' --weekId '+str(i)+''
                                                       ' --jobType experianOnly '
                                                       ' --espressoAggTransposedPath ' + BASE_PATH+''+str(i)+'w/' + PLATFORM + '/Small/Weekly/Transpose_live_'+str(i)+'w'+'/part'+str(j)+' '
                                                       ' --experianLookUpPath ' + BASE_OUTPUT_PATH + PLATFORM + '/Small/experian_only_roster_live_'+WEEK_ID+'w/incremental_small '
                                                       ' --expOnlyTransposedPath '+ BASE_OUTPUT_PATH + PLATFORM + '/Small/Weekly/experian_transpose_'+str(i)+'w_weekly/part'+str(j)+' ',
                                               dag=dag,
                                               ssh_hook=sshHook
                                               )
                if(temparoty_counter == 2):
                    Experian_filter_2.append(Experian_filter)
                if(temparoty_counter == 3):
                    Experian_filter_3.append(Experian_filter)
                if(temparoty_counter == 4):
                    Experian_filter_4.append(Experian_filter)
                if(temparoty_counter == 5):
                    Experian_filter_5.append(Experian_filter)
                if(temparoty_counter == 6):
                    Experian_filter_6.append(Experian_filter)
                Experian_filter.set_upstream(Run_Transpose_weekly_for_part[j])

    Run_Matrix_JOBS = []
    #Run_Matrix_for_Partition from 0 to 24
    for i in range(25):
        Run_Matrix = SSHOperator(task_id='run_matrix_for_part_'+str(i),
                                 command='spark-submit --master yarn '
                                         ' --driver-memory 1g'
                                         ' --conf spark.dynamicAllocation.minExecutors=5 '
                                         ' --conf spark.dynamicAllocation.maxExecutors=40 '
                                         ' --conf spark.dynamicAllocation.initialExecutors=10 '
                                         ' --class com.comscore.xp.etv.drivers.Main'
                                         ' --num-executors 40'
                                         ' --executor-memory 29g'
                                         ' --executor-cores 6 '+ETV_TV_JAR+''
                                         ' --sparkSerializer org.apache.spark.serializer.KryoSerializer '
                                         ' --primaryStorageLevel MEMORY_AND_DISK_SER '
                                         ' --secondaryStorageLevel DISK_ONLY '
                                         ' --appName ETV_smallIncremental_weekly_6WeeksMatrix_live_'+str(WEEK_ID)+'w_partition_'+str(i)+' '
                                         ' --weekId '+WEEK_ID+''
                                         ' --jobType 6WeeksMatrix '
                                         ' --expOnlyTransposedCurrentWeekPath '+ BASE_OUTPUT_PATH + PLATFORM + '/Small/Weekly/experian_transpose_'+(WEEK_ID)+'w_weekly/part'+str(i)+' '
                                         ' --expOnlyTransposedPrevWeek1Path ' + BASE_OUTPUT_PATH + PLATFORM + '/Small/Weekly/experian_transpose_'+(str(int(WEEK_ID)-1))+'w_weekly/part'+str(i)+' '
                                         ' --expOnlyTransposedPrevWeek2Path '+ BASE_OUTPUT_PATH + PLATFORM + '/Small/Weekly/experian_transpose_'+str((int(WEEK_ID)-2))+'w_weekly/part'+str(i)+' '
                                         ' --expOnlyTransposedPrevWeek3Path ' + BASE_OUTPUT_PATH + PLATFORM + '/Small/Weekly/experian_transpose_'+str((int(WEEK_ID)-3))+'w_weekly/part'+str(i)+' '
                                         ' --expOnlyTransposedPrevWeek4Path '+BASE_OUTPUT_PATH + PLATFORM + '/Small/Weekly/experian_transpose_'+str((int(WEEK_ID)-4))+'w_weekly/part'+str(i)+' '
                                         ' --expOnlyTransposedPrevWeek5Path '+BASE_OUTPUT_PATH + PLATFORM + '/Small/Weekly/experian_transpose_'+str((int(WEEK_ID)-5))+'w_weekly/part'+str(i)+' '
                                         ' --espressoAggMatrix '+BASE_OUTPUT_PATH + PLATFORM + '/Small/Weekly/espresso_agg_matrix_weekly/part'+str(i)+' ',
                                 dag=dag,
                                 ssh_hook=sshHook
                                 )
        Run_Matrix.set_upstream(Experian_filter_1[i])
        if RUN_WITH_BLANK_FILES == "FALSE":
            Run_Matrix.set_upstream(Experian_filter_2[i])
            Run_Matrix.set_upstream(Experian_filter_3[i])
            Run_Matrix.set_upstream(Experian_filter_4[i])
            Run_Matrix.set_upstream(Experian_filter_5[i])
            Run_Matrix.set_upstream(Experian_filter_6[i])
        Run_Matrix_JOBS.append(Run_Matrix)

    Run_overlap_smalls_dag_list = []

    for i in range(25):
        Run_overlap_small_inputs = SSHOperator(task_id='Run_Overlap_Small_Inputs_For_Part_' + str(i) + '',
                                               command='spark-submit --master yarn'
                                                       ' --driver-memory 1g'
                                                       ' --conf spark.dynamicAllocation.minExecutors=5'
                                                       ' --conf spark.dynamicAllocation.maxExecutors=40'
                                                       ' --conf spark.dynamicAllocation.initialExecutors=10'
                                                       ' --conf spark.sql.shuffle.partitions=1000'
                                                       ' --conf spark.network.timeout=600'
                                                       ' --class com.comscore.xp.etv.drivers.Main'
                                                       ' --num-executors 30'
                                                       ' --executor-memory 29g'
                                                       ' --executor-cores 6 ' + ETV_TV_JAR + ''
                                                       ' --sparkSerializer org.apache.spark.serializer.KryoSerializer'
                                                       ' --primaryStorageLevel MEMORY_AND_DISK_SER'
                                                       ' --secondaryStorageLevel DISK_ONLY'
                                                       ' --appName ETV_TV_overlap_small_' + PLATFORM + '_'+str(WEEK_ID)+'w_partition_' + str(i) + ''
                                                       ' --weekId  '+WEEK_ID+''
                                                       ' --platform ' + PLATFORM + ''
                                                       ' --jobType overlapSmall'
                                                       ' --espressoAggMatrix '+BASE_OUTPUT_PATH + PLATFORM + '/Small/Weekly/espresso_agg_matrix_weekly/part'+str(i)+''
                                                       ' --personsLookupPath ' + BASE_OUTPUT_PATH + PLATFORM + '/Small/experian_only_roster_live_'+WEEK_ID+'w/overlap_small'
                                                       ' --overlapSmallComputation ' + BASE_OUTPUT_PATH + PLATFORM + '/Small/Weekly/overlap_small_inputs/output/base/part' + str(i) + '',
                                               dag=dag,
                                               ssh_hook=sshHook
                                               )
        Run_overlap_smalls_dag_list.append(Run_overlap_small_inputs)
        Run_overlap_small_inputs.set_upstream(Run_Matrix_JOBS[i])

    Run_overlap_small_transformGP_dag_list = []
    for i in range(25):
        Run_overlap_small_final_incremental_report = SSHOperator(task_id='Run_Final_Overlap_SmallTransformGP_Report_For_Part_' + str(i) + '',
                                                                 command='spark-submit --master yarn'
                                                                         ' --driver-memory 1g'
                                                                         ' --class com.comscore.xp.etv.drivers.Main'
                                                                         ' --executor-memory 25g'
                                                                         ' --executor-cores 6 ' + ETV_TV_JAR + ' '
                                                                         ' --sparkSerializer org.apache.spark.serializer.KryoSerializer'
                                                                         ' --primaryStorageLevel MEMORY_AND_DISK_SER'
                                                                         ' --secondaryStorageLevel DISK_ONLY'
                                                                         ' --appName ETV_small_final_report_' + PLATFORM + '_'+str(WEEK_ID)+'w_partition_' + str(i) + ' '
                                                                         ' --weekId ' + WEEK_ID + ''
                                                                         ' --jobType overlapSmallTransformGP'
                                                                         ' --platform ' + PLATFORM + ''
                                                                         ' --overlapSmallComputationPath ' +BASE_OUTPUT_PATH + PLATFORM +'/Small/Weekly/overlap_small_inputs/output/base/part' + str(i) + ''
                                                                         ' --timeIdTsvLookupPath ' + BASE_OUTPUT_PATH + PLATFORM +'/Small/reporting_tsv_timeId_windows_live_'+WEEK_ID+'w.txt'
                                                                         ' --overlapSmallComputerExpandedPath '+ BASE_OUTPUT_PATH + PLATFORM + '/Small/Weekly/overlap_small_transformGP/part' + str(i) + '',
                                                                 dag=dag,
                                                                 ssh_hook=sshHook
                                                                 )
        Run_overlap_small_transformGP_dag_list.append(Run_overlap_small_final_incremental_report)
        Run_overlap_small_final_incremental_report.set_upstream(Run_overlap_smalls_dag_list[i])

    small_Computer_dag_list = []

    for i in range(25):
        Run_smalls_for_part = SSHOperator(task_id='Run_Smalls_For_Part_' + str(i),
                                          command='spark-submit --master yarn'
                                                  ' --driver-memory 1g'
                                                  ' --conf spark.sql.shuffle.partitions=1000'
                                                  ' --conf spark.network.timeout=600'
                                                  ' --class com.comscore.xp.etv.drivers.Main '
                                                  ' --num-executors 30'
                                                  ' --executor-memory 29g'
                                                  ' --executor-cores 6  ' + ETV_TV_JAR + ''
                                                  ' --sparkSerializer org.apache.spark.serializer.KryoSerializer'
                                                  ' --primaryStorageLevel MEMORY_AND_DISK_SER'
                                                  ' --secondaryStorageLevel DISK_ONLY'
                                                  ' --appName ETV_TV_small_computer_weekly_' + PLATFORM + '_' +str(WEEK_ID)+ 'w_partition_' + str(i) + ' '
                                                  ' --weekId ' + WEEK_ID + ''
                                                  ' --jobType smallComputer'
                                                  ' --platform ' + PLATFORM + ''
                                                  ' --espressoAggMatrix '+BASE_OUTPUT_PATH + PLATFORM + '/Small/Weekly/espresso_agg_matrix_weekly/part'+str(i)+''
                                                  ' --lttDemoLookUpPath ' + BASE_OUTPUT_PATH +PLATFORM+ '/Small/singular_key_demo_lookup_live_'+ WEEK_ID + 'w'
                                                  ' --smallComputation ' + BASE_OUTPUT_PATH +PLATFORM+ '/Small/Weekly/Smalls_live/part' + str(i),
                                          dag=dag,
                                          ssh_hook=sshHook
                                          )
        small_Computer_dag_list.append(Run_smalls_for_part)
        Run_smalls_for_part.set_upstream(Run_overlap_small_transformGP_dag_list[i])

    transform_GP_weekly_dag_list = []
    for i in range(25):
        Run_final_incremental_report = SSHOperator(task_id='Run_Final_Incremental_Report_For_Part_' + str(i) + '',
                                                   command='spark-submit --master yarn'
                                                           ' --driver-memory 1g'
                                                           ' --class com.comscore.xp.etv.drivers.Main'
                                                           ' --executor-memory 25g'
                                                           ' --executor-cores 6 ' + ETV_TV_JAR + ' '
                                                           ' --sparkSerializer org.apache.spark.serializer.KryoSerializer'
                                                           ' --primaryStorageLevel MEMORY_AND_DISK_SER'
                                                           ' --secondaryStorageLevel DISK_ONLY'
                                                           ' --appName ETV_small_final_report_' + PLATFORM + '_' + str(WEEK_ID)+ 'w_partition_' + str(i) + ' '
                                                           ' --weekId ' + WEEK_ID + ''
                                                           ' --jobType transformGP'
                                                           ' --platform ' + PLATFORM + ''                                                                                                                                                                       ' --smallComputerPath ' + BASE_OUTPUT_PATH +PLATFORM+ '/Small/Weekly/Smalls_live/part' + str(i)+''
                                                           ' --smallComputerExpandedPath ' + BASE_OUTPUT_PATH +PLATFORM+ '/Small/Weekly/Smalls_live_transformGP/part' + str(i),
                                                   dag=dag,
                                                   ssh_hook=sshHook
                                                   )
        transform_GP_weekly_dag_list.append(Run_final_incremental_report)
        Run_final_incremental_report.set_upstream(small_Computer_dag_list[i])
    # emr.delete_cluster().set_upstream(Run_final_incremental_report[i])

    #output
    """overlapSmall"""
    # HADOOP- /mapr/ri1.comscore.com/output/etv_tv/<WEEK>w/live/Small/Weekly/OverlapSmall_live_<WEEK>w/
    # SNOWFLAKE_TABLE - <SCHEMA>.live_overlap_small_input

    Export_live_overlap_small_input_S3_SnowSQL = BashOperator(task_id='Export_live_overlap_small_input_S3_SnowSQL',
                                                              bash_command='snowsql --config {} '.format(CONFIG_PATH) +
                                                                           '-f {}Load_live_overlap_small_input_s3_snowflake.sql '.format(BASE_SQL_PATH) +
                                                                           '-D WEEK_SCHEMA_0={} '.format(WEEK_SCHEMA_0) +
                                                                           '-D TEST_PREFIX={} '.format(TEST_PREFIX) +
                                                                           '-D PLATFORM={} '.format(PLATFORM) +
                                                                           '-D WEEK_ID={}'.format(str(WEEK_ID)+'w')
                                                              ,dag=dag)

    """smallComputer"""
    # HADOOP -/mapr/ri1.comscore.com/output/etv_tv/<WEEK>w/live/Small/Weekly/Smalls_live_<WEEK>w/part0/gpTableFlag=true
    # SNOWFLAKE_TABLE - <SCHEMA>.live_inclusive_small

    Export_live_inclusive_small_S3_SnowSQL = BashOperator(task_id='Export_live_inclusive_small_S3_SnowSQL',
                                                          bash_command='snowsql --config {} '.format(CONFIG_PATH) +
                                                                       '-f {}Load_live_inclusive_small_s3_snowflake.sql '.format(BASE_SQL_PATH) +
                                                                       '-D WEEK_SCHEMA_0={} '.format(WEEK_SCHEMA_0) +
                                                                       '-D TEST_PREFIX={} '.format(TEST_PREFIX) +
                                                                       '-D PLATFORM={} '.format(PLATFORM) +
                                                                       '-D WEEK_ID={}'.format(str(WEEK_ID)+'w')
                                                          ,dag=dag)

    # HADOOP PATH - /mapr/ri1.comscore.com/output/etv_tv/<WEEK>w/live/Small/Weekly/Smalls_live_<WEEK>w/part0/gpTableFlag=false
    # SNOWFLAKE_TABLE - <SCHEMA>.live_inclusive_small_w2w

    Export_live_inclusive_small_w2w_S3_SnowSQL = BashOperator(task_id='Export_live_inclusive_small_w2w_S3_SnowSQL',
                                                              bash_command='snowsql --config {} '.format(CONFIG_PATH) +
                                                                           '-f {}Load_live_inclusive_small_w2w_s3_snowflake.sql '.format(BASE_SQL_PATH) +
                                                                           '-D WEEK_SCHEMA_0={} '.format(WEEK_SCHEMA_0) +
                                                                           '-D TEST_PREFIX={} '.format(TEST_PREFIX) +
                                                                           '-D PLATFORM={} '.format(PLATFORM) +
                                                                           '-D WEEK_ID={}'.format(str(WEEK_ID)+'w')
                                                              ,dag=dag)

    Export_live_overlap_small_input_S3_SnowSQL.set_upstream(transform_GP_weekly_dag_list)
    Export_live_inclusive_small_S3_SnowSQL.set_upstream(Export_live_overlap_small_input_S3_SnowSQL)
    Export_live_inclusive_small_w2w_S3_SnowSQL.set_upstream(Export_live_inclusive_small_S3_SnowSQL)

    if DATA_QUALITY_CHECK == "TRUE":
        dqc_inc_small = BashOperator(
            task_id='dqc_inc_small',
            bash_command='{0}/dataqualitycheck.sh Incremental small {1} LIVE \"@table_name={2}.comscore_etv_live_inclusive_small_tsv @table_previous={3}.comscore_etv_live_inclusive_small_tsv\" {0} {4}'.format(DQC_PATH, WEEK_ID, WEEK_SCHEMA_0, PREVIOUS_WEEK_1, CONFIG_PATH)
        , dag=dag)

        dqc_inc_small.set_upstream(Export_live_inclusive_small_S3_SnowSQL)

    return dag

etv_tv_live_smalls_weekly_spark_alpha_dag = etv_tv_live_smalls_weekly_spark_alpha('etv_tv_live_smalls_weekly_spark_alpha')
