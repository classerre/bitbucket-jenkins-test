from datetime import datetime
from datetime import timedelta
from airflow.contrib.operators.ssh_operator import SSHOperator
from airflow.models import DAG
from airflow.models import Variable
from airflow.operators.bash_operator import BashOperator
from airflow.operators.dummy_operator import DummyOperator
from csdatetools import csdatetools as csd
from ecs_operator import ECSOperator
from emr_operations import emr_tasks
from comscoreEmail import notify_email

default_args = {
    'owner': 'mrudra',
    'depends_on_past': False,
    'start_date': datetime(2019, 1, 31),
    'email': [
        'sgude@comscore.com, vkomuravelli@comscore.com, skamidi@comscore.com, rbhallamudi@comscore.com, gmuthyala@comscore.com'],
    'email_on_failure': False,
    'on_failure_callback': notify_email,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=1)
}


# 'etv_batch4.etv_ott_dg_regression'
def etv_ott_dg_regression(dag_name):
    dag = DAG(dag_name, default_args=default_args, schedule_interval=None)

    # Variables
    BASE_SQL_PATH = Variable.get("base_sql_path")
    UTILITY_PATH = Variable.get("base_sql_path") + 'utilities'


    WEEK_ID = Variable.get("week_id")
    #CONFIG_PATH = Variable.get("regression_config_path")#Variable.get("config_path")
    CONFIG_PATH = Variable.get("config_path")
    RUN_BATCH = True  # need to parameterize
    emr = emr_tasks(dag)
    sshHook = emr.get_ssh_hook("etv_ott_dg_regression_connection")
    time_type = Variable.get("time_type")
    test_suffix = ""#_test
    file_test_suffix = ""#test/

    # Derived Variables
    Week = 'W' + WEEK_ID
    PREVIOUS_WEEK_1 = 'W' + str(int(WEEK_ID) - 1)
    PREVIOUS_WEEK_2 = 'W' + str(int(WEEK_ID) - 2)
    PREVIOUS_WEEK_3 = 'W' + str(int(WEEK_ID) - 3)
    PREVIOUS_WEEK_4 = 'W' + str(int(WEEK_ID) - 4)
    PREVIOUS_WEEK_5 = 'W' + str(int(WEEK_ID) - 5)

    PREVIOUS_WEEK_ID_1 = str(int(WEEK_ID) - 1)
    PREVIOUS_WEEK_ID_2 = str(int(WEEK_ID) - 2)
    PREVIOUS_WEEK_ID_3 = str(int(WEEK_ID) - 3)
    PREVIOUS_WEEK_ID_4 = str(int(WEEK_ID) - 4)
    PREVIOUS_WEEK_ID_5 = str(int(WEEK_ID) - 5)

    borg_level_sql_path = BASE_SQL_PATH + 'etv_ott_dg_regression/'

    month_id = str(csd.time_id_to_month_id(csd.get_last_time_id_in_week_id(int(WEEK_ID))))
    prev_month_id = str(csd.time_id_to_month_id(csd.get_last_time_id_in_week_id(int(WEEK_ID) - 1)))
    BROADCAST_MONTH_ID_A = str(
        csd.time_id_to_broadcast_month(csd.datetime_to_time_id((csd.get_first_day_in_gregorian_month(int(month_id))))))
    BROADCAST_MONTH_ID_B = str(
        csd.time_id_to_broadcast_month(csd.datetime_to_time_id(csd.get_last_day_in_gregorian_month(int(month_id)))))
    PPM_MONTH_ID = str(int(month_id) - 1)
    START_BC_TIMEID = str(csd.datetime_to_time_id(csd.get_first_day_in_gregorian_month(int(month_id))))

    if BROADCAST_MONTH_ID_A == month_id:
        FIRST_WEEK_MONTH = str(
            csd.time_id_to_week_id(csd.datetime_to_time_id(csd.get_first_day_in_gregorian_month(int(month_id)))))
    else:
        FIRST_WEEK_MONTH = str(
            int(csd.time_id_to_week_id(csd.datetime_to_time_id(csd.get_first_day_in_gregorian_month(int(month_id))))) + 1)

    if BROADCAST_MONTH_ID_B == month_id:
        LAST_WEEK_MONTH = str(
            csd.time_id_to_week_id(csd.datetime_to_time_id(csd.get_last_day_in_gregorian_month(int(month_id)))))
    else:
        LAST_WEEK_MONTH = str(
            int(csd.time_id_to_week_id(csd.datetime_to_time_id(csd.get_last_day_in_gregorian_month(int(month_id))))) - 1)

    DATA_EXPORT_AWS_SCRIPT_PATH = Variable.get(
        "data_export_aws_script_path")  # '/mapr/ri1.comscore.com/test_data/census/dags/SnowflakeSQL/DataCopy/dataExport_aws.sh'
    MOBILENS_S3_PATH = Variable.get(
        "mobilens_s3_path") + month_id + test_suffix  # "s3://csxpdev-xp-east/etv/etv-ecs/ott-etl/m" + month_id
    MOBILENS_PATH = Variable.get(
        "mobilens_smb_path") + month_id  # "/mnt/smb/cviadmfs01/mobile/MobilensAdvantage/Handoffs/OTT/m" + month_id
    base_s3_path = Variable.get("base_s3_loc") +"/"+ WEEK_ID + "w/"  # "s3://csxpdev-xp-east/etv/HDFS/"+WEEK_ID+"w/"
    s3_snowflake_staging_path = Variable.get("base_s3_path")  # "@public.hdfs"
    base_bin_path = Variable.get("emr_loc_base_path")  # '/home/hadoop/'
    pig_source_file_path = base_bin_path + 'ott_hh_pattern_agg.pig'  # '/home/hadoop/ott_hh_pattern_agg.pig'
    base_hadoop_path = Variable.get("emr_hadoop_base_path")  # '/user/hadoop/'
    s3_staging_path = Variable.get("base_s3_loc") + '/'  # 's3://csxpdev-xp-east/etv/HDFS/'
    windows_ecs_base_path = Variable.get("windows_ecs_base_path")#s3://csxpdev-xp-east/ott-etl/output/
    upstream_data_s3_path = Variable.get("base_s3_loc") + "/" + Variable.get(
        "upstream_path") +"/"+ WEEK_ID + "w/"  # 's3://csxpdev-xp-east/etv/HDFS/Data_Loads_Run/' + WEEK_ID +' w/'
    data_loads_path = Variable.get("base_s3_loc") + "/" + Variable.get("upstream_path") + "/"
    s3_threshold_cap_path = Variable.get("s3_threshold_cap_path")

    ETV_EXPANDED_DEMO_BREAKOUT_WEEK = 905

    week_ptr = WEEK_ID
    latte_table = 'etv_ott_latte_inclusive'
    latte_raw = 'etv_ott_latte_raw'
    table_name_part1 = 'weekly_latte'
    table_name_part3 = 'fullweek_latte'
    latte_output_table = 'etv_ott_final_venti_prod'
    latte_final = 'etv_ott_latte_inclusive_final'
    output_with_mrs = 'etv_ott_final_venti'

    if LAST_WEEK_MONTH == str(WEEK_ID):
        tvd_daily_duration_path = ','.join([s3_staging_path + str(x) + 'w/daily_duration_tvd_apply/finalclose/' + str(x) +'w/'
                                            for x in range(FIRST_WEEK_MONTH, LAST_WEEK_MONTH + 1)])
    else:
        tvd_daily_duration_path = base_s3_path + 'daily_duration_tvd_apply/finalclose/' + WEEK_ID + 'w/'

    if RUN_BATCH == True:

        # need to parameterise distinct_keywords_ file name
        copy_data_files_to_hadoop = SSHOperator(task_id='copy_data_files_to_hadoop',
                                                command='''hadoop fs -cp ''' + upstream_data_s3_path + '''cfd_''' + WEEK_ID + '''w_finalclose.ser.gz /user/hadoop &&
                                           hadoop fs -cp ''' + upstream_data_s3_path + '''cs_terms_in_domain_''' + WEEK_ID + '''w_finalclose.bcp.gz /user/hadoop &&
                                           hadoop fs -cp ''' + data_loads_path + '''monthly/'''+ month_id +'''m/distinct_keywords/distinct_keywords_'''+ month_id +'''m.txt /user/hadoop &&
                                           hadoop fs -cp ''' + data_loads_path + '''public/streaming_type_config/streaming_type_config.txt /user/hadoop && 
                                           hadoop fs -cp ''' + data_loads_path + '''public/abc_dedup/abcDedupConfig.txt /user/hadoop''',
                                                dag=dag,
                                                ssh_hook=sshHook)

        delete_ott_hh_pattern_agg = BashOperator(
            task_id='delete_ott_hh_pattern_agg'
            , bash_command='{}/deleteS3Object.sh '.format(UTILITY_PATH) +
                           '--s3Path {0}ott_hh_pattern_agg/output'.format(base_s3_path)
            , dag=dag
        )

        ott_hh_pattern_agg = SSHOperator(task_id='ott_hh_pattern_agg',
                                         command='pig -x mr'
                                                 ' -F -f ' + pig_source_file_path + ''
                                                                                    ' -p jobname=etv_ott_hh_pattern_agg_' + WEEK_ID + 'w'
                                                                                                                                      ' -p time_type=' + time_type + ''
                                                                                                                                                                     ' -p monthid=' + month_id + ''
                                                                                                                                                                                                 ' -p week_id=' + WEEK_ID + ''
                                                                                                                                                                                                                            ' -p first_week_of_the_month=' + FIRST_WEEK_MONTH + ''
                                                                                                                                                                                                                                                                                ' -p last_week_of_the_month=' + LAST_WEEK_MONTH + ''
                                                                                                                                                                                                                                                                                                                                  ' -p squeal_jar=' + base_bin_path + 'squeal.jar'
                                                                                                                                                                                                                                                                                                                                                                      ' -p beacon_sesh_pig_udf=' + base_bin_path + 'census-stream-sense-beacon-session-pig-udf.jar'
                                                                                                                                                                                                                                                                                                                                                                                                                   ' -p daily_duration_pig_udf=' + base_bin_path + 'census-stream-sense-daily-duration-pig-udf.jar'
                                                                                                                                                                                                                                                                                                                                                                                                                                                                   ' -p cfd_ser_file=' + base_hadoop_path + 'cfd_' + WEEK_ID + 'w_finalclose.ser.gz'
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               ' -p domain_cache=' + base_hadoop_path + 'cs_terms_in_domain_' + WEEK_ID + 'w_finalclose.bcp.gz'                                                                                       ' -p cs_utils_jar=' + base_bin_path + 'csutils.jar'
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ' -p cs_pig_utils_jar=' + base_bin_path + 'cs-pig-utils-udf.jar'
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      ' -p census_stream_sense_common_udf=' + base_bin_path + 'census-stream-sense-common-pig-udf.jar'
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ' -p distinct_keywords_file=' + base_hadoop_path + 'distinct_keywords_' + month_id + 'm.txt'
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   ' -p abc_dedupe_config_file=' + base_hadoop_path + 'abcDedupConfig.txt'
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      ' -p keywords_config=' + base_hadoop_path + 'streaming_type_config.txt'
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  ' -p day_part_transpose_py=' + base_bin_path + 'daypart_transpose.py'
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 ' -p daypart_lookup=' + data_loads_path + 'public/daypart_lookup.txt'
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ' -p tvd_daily_duration_path=' + tvd_daily_duration_path + ''
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      ' -p etv_ott_hh_pattern_agg_path=' + base_s3_path + 'ott_hh_pattern_agg/'+ file_test_suffix +'output',
                                         dag=dag,
                                         ssh_hook=sshHook)
        #delete_ott_hh_pattern_agg.set_upstream(copy_data_files_to_hadoop)
        #ott_hh_pattern_agg.set_upstream(delete_ott_hh_pattern_agg)

        create_cluster = emr.create_emr_cluster("ETV_OTT_DG_Regression", "29", "800")
        update_emr_connection = emr.update_airflow_emr_connection("ETV_OTT_DG_Regression",
                                                                  "etv_ott_dg_regression_connection")
        copy_binaries = emr.copy_binaries(sshHook)
        #delete_cluster = emr.delete_cluster("ETV_OTT_DG_Regression")

        create_cluster >> emr.sleep_operation() >> update_emr_connection >> copy_binaries
        copy_binaries >> copy_data_files_to_hadoop >> delete_ott_hh_pattern_agg >> ott_hh_pattern_agg #>> delete_cluster

        # depends on ott_hh_pattern_agg
        # exports data from s3 to snowflake table
        load_OTT_HH_pattern_agg = BashOperator(task_id='load_OTT_HH_pattern_agg',
                                               bash_command='snowsql --config ' + CONFIG_PATH + '' +
                                                            ' -f ' + borg_level_sql_path + 'load_ott_hh_pattern_agg.sql'
                                                                                           ' -D  WEEK_ID=' + WEEK_ID + ''
                                                                                                                       ' -D  WEEK_SCHEMA_0=' + Week + ''
                                                                                                                                                      ' -D  test_suffix=' + test_suffix + '',
                                               dag=dag
                                               )
        # depends on load_OTT_HH_pattern_agg
        ETV_OTT_HWE = BashOperator(task_id='ETV_OTT_HWE',
                                   bash_command='snowsql --config ' + CONFIG_PATH + '' +
                                                ' -f ' + borg_level_sql_path + 'ott_hwe.sql'
                                                                               ' -D  WEEK_SCHEMA_0=' + Week + '' +
                                                ' -D  WEEK_ID=' + WEEK_ID + ''
                                                                            ' -D  test_suffix=' + test_suffix + '',
                                   dag=dag
                                   )

        # depends on ETV_OTT_HWE
        push_static_PPM_schedulized = BashOperator(task_id='push_static_PPM_schedulized',
                                                   bash_command='snowsql --config ' + CONFIG_PATH + '' +
                                                                ' -f ' + borg_level_sql_path + 'export_ppm_schedulized.sql'
                                                                                               ' -D  WEEK_SCHEMA_5=' + PREVIOUS_WEEK_5 + '' +
                                                                ' -D  WEEK_SCHEMA_4=' + PREVIOUS_WEEK_4 + '' +
                                                                ' -D  WEEK_SCHEMA_3=' + PREVIOUS_WEEK_3 + '' +
                                                                ' -D  WEEK_SCHEMA_2=' + PREVIOUS_WEEK_2 + '' +
                                                                ' -D  WEEK_SCHEMA_1=' + PREVIOUS_WEEK_1 + '' +
                                                                ' -D  WEEK_SCHEMA_0=' + Week + '' +
                                                                ' -D  WEEK_5=' + PREVIOUS_WEEK_ID_5 + '' +
                                                                ' -D  WEEK_4=' + PREVIOUS_WEEK_ID_4 + '' +
                                                                ' -D  WEEK_3=' + PREVIOUS_WEEK_ID_3 + '' +
                                                                ' -D  WEEK_2=' + PREVIOUS_WEEK_ID_2 + '' +
                                                                ' -D  WEEK_1=' + PREVIOUS_WEEK_ID_1 + '' +
                                                                ' -D  WEEK_0=' + WEEK_ID + ''
                                                                                           ' -D  test_suffix=' + test_suffix + '',
                                                   dag=dag
                                                   )

        # depends on push_static_PPM_schedulized
        push_acxiom_gender_age_lookup = BashOperator(task_id='push_acxiom_gender_age_lookup',
                                                     bash_command='snowsql --config ' + CONFIG_PATH + '' +
                                                                  ' -f ' + borg_level_sql_path + 'export_acxiom_gender_age_lookup.sql'
                                                                                                 ' -D  MONTH_ID=' + PPM_MONTH_ID + '',
                                                     dag=dag
                                                     )

        # depends on push_static_PPM_schedulized
        PPM_PWE = BashOperator(task_id='PPM_PWE',
                               bash_command='snowsql --config ' + CONFIG_PATH + '' +
                                            ' -f ' + borg_level_sql_path + 'ppm_pwe.sql' +
                                            ' -D  WEEK_SCHEMA_5=' + PREVIOUS_WEEK_5 + '' +
                                            ' -D  WEEK_SCHEMA_4=' + PREVIOUS_WEEK_4 + '' +
                                            ' -D  WEEK_SCHEMA_3=' + PREVIOUS_WEEK_3 + '' +
                                            ' -D  WEEK_SCHEMA_2=' + PREVIOUS_WEEK_2 + '' +
                                            ' -D  WEEK_SCHEMA_1=' + PREVIOUS_WEEK_1 + '' +
                                            ' -D  WEEK_SCHEMA_0=' + Week + '' +
                                            ' -D  WEEK_ID=' + WEEK_ID + '' +
                                            ' -D  start_timeId_of_bc_month=' + START_BC_TIMEID + ''
                                                                                                 ' -D  test_suffix=' + test_suffix + '',
                               dag=dag
                               )

        # depends on PPM_PWE
        PPM_training_data = BashOperator(task_id='PPM_training_data',
                                         bash_command='snowsql --config ' + CONFIG_PATH + '' +
                                                      ' -f ' + borg_level_sql_path + 'ppm_training_data.sql' +
                                                      ' -D  WEEK_SCHEMA_0=' + Week + '' +
                                                      ' -D  WEEK_ID=' + WEEK_ID + ''
                                                                                  ' -D  test_suffix=' + test_suffix + '',
                                         dag=dag
                                         )

        # export ppm_training data from snowflake to S3 (WIP)
        export_ppm_training_data_to_s3 = BashOperator(task_id="Export_PPM_Data_From_Snowflake_To_S3",
                                                      bash_command='snowsql --config ' + CONFIG_PATH + '' +
                                                                   ' -f ' + BASE_SQL_PATH + 'exportData.sql' +
                                                                   ' -D  s3Path=' + s3_snowflake_staging_path + '/' + WEEK_ID + 'w/etv_ott_coffee_filter/output/ppm_training_data' +test_suffix +'.csv' +
                                                                   ' -D sourceTable=' + Week + '.hh_mins_by_genre_encode'+ test_suffix+ ' ' +
                                                                   ' -D outputFormat=my_csv_format_uncompressed ' +
                                                                   ' -D exportType=True' +
                                                                   ' -D withHeader=False',
                                                      dag=dag)

        delete_ott_coffee_filter_wrapper = BashOperator(
            task_id='delete_ott_coffee_filter_wrapper'
            , bash_command='{}/deleteS3Object.sh '.format(UTILITY_PATH) +
                           '--s3Path {0}coffee_filter_model/{1}'.format(base_s3_path,test_suffix)
            , dag=dag
        )

        # depends on PPM_training_data (WIP)
        # R script operation
        ott_coffee_filter_wrapper = ECSOperator(
            task_definition="ott_coffee_filter_task",
            launch_type="FARGATE",
            cluster="Fargate-Cluster",
            network_configuration={
                'awsvpcConfiguration': {
                    'subnets': [
                        'subnet-0d6d29c0d1dfe14f1',
                    ],
                    'securityGroups': [
                        'sg-02b1cb2a987877ff1',
                    ],
                    'assignPublicIp': 'ENABLED'
                }
            },
            overrides={
                'containerOverrides': [
                    {
                        'name': 'ott_coffee_filter',
                        'command': [
                            '-s3InputFilePath', base_s3_path + 'etv_ott_coffee_filter/output/ppm_training_data' +test_suffix +'.csv',
                            '-s3OutputFilePath', base_s3_path + 'coffee_filter_model/' +file_test_suffix +'output/beta_logit_model.csv',
                            '-s3OutputHTMLlogFilePath', base_s3_path + 'coffee_filter_model/' +file_test_suffix +'output/logs.html'
                        ]
                    }
                ]
            },
            task_id="run_ott_coffee_filter",
            dag=dag)

        ott_coffee_filter_wrapper.set_upstream(delete_ott_coffee_filter_wrapper)

        # export espresso live regression data from S3 to Snowflake (WIP)
        Export_Espresso_Live_Regression_S3_To_SnowSQL = BashOperator(
            task_id='Export_beta_logit_model_S3_To_SnowSQL',
            bash_command='snowsql --config ' + CONFIG_PATH + ''
                         + ' -f ' + borg_level_sql_path + 'load_beta_logit_model_S3_to_snowflake.sql'
                         + ' -D  WEEK_SCHEMA_0=' + Week + ''
                         + ' -D  WEEK_ID=' + WEEK_ID + ''
                                                       ' -D  test_suffix=' + test_suffix + ''
            , dag=dag)

        # depends on R script operation(ott_coffee_filter_wrapper)
        ott_coffee_filter_apply = BashOperator(task_id='ott_coffee_filter_apply',
                                               bash_command='snowsql --config ' + CONFIG_PATH + '' +
                                                            ' -f ' + borg_level_sql_path + 'ott_coffee_filter_apply.sql' +
                                                            ' -D  WEEK_SCHEMA_0=' + Week + '' +
                                                            ' -D  WEEK_ID=' + WEEK_ID + ''
                                                                                        ' -D  test_suffix=' + test_suffix + '',
                                               dag=dag
                                               )
        delete_sync_mobilens = BashOperator(
            task_id='delete_sync_mobilens'
            , bash_command='{}/deleteS3Object.sh '.format(UTILITY_PATH) +
                           '--s3Path {0}{1}'.format(MOBILENS_S3_PATH,test_suffix)
            , dag=dag
        )

        sync_mobilens = BashOperator(
            task_id="sync_mobilens",
            bash_command=(
                    "if [ ! -d " + MOBILENS_PATH + " ]; then echo '" + MOBILENS_PATH + " does not exist!'; exit 1; fi && " +
                    "aws s3 sync " + MOBILENS_PATH + " " + MOBILENS_S3_PATH + test_suffix
            ),
            dag=dag
        )
        sync_mobilens.set_upstream(delete_sync_mobilens)

        delete_generate_target_and_population_estimate = BashOperator(
            task_id='delete_generate_target_and_population_estimate'
            , bash_command='{}/deleteS3Object.sh '.format(UTILITY_PATH) +
                           '--s3Path {0}{1}{2}w'.format(windows_ecs_base_path,file_test_suffix,WEEK_ID)
            , dag=dag
        )

        # depends on ott_coffee_filter_apply
        if WEEK_ID >= ETV_EXPANDED_DEMO_BREAKOUT_WEEK:
            generate_target_and_population_estimate = ECSOperator(
                task_definition="ott-etl-linux_task",
                launch_type="FARGATE",
                cluster="Fargate-Cluster",
                network_configuration={
                    'awsvpcConfiguration': {
                        'subnets': [
                            'subnet-0d6d29c0d1dfe14f1',
                        ],
                        'securityGroups': [
                            'sg-02b1cb2a987877ff1',
                        ],
                        'assignPublicIp': 'ENABLED'
                    }
                },
                overrides={
                    "containerOverrides": [
                        {
                            "name": "ott-etl-linux",
                            "command": [
                                "-d", MOBILENS_S3_PATH + "/ott_demo_targets_" + month_id + "m_US_v3.txt",
                                "-p", MOBILENS_S3_PATH + "/ott_population_targets_" + month_id + "m_US_v3.txt",
                                "-m", month_id,
                                "-o", "s3://csxpdev-xp-east/ott-etl/output/" + WEEK_ID + "w"
                            ]
                        }
                    ]
                },
                task_id="run_ott_etl",
                dag=dag
            )
            #delete_generate_target_and_population_estimate.set_upstream([sync_mobilens,ott_coffee_filter_apply])
            generate_target_and_population_estimate.set_upstream(delete_generate_target_and_population_estimate)

        # depends on ott_coffee_filter_apply
        if WEEK_ID < ETV_EXPANDED_DEMO_BREAKOUT_WEEK:
            generate_target_and_population_estimate = DummyOperator(task_id='dummy2', dag=dag)
            generate_target_and_population_estimate.set_upstream(delete_generate_target_and_population_estimate)

        delete_generate_experian_target_and_population_estimate = BashOperator(
            task_id='delete_generate_experian_target_and_population_estimate'
            , bash_command='{}/deleteS3Object.sh '.format(UTILITY_PATH) +
                           '--s3Path {0}{1}experian/{2}w'.format(windows_ecs_base_path,file_test_suffix,WEEK_ID)
            , dag=dag
        )

        # depends on ott_coffee_filter_apply
        if WEEK_ID >= ETV_EXPANDED_DEMO_BREAKOUT_WEEK:
            #            generate_experian_target_and_population_estimate = ECSOperator(
            #                task_definition="ott-etl-linux_task",
            #                launch_type="EC2",
            #                cluster="Windows-ECS",
            #                overrides={
            #                    "containerOverrides": [
            #                        {
            #                            "name": "ott-etl-linux",
            #                            "command": [
            #                                MOBILENS_S3_PATH + "/ott_demo_targets_" + month_id + "m_US_v3.txt",
            #                                MOBILENS_S3_PATH + "/ott_population_targets_" + month_id + "m_US_v3.txt",
            #                                month_id,
            #                                windows_ecs_base_path + file_test_suffix + "experian/" + WEEK_ID + "w"
            #                                # "s3://csxpdev-xp-east/ott-etl/output/experian/" + WEEK_ID + "w"
            #                            ]
            #                        }
            #                    ]
            #                },
            #                task_id="run_ott_etl_experian",
            #                dag=dag
            #            )
            generate_experian_target_and_population_estimate = ECSOperator(
                task_definition="ott-etl-linux_task",
                launch_type="FARGATE",
                cluster="Fargate-Cluster",
                network_configuration={
                    'awsvpcConfiguration': {
                        'subnets': [
                            'subnet-0d6d29c0d1dfe14f1',
                        ],
                        'securityGroups': [
                            'sg-02b1cb2a987877ff1',
                        ],
                        'assignPublicIp': 'ENABLED'
                    }
                },
                overrides={
                    "containerOverrides": [
                        {
                            "name": "ott-etl-linux",
                            "command": [
                                "-d",MOBILENS_S3_PATH + "/ott_demo_targets_" + month_id + "m_US_v3.txt",
                                "-p",MOBILENS_S3_PATH + "/ott_population_targets_" + month_id + "m_US_v3.txt",
                                "-m",month_id,
                                "-o",windows_ecs_base_path + file_test_suffix + "experian/" + WEEK_ID + "w"
                                # "s3://csxpdev-xp-east/ott-etl/output/experian/" + WEEK_ID + "w"
                            ]
                        }
                    ]
                },
                task_id="run_ott_etl_experian",
                dag=dag
            )
            #delete_generate_experian_target_and_population_estimate.set_upstream([sync_mobilens,ott_coffee_filter_apply])
            generate_experian_target_and_population_estimate.set_upstream(
                delete_generate_experian_target_and_population_estimate)

        # depends on ott_coffee_filter_apply
        if WEEK_ID < ETV_EXPANDED_DEMO_BREAKOUT_WEEK:
            # .exe file execution 4
            generate_experian_target_and_population_estimate = DummyOperator(task_id='dummy4', dag=dag)
            generate_experian_target_and_population_estimate.set_upstream(
                delete_generate_experian_target_and_population_estimate)

        # depends on ott_coffee_filter_apply
        generate_sample_for_week = BashOperator(task_id='generate_sample_for_week',
                                                bash_command='snowsql --config ' + CONFIG_PATH + '' +
                                                             ' -f ' + borg_level_sql_path + 'ott_cbp_weights_pwe_handoff.sql' +
                                                             ' -D  WEEK_SCHEMA_0=' + Week +
                                                             ' -D  WEEK_ID=' + WEEK_ID + ''
                                                                                         ' -D  test_suffix=' + test_suffix + '',
                                                dag=dag
                                                )

        # depends on generate_sample_for_week
        generate_experian_sample_for_week = BashOperator(task_id='generate_experian_sample_for_week',
                                                         bash_command='snowsql --config ' + CONFIG_PATH + '' +
                                                                      ' -f ' + borg_level_sql_path + 'ott_experian_weights_pwe_handoff.sql' +
                                                                      ' -D  WEEK_SCHEMA_0=' + Week + '' +
                                                                      ' -D  WEEK_ID=' + WEEK_ID + ''
                                                                                                  ' -D  test_suffix=' + test_suffix + '',
                                                         dag=dag
                                                         )
        delete_export_cbp_weights_pwe_handoff_to_s3 = BashOperator(
            task_id='delete_export_cbp_weights_pwe_handoff_to_s3'
            , bash_command='{}/deleteS3Object.sh '.format(UTILITY_PATH) +
                           '--s3Path {0}generate_sample_for_week/{1}output'.format(base_s3_path, file_test_suffix)
            , dag=dag
        )

        # depends on generate_sample_for_week
        export_cbp_weights_pwe_handoff_to_s3 = BashOperator(task_id="export_cbp_weights_pwe_handoff_to_s3",
                                                            bash_command='snowsql --config ' + CONFIG_PATH + '' +
                                                                         ' -f ' + borg_level_sql_path + 'exportSampleData.sql' +
                                                                         ' -D  s3Path=' + s3_snowflake_staging_path + '/' + WEEK_ID + 'w/generate_sample_for_week/'+ file_test_suffix +'output/cbp_weights_pwe_handoff.csv' +
                                                                         ' -D sourceTable=' + Week + '.cbp_weights_handoff_output'+ test_suffix+ ' ' +
                                                                         ' -D outputFormat=csv_format_comma_separated_uncompressed ' +
                                                                         ' -D exportType=True' +
                                                                         ' -D withHeader=True',
                                                            dag=dag)
        #delete_export_cbp_weights_pwe_handoff_to_s3.set_upstream([sync_mobilens,ott_coffee_filter_apply])
        export_cbp_weights_pwe_handoff_to_s3.set_upstream(delete_export_cbp_weights_pwe_handoff_to_s3)

        delete_weights_run = BashOperator(
            task_id='delete_weights_run'
            , bash_command='{}/deleteS3Object.sh '.format(UTILITY_PATH) +
                           '--s3Path {0}weights_run/{1}output'.format(base_s3_path, file_test_suffix)
            , dag=dag
        )

        # depends on generate_target_and_population_estimate,export_cbp_weights_pwe_handoff_to_s3
        # .exe task
        weights_run = ECSOperator(
            task_id="weights_run",
            task_definition="weightssystem-linux_task",
            launch_type="FARGATE",
            cluster="Fargate-Cluster",
            network_configuration={
                'awsvpcConfiguration': {
                    'subnets': [
                        'subnet-0d6d29c0d1dfe14f1',
                    ],
                    'securityGroups': [
                        'sg-02b1cb2a987877ff1',
                    ],
                    'assignPublicIp': 'ENABLED'
                }
            },
            overrides={
                'containerOverrides': [
                    {
                        'name': 'weightssystem-linux',
                        'command': [
                            '-s3_sample_path', base_s3_path+'generate_sample_for_week/output/cbp_weights_pwe_handoff.csv',
                            '-s3_strata_info_path','s3://csxpdev-xp-east/ott-etl/output/' + WEEK_ID + 'w/us_uverse_OTT_targets_acxiom_'+month_id+'m.csv',
                            '-s3_population_estimates_path','s3://csxpdev-xp-east/ott-etl/output/' + WEEK_ID + 'w/population_estimates_us_uverse_OTT_'+month_id+'m.csv',
                            '-s3_threshold_cap_path','s3://csxpdev-xp-east/etv/etv-ecs/weights/iss_parms_gsma_weights.csv',
                            '-s3_output_folder',base_s3_path+ 'weights_run/output',
                            '-country_num','840',
                            '-location','All_loc',
                            '-email_address','pvaldes@comscore.com',
                            '-max_iterations','100',
                            '-max_threads','100',
                            '-weights_output_name','weights_ott_'+month_id+'m',
                            '-weights_distribution_output_name','us_weights_intensity_dist_att_uverse_'+month_id+'m',
                            '-weights_info_name','us_weights_info_intensity_att_uverse_'+month_id+'m',
                            '-weights_log_name','us_logs_intensity_att_uverse_'+month_id+'m'
                        ]
                    }
                ]
            },
            dag=dag
        )

        weights_run.set_upstream(delete_weights_run)

        delete_export_cbp_experian_weights_pwe_handoff_to_s3 = BashOperator(
            task_id='delete_export_cbp_experian_weights_pwe_handoff_to_s3'
            , bash_command='{}/deleteS3Object.sh '.format(UTILITY_PATH) +
                           '--s3Path {0}generate_experian_sample_for_week/{1}output'.format(base_s3_path, file_test_suffix)
            , dag=dag
        )

        # depends on generate_experian_sample_for_week
        export_cbp_experian_weights_pwe_handoff_to_s3 = BashOperator(
            task_id="export_cbp_experian_weights_pwe_handoff_to_s3",
            bash_command='snowsql --config ' + CONFIG_PATH + '' +
                         ' -f ' + borg_level_sql_path + 'exportSampleData.sql' +
                         ' -D  s3Path=' + s3_snowflake_staging_path + '/' + WEEK_ID + 'w/generate_experian_sample_for_week/'+ file_test_suffix +'output/cbp_experian_weights_pwe_handoff.csv' +
                         ' -D sourceTable=' + Week + '.experian_handoff_output'+ test_suffix+ ' ' +
                         ' -D outputFormat=csv_format_comma_separated_uncompressed ' +
                         ' -D exportType=True' +
                         ' -D withHeader=True',
            dag=dag)

        #delete_export_cbp_experian_weights_pwe_handoff_to_s3.set_upstream(generate_experian_sample_for_week)
        export_cbp_experian_weights_pwe_handoff_to_s3.set_upstream(delete_export_cbp_experian_weights_pwe_handoff_to_s3)

        delete_experian_weights_run = BashOperator(
            task_id='delete_experian_weights_run'
            , bash_command='{}/deleteS3Object.sh '.format(UTILITY_PATH) +
                           '--s3Path {0}experian_weights_run/{1}output'.format(base_s3_path, file_test_suffix)
            , dag=dag
        )
        # depends on generate_experian_target_and_population_estimate,export_cbp_experian_weights_pwe_handoff_to_s3
        # .exe task
        experian_weights_run = ECSOperator(
            task_id="experian_weights_run",
            task_definition="weightssystem-linux_task",
            launch_type="FARGATE",
            cluster="Fargate-Cluster",
            network_configuration={
                'awsvpcConfiguration': {
                    'subnets': [
                        'subnet-0d6d29c0d1dfe14f1',
                    ],
                    'securityGroups': [
                        'sg-02b1cb2a987877ff1',
                    ],
                    'assignPublicIp': 'ENABLED'
                }
            },
            overrides={
                'containerOverrides': [
                    {
                        'name': 'weightssystem-linux',
                        'command': [
                            '-s3_sample_path', base_s3_path+'generate_experian_sample_for_week/output/cbp_experian_weights_pwe_handoff.csv',
                            '-s3_strata_info_path','s3://csxpdev-xp-east/ott-etl/output/experian/' + WEEK_ID + 'w/us_uverse_OTT_targets_acxiom_'+month_id+'m.csv',
                            '-s3_population_estimates_path','s3://csxpdev-xp-east/ott-etl/output/experian/' + WEEK_ID + 'w/population_estimates_us_uverse_OTT_'+month_id+'m.csv',
                            '-s3_threshold_cap_path','s3://csxpdev-xp-east/etv/etv-ecs/iss_parms_gsma_weights.csv',
                            '-s3_output_folder','s3://csxpdev-xp-east/etv/HDFS/' + WEEK_ID + 'w/experian_weights_run/output',
                            '-country_num','840',
                            '-location','All_loc',
                            '-email_address','pvaldes@comscore.com',
                            '-max_iterations','100',
                            '-max_threads','100',
                            '-weights_output_name','weights_ott_'+month_id+'m',
                            '-weights_distribution_output_name','us_weights_intensity_dist_att_uverse_'+month_id+'m',
                            '-weights_info_name','us_weights_info_intensity_att_uverse_'+month_id+'m',
                            '-weights_log_name','us_logs_intensity_att_uverse_'+month_id+'m'
                        ]
                    }
                ]
            },
            dag=dag
        )

        #delete_experian_weights_run.set_upstream([generate_experian_target_and_population_estimate,export_cbp_experian_weights_pwe_handoff_to_s3])
        experian_weights_run.set_upstream(delete_experian_weights_run)

        #   depends on experian_weights_run and weights_run
        OTT_PPM_PWE = BashOperator(task_id='OTT_PPM_PWE',
                                   bash_command='snowsql --config ' + CONFIG_PATH + '' +
                                                ' -f ' + borg_level_sql_path + 'ott_ppm_pwe.sql' +
                                                ' -D  WEEK_SCHEMA_0=' + Week + '' +
                                                ' -D  WEEK_SCHEMA_5=' + PREVIOUS_WEEK_5 + '' +
                                                ' -D  WEEK_SCHEMA_4=' + PREVIOUS_WEEK_4 + '' +
                                                ' -D  WEEK_SCHEMA_3=' + PREVIOUS_WEEK_3 + '' +
                                                ' -D  WEEK_SCHEMA_2=' + PREVIOUS_WEEK_2 + '' +
                                                ' -D  WEEK_SCHEMA_1=' + PREVIOUS_WEEK_1 + '' +
                                                ' -D  WEEK_5=' + PREVIOUS_WEEK_ID_5 + '' +
                                                ' -D  WEEK_4=' + PREVIOUS_WEEK_ID_4 + '' +
                                                ' -D  WEEK_3=' + PREVIOUS_WEEK_ID_3 + '' +
                                                ' -D  WEEK_2=' + PREVIOUS_WEEK_ID_2 + '' +
                                                ' -D  WEEK_1=' + PREVIOUS_WEEK_ID_1 + '' +
                                                ' -D  WEEK_0=' + WEEK_ID + '' +
                                                ' -D  MONTH_ID=' + month_id + ''
                                                                              ' -D  test_suffix=' + test_suffix + '',
                                   dag=dag
                                   )

        #   depends on  OTT_PPM_PWE
        OTT_espresso = BashOperator(task_id='OTT_espresso',
                                    bash_command='snowsql --config ' + CONFIG_PATH + '' +
                                                 ' -f ' + borg_level_sql_path + '11_regular_espresso.sql' +
                                                 ' -D  WEEK_SCHEMA_0=' + Week + '' +
                                                 ' -D  WEEK_ID=' + WEEK_ID + '' +
                                                 ' -D  MONTH_ID=' + month_id + ''
                                                                               ' -D  test_suffix=' + test_suffix + '',
                                    dag=dag
                                    )

        # depends on OTT_espresso
        OTT_regression_espresso = BashOperator(task_id='OTT_regression_espresso',
                                               bash_command='snowsql --config ' + CONFIG_PATH + '' +
                                                            ' -f ' + borg_level_sql_path + '12_regression_espresso.sql' +
                                                            ' -D  WEEK_SCHEMA_0=' + Week + '' +
                                                            ' -D  WEEK_ID=' + WEEK_ID + '' +
                                                            ' -D  MONTH_ID=' + month_id + ''
                                                                                          ' -D  test_suffix=' + test_suffix + '',
                                               dag=dag
                                               )

        # depends on OTT_regression_espresso
        truncate_dictionary_for_processing = BashOperator(task_id='truncate_dictionary_for_processing',
                                                          bash_command='snowsql --config ' + CONFIG_PATH + '' +
                                                                       ' -f ' + borg_level_sql_path + 'OTT_subset_espresso.sql' +
                                                                       ' -D  WEEK_SCHEMA_0=' + Week + '' +
                                                                       ' -D  WEEK_ID=' + WEEK_ID + ''
                                                                                                   ' -D  test_suffix=' + test_suffix + '',
                                                          dag=dag
                                                          )

        # depends on truncate_dictionary_for_processing
        OTT_espresso_agg = BashOperator(task_id='OTT_espresso_agg',
                                        bash_command='snowsql --config ' + CONFIG_PATH + '' +
                                                     ' -f ' + borg_level_sql_path + 'espresso_agg_max_prob.sql' +
                                                     ' -D  WEEK_SCHEMA_0=' + Week + '' +
                                                     ' -D  WEEK_SCHEMA_5=' + PREVIOUS_WEEK_5 + '' +
                                                     ' -D  WEEK_SCHEMA_4=' + PREVIOUS_WEEK_4 + '' +
                                                     ' -D  WEEK_SCHEMA_3=' + PREVIOUS_WEEK_3 + '' +
                                                     ' -D  WEEK_SCHEMA_2=' + PREVIOUS_WEEK_2 + '' +
                                                     ' -D  WEEK_SCHEMA_1=' + PREVIOUS_WEEK_1 + '' +
                                                     ' -D  WEEK_5=' + PREVIOUS_WEEK_ID_5 + '' +
                                                     ' -D  WEEK_4=' + PREVIOUS_WEEK_ID_4 + '' +
                                                     ' -D  WEEK_3=' + PREVIOUS_WEEK_ID_3 + '' +
                                                     ' -D  WEEK_2=' + PREVIOUS_WEEK_ID_2 + '' +
                                                     ' -D  WEEK_1=' + PREVIOUS_WEEK_ID_1 + '' +
                                                     ' -D  WEEK_0=' + WEEK_ID + '' +
                                                     ' -D  MONTH_1=' + str(int(month_id) - 1) + '' +
                                                     ' -D  MONTH_0=' + month_id + ''
                                                                                  ' -D  test_suffix=' + test_suffix + '',
                                        dag=dag
                                        )

        # depends on OTT_espresso_agg
        OTT_hh_web_agg = BashOperator(task_id='OTT_hh_web_agg',
                                      bash_command='snowsql --config ' + CONFIG_PATH + '' +
                                                   ' -f ' + borg_level_sql_path + 'etv_ott_hhwa.sql' +
                                                   ' -D  WEEK_SCHEMA_0=' + Week + '' +
                                                   ' -D  WEEK_ID=' + WEEK_ID + ''
                                                                               ' -D  test_suffix=' + test_suffix + '',
                                      dag=dag
                                      )
        delete_hh_generate_target_and_population = BashOperator(
            task_id='delete_hh_generate_target_and_population'
            , bash_command='{}/deleteS3Object.sh '.format(UTILITY_PATH) +
                           '--s3Path {0}hh_weights/{1}output'.format(base_s3_path, file_test_suffix)
            , dag=dag
        )

        # depends on OTT_hh_web_agg
        # java task 1
        hh_generate_target_and_population = ECSOperator(
            task_id="hh_generate_target_and_population",
            task_definition="weights-enumeration-file-generator_task",
            launch_type="FARGATE",
            cluster="Fargate-Cluster",
            network_configuration={
                'awsvpcConfiguration': {
                    'subnets': [
                        'subnet-0d6d29c0d1dfe14f1',
                    ],
                    'securityGroups': [
                        'sg-02b1cb2a987877ff1',
                    ],
                    'assignPublicIp': 'ENABLED'
                }
            },
            overrides={
                'containerOverrides': [
                    {
                        'name': 'weights-enumeration-file-generator',
                        'command': [
                            'weights-enumeration-file-generator-hh-2.0.py',
                            'java',
                            '-Xmx4g',
                            '-jar',
                            'weights-enumeration-file-generator-hh-2.0.jar',
                            '-m ' + month_id,
                            '-l hh',
                            '-d ' + MOBILENS_S3_PATH + '/ott_demo_targets_' + month_id + 'm_US_hh.txt',
                            '-p ' + MOBILENS_S3_PATH + '/ott_population_targets_' + month_id + 'm_US_hh.txt',
                            '-e ' + base_s3_path + 'hh_weights/'+ file_test_suffix +'output/population_estimates_hh_us_uverse_OTT_' + month_id + 'm.csv',
                            '-t ' + base_s3_path + 'hh_weights/'+file_test_suffix +'output/us_uverse_hh_OTT_targets_acxiom_' + month_id + 'm.csv'
                        ]
                    }
                ]
            },

            dag=dag)
        #delete_hh_generate_target_and_population.set_upstream(OTT_hh_web_agg)
        hh_generate_target_and_population.set_upstream(delete_hh_generate_target_and_population)
        # depends on hh_generate_target_and_population(java task1)
        generate_hh_sample = BashOperator(task_id='generate_hh_sample',
                                          bash_command='snowsql --config ' + CONFIG_PATH + '' +
                                                       ' -f ' + borg_level_sql_path + 'ott_cbp_weights_pwe_handoff_hh.sql' +
                                                       ' -D  WEEK_SCHEMA_0=' + Week + '' +
                                                       ' -D  WEEK_ID=' + WEEK_ID + ''
                                                                                   ' -D  test_suffix=' + test_suffix + '',
                                          dag=dag
                                          )
        delete_export_cbp_weights_hh_pwe_handoff_to_s3 = BashOperator(
            task_id='delete_export_cbp_weights_hh_pwe_handoff_to_s3'
            , bash_command='{}/deleteS3Object.sh '.format(UTILITY_PATH) +
                           '--s3Path {0}generate_hh_sample_for_week/{1}output'.format(base_s3_path, file_test_suffix)
            , dag=dag
        )
        # depends on generate_hh_sample
        export_cbp_weights_hh_pwe_handoff_to_s3 = BashOperator(task_id="export_cbp_weights_hh_pwe_handoff_to_s3",
                                                               bash_command='snowsql --config ' + CONFIG_PATH + '' +
                                                                            ' -f ' + borg_level_sql_path + 'exportHHSampleData.sql' +
                                                                            ' -D  s3Path=' + s3_snowflake_staging_path + '/' + WEEK_ID + 'w/generate_hh_sample_for_week/'+ file_test_suffix +'output/cbp_weights_hh_pwe_handoff.csv' +
                                                                            ' -D sourceTable=' + Week + '.cbp_weights_hh_handoff_output'+test_suffix+' ' +
                                                                            ' -D outputFormat=csv_format_comma_separated_uncompressed ' +
                                                                            ' -D exportType=True' +
                                                                            ' -D withHeader=True',
                                                               dag=dag)
        #delete_export_cbp_weights_hh_pwe_handoff_to_s3.set_upstream(generate_hh_sample)
        export_cbp_weights_hh_pwe_handoff_to_s3.set_upstream(delete_export_cbp_weights_hh_pwe_handoff_to_s3)

        delete_hh_weights_run = BashOperator(
            task_id='delete_hh_weights_run'
            , bash_command='{}/deleteS3Object.sh '.format(UTILITY_PATH) +
                           '--s3Path {0}hh_weights_run/{1}output'.format(base_s3_path, file_test_suffix)
            , dag=dag
        )

        # depends on hh_generate_target_and_population,export_cbp_weights_hh_pwe_handoff_to_s3
        # .exe script
        hh_weights_run = ECSOperator(
            task_id="hh_weights_run",
            task_definition="weightssystem-linux_task",
            launch_type="FARGATE",
            cluster="Fargate-Cluster",
            network_configuration={
                'awsvpcConfiguration': {
                    'subnets': [
                        'subnet-0d6d29c0d1dfe14f1',
                    ],
                    'securityGroups': [
                        'sg-02b1cb2a987877ff1',
                    ],
                    'assignPublicIp': 'ENABLED'
                }
            },
            overrides={
                'containerOverrides': [
                    {
                        'name': 'weightssystem-linux',
                        'command': [
                            '-s3_sample_path', base_s3_path+'generate_hh_sample_for_week/output/cbp_weights_hh_pwe_handoff.csv',
                            '-s3_strata_info_path', base_s3_path+'hh_weights/output/us_uverse_hh_OTT_targets_acxiom_'+month_id+'m.csv',
                            '-s3_population_estimates_path', base_s3_path+'hh_weights/output/population_estimates_hh_us_uverse_OTT_'+month_id+'m.csv',
                            '-s3_threshold_cap_path','s3://csxpdev-xp-east/etv/etv-ecs/iss_parms_gsma_weights.csv',
                            '-s3_output_folder', base_s3_path+'hh_weights_run/output',
                            '-country_num','840',
                            '-level','household',
                            '-sample_key_columns','country_num,location,machine_id',
                            '-location','All_loc',
                            '-email_address','mrudra@comscore.com',
                            '-max_iterations','100',
                            '-max_threads','100',
                            '-weights_output_name','weights_ott_'+month_id+'m',
                            '-weights_distribution_output_name','us_weights_intensity_dist_att_uverse_'+month_id+'m',
                            '-weights_info_name','us_weights_info_intensity_att_uverse_'+month_id+'m',
                            '-weights_log_name','us_logs_intensity_att_uverse_'+month_id+'m'
                        ]
                    }
                ]
            },
            dag=dag
        )

        #delete_hh_weights_run.set_upstream([hh_generate_target_and_population,export_cbp_weights_hh_pwe_handoff_to_s3])
        hh_weights_run.set_upstream(delete_hh_weights_run)

        # depends on    hh_weights_run and experian_weights_run
        hh_truncate_dictionary_for_processing = BashOperator(task_id='hh_truncate_dictionary_for_processing',
                                                             bash_command='snowsql --config ' + CONFIG_PATH + '' +
                                                                          ' -f ' + borg_level_sql_path + 'OTT_subset_hh_web_agg.sql' +
                                                                          ' -D  WEEK_SCHEMA_0=' + Week + '' +
                                                                          ' -D  WEEK_ID=' + WEEK_ID + ''
                                                                                                      ' -D  test_suffix=' + test_suffix + '',
                                                             dag=dag
                                                             )

        # depends on hh_weights_run, weights_run, hh_truncate_dictionary_for_processing    and OTT_hh_web_agg
        hh_ETV_OTT_latte = BashOperator(task_id='hh_ETV_OTT_latte',
                                        bash_command='snowsql --config ' + CONFIG_PATH + '' +
                                                     ' -f ' + borg_level_sql_path + 'ott_etv_latte_with_hh_weights.sql' +
                                                     ' -D  WEEK_SCHEMA_0=' + Week + '' +
                                                     ' -D  WEEK_SCHEMA_5=' + PREVIOUS_WEEK_5 + '' +
                                                     ' -D  WEEK_SCHEMA_4=' + PREVIOUS_WEEK_4 + '' +
                                                     ' -D  WEEK_SCHEMA_3=' + PREVIOUS_WEEK_3 + '' +
                                                     ' -D  WEEK_SCHEMA_2=' + PREVIOUS_WEEK_2 + '' +
                                                     ' -D  WEEK_SCHEMA_1=' + PREVIOUS_WEEK_1 + '' +
                                                     ' -D  WEEK_5=' + PREVIOUS_WEEK_ID_5 + '' +
                                                     ' -D  WEEK_4=' + PREVIOUS_WEEK_ID_4 + '' +
                                                     ' -D  WEEK_3=' + PREVIOUS_WEEK_ID_3 + '' +
                                                     ' -D  WEEK_2=' + PREVIOUS_WEEK_ID_2 + '' +
                                                     ' -D  WEEK_1=' + PREVIOUS_WEEK_ID_1 + '' +
                                                     ' -D  WEEK_ID=' + WEEK_ID + '' +
                                                     ' -D  MONTH_ID=' + month_id + ''
                                                                                   ' -D  test_suffix=' + test_suffix + '',
                                        dag=dag
                                        )

        # depends on hh_ETV_OTT_latte
        ETV_OTT_incremental_part1 = BashOperator(task_id='ETV_OTT_incremental_part1',
                                                 bash_command='snowsql --config ' + CONFIG_PATH + '' +
                                                              ' -f ' + borg_level_sql_path + 'ott_incremental_part1.sql' +
                                                              ' -D  WEEK_SCHEMA_0=' + Week + '' +
                                                              ' -D  WEEK_SCHEMA_5=' + PREVIOUS_WEEK_5 + '' +
                                                              ' -D  WEEK_SCHEMA_4=' + PREVIOUS_WEEK_4 + '' +
                                                              ' -D  WEEK_SCHEMA_3=' + PREVIOUS_WEEK_3 + '' +
                                                              ' -D  WEEK_SCHEMA_2=' + PREVIOUS_WEEK_2 + '' +
                                                              ' -D  WEEK_SCHEMA_1=' + PREVIOUS_WEEK_1 + '' +
                                                              ' -D  WEEK_5=' + PREVIOUS_WEEK_ID_5 + '' +
                                                              ' -D  WEEK_4=' + PREVIOUS_WEEK_ID_4 + '' +
                                                              ' -D  WEEK_3=' + PREVIOUS_WEEK_ID_3 + '' +
                                                              ' -D  WEEK_2=' + PREVIOUS_WEEK_ID_2 + '' +
                                                              ' -D  WEEK_1=' + PREVIOUS_WEEK_ID_1 + '' +
                                                              ' -D  WEEK_0=' + WEEK_ID + ''
                                                                                         ' -D  MONTH_ID=' + month_id + ''
                                                                                                                       ' -D  PREV_MONTH_ID=' + prev_month_id + ''
                                                                                                                                                               ' -D  test_suffix=' + test_suffix + '',
                                                 dag=dag
                                                 )

        # depends on ETV_OTT_incremental_part1
        ETV_OTT_incremental_part2 = BashOperator(task_id='ETV_OTT_incremental_part2',
                                                 bash_command='python ' + borg_level_sql_path + 'ott_incremental_part2_loop.py ' + WEEK_ID + ' ' + test_suffix + '',
                                                 dag=dag
                                                 )

        # depends on ETV_OTT_incremental_part2
        ETV_OTT_incremental_part3 = BashOperator(task_id='ETV_OTT_incremental_part3',
                                                 bash_command='snowsql --config ' + CONFIG_PATH + '' +
                                                              ' -f ' + borg_level_sql_path + 'ott_incremental_part3.sql' +
                                                              ' -D  WEEK_SCHEMA_0=' + Week + '' +
                                                              ' -D  WEEK_0=' + WEEK_ID + ''
                                                                                         ' -D  test_suffix=' + test_suffix + '',
                                                 dag=dag
                                                 )

        # depends on ETV_OTT_incremental_part3
        ETV_OTT_incremental_part4 = BashOperator(task_id='ETV_OTT_incremental_part4',
                                                 bash_command='python ' + borg_level_sql_path + 'ott_incremental_part4.py ' + WEEK_ID + ' ' + test_suffix + '',
                                                 dag=dag
                                                 )

        # depends on ETV_OTT_incremental_part4
        ETV_OTT_incremental_part5 = BashOperator(task_id='ETV_OTT_incremental_part5',
                                                 bash_command='snowsql --config ' + CONFIG_PATH + '' +
                                                              ' -f ' + borg_level_sql_path + 'ott_incremental_part5.sql' +
                                                              ' -D  WEEK_SCHEMA_0=' + Week + '' +
                                                              ' -D  WEEK_0=' + WEEK_ID + ''
                                                                                         ' -D  test_suffix=' + test_suffix + '',
                                                 dag=dag
                                                 )

        # depends on ETV_OTT_incremental_part5
        etv_ott_hierarchy_fixes_part1 = BashOperator(task_id='etv_ott_hierarchy_fixes_part1',
                                                     bash_command='snowsql --config ' + CONFIG_PATH + '' +
                                                                  ' -f ' + borg_level_sql_path + 'etv_ott_hierarchy_fixes_1.sql' +
                                                                  ' -D  WEEK_SCHEMA_0=' + Week + '' +
                                                                  ' -D  latte_table=' + latte_table + '' +
                                                                  ' -D  latte_raw=' + latte_raw + ''
                                                                                                  ' -D  WEEK_ID=' + WEEK_ID + ''
                                                                                                                              ' -D  test_suffix=' + test_suffix + '',
                                                     dag=dag
                                                     )

        # change python connection strings
        # depends on etv_ott_hierarchy_fixes_part1
        etv_ott_hierarchy_fixes_part2 = BashOperator(task_id='etv_ott_hierarchy_fixes_part2',
                                                     bash_command='python ' + borg_level_sql_path + 'etv_ott_hierarchy_fixes_part1.py ' + week_ptr + ' ' + FIRST_WEEK_MONTH + ' ' + table_name_part1 + ' ' + test_suffix + '',
                                                     dag=dag
                                                     )

        # depends on etv_ott_hierarchy_fixes_part2
        etv_ott_hierarchy_fixes_part3 = BashOperator(task_id='etv_ott_hierarchy_fixes_part3',
                                                     bash_command='snowsql --config ' + CONFIG_PATH + '' +
                                                                  ' -f ' + borg_level_sql_path + 'etv_ott_hierarchy_fixes_2.sql'
                                                                                                 ' -D  test_suffix=' + test_suffix + '',
                                                     dag=dag
                                                     )

        # depends on etv_ott_hierarchy_fixes_part3
        etv_ott_hierarchy_fixes_part4 = BashOperator(task_id='etv_ott_hierarchy_fixes_part4',
                                                     bash_command='python ' + borg_level_sql_path + 'etv_ott_hierarchy_fixes_part2.py ' + WEEK_ID + ' ' + test_suffix + '',
                                                     dag=dag
                                                     )

        # depends on etv_ott_hierarchy_fixes_part4
        etv_ott_hierarchy_fixes_part5 = BashOperator(task_id='etv_ott_hierarchy_fixes_part5',
                                                     bash_command='snowsql --config ' + CONFIG_PATH + '' +
                                                                  ' -f ' + borg_level_sql_path + 'etv_ott_hierarchy_fixes_3.sql' +
                                                                  ' -D  WEEK_SCHEMA_0=' + Week + '' +
                                                                  ' -D  latte_final=' + latte_final + '' +
                                                                  ' -D  output_table=' + latte_output_table + ''
                                                                                                              ' -D  test_suffix=' + test_suffix + '',
                                                     dag=dag
                                                     )

        # depends on etv_ott_hierarchy_fixes_part5
        etv_ott_hierarchy_fixes_part6 = BashOperator(task_id='etv_ott_hierarchy_fixes_part6',
                                                     bash_command='python ' + borg_level_sql_path + 'etv_ott_hierarchy_fixes_part1.py ' + week_ptr + ' ' + str(
                                                         int(WEEK_ID) - 5) + ' ' + table_name_part3 + ' ' + test_suffix + '',
                                                     dag=dag
                                                     )

        # depends on etv_ott_hierarchy_fixes_part6
        etv_ott_hierarchy_fixes_part7 = BashOperator(task_id='etv_ott_hierarchy_fixes_part7',
                                                     bash_command='snowsql --config ' + CONFIG_PATH + '' +
                                                                  ' -f ' + borg_level_sql_path + 'etv_ott_hierarchy_fixes_4.sql' +
                                                                  ' -D  WEEK_SCHEMA_0=' + Week + '' +
                                                                  ' -D  OUTPUT_WITH_MRS=' + output_with_mrs + '' +
                                                                  ' -D  output_table=' + latte_output_table + ''
                                                                                                              ' -D  test_suffix=' + test_suffix + '',
                                                     dag=dag
                                                     )

        load_OTT_HH_pattern_agg.set_upstream(ott_hh_pattern_agg)
        ETV_OTT_HWE.set_upstream(load_OTT_HH_pattern_agg)
        push_static_PPM_schedulized.set_upstream(ETV_OTT_HWE)
        push_acxiom_gender_age_lookup.set_upstream(push_static_PPM_schedulized)
        PPM_PWE.set_upstream(push_static_PPM_schedulized)
        PPM_training_data.set_upstream(PPM_PWE)

        export_ppm_training_data_to_s3.set_upstream(PPM_training_data)
        delete_ott_coffee_filter_wrapper.set_upstream(export_ppm_training_data_to_s3)
        #ott_coffee_filter_wrapper.set_upstream(export_ppm_training_data_to_s3)
        Export_Espresso_Live_Regression_S3_To_SnowSQL.set_upstream(ott_coffee_filter_wrapper)

        ott_coffee_filter_apply.set_upstream(Export_Espresso_Live_Regression_S3_To_SnowSQL)
        delete_generate_target_and_population_estimate.set_upstream([sync_mobilens,ott_coffee_filter_apply])
        delete_generate_experian_target_and_population_estimate.set_upstream([sync_mobilens,ott_coffee_filter_apply])
        #generate_experian_target_and_population_estimate.set_upstream(ott_coffee_filter_apply)
        generate_sample_for_week.set_upstream(ott_coffee_filter_apply)
        delete_export_cbp_weights_pwe_handoff_to_s3.set_upstream(generate_sample_for_week)
        #export_cbp_weights_pwe_handoff_to_s3.set_upstream(generate_sample_for_week)
        generate_experian_sample_for_week.set_upstream(generate_sample_for_week)
        delete_weights_run.set_upstream([generate_target_and_population_estimate, export_cbp_weights_pwe_handoff_to_s3])
        delete_export_cbp_experian_weights_pwe_handoff_to_s3.set_upstream(generate_experian_sample_for_week)
        delete_experian_weights_run.set_upstream(
            [generate_experian_target_and_population_estimate, export_cbp_experian_weights_pwe_handoff_to_s3])
        OTT_PPM_PWE.set_upstream(experian_weights_run)
        OTT_espresso.set_upstream(OTT_PPM_PWE)

        OTT_regression_espresso.set_upstream(OTT_espresso)
        truncate_dictionary_for_processing.set_upstream(OTT_regression_espresso)
        OTT_espresso_agg.set_upstream(truncate_dictionary_for_processing)

        OTT_hh_web_agg.set_upstream(OTT_espresso_agg)
        delete_hh_generate_target_and_population.set_upstream(OTT_hh_web_agg)
        generate_hh_sample.set_upstream(hh_generate_target_and_population)
        delete_export_cbp_weights_hh_pwe_handoff_to_s3.set_upstream(generate_hh_sample)
        delete_hh_weights_run.set_upstream([hh_generate_target_and_population, export_cbp_weights_hh_pwe_handoff_to_s3])
        hh_truncate_dictionary_for_processing.set_upstream([hh_weights_run, experian_weights_run])
        hh_ETV_OTT_latte.set_upstream([hh_weights_run, weights_run, hh_truncate_dictionary_for_processing, OTT_hh_web_agg])

        ETV_OTT_incremental_part1.set_upstream(hh_ETV_OTT_latte)
        ETV_OTT_incremental_part2.set_upstream(ETV_OTT_incremental_part1)
        ETV_OTT_incremental_part3.set_upstream(ETV_OTT_incremental_part2)
        ETV_OTT_incremental_part4.set_upstream(ETV_OTT_incremental_part3)
        ETV_OTT_incremental_part5.set_upstream(ETV_OTT_incremental_part4)
        etv_ott_hierarchy_fixes_part1.set_upstream(ETV_OTT_incremental_part5)
        etv_ott_hierarchy_fixes_part2.set_upstream(etv_ott_hierarchy_fixes_part1)
        etv_ott_hierarchy_fixes_part3.set_upstream(etv_ott_hierarchy_fixes_part2)
        etv_ott_hierarchy_fixes_part4.set_upstream(etv_ott_hierarchy_fixes_part3)
        etv_ott_hierarchy_fixes_part5.set_upstream(etv_ott_hierarchy_fixes_part4)
        etv_ott_hierarchy_fixes_part6.set_upstream(etv_ott_hierarchy_fixes_part5)
        etv_ott_hierarchy_fixes_part7.set_upstream(etv_ott_hierarchy_fixes_part6)
    return dag;

etv_ott_dg_regression_dag = etv_ott_dg_regression('etv_ott_dg_regression')