__author__ = 'hsalinas'

import calendar
from datetime import datetime, timedelta

__DIFF_SECONDS_UNIX_TS_TO_2K = 946598400
__NUMBER_OF_SECONDS_PER_DAY = 86400
__OFFSET_FOR_WEEK_ID = 4
__NUMBER_OF_DAYS_PER_WEEK = 7
__NUMBER_OF_MONTHS_PER_YEAR = 12
__DAYS_PER_YEAR = 365
__NUMBER_OF_MONTHS = 12


def total_seconds(td):
    return (td.microseconds + (td.seconds + td.days * 24 * 3600) * 10**6) / 10**6


def datetime_to_timestamp(dt):
    return total_seconds(dt - datetime(1970, 1, 1))


def timestamp_to_ss2k(timestamp):
    return timestamp - __DIFF_SECONDS_UNIX_TS_TO_2K


def time_id_to_day_number(time_id):
    day = time_id + __OFFSET_FOR_WEEK_ID + 1
    day %= __NUMBER_OF_DAYS_PER_WEEK
    if day == 0:
        return __NUMBER_OF_DAYS_PER_WEEK
    else:
        return day


def is_leap(year):
    if year % 4 == 0:
        if year % 100 == 0:
            if year % 400 == 0:
                return True
            return False
        return True
    return False


def month_id_to_year(month_id):
    return int(2000 + ((month_id - 1) / __NUMBER_OF_MONTHS))


def time_id_to_month_id(time_id):
    ss2k = time_id_to_ss2k(time_id)
    dt = ss2k_to_datetime(ss2k)
    y = int(dt.strftime("%Y"))
    m = int(dt.strftime("%m"))
    return (12 * (y - 2000)) + m


def time_id_to_ss2k(time_id):
    return time_id * (24 * 60 * 60)


def time_id_to_yyyymmdd(time_id):
    ss2k = time_id_to_ss2k(time_id)
    return ss2k_to_datetime(ss2k).strftime("%Y-%m-%d")


def month_length(month_number, is_leap_year):
    if month_number == 2:
        if is_leap_year:
            return 29
        else:
            return 28
    if month_number <= 7:
        if month_number % 2 == 0:
            return 30
        else:
            return 31
    else:
        if month_number % 2 == 0:
            return 31
        else:
            return 30


def time_id_to_broadcast_month(time_id):
    day_number = time_id_to_day_number(time_id)
    diff = __NUMBER_OF_DAYS_PER_WEEK - day_number
    if day_number == __NUMBER_OF_SECONDS_PER_DAY:
        return time_id_to_month_id(time_id)
    else:
        return time_id_to_month_id(time_id + diff)


def week_id_to_broadcast_month(week_id):
    time_id = week_id * __NUMBER_OF_DAYS_PER_WEEK
    return time_id_to_broadcast_month(time_id)


def ss2k_to_timestamp(ss2k):
    return int(ss2k) + __DIFF_SECONDS_UNIX_TS_TO_2K


def ss2k_to_datetime(ss2k):
    return datetime.utcfromtimestamp(ss2k_to_timestamp(ss2k))


def datetime_to_ss2k(dt):
    return timestamp_to_ss2k(datetime_to_timestamp(dt))


def ss2k_to_time_id(ss2k):
    return int(ss2k/__NUMBER_OF_SECONDS_PER_DAY)


def ss2k_to_media_day(ss2k):
    time_id = ss2k_to_time_id(ss2k)
    if int((ss2k % 86400)/3600) < 6:
        return time_id - 1
    else:
        return time_id


def datetime_to_time_id(dt):
    return int(ss2k_to_time_id(datetime_to_ss2k(dt)))


def time_id_to_week_id(time_id):
    return (time_id + __OFFSET_FOR_WEEK_ID) / __NUMBER_OF_DAYS_PER_WEEK


def datetime_to_week_id(dt):
    return time_id_to_week_id(datetime_to_time_id(dt))


def __add_months(date, months):
    month = date.month - 1 + months
    year = date.year + month / __NUMBER_OF_MONTHS_PER_YEAR
    month = month % __NUMBER_OF_MONTHS_PER_YEAR + 1
    # only called with day=1, the best case scenario
    day = min(date.day, calendar.monthrange(year, month)[1])
    return datetime(year, month, day)


def get_prev_sunday(date):
    return date - timedelta(days=(date.weekday()+1) % __NUMBER_OF_DAYS_PER_WEEK)


def get_prev_monday(date):
    return date - timedelta(days=(date.weekday()) % 6)


def get_first_day_in_gregorian_month(month_id):
    return __add_months(datetime(year=1999, month=12, day=1), month_id)


def get_last_day_in_gregorian_month(month_id):
    return __add_months(datetime(year=1999, month=12, day=1), month_id + 1) - timedelta(days=1)


def get_first_day_in_broadcast_month(month_id):
    return get_prev_monday(get_first_day_in_gregorian_month(month_id))


def get_last_day_in_broadcast_month(month_id):
    return get_prev_sunday(get_last_day_in_gregorian_month(month_id))


# TO-DO: make it more efficient avoiding using datetime (like csutils)
def broadcast_month_to_first_time_id(broadcast_month):
    dt = get_first_day_in_broadcast_month(broadcast_month)
    return datetime_to_time_id(dt)


def date_in_broadcast_month(date, month_id):
    return get_first_day_in_broadcast_month(month_id) <= date <= get_last_day_in_broadcast_month(month_id)


def datetime_to_broadcast_month(dt):
    return time_id_to_broadcast_month(datetime_to_time_id(dt))


def get_first_week_id_in_broadcast_month(month):
    time_id = broadcast_month_to_first_time_id(month)
    return time_id_to_week_id(time_id)


def get_last_week_id_in_broadcast_month(month):
    return get_first_week_id_in_broadcast_month(month+1)-1


def get_weeks_in_broadcast_month(month):
    first_week_id = get_first_week_id_in_broadcast_month(month)
    last_week_id = get_last_week_id_in_broadcast_month(month)
    return range(first_week_id, last_week_id+1)


def get_first_time_id_in_week_id(week_id):
    if week_id < 0:
        return -1
    time_id = int(week_id) * __NUMBER_OF_DAYS_PER_WEEK
    day = time_id_to_day_number(time_id)

    if day == 1:
        return time_id
    else:
        time_id = time_id - day + 1 if time_id - day + 1 > 0 else 1
        return time_id


def get_last_time_id_in_week_id(week_id):
    if week_id < 0:
        return -1
    else:
        return get_first_time_id_in_week_id(week_id+1)-1


def week_id_to_time_ids(week_id):
    if week_id < 0:
        return -1
    else:
        return range(get_first_time_id_in_week_id(week_id), get_last_time_id_in_week_id(week_id)+1)


def nielsen_week_to_week_id(nielsen_week):
    # input example: 'NOV2014_W1'
    week_number = int(nielsen_week[-1])-1
    day_number = "%02d" % (week_number*7+1)
    formatted_string = nielsen_week[:-3] + day_number
    return datetime_to_week_id(datetime.strptime(formatted_string, "%b%Y%d"))
