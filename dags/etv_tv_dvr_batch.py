from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.models import Variable
from comscoreEmail import notify_email
from csdatetools.csdatetools import *

default_args={
    'owner':'etv',
    'depends_on_past':False,
    'start_date':datetime(2019, 1, 31),
    'email': ['sgude@comscore.com, vkomuravelli@comscore.com, skamidi@comscore.com'],
    'email_on_failure':False,
    'email_on_retry':False,
    'retries':0,
    'retry_delay':timedelta(minutes=3),
    'on_failure_callback': notify_email
}

config_path=Variable.get("config_path")
sqlfile_base_path=Variable.get("base_sql_path")
DQC_PATH = Variable.get("data_quality_check_path")
DATA_QUALITY_CHECK = Variable.get("data_quality_check")
sqlfile_path=sqlfile_base_path+'etv_tv_dvr_batch/'
WEEK_ID= Variable.get("week_id")
MONTH_ID=str(time_id_to_broadcast_month(get_first_time_id_in_week_id(WEEK_ID)))

DPs=[35,34,33,32,31,30,29,28,27,26,16,12,10,9,8,2,1,0]
WeekID_0=WEEK_ID
WeekID_1=str(int(WEEK_ID) - 1)
WeekID_2=str(int(WEEK_ID) - 2)
WeekID_3=str(int(WEEK_ID) - 3)
WeekID_4=str(int(WEEK_ID) - 4)
WeekID_5=str(int(WEEK_ID) - 5)
WEEK_SCHEMA_0 = 'W' + WeekID_0
PREVIOUS_WEEK_1 = 'W' + WeekID_1
start_time_id= str(get_first_time_id_in_week_id(int(WEEK_ID))) #
end_time_id= str(get_last_time_id_in_week_id(int(WEEK_ID))) #
START_DATE = str(time_id_to_yyyymmdd(int(start_time_id)))
END_DATE = str(time_id_to_yyyymmdd(int(end_time_id)))	
PRODUCT='ETV'
PLATFORM='DVR'
tunes_table ="comscore_ren_schedulized_dvr_tunes_enh"
tunes_type='viewership'

def etv_tv_dvr_batch(dag_name):
    dag= DAG(dag_name, default_args=default_args, schedule_interval=None)

    PROCESS_LIST=["ESP","DVRVIEW","DVRINCSMALL","DVRLATTE","DVRINC","DVRBOUNDING","MONTHLY"]

    if "ESP"  in PROCESS_LIST:
        DVRESP_etv_espresso_dvr_post_hwe =BashOperator(
                task_id='DVRESP_etv_espresso_dvr_post_hwe',
                bash_command='snowsql --config ' +config_path +''
                        ' -f '+sqlfile_path + 'dvr_espresso.sql' +''
                        ' -D  WEEK_SCHEMA_0=w'+WeekID_0  +''
                        ' -D  WEEK_SCHEMA_1=w'+WeekID_1  +''
                        ' -D  WEEK_SCHEMA_2=w'+WeekID_2  +''
                        ' -D  WEEK_SCHEMA_3=w'+WeekID_3  +''
                        ' -D  WEEK_SCHEMA_4=w'+WeekID_4  +''
                        ' -D  WEEK_SCHEMA_5=w'+WeekID_5  +''
                        ' -D  Week_0=' +WeekID_0  +''
                        ' -D  Week_1=' +WeekID_1  +''
                        ' -D  Week_2=' +WeekID_2  +''
                        ' -D  Week_3=' +WeekID_3  +''
                        ' -D  Week_4=' +WeekID_4  +''
                        ' -D  Week_5=' +WeekID_5  +''
                        ' -D  MONTH_ID='  +MONTH_ID  +'',
                        dag=dag)

        DVRESP_etv_espresso_dvr_regcoef_apply_prob_reweight =BashOperator(
                task_id='DVRESP_etv_espresso_dvr_regcoef_apply_prob_reweight',
                bash_command='snowsql --config ' +config_path +''
                        ' -f '+sqlfile_path + 'regcoef_apply_prob_reweight.sql' +''
                        ' -D  WEEK_SCHEMA_0=w'+WeekID_0  +''
                        ' -D Week_0=' +WeekID_0  +''
                        ,dag=dag)

        if DATA_QUALITY_CHECK == "TRUE":
            dqc_dvr_reweight = BashOperator(
                task_id='dqc_dvr_reweight',
                bash_command='{0}/dataqualitycheck.sh Espresso espresso_reweight {1} DVR \"@table_name={2}.etv_experian_dvr_espresso_reweight @table_previous={3}.etv_experian_dvr_espresso_reweight\" {0} {4}'.format(DQC_PATH, WEEK_ID, WEEK_SCHEMA_0, PREVIOUS_WEEK_1, config_path)
            , dag=dag)

        truncate_dictionary_for_processing =BashOperator(
                task_id='truncate_dictionary_for_processing',
                bash_command='snowsql --config ' +config_path +''
                        ' -f '+sqlfile_path + 'dvr_dictionaries_subset.sql' +''
                        ' -D  WEEK_SCHEMA_0=w'+WeekID_0  +''
                        ' -D Week_0=' +WeekID_0  +''
                        ,dag=dag)

        DVRESP_etv_espresso_dvr_Agg =BashOperator(
                task_id='DVRESP_etv_espresso_dvr_Agg',
                bash_command='snowsql --config ' +config_path +''
                        ' -f '+sqlfile_path + 'dvr_espresso_agg.sql' +''
                        ' -D  WEEK_SCHEMA_0=w'+WeekID_0  +''
                        ' -D Week_0=' +WeekID_0  +''
                        ' -D start_time_id=' + start_time_id  +''
                        ' -D end_time_id=' + end_time_id  +''
                        ,dag=dag)

        if DATA_QUALITY_CHECK == "TRUE":
            dqc_dvr_agg = BashOperator(
                task_id='dqc_dvr_agg',
                bash_command='{0}/dataqualitycheck.sh Espresso espresso_agg {1} DVR \"@table_name={2}.etv_dvr_espresso_agg_weights @table_previous={3}.etv_dvr_espresso_agg_weights\" {0} {4}'.format(DQC_PATH, WEEK_ID, WEEK_SCHEMA_0, PREVIOUS_WEEK_1, config_path)
            , dag=dag)


    if "DVRVIEW" in PROCESS_LIST:
        DVRVIEW_dvr_viewership =BashOperator(
                task_id='DVRVIEW_dvr_viewership',
                bash_command='snowsql --config ' +config_path +''
                        ' -f '+sqlfile_path + 'dvr_viewership.sql' +''
                        ' -D  WEEK_SCHEMA_0=w'+WeekID_0  +''
                        ' -D  WEEK_SCHEMA_1=w'+WeekID_1 +''
                        ' -D Week_0=' +WeekID_0  +''
                        ' -D Week_5=' +WeekID_5 +''
                        ' -D start_time_id=' + start_time_id  +''
                        ' -D end_time_id=' + end_time_id  +''
                        ' -D PLATFORM=' + PLATFORM  +''
                        ' -D tunes_type=' + tunes_type  +''
						' -D START_DATE=' + START_DATE  +''
						' -D END_DATE=' + END_DATE  +''
                        ,dag=dag)

    if "DVRINCSMALL" in PROCESS_LIST:
        inp_prep_and_out_tab_creation =BashOperator(
                task_id='inp_prep_and_out_tab_creation',
                bash_command='snowsql --config ' +config_path +''
                        ' -f '+sqlfile_path + 'inclusive_small_prep.sql' +''
                        ' -D  WEEK_SCHEMA_0=w'+WeekID_0  +''
                        ' -D  WEEK_SCHEMA_1=w'+WeekID_1  +''
                        ' -D  WEEK_SCHEMA_2=w'+WeekID_2  +''
                        ' -D  WEEK_SCHEMA_3=w'+WeekID_3  +''
                        ' -D  WEEK_SCHEMA_4=w'+WeekID_4  +''
                        ' -D  WEEK_SCHEMA_5=w'+WeekID_5  +''
                        ' -D  Week_0=' +WeekID_0  +''
                        ' -D  Week_1=' +WeekID_1  +''
                        ' -D  Week_2=' +WeekID_2  +''
                        ' -D  Week_3=' +WeekID_3  +''
                        ' -D  Week_4=' +WeekID_4  +''
                        ' -D  Week_5=' +WeekID_5  +''
                        ,dag=dag)

        if "ESP" in PROCESS_LIST :
            if DATA_QUALITY_CHECK == "TRUE":
                inp_prep_and_out_tab_creation.set_upstream(dqc_dvr_agg)
            else:
                inp_prep_and_out_tab_creation.set_upstream(DVRESP_etv_espresso_dvr_Agg)

        if DATA_QUALITY_CHECK == "TRUE":
            dqc_dvr_small = BashOperator(
                task_id='dqc_dvr_small',
                bash_command='{0}/dataqualitycheck.sh Incremental small {1} DVR \"@table_name={2}.dvr_inclusive_small @table_previous={3}.dvr_inclusive_small\" {0} {4}'.format(DQC_PATH, WEEK_ID, WEEK_SCHEMA_0, PREVIOUS_WEEK_1, config_path)
            , dag=dag)

        for DP in DPs:
            DVRINCSMALL_dvr_Incremental_Small="""DVRINCSMALL_dvr_Incremental_Small_for_DP_{0}=BashOperator(
                task_id='DVRINCSMALL_dvr_Incremental_Small_for_DP_{0}',
                bash_command='snowsql --config ' +config_path +''
                        ' -f '+sqlfile_path + 'inclusive_small_daypart_loop.sql' +''
                        #/mapr/ia1.comscore.com/bin/dw/production/etv/etv_smalls/DVR-VOD/inclusive_small_daypart_loop.sql
                        ' -D  WEEK_SCHEMA_0=w{2}'
                        ' -D Week_0={2}'
                        ' -D DP={0}'
                        ,dag=dag)""".format(DP , PLATFORM , WeekID_0)

            exec(DVRINCSMALL_dvr_Incremental_Small)

            temp="""DVRINCSMALL_dvr_Incremental_Small_for_DP_{0}.set_upstream(inp_prep_and_out_tab_creation)""".format(DP)
            exec(temp)

            if DATA_QUALITY_CHECK == "TRUE":
                dqc_temp="""dqc_dvr_small.set_upstream(DVRINCSMALL_dvr_Incremental_Small_for_DP_{0})""".format(DP)
                exec(dqc_temp)


    if "DVRLATTE"  in PROCESS_LIST:
        DVRLATTE_dvr_input_format =BashOperator(
                task_id='DVRLATTE_dvr_input_format',
                bash_command='snowsql --config ' +config_path +''
                        ' -f '+sqlfile_path + 'latte_0_input_format_dvr.sql' +''
                        #/mapr/ia1.comscore.com/bin/dw/production/etv/etv_pre_latte_input_format/dvr/latte_0_input_format_dvr.sql
                        ' -D  WEEK_SCHEMA_0=w'+WeekID_0  +''
                        ' -D Week_0=' +WeekID_0  +''
                        ' -D  PLATFORM='  +PLATFORM  +''
                        ,dag=dag)

        if DATA_QUALITY_CHECK == "TRUE":
            dqc_dvr_venti = BashOperator(
                task_id='dqc_dvr_venti',
                bash_command='{0}/dataqualitycheck.sh Viewership viewership {1} DVR \"@table_name={2}.etv_dvr_venti @table_previous={3}.etv_dvr_venti\" {0} {4}'.format(DQC_PATH, WEEK_ID, WEEK_SCHEMA_0, PREVIOUS_WEEK_1, config_path)
            , dag=dag)

        Prepare_LaTTe_input_data =BashOperator(
                task_id='Prepare_LaTTe_input_data',
                bash_command='snowsql --config ' +config_path +''
                        ' -f '+sqlfile_path + 'latte_1_data_prep.sql' +''
                        #/mapr/ia1.comscore.com/bin/dw/production/etv/etv_LaTTe/Live-DVR-VOD/latte_1_data_prep.sql
                        ' -D  WEEK_SCHEMA_0=w'+WeekID_0  +''
                        ' -D Week_0=' +WeekID_0  +''
                        ,dag=dag)

        LaTTe_Merge_final_table_creation =BashOperator(
                task_id='LaTTe_Merge_final_table_creation',
                bash_command='snowsql --config ' +config_path +''
                        ' -f '+sqlfile_path + 'latte_3_merge_output.sql' +''
                        ' -D  WEEK_SCHEMA_0=w'+WeekID_0  +''
                        ' -D Week_0=' +WeekID_0  +''
                        ,dag=dag)


        if DATA_QUALITY_CHECK == "TRUE":
            dqc_dvr_latte = BashOperator(
                task_id='dqc_dvr_latte',
                bash_command='{0}/dataqualitycheck.sh Latte latte {1} DVR \"@table_name={2}.etv_experian_dvr_latte @table_previous={3}.etv_experian_dvr_latte\" {0} {4}'.format(DQC_PATH, WEEK_ID, WEEK_SCHEMA_0, PREVIOUS_WEEK_1, config_path)
            , dag=dag)


        for DP in DPs:
            DVRINCSMALL_dvr_Incremental_Small="""DVRINCSMALL_dvr_Incremental_Small_for_DP_{0}=BashOperator(
                task_id='DVRINCSMALL_dvr_Incremental_Small_for_DP_{0}',
                bash_command='snowsql --config ' +config_path +''
                        ' -f '+sqlfile_path + 'inclusive_small_daypart_loop.sql' +''
                        ' -D  WEEK_SCHEMA_0=w{2}'
                        ' -D Week_0={2}'
                        ' -D DP={0}'
                        ,dag=dag)""".format(DP , PLATFORM , WeekID_0)

            exec(DVRINCSMALL_dvr_Incremental_Small)

            Demodist_VPVH_CVF_for_Daypart="""Demodist_VPVH_CVF_for_Daypart_{0} =BashOperator(
                task_id='Demodist_VPVH_CVF_for_Daypart_{0}',
                bash_command='snowsql --config ' +config_path +''
                        ' -f '+sqlfile_path + 'latte_2a_demodist_vpvh_cvf.sql' +''
                        ' -D  WEEK_SCHEMA_0=w{2}'
                        ' -D Week_0={2}'
                        ' -D DP={0}'
                        ,dag=dag)""".format(DP, PLATFORM , WeekID_0 )

            exec(Demodist_VPVH_CVF_for_Daypart)

            LaTTe_daypart_loop_for_Daypart_1_DP="""LaTTe_daypart_loop_for_Daypart_DP_1_{0}=BashOperator(task_id='LaTTe_daypart_loop_for_Daypart_DP_1_{0}',
            bash_command='snowsql --config ' +config_path +''
                ' -f '+sqlfile_path + 'latte_2_daypart_loop_part1.sql' +''
                ' -D  WEEK_SCHEMA_0=w{2}'
                ' -D Week_0={2}'
                ' -D DP={0}'
                ,dag=dag)""".format(DP, PLATFORM , WeekID_0 )

            exec(LaTTe_daypart_loop_for_Daypart_1_DP)

            latte_2_daypart_loop="""latte_2_daypart_loop_{2}=BashOperator(
            task_id='latte_2_daypart_loop_{2}',
            bash_command='python {0} {1} {2} "w{1}.dvr_latte_tv_hierarchy" ',
            dag=dag)""".format(sqlfile_path + "latte_2_daypart_loop.py" ,WeekID_0 , DP)

            exec(latte_2_daypart_loop)

            LaTTe_daypart_loop_for_Daypart_2_DP="""LaTTe_daypart_loop_for_Daypart_DP_2_{0} =BashOperator(
                task_id='LaTTe_daypart_loop_for_Daypart_DP_2_{0}',
                bash_command='snowsql --config ' +config_path +''
                        ' -f '+sqlfile_path + 'latte_2_daypart_loop_part2.sql' +''
                        ' -D  WEEK_SCHEMA_0=w{2}'
                        ' -D Week_0={2}'
                        ' -D DP={0}'
                        ,dag=dag)""".format(DP, PLATFORM , WeekID_0 )

            exec(LaTTe_daypart_loop_for_Daypart_2_DP)

            DP_Output_Insert_to_the_Master_Table="""DP{0}_Output_Insert_to_the_Master_Table=BashOperator(
                task_id='DP{0}_Output_Insert_to_the_Master_Table',
                bash_command='snowsql --config ' +config_path +''
                        ' -f '+sqlfile_path + 'latte_4_output_loop.sql' +''
                        ' -D  WEEK_SCHEMA_0=w{2}'
                        ' -D Week_0={2} '
                        ' -D DP={0}'
                        ,dag=dag)""".format(DP, PLATFORM , WeekID_0 )

            exec(DP_Output_Insert_to_the_Master_Table)

            if DATA_QUALITY_CHECK == "TRUE":
                dqc_temp="""dqc_dvr_latte.set_upstream(DP{0}_Output_Insert_to_the_Master_Table)""".format(DP)
                exec(dqc_temp)


    if "DVRINC" in PROCESS_LIST:
        Output_Table_Creation=BashOperator(
                task_id='Output_Table_Creation',
                bash_command='snowsql --config ' +config_path +''
                        ' -f '+sqlfile_path + 'incremental_pt0.sql' +''
                        ' -D  WEEK_SCHEMA_0=w'+WeekID_0  +''
                        ' -D  WEEK_SCHEMA_1=w'+WeekID_1  +''
                        ' -D Week_0=' +WeekID_0  +''
                        ' -D Week_1=' +WeekID_1 +''
                        ,dag=dag)

        Week_to_week_MLE=BashOperator(
                task_id='Week_to_week_MLE',
                bash_command='snowsql --config ' +config_path +''
                        ' -f '+sqlfile_path + 'incremental_pt1.sql' +''
                        ' -D  WEEK_SCHEMA_0=w'+WeekID_0  +''
                        ' -D  WEEK_SCHEMA_1=w'+WeekID_1  +''
                        ' -D Week_0=' +WeekID_0  +''
                        ' -D Week_1=' +WeekID_1 +''
                        ,dag=dag)

        four_Plus_and_eight_plus=BashOperator(
                task_id='four_Plus_and_eight_plus',
                bash_command='snowsql --config ' +config_path +''
                        ' -f '+sqlfile_path + 'incremental_pt2.sql' +''
                        ' -D  WEEK_SCHEMA_0=w'+WeekID_0  +''
                        ' -D  WEEK_SCHEMA_1=w'+WeekID_1  +''
                        ' -D Week_0=' +WeekID_0  +''
                        ' -D Week_1=' +WeekID_1 +''
                        ,dag=dag)

        Within_the_week_Prep=BashOperator(
                task_id='Within_the_week_Prep',
                bash_command='snowsql --config ' +config_path +''
                        ' -f '+sqlfile_path + 'incremental_pt3.sql' +''
                        ' -D  WEEK_SCHEMA_0=w'+WeekID_0  +''
                        ' -D  WEEK_SCHEMA_1=w'+WeekID_1  +''
                        ' -D Week_0=' +WeekID_0  +''
                        ' -D Week_1=' +WeekID_1 +''
                        ,dag=dag)

        i=1
        while(i<=10) :
            Within_the_week_loop="""Within_the_week_loop_{0}=BashOperator(
                task_id='Within_the_week_loop_{0}',
                bash_command='snowsql --config ' +config_path +''
                        ' -f '+sqlfile_path + 'incremental_pt4.sql' +''
                        ' -D  WEEK_SCHEMA_0=w{2} '
                        ' -D  WEEK_SCHEMA_1=w{3}'
                        ' -D Week_0={2} '
                        ' -D Week_1={3}'
                        ' -D  current={0}'
                        ,dag=dag)""".format(i , PLATFORM ,WeekID_0 , WeekID_1 )
            i+=1

            exec(Within_the_week_loop)

        Final_output=BashOperator(
                task_id='Final_output',
                bash_command='snowsql --config ' +config_path +''
                        ' -f '+sqlfile_path + 'incremental_pt5.sql' +''
                        ' -D  WEEK_SCHEMA_0=w'+WeekID_0  +''
                        ' -D  WEEK_SCHEMA_1=w'+WeekID_1  +''
                        ' -D Week_0=' +WeekID_0  +''
                        ' -D Week_1=' +WeekID_1 +''
                        ,dag=dag)



        Clean_up_intermediate_tables=BashOperator(
                task_id='Clean_up_intermediate_tables',
                bash_command='snowsql --config ' +config_path +''
                        ' -f '+sqlfile_path + 'incremental_pt6.sql' +''
                        ' -D  WEEK_SCHEMA_0=w'+WeekID_0  +''
                        ' -D  WEEK_SCHEMA_1=w'+WeekID_1  +''
                        ' -D Week_0=' +WeekID_0  +''
                        ' -D Week_1=' +WeekID_1 +''
                        ,dag=dag)

        if DATA_QUALITY_CHECK == "TRUE":
            dqc_dvr_inc_latte = BashOperator(
                task_id='dqc_dvr_inc_latte',
                bash_command='{0}/dataqualitycheck.sh Incremental incremental {1} DVR \"@table_name={2}.etv_dvr_latte_inclusive @table_previous={3}.etv_dvr_latte_inclusive\" {0} {4}'.format(DQC_PATH, WEEK_ID, WEEK_SCHEMA_0, PREVIOUS_WEEK_1, config_path)
            , dag=dag)

    if "DVRBOUNDING" in PROCESS_LIST:

        tv_bounding_step = BashOperator(
               task_id= 'tv_bounding_step',
               bash_command= """{0} {1} {2} w{3} w{4} {3} {4} {5} {6} {7}"""
                               .format(sqlfile_path+ "tv_bounding/Execute_tv_bounding_step.sh"
                               ,sqlfile_path+'tv_bounding/', config_path, WeekID_0, WeekID_1, PLATFORM,start_time_id, end_time_id)
                               ,dag=dag)

        if DATA_QUALITY_CHECK == "TRUE":
            dqc_dvr_bounding = BashOperator(
                task_id='dqc_dvr_bounding',
                bash_command='{0}/dataqualitycheck.sh Bounding bounding {1} DVR \"@table_name={2}.etv_dvr_latte_inclusive_bounded @table_previous={3}.etv_dvr_latte_inclusive_bounded\" {0} {4}'.format(DQC_PATH, WEEK_ID, WEEK_SCHEMA_0, PREVIOUS_WEEK_1, config_path)
            , dag=dag)

            dqc_dvr_bounding.set_upstream(tv_bounding_step)

    current_bmonth = week_id_to_broadcast_month(int(WeekID_0))
    first_week_of_bmonth = get_first_week_id_in_broadcast_month(current_bmonth)


    if int(WeekID_0) > int(first_week_of_bmonth) and "MONTHLY" in PROCESS_LIST:
        current_bmonth=time_id_to_broadcast_month(get_first_time_id_in_week_id(WEEK_ID))
        first_week_of_bmonth=get_first_week_id_in_broadcast_month(current_bmonth)

        if WeekID_0 == str(int(get_first_week_id_in_broadcast_month(week_id_to_broadcast_month(int(WeekID_0)))) + 1):
            single_PLATFORM_DVR_small_inputs_prep=BashOperator(
                task_id='single_PLATFORM_DVR_small_inputs_prep',
                bash_command='snowsql --config ' +config_path +''
                        ' -f '+sqlfile_path + '01_single_platform_small_inputs_prep_2nd_week.sql' +''
                        ' -D Week_0=' +WeekID_0  +''
                        ' -D Week_1=' +WeekID_1  +''
                        ' -D  WEEK_SCHEMA_0=w'+WeekID_0  +''
                        ,dag=dag)
        else :
            get_espresso_agg_a_weeks = BashOperator(
                    task_id='get_espresso_agg_a_weeks',
                    bash_command='python {0} {1} {2}'\
                    .format(sqlfile_path + "get_espresso_agg_a_weeks.py"\
                    ,WeekID_0, PLATFORM),\
                    dag=dag)

            single_PLATFORM_DVR_small_inputs_prep=BashOperator(
                task_id='single_PLATFORM_DVR_small_inputs_prep',
                bash_command='snowsql --config ' +config_path +''
                        ' -f '+sqlfile_path + '01_single_platform_small_inputs_prep.sql' +''
                        ' -D Week_0=' +WeekID_0  +''
                        ' -D Week_1=' +WeekID_1  +''
                        ' -D  WEEK_SCHEMA_0=w'+WeekID_0  +''
                        ,dag=dag)

        single_PLATFORM_DVR_small=BashOperator(
            task_id='single_PLATFORM_DVR_small',
            bash_command='snowsql --config ' +config_path +''
                    ' -f '+sqlfile_path + '02_single_platform_small.sql' +''
                    ' -D Week_0=' +WeekID_0  +''
                    ' -D  WEEK_SCHEMA_0=w'+WeekID_0  +''
                    ,dag=dag)

        if WeekID_0 == str(int(get_first_week_id_in_broadcast_month(week_id_to_broadcast_month(int(WeekID_0)))) + 1):
            single_PLATFORM_DVR_overlap_w_final_format_if_last_week=BashOperator(
                task_id='single_PLATFORM_DVR_overlap_w_final_format_if_last_week',
                bash_command='snowsql --config ' +config_path +''
                        ' -f '+sqlfile_path + '03_single_platform_overlap_2nd_week' +''
                        ' -D Week_0=' +WeekID_0  +''
                        ' -D  WEEK_SCHEMA_0=w'+WeekID_0  +''
                        ' -D  WEEK_SCHEMA_1=w'+WeekID_1  +''
                        ,dag=dag)
        else :
            single_PLATFORM_DVR_overlap_w_final_format_if_last_week=BashOperator(
                task_id='single_PLATFORM_DVR_overlap_w_final_format_if_last_week',
                bash_command='snowsql --config ' +config_path +''
                        ' -f '+sqlfile_path + '03_single_platform_overlap.sql' +''
                        ' -D Week_0=' +WeekID_0  +''
                        ' -D  WEEK_SCHEMA_0=w'+WeekID_0  +''
                        ' -D  WEEK_SCHEMA_1=w'+WeekID_1  +''
                        ,dag=dag)


        if WeekID_0 == str(time_id_to_week_id(broadcast_month_to_last_time_id(week_id_to_broadcast_month(int(WeekID_0))))):
            single_PLATFORM_DVR_overlap_w_final_format_if_last_week_1=BashOperator(
                task_id='single_PLATFORM_DVR_overlap_w_final_format_if_last_week_1',
                bash_command='snowsql --config ' +config_path +''
                        ' -f '+sqlfile_path + '04_single_platform_final_format.sql' +''
                        ' -D Week_0=' +WeekID_0  +''
                        ' -D  WEEK_SCHEMA_0=w'+WeekID_0  +''
                        ' -D  WEEK_SCHEMA_1=w'+WeekID_1  +''
                        ' -D  MONTH_ID='+MONTH_ID  +''
                        ,dag=dag)

    # DAG DIRECTION
    if "ESP" in PROCESS_LIST :
        DVRESP_etv_espresso_dvr_regcoef_apply_prob_reweight.set_upstream(DVRESP_etv_espresso_dvr_post_hwe)

        if DATA_QUALITY_CHECK == "TRUE":
            dqc_dvr_reweight.set_upstream(DVRESP_etv_espresso_dvr_regcoef_apply_prob_reweight)
            truncate_dictionary_for_processing.set_upstream(dqc_dvr_reweight)
        else:
            truncate_dictionary_for_processing.set_upstream(DVRESP_etv_espresso_dvr_regcoef_apply_prob_reweight)

        DVRESP_etv_espresso_dvr_Agg.set_upstream(truncate_dictionary_for_processing)

    if "DVRLATTE" in PROCESS_LIST:
        if "DVRVIEW" in PROCESS_LIST and "ESP" in PROCESS_LIST:
            DVRLATTE_dvr_input_format.set_upstream(DVRVIEW_dvr_viewership)
            DVRLATTE_dvr_input_format.set_upstream(DVRESP_etv_espresso_dvr_post_hwe)

            if DATA_QUALITY_CHECK == "TRUE":
                dqc_dvr_agg.set_upstream(DVRESP_etv_espresso_dvr_Agg)
                Prepare_LaTTe_input_data.set_upstream(dqc_dvr_agg)
            else:
                Prepare_LaTTe_input_data.set_upstream(DVRESP_etv_espresso_dvr_Agg)

        if DATA_QUALITY_CHECK == "TRUE":
            dqc_dvr_venti.set_upstream(DVRLATTE_dvr_input_format)
            LaTTe_Merge_final_table_creation.set_upstream(dqc_dvr_venti)
            Prepare_LaTTe_input_data.set_upstream(dqc_dvr_venti)
        else:
            Prepare_LaTTe_input_data.set_upstream(DVRLATTE_dvr_input_format)
            LaTTe_Merge_final_table_creation.set_upstream(DVRLATTE_dvr_input_format)

        for DP in DPs:
            temp_1="""Demodist_VPVH_CVF_for_Daypart_{0}.set_upstream(Prepare_LaTTe_input_data)""".format(DP)
            exec(temp_1)
            temp_2="""LaTTe_daypart_loop_for_Daypart_DP_1_{0}.set_upstream(Demodist_VPVH_CVF_for_Daypart_{0})""".format(DP)
            exec(temp_2)
            temp_2_1="""latte_2_daypart_loop_{0}.set_upstream(LaTTe_daypart_loop_for_Daypart_DP_1_{0})""".format(DP)
            exec(temp_2_1)
            temp_2_2="""LaTTe_daypart_loop_for_Daypart_DP_2_{0}.set_upstream(latte_2_daypart_loop_{0})""".format(DP)
            exec(temp_2_2)


            temp_3="""DP{0}_Output_Insert_to_the_Master_Table.set_upstream([LaTTe_daypart_loop_for_Daypart_DP_2_{0},LaTTe_Merge_final_table_creation])""".format(DP)
            exec(temp_3)


    if "DVRINC" in PROCESS_LIST:
        if "DVRLATTE" in PROCESS_LIST:
            if DATA_QUALITY_CHECK == "TRUE":
                Output_Table_Creation.set_upstream([dqc_dvr_small,dqc_dvr_latte])
            else:
                for DP in DPs:
                    temp_5="""Output_Table_Creation.set_upstream([DVRINCSMALL_dvr_Incremental_Small_for_DP_{0} , DP{0}_Output_Insert_to_the_Master_Table])""".format(DP)
                    exec(temp_5)



        Week_to_week_MLE.set_upstream(Output_Table_Creation)

        four_Plus_and_eight_plus.set_upstream(Output_Table_Creation)

        Within_the_week_Prep.set_upstream(Week_to_week_MLE)

        i=1
        while i <=10:
            if i==1:
                temp_9="""Within_the_week_loop_{0}.set_upstream(Within_the_week_Prep)""".format(i)
                exec(temp_9)
            else :
                j=i-1
                temp_10="""Within_the_week_loop_{0}.set_upstream([Within_the_week_Prep,Within_the_week_loop_{1}])""".format(i,j)
                exec(temp_10)


            i+=1

        Final_output.set_upstream(four_Plus_and_eight_plus)

        i=1
        while i <=10:
            temp_12="""Final_output.set_upstream(Within_the_week_loop_{0})""".format(i)
            exec(temp_12)
            i+=1

        Clean_up_intermediate_tables.set_upstream(Final_output)

    if "DVRBOUNDING" in PROCESS_LIST:
        if "DVRINC" in PROCESS_LIST:
            if DATA_QUALITY_CHECK == "TRUE":
                dqc_dvr_inc_latte.set_upstream(Clean_up_intermediate_tables)
                tv_bounding_step.set_upstream(dqc_dvr_inc_latte)
            else:
                tv_bounding_step.set_upstream(Clean_up_intermediate_tables)

    if int(WeekID_0) > int(first_week_of_bmonth) and "MONTHLY" in PROCESS_LIST:
        if "DVRBOUNDING" in PROCESS_LIST:
            if WeekID_0 ==str(int(get_first_week_id_in_broadcast_month(week_id_to_broadcast_month(int(WeekID_0)))) + 1):
                if DATA_QUALITY_CHECK == "TRUE":
                    single_PLATFORM_DVR_small_inputs_prep.set_upstream(dqc_dvr_bounding)
                else:
                    single_PLATFORM_DVR_small_inputs_prep.set_upstream(tv_bounding_step)
            else:
                if DATA_QUALITY_CHECK == "TRUE":
                    get_espresso_agg_a_weeks.set_upstream(dqc_dvr_bounding)
                    single_PLATFORM_DVR_small_inputs_prep.set_upstream(get_espresso_agg_a_weeks)
                else:
                    get_espresso_agg_a_weeks.set_upstream(tv_bounding_step)
                    single_PLATFORM_DVR_small_inputs_prep.set_upstream(get_espresso_agg_a_weeks)
            single_PLATFORM_DVR_small.set_upstream(single_PLATFORM_DVR_small_inputs_prep)
            single_PLATFORM_DVR_overlap_w_final_format_if_last_week.set_upstream(single_PLATFORM_DVR_small)

        if WeekID_0 == str(time_id_to_week_id(broadcast_month_to_last_time_id(week_id_to_broadcast_month(int(WeekID_0))))):
            single_PLATFORM_DVR_overlap_w_final_format_if_last_week_1.set_upstream(single_PLATFORM_DVR_overlap_w_final_format_if_last_week)

    return dag

etv_tv_dvr_batch_dag = etv_tv_dvr_batch('etv_tv_dvr_batch')
