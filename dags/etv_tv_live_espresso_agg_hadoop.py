from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from datetime import datetime, timedelta
from airflow.models import Variable
from comscoreEmail import notify_email

default_args = {
    'owner': 'etv',
    'depends_on_past': False,
    'start_date': datetime(2019, 1, 31),
    'email': [
        'sgude@comscore.com, vkomuravelli@comscore.com, skamidi@comscore.com, rbhallamudi@comscore.com, gmuthyala@comscore.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=1),
    'on_failure_callback': notify_email
}


def etv_tv_live_espresso_agg_hadoop(dag_name):
    dag = DAG(dag_name, default_args=default_args, schedule_interval=None)    

    # Variables
    BASE_SQL_PATH = Variable.get("base_sql_path") + 'etv_tv_live_espresso_agg_hadoop'
    WEEK_ID = Variable.get("week_id")   
    CONFIG_PATH = Variable.get("config_path")
    DQC_PATH = Variable.get("data_quality_check_path")
    DATA_QUALITY_CHECK = Variable.get("data_quality_check")
    PULL_INPUTS = True  # "{{ dag_run.conf['pull_inputs'] }}"

    # Derived Variables
    Week = 'W' + WEEK_ID
    PREVIOUS_WEEK_1 = 'W' + str(int(WEEK_ID) - 1)

    if PULL_INPUTS == True:
        # etv_experian_live_espresso_reweight_986w
        # stacked_xmedia_etv_parentage_986w
        # stacked_xmedia_etv_parentage_daypart_986w
        truncate_espresso_agg_inputs = BashOperator(
            task_id='truncate_espresso_agg_inputs',
            bash_command='snowsql --config {}'.format(CONFIG_PATH) +
                         ' -f {}/truncate_esp_agg_inputs_cab_hierarchy.sql'.format(BASE_SQL_PATH) +
                         ' -D  WEEK_SCHEMA_0={}'.format(Week),
            dag=dag
        )

        # creates live_espresso_out_942w == etv_experian_espresso_reweight_live_942w
        extract_espresso_agg = BashOperator(
            task_id='extract_espresso_agg',
            bash_command='snowsql --config {}'.format(CONFIG_PATH) +
                         ' -f {}/for_espresso_agg_hadoop_1.sql'.format(BASE_SQL_PATH) +
                         ' -D  WEEK_SCHEMA_0={}'.format(Week),
            dag=dag
        )
        # mr_export
        master_roster_export = BashOperator(
            task_id='master_roster_export',
            bash_command='snowsql --config {}'.format(CONFIG_PATH) +
                         ' -f {}/master_roster_export_for_hadoop.sql'.format(BASE_SQL_PATH) +
                         ' -D  WEEK_SCHEMA_0={}'.format(Week),
            dag=dag
        )

        # pig script
        run_espresso_agg = BashOperator(
            task_id='run_espresso_agg',
            bash_command='snowsql --config {}'.format(CONFIG_PATH) + '' +
                         ' -f {}/live_espresso_agg.sql'.format(BASE_SQL_PATH) +
                         ' -D  WEEK_SCHEMA_0={}'.format(Week),
            dag=dag
        )

        drop_temporary_espresso_agg = BashOperator(
            task_id='drop_temporary_espresso_agg',
            bash_command='snowsql --config {}'.format(CONFIG_PATH) +
                         ' -f {}/for_espresso_agg_hadoop_2.sql'.format(BASE_SQL_PATH) +
                         ' -D  WEEK_SCHEMA_0={}'.format(Week),
            dag=dag
        )

        run_generic_stats_for_espresso_agg = BashOperator(
            task_id='run_generic_stats_for_espresso_agg',
            bash_command='snowsql --config {}'.format(CONFIG_PATH) +
                         ' -f {}/generic_espresso_stats.sql'.format(BASE_SQL_PATH) +
                         ' -D  WEEK_SCHEMA_0={}'.format(Week),
            dag=dag
        )
	if DATA_QUALITY_CHECK == "TRUE":
		DQC_for_Agg	= BashOperator(task_id='DQC_for_Agg',
			bash_command = DQC_PATH+'/dataqualitycheck.sh Espresso espresso_agg '+WEEK_ID+' LIVE \"@table_name='+Week
			+'.live_agg_stats @table_previous='+PREVIOUS_WEEK_1+'.live_agg_stats\" '+DQC_PATH+ ' '+CONFIG_PATH,
			dag=dag
			)
		DQC_for_Agg.set_upstream(run_generic_stats_for_espresso_agg)

        extract_espresso_agg.set_upstream(truncate_espresso_agg_inputs)
        run_espresso_agg.set_upstream([extract_espresso_agg, truncate_espresso_agg_inputs])
        drop_temporary_espresso_agg.set_upstream([run_espresso_agg, extract_espresso_agg])
        run_generic_stats_for_espresso_agg.set_upstream(run_espresso_agg)

    return dag


etv_tv_live_espresso_agg_hadoop_dag = etv_tv_live_espresso_agg_hadoop('etv_tv_live_espresso_agg_hadoop')
