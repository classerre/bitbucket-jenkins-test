__author__ = " skamidi "

import airflow

"""  
       importing dependencies for etv_batch5 
"""
from etv_total_audience import etv_total_audience
from etv_etl import etv_etl

from airflow.models import DAG
from airflow.models import Variable
from airflow.operators.subdag_operator import SubDagOperator
from datetime import datetime, timedelta
from airflow.executors.celery_executor import CeleryExecutor
from comscoreEmail import returnEmailOperator

BATCH_NAME = 'etv_batch5'
WEEK_ID = Variable.get("week_id")

args = {
    'owner': 'etv',
    'depends_on_past': False,
    'start_date': datetime(2019, 1, 24),
    'email': ['sgude@comscore.com', 'vkomuravelli@comscore.com', 'skamidi@comscore.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=1)
}

def etv_batch5(dag_name):
    dag = DAG(dag_name, default_args=args, schedule_interval=None)

    etv_total_audience_dag = SubDagOperator(
        executor=CeleryExecutor(),
        task_id='etv_total_audience',
        subdag=etv_total_audience(dag_name + '.etv_total_audience'),
        default_args=args,
        dag=dag)

    etv_etl_dag = SubDagOperator(
        executor=CeleryExecutor(),
        task_id='etv_etl',
        subdag=etv_etl(dag_name + '.etv_etl'),
        default_args=args,
        dag=dag)

    # Dependencies :
    etv_etl_dag.set_upstream(etv_total_audience_dag)

    notify_success = returnEmailOperator(dag, BATCH_NAME, WEEK_ID)
    notify_success.set_upstream(etv_etl_dag)
    return dag

etv_batch5_dag = etv_batch5(BATCH_NAME)