from airflow.models import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.models import Variable
from datetime import datetime, timedelta
from csdatetools import csdatetools
from comscoreEmail import notify_email

default_args = {
    'owner': 'etv',
    'depends_on_past': False,
    'start_date': datetime(2019, 1, 31),
    'email': ['ssubudhi@comscore.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=3),
    'on_failure_callback': notify_email
}

def etv_total_audience(dag_name):
    dag = DAG(dag_name, default_args=default_args, schedule_interval=None)

    CONFIG_PATH = Variable.get("config_path")
    BASE_SQL_PATH = Variable.get("base_sql_path") + "etv_total_audience"
    WEEK_ID = Variable.get("week_id")
    WEEK_SCHEMA_0 = 'W' + WEEK_ID
    WEEK_SCHEMA_1 = 'W' + str(int(WEEK_ID) - 1)
    MONTH_ID = str(csdatetools.time_id_to_broadcast_month(csdatetools.get_first_time_id_in_week_id(int(WEEK_ID))))

    Total_Audience = BashOperator(
        task_id='Total_Audience',
        bash_command='snowsql --config {}'.format(CONFIG_PATH) +
                     ' -f {}/etv_total_audience.sql '.format(BASE_SQL_PATH) +
                     ' -D WEEK_SCHEMA_0={} '.format(WEEK_SCHEMA_0) +
                     ' -D WEEK_SCHEMA_1={} '.format(WEEK_SCHEMA_1) +
                     ' -D MONTH_0={} '.format(MONTH_ID) +
                     ' -D WEEK_0={} '.format(WEEK_ID)
        , dag=dag)
    return dag

etv_total_audience_dag = etv_total_audience('etv_total_audience')
