from datetime import datetime
from datetime import timedelta

from airflow.models.dag import DAG
from airflow.models.variable import Variable
from airflow.operators.bash_operator import BashOperator

default_args = {
    'owner': 'vkomuravelli',
    'depends_on_past': False,
    'start_date': datetime(2019, 1, 31),
    'email': [
        'sgude@comscore.com, vkomuravelli@comscore.com, ' +
        'skamidi@comscore.com, rbhallamudi@comscore.com, gmuthyala@comscore.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=1)
}

dag = DAG('ETV_TV_DataQuality_Check', default_args=default_args, schedule_interval=None)

# Variables	
BASE_SQL_PATH = Variable.get("base_sql_path")  # '/mapr/ri1.comscore.com/test_data/census/dags/SnowflakeSQL/' #
WEEK_ID = Variable.get("week_id")  # '970' #

DataQuality_Check_Bounding = BashOperator(task_id='DataQuality_Check_Bounding',
                                          bash_command='/home/vkomuravelli/datacheck/dataqualitycheck.sh bounding 989' +
                                                       ' DVR  \"@table_name=W989.ETV_DVR_LATTE_INCLUSIVE_BOUNDED ' +
                                                       '@table_previous=W989.ETV_DVR_LATTE_INCLUSIVE_BOUNDED\"',
                                          dag=dag
                                          )
