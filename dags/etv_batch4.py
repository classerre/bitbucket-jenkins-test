__author__ = " vidyan komuravelli "

import airflow

"""  
       importing dependencies for etv_batch4
"""
from etv_ott_dg_regression import etv_ott_dg_regression
from etv_total_digital_ott_tld_overlap import etv_total_digital_ott_tld_overlap
from etv_weekly_digital import etv_weekly_digital

from airflow.models import DAG
from airflow.models import Variable
from airflow.operators.subdag_operator import SubDagOperator
from datetime import datetime, timedelta
from airflow.executors.celery_executor import CeleryExecutor
from comscoreEmail import returnEmailOperator

BATCH_NAME = 'etv_batch4'
WEEK_ID = Variable.get("week_id")

args = {
    'owner': 'etv',
    'depends_on_past': False,
    'start_date': datetime(2019, 1, 24),
    'email': ['sgude@comscore.com', 'vkomuravelli@comscore.com', 'skamidi@comscore.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=1)
}


def etv_batch4(dag_name):
    dag = DAG(dag_name, default_args=args, schedule_interval=None)

    etv_weekly_digital_dag = SubDagOperator(
        executor=CeleryExecutor(),
        task_id='etv_weekly_digital',
        subdag=etv_weekly_digital(dag_name + '.etv_weekly_digital'),
        default_args=args,
        dag=dag)

    etv_ott_dg_regression_dag = SubDagOperator(
        executor=CeleryExecutor(),
        task_id='etv_ott_dg_regression',
        subdag=etv_ott_dg_regression(dag_name + '.etv_ott_dg_regression'),
        default_args=args,
        dag=dag)

    etv_total_digital_ott_tld_overlap_dag = SubDagOperator(
        executor=CeleryExecutor(),
        task_id='etv_total_digital_ott_tld_overlap', 
        subdag=etv_total_digital_ott_tld_overlap(dag_name + '.etv_total_digital_ott_tld_overlap'),
        default_args=args,
        dag=dag)
          
    # Dependencies :
    etv_total_digital_ott_tld_overlap_dag.set_upstream(etv_weekly_digital_dag)
    etv_total_digital_ott_tld_overlap_dag.set_upstream(etv_ott_dg_regression_dag)

    notify_success = returnEmailOperator(dag, BATCH_NAME, WEEK_ID)
    notify_success.set_upstream(etv_total_digital_ott_tld_overlap_dag)
    return dag

etv_batch4_dag = etv_batch4(BATCH_NAME)



