from datetime import datetime, timedelta
from airflow.contrib.operators.ssh_operator import SSHOperator
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator
from airflow.contrib.hooks.ssh_hook import SSHHook

# scripts
cluster_creation = '/mapr/ia1.comscore.com/bin/staging/etv/dags/Create_EMR_Cluster.sh '
connection_update = '/mapr/ia1.comscore.com/bin/staging/etv/dags/Update_EMR_Connection.sh '
terminate_cluster = '/mapr/ia1.comscore.com/bin/staging/etv/dags/Terminate_EMR_Cluster.sh '

class emr_tasks():
    def __init__(self, dag):
        self.dag = dag
        
    def create_emr_cluster(self, jobName, instanceCount, ebsSizeInGB):
        Trigger_Create_Cluster = BashOperator(
            task_id='Create_EMRCluster',
            bash_command=cluster_creation + jobName + ' ' + instanceCount + ' ' + ebsSizeInGB,
            dag=self.dag
        )
        return Trigger_Create_Cluster

    def sleep_operation(self):
        WaitForClusterCreation = BashOperator(
            task_id='Sleep', bash_command='sleep 600', dag=self.dag
        )
        return WaitForClusterCreation

    def update_airflow_emr_connection(self, jobName, connId):
        UpdateConnection = BashOperator(
            task_id='Update_EMR_Connection',
            bash_command=connection_update + jobName + ' ' + connId,
            dag=self.dag
        )
        return UpdateConnection

    def copy_binaries(self,sshHook): 
    
        CopyBinaries = SSHOperator(task_id='copy_binaries_from_s3',
                                   command='aws s3 cp s3://csxpdev-xp-east/etv/bin/ . --recursive ',
                                   dag=self.dag,
                                   ssh_hook=sshHook)
        return CopyBinaries

    def wait_for_5mins(self, dag):
        WaitFor5Mins = BashOperator(
            task_id='Sleep2',
            bash_command='sleep 300',
            dag=dag,
            trigger_rule="all_done"
        )

    def delete_cluster(self, jobName):
        DeleteCluster = BashOperator(
            task_id='Delete_EMRCluster',
            bash_command=terminate_cluster + jobName,
            dag=self.dag,
            trigger_rule="all_done"
        )
        return DeleteCluster

    def get_ssh_hook(self, connection_name):
        return SSHHook(ssh_conn_id=connection_name)
