from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from datetime import datetime, timedelta  
from airflow.models import Variable
from airflow.contrib.operators.ssh_operator import SSHOperator
from airflow.contrib.hooks.ssh_hook import SSHHook
from emr_operations import emr_tasks
from comscoreEmail import notify_email
from csdatetools import csdatetools

default_args = {
    'owner': 'etv',
    'depends_on_past': False,
    'start_date': datetime(2019, 1, 31),
    'email': ['sgude@comscore.com, vkomuravelli@comscore.com, skamidi@comscore.com, rbhallamudi@comscore.com, gmuthyala@comscore.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'on_failure_callback': notify_email,
    'retries': 0,
    'retry_delay': timedelta(minutes=1)
}

def etv_weekly_digital(dag_name):
    dag = DAG(dag_name, default_args=default_args, schedule_interval=None)
    emr = emr_tasks(dag)
    sshHook = emr.get_ssh_hook("weekly_digital")

    # Variables    
    BASE_SQL_PATH = Variable.get("base_sql_path")
    WEEK_ID =  Variable.get("week_id")
    CONFIG_PATH = Variable.get("config_path")
    RUN_BATCH = Variable.get("etv_weekly_digital_run_batch")
    PROVIDER_TRUNC = Variable.get("etv_weekly_digital_provider_trunc")
    BASE_SQL_PATH = BASE_SQL_PATH + 'etv_weekly_digital/' 
    BASE_s3_LOC_PATH = Variable.get("base_s3_loc") +'/'   #"s3://csxpdev-xp-east/etv/HDFS/"
    BASE_s3_PATH_WEEKLY = Variable.get("base_s3_path")+'/'+WEEK_ID+'w'
    BASE_s3_PATH_UPSTREAM_PATH = BASE_s3_LOC_PATH+Variable.get("upstream_path")+'/'
    UTILITY_PATH = Variable.get("base_sql_path") + 'utilities'

    #emr dependencies
    PIG_FILE_LOCATION = '/home/hadoop/TVD_Applied_DD_ABC_Dedup.pig'
    ETV_BASE_PWE_PIG_FILE_LOCATION='/home/hadoop/etv_base_pwe.pig'
    SQUEL_JAR='/home/hadoop/squeal.jar'
    ETV_DIGITAL_UV_JAR='/home/hadoop/etv-digital-uvs.jar'
    WEEKLY_UV_TARGET_PIG_FILE_LOCATION='/home/hadoop/weekly_uv_targets.pig'
    STREAM_SENSE_COMMON_PIG_UDF_JAR_LOCATION ='/home/hadoop/census-stream-sense-common-pig-udf.jar' 
    CS_PIG_UTIL_JAR='/home/hadoop/cs-pig-utils.jar'
    PWE_CBP_PROCESS_PIG_JAR='/home/hadoop/pwe-cbp-processes.jar'
    UV_TARGET_PROJECTION_JAR_FILE_LOCATION ='/home/hadoop/uv-targets-projection.jar'




    #Derived Variables
    Week = 'w' + WEEK_ID;
    PREVIOUS_WEEK_1 =   str(int(WEEK_ID) - 1)+'w'
    PREVIOUS_WEEK_2 =  str(int(WEEK_ID) - 2)+'w'
    PREVIOUS_WEEK_3 =  str(int(WEEK_ID) - 3)+'w'
    PREVIOUS_WEEK_4 =  str(int(WEEK_ID) - 4)+'w'
    PREVIOUS_WEEK_5 =  str(int(WEEK_ID) - 5)+'w'

    PREVIOUS_WEEK_ID_1 = str(int(WEEK_ID) - 1)
    PREVIOUS_WEEK_ID_2 = str(int(WEEK_ID) - 2)
    PREVIOUS_WEEK_ID_3 = str(int(WEEK_ID) - 3)
    PREVIOUS_WEEK_ID_4 = str(int(WEEK_ID) - 4)
    PREVIOUS_WEEK_ID_5 = str(int(WEEK_ID) - 5)

    # Function calls 
    MONTH_ID = str(csdatetools.time_id_to_month_id(csdatetools.get_last_time_id_in_week_id(int(WEEK_ID)))); # '228' 
    TIME_TYPE='2'

    IS_LAST_WEEKID = 'FALSE'

    if MONTH_ID != str(csdatetools.time_id_to_month_id(csdatetools.get_last_time_id_in_week_id(int(WEEK_ID) + 1))):
        IS_LAST_WEEKID = 'TRUE'

    if RUN_BATCH == 'true':

        delete_Create_TSV_Lookup = BashOperator(
            task_id='delete_Create_TSV_Lookup'
            , bash_command='{}/deleteS3Object.sh '.format(UTILITY_PATH) +
                           '--s3Path {0}/tsv_base/'.format(BASE_s3_LOC_PATH+str(WEEK_ID)+'w')
            , dag=dag
        )
        Create_TSV_Lookup = BashOperator(task_id='Create_TSV_Lookup',
                                            bash_command='snowsql --config ' + CONFIG_PATH + ''
                                                ' -f '+ BASE_SQL_PATH + 'weekly_create_tsv_base.sql'
                                                ' -D  WEEK_SCHEMA_0=' + Week  + ''
                                                ' -D withHeader='+'False'+''
                                                ' -D exportType='+'False'+''
                                                ' -D outputFormat=my_csv_format_uncompressed'+''
                                                ' -D  s3Path='+BASE_s3_PATH_WEEKLY+'/tsv_base/', 
                                            dag=dag                                        
                                            )
        Create_TSV_Lookup.set_upstream(delete_Create_TSV_Lookup)

        delete_Create_Extended_Demos = BashOperator(
            task_id='delete_Create_Extended_Demos'
            , bash_command='{}/deleteS3Object.sh '.format(UTILITY_PATH) +
                           '--s3Path {0}/weekly_extended_demos_lookup/'.format(BASE_s3_LOC_PATH+str(WEEK_ID)+'w')
            , dag=dag
        )
        delete_Create_Extended_Demos.set_upstream(Create_TSV_Lookup)
        Create_Extended_Demos = BashOperator(task_id='Create_Extended_Demos',
                                            bash_command='snowsql --config ' + CONFIG_PATH + ''
                                                ' -f '+ BASE_SQL_PATH + 'weekly_extended_demos_lookup.sql'
                                                ' -D  WEEK_SCHEMA_0=' + Week  + ''
                                                ' -D withHeader='+'False'+''
                                                ' -D exportType='+'False'+''
                                                ' -D outputFormat=my_csv_format_uncompressed'+''
                                                ' -D  s3Path='+BASE_s3_PATH_WEEKLY+'/weekly_extended_demos_lookup/',
                                            dag=dag                                        
                                            )

        Create_Extended_Demos.set_upstream(delete_Create_Extended_Demos)

        if PROVIDER_TRUNC == 'true':
            delete_Create_WhiteList = BashOperator(
                task_id='delete_Create_WhiteList'
                , bash_command='{}/deleteS3Object.sh '.format(UTILITY_PATH) +
                               '--s3Path {0}/ETV_Digital_whitelist/'.format(BASE_s3_LOC_PATH+str(WEEK_ID)+'w')
                , dag=dag
            )
            Create_WhiteList = BashOperator(task_id='Create_WhiteList',
                                            bash_command='snowsql --config ' + CONFIG_PATH + ''
                                                ' -f '+ BASE_SQL_PATH + 'whitelist.sql'
                                                ' -D  WEEK_SCHEMA_0=' + Week  + ''
                                                ' -D withHeader='+'False'+''
                                                ' -D exportType='+'False'+''
                                                ' -D outputFormat=my_csv_format_uncompressed'+''
                                                ' -D s3Path='+BASE_s3_PATH_WEEKLY+'/ETV_Digital_whitelist/', 
                                            dag=dag                                        
                                            )
            delete_Create_WhiteList.set_upstream(Create_Extended_Demos)
            Create_WhiteList.set_upstream(delete_Create_WhiteList)

        ABC_Dedupe_TVD_Daily_Duration = SSHOperator(task_id='ABC_Dedupe_TVD_Daily_Duration',
                                                    command='PIG_HEAPSIZE=8192 pig -x mr' 
                                                        ' -f '+PIG_FILE_LOCATION+''
                                                        ' -p census_stream_sense_common_udf='+STREAM_SENSE_COMMON_PIG_UDF_JAR_LOCATION+''
                                                        ' -p distinct_keywords_file='+BASE_s3_PATH_UPSTREAM_PATH+'monthly/'+MONTH_ID+'m/distinct_keywords/distinct_keywords_'+MONTH_ID+'m.txt'
                                                        ' -p keywords_config='+BASE_s3_PATH_UPSTREAM_PATH+'public/streaming_type_config/streaming_type_config.txt'
                                                        ' -p whitelist='+BASE_s3_LOC_PATH+str(WEEK_ID)+'w/ETV_Digital_whitelist/'
                                                        ' -p timetype='+str(TIME_TYPE)+''
                                                        ' -p monthid='+str(MONTH_ID)+''
                                                        ' -p job_name=ABC_Dedupe_TVD_DD_'+Week+''
                                                        ' -p pool=prod.monthly'
                                                        ' -p exclusionFile='+BASE_s3_PATH_UPSTREAM_PATH+'public/abc_dedup/abcDedupConfig.txt'
                                                        ' -p output='+BASE_s3_LOC_PATH+str(WEEK_ID)+'w/abc_deduped_tvd_dd/'
                                                        ' -p daily_duration=\''+BASE_s3_LOC_PATH+str(WEEK_ID)+'w/daily_duration_tvd_apply/finalclose/'+'{'+WEEK_ID+'w,'+PREVIOUS_WEEK_1+','+PREVIOUS_WEEK_2+','+PREVIOUS_WEEK_3+','+PREVIOUS_WEEK_4+','+PREVIOUS_WEEK_5+'}/\'',
                                                dag=dag,
                                                ssh_hook=sshHook
                                                )
        ETV_Base_PWE = SSHOperator(task_id='ETV_Base_PWE',
                                    command='pig -x mr' 
                                        ' -f '+ETV_BASE_PWE_PIG_FILE_LOCATION+''
                                        ' -p job_name=ETV_Digital_Weekly_Base_PWE_'+Week+''
                                        ' -p pool=prod.monthly' 
                                        ' -p daily_duration='+BASE_s3_LOC_PATH+str(WEEK_ID)+'w/abc_deduped_tvd_dd/' 
                                        ' -p activity='+BASE_s3_PATH_UPSTREAM_PATH+str(WEEK_ID)+'w/person_affinity_activity/part*' 
                                        ' -p census_stream_sense_common_udf='+STREAM_SENSE_COMMON_PIG_UDF_JAR_LOCATION+'' 
                                        ' -p cs_pig_utils='+CS_PIG_UTIL_JAR+'' 
                                        ' -p distinct_keywords='+BASE_s3_PATH_UPSTREAM_PATH+'monthly/'+MONTH_ID+'m/distinct_keywords/distinct_keywords_'+MONTH_ID+'m.txt' 
                                        ' -p keywords_config='+BASE_s3_PATH_UPSTREAM_PATH+'public/streaming_type_config/streaming_type_config.txt' 
                                        ' -p tsv_base='+BASE_s3_LOC_PATH+str(WEEK_ID)+'w/tsv_base/'
                                        ' -p extended_demos='+BASE_s3_LOC_PATH+str(WEEK_ID)+'w/weekly_extended_demos_lookup/'
                                        ' -p broadcast_time_utils_jar='+PWE_CBP_PROCESS_PIG_JAR+'' 
                                        ' -p output='+BASE_s3_LOC_PATH+str(WEEK_ID)+'w/base_pwe'
                                        ' -p squeal_jar='+SQUEL_JAR+'' 
                                        ' -p staticTimeId='+str(csdatetools.get_first_time_id_in_week_id(int(WEEK_ID)))+'' ,
                                dag=dag,
                                ssh_hook=sshHook
                                )
        
        UV_Targets = SSHOperator(task_id='UV_Targets',
                                    command='PIG_HEAPSIZE=8192 pig '
                                        ' -Dpig.additional.jars='+UV_TARGET_PROJECTION_JAR_FILE_LOCATION+''
                                        ' -x mr -f '+WEEKLY_UV_TARGET_PIG_FILE_LOCATION+'' 
                                        ' -p job_name=ETV_Digital_Weekly_UV_Targets_'+str(WEEK_ID)+'w'
                                        ' -p pool=prod.monthly' 
                                        ' -p output='+BASE_s3_LOC_PATH+str(WEEK_ID)+'w/targets/' 
                                        ' -p daily_duration='+BASE_s3_LOC_PATH+str(WEEK_ID)+'w/abc_deduped_tvd_dd/'  
                                        ' -p survival_curve='+BASE_s3_PATH_UPSTREAM_PATH+'static/uv_projection/dev_handoff_discount_factors_etv_197m.txt' 
                                        ' -p ppc_intermediate='+BASE_s3_LOC_PATH+str(WEEK_ID)+'w/base_pwe/ppc_intermediate' 
                                        ' -p census_stream_sense_common_udf='+STREAM_SENSE_COMMON_PIG_UDF_JAR_LOCATION+'' 
                                        ' -p cs_pig_utils='+ CS_PIG_UTIL_JAR+''
                                        ' -p distinct_keywords='+BASE_s3_PATH_UPSTREAM_PATH+'monthly/'+MONTH_ID+'m/distinct_keywords/distinct_keywords_'+MONTH_ID+'m.txt'  
                                        ' -p keywords_config='+BASE_s3_PATH_UPSTREAM_PATH+'public/streaming_type_config/streaming_type_config.txt'  
                                        ' -p ETV_DIGITAL_UV_JAR='+ETV_DIGITAL_UV_JAR,
                                dag=dag,
                                ssh_hook=sshHook
                                )
        UV_Overlap_Calculation = SSHOperator(task_id='UV_Overlap_Calculation',
                                            command='hadoop jar ' +UV_TARGET_PROJECTION_JAR_FILE_LOCATION+''
                                            ' com.comscore.etv.uv.overlap.UvOverlapMain' 
                                            ' -jobName ETV_Digital_Weekly_UV_Overlap_Calculation_'+str(WEEK_ID)+'w' 
                                            ' -overlapCoefficients '+BASE_s3_PATH_UPSTREAM_PATH+'static/uv_projection/uv_overlap_coefficients_866w.txt' 
                                            ' -uvTargets '+BASE_s3_LOC_PATH+str(WEEK_ID)+'w/targets/' 
                                            ' -output ' +BASE_s3_LOC_PATH+str(WEEK_ID)+'w/overlap/'  ,
                                        dag=dag,
                                        ssh_hook=sshHook
                                        )

        UV_MRS_Calculation = BashOperator(task_id='UV_MRS_Calculation',
                                        bash_command='snowsql --config ' + CONFIG_PATH + ''
                                            ' -f '+ BASE_SQL_PATH + 'weekly_uv_mrs.sql'
                                            ' -D  WEEK_SCHEMA_0=' + Week  + ''
                                            ' -D uvTargets='+BASE_s3_PATH_WEEKLY+'/overlap/'
                                            ' -D cbp_pwe='+BASE_s3_PATH_WEEKLY+'/base_pwe/cbp_pwe',
                                        dag=dag                                        
                                        )

        UV_Projection = BashOperator(task_id='UV_Projection',
                                        bash_command='snowsql --config ' + CONFIG_PATH + ''
                                            ' -f '+ BASE_SQL_PATH + 'weekly_uv_projection.sql'
                                            ' -D  WEEK_SCHEMA_0=' + Week  + ''
                                            ' -D  additiveTargets='+BASE_s3_PATH_WEEKLY+'/overlap/', 
                                        dag=dag                                        
                                        )

        TSV_Reporting_Windows = BashOperator(task_id='TSV_Reporting_Windows',
                                        bash_command='snowsql --config ' + CONFIG_PATH + ''
                                            ' -f '+ BASE_SQL_PATH + 'calculate_tsv_reporting_windows.sql'
                                            ' -D  WEEK_ID=' + WEEK_ID  + ''
                                            ' -D  WEEK_SCHEMA_0=' + Week  + ''
                                            ' -D  month_id=' + MONTH_ID  + ''
                                            ' -D  last_weekid_boolean=' + IS_LAST_WEEKID  + '', 
                                        dag=dag                                        
                                        )

        Digital_PWE_Transform = BashOperator(task_id='Digital_PWE_Transform',
                                        bash_command='snowsql --config ' + CONFIG_PATH + ''
                                            ' -f '+ BASE_SQL_PATH + 'weekly_pwe_transform.sql'
                                            ' -D  WEEK_SCHEMA_0=' + Week  + '', 
                                        dag=dag                                        
                                        )

        ETV_Digital_Daily_Additives = BashOperator(task_id= 'ETV_Digital_Daily_Additives',
                                        bash_command='python ' + BASE_SQL_PATH + 'digital_step_2.py ' + PREVIOUS_WEEK_ID_1,
                                           dag=dag
                                        )

        Digital_UVs = BashOperator(task_id= 'DIGITAL_UVs',
                                    bash_command='snowsql --config ' + CONFIG_PATH + ''
                                              ' -f '+ BASE_SQL_PATH + 'digital_step_2.sql'
                                              ' -D  WEEK_SCHEMA_0='+ Week  +''
                                            ' -D  WEEK_SCHEMA_1=w' + PREVIOUS_WEEK_ID_1  + ''
                                            ' -D  WEEK_SCHEMA_3=w' + PREVIOUS_WEEK_ID_3  + ''
                                                ' -D  WEEK_0=' + WEEK_ID  +'',
                                    dag=dag)    

        Digital_Venti_Transform = BashOperator(task_id='Digital_Venti_Transform',
                                        bash_command='snowsql --config ' + CONFIG_PATH + ''
                                            ' -f '+ BASE_SQL_PATH + 'weekly_venti_transform.sql'
                                            ' -D  WEEK_SCHEMA_0=' + Week  + '', 
                                        dag=dag                                        
                                        )
        
     

    if RUN_BATCH == 'true':
        Create_Extended_Demos.set_upstream(Create_TSV_Lookup)
        temp =Create_Extended_Demos;
        if PROVIDER_TRUNC == 'true':
            Create_WhiteList.set_upstream(Create_Extended_Demos)
            ABC_Dedupe_TVD_Daily_Duration.set_upstream(Create_WhiteList)
            temp = Create_WhiteList
        else:
            ABC_Dedupe_TVD_Daily_Duration.set_upstream(Create_Extended_Demos) 
        temp >> emr.create_emr_cluster("ETV_WEEKLY_DIGITAL","40","3000") >> emr.sleep_operation() >> emr.update_airflow_emr_connection("ETV_WEEKLY_DIGITAL","weekly_digital") >>  emr.copy_binaries(sshHook) >> ABC_Dedupe_TVD_Daily_Duration  >> ETV_Base_PWE >> UV_Targets >> UV_Overlap_Calculation >> emr.delete_cluster("ETV_WEEKLY_DIGITAL")
        UV_MRS_Calculation.set_upstream(UV_Overlap_Calculation)
        UV_Projection.set_upstream(UV_MRS_Calculation)
        TSV_Reporting_Windows.set_upstream(UV_Projection)
        Digital_PWE_Transform.set_upstream(TSV_Reporting_Windows)
        ETV_Digital_Daily_Additives.set_upstream(Digital_PWE_Transform)
        Digital_UVs.set_upstream(ETV_Digital_Daily_Additives)
        Digital_Venti_Transform.set_upstream(Digital_UVs)
    return dag

etv_weekly_digital_dag = etv_weekly_digital('etv_weekly_digital')