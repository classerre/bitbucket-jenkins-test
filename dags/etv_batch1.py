__author__ = " Vidyan Komuravelli "

"""
       importing dependencies for etv_batch1
"""
from etv_tv_live_viewership import etv_tv_live_viewership
from etv_tv_dvr_vod_hwe import etv_tv_dvr_vod_hwe
from etv_tv_hwe_weights import etv_tv_hwe_weights
from etv_tv_live_espresso_agg_hadoop import etv_tv_live_espresso_agg_hadoop
from etv_tv_latte_pig import etv_tv_latte_pig

from airflow.models import DAG
from airflow.models import Variable
from airflow.operators.subdag_operator import SubDagOperator
from datetime import datetime, timedelta
from airflow.executors.celery_executor import CeleryExecutor
from comscoreEmail import returnEmailOperator

BATCH_NAME = 'etv_batch1'
WEEK_ID = Variable.get("week_id")

args = {
    'owner': 'etv',
    'depends_on_past': False,
    'start_date': datetime(2019, 1, 24),
    'email': ['sgude@comscore.com', 'vkomuravelli@comscore.com', 'skamidi@comscore.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=1)
}


def etv_batch1(dag_name):
    dag = DAG(dag_name, default_args=args, schedule_interval=None)

    etv_live_viewership_dag = SubDagOperator(
        executor=CeleryExecutor(),
        task_id='etv_tv_live_viewership',
        subdag=etv_tv_live_viewership(dag_name + '.etv_tv_live_viewership'),
        default_args=args,
        dag=dag)

    etv_tv_dvr_vod_hwe_dag = SubDagOperator(
        executor=CeleryExecutor(),
        task_id='etv_tv_dvr_vod_hwe',
        subdag=etv_tv_dvr_vod_hwe(dag_name + '.etv_tv_dvr_vod_hwe'),
        default_args=args,
        dag=dag)

    etv_tv_hwe_weights_dag = SubDagOperator(
        executor=CeleryExecutor(),
        task_id='etv_tv_hwe_weights',
        subdag=etv_tv_hwe_weights(dag_name + '.etv_tv_hwe_weights'),
        default_args=args,
        dag=dag)

    etv_tv_live_espresso_agg_hadoop_dag = SubDagOperator(
        executor=CeleryExecutor(),
        task_id='etv_tv_live_espresso_agg_hadoop',
        subdag=etv_tv_live_espresso_agg_hadoop(dag_name + '.etv_tv_live_espresso_agg_hadoop'),
        default_args=args,
        dag=dag)

    etv_tv_latte_pig_dag = SubDagOperator(
        executor=CeleryExecutor(),
        task_id='etv_tv_latte_pig',
        subdag=etv_tv_latte_pig(dag_name + '.etv_tv_latte_pig'),
        default_args=args,
        dag=dag)

    # Dependencies :
    etv_tv_live_espresso_agg_hadoop_dag.set_upstream(etv_tv_hwe_weights_dag)
    etv_tv_latte_pig_dag.set_upstream(etv_tv_live_espresso_agg_hadoop_dag)
    etv_tv_latte_pig_dag.set_upstream(etv_live_viewership_dag)

    notify_success = returnEmailOperator(dag, BATCH_NAME, WEEK_ID)
    notify_success.set_upstream(etv_tv_latte_pig_dag)
    return dag


etv_batch1_dag = etv_batch1(BATCH_NAME)
