from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
import re
from airflow.models import Variable
from comscoreEmail import notify_email
from csdatetools import csdatetools


default_args = {
    'owner': 'etv',
    'depends_on_past': False,
    'start_date': datetime(2019, 1, 31),
    'email': ['sgude@comscore.com, vkomuravelli@comscore.com, skamidi@comscore.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=3),
    'on_failure_callback': notify_email
}

BASE_SQL_PATH = Variable.get("base_sql_path") + "etv_tv_vod_batch/"
BASE_PYTHON_PATH = Variable.get("base_sql_path") + "etv_tv_vod_batch/"
BASE_BASH_PATH = Variable.get("base_sql_path") + "etv_tv_vod_batch/"
WEEK_ID = Variable.get("week_id")
CONFIG_PATH = Variable.get("config_path")
DQC_PATH = Variable.get("data_quality_check_path")
DATA_QUALITY_CHECK = Variable.get("data_quality_check")



WEEK_5 = str(int(WEEK_ID)-5)
WEEK_4 = str(int(WEEK_ID)-4)
WEEK_3 = str(int(WEEK_ID)-3)
WEEK_2 = str(int(WEEK_ID)-2)
WEEK_1 = str(int(WEEK_ID)-1)
WEEK_0 = WEEK_ID
WEEK_SCHEMA_5  = 'W'+ str(int(WEEK_ID)-5)
WEEK_SCHEMA_4  = 'W'+ str(int(WEEK_ID)-4)
WEEK_SCHEMA_3  = 'W'+ str(int(WEEK_ID)-3)
WEEK_SCHEMA_2  = 'W'+ str(int(WEEK_ID)-2)
WEEK_SCHEMA_1  = 'W'+ str(int(WEEK_ID)-1)
WEEK_SCHEMA_0  = 'W'+ WEEK_ID
WEEKID_0 = int(WEEK_0)

start_time_id = str(csdatetools.get_first_time_id_in_week_id(int(WEEK_ID)))
month_id = str(csdatetools.time_id_to_broadcast_month(int(start_time_id)))
end_time_id = str(csdatetools.get_last_time_id_in_week_id(int(WEEK_ID)))
month_id_min2= str(int(csdatetools.time_id_to_month_id(int(end_time_id)))-2)
platform = 'VOD'
tunes_table = 'comscore_ren_vod_repo'
tunes_type = 'viewership'

processes = Variable.get("etv_tv_vod_batch_Processlist")
process_list = processes.split(",") #['ESP','VODVIEW','VODINCSMALL', 'VODLATTE', 'VODINC', 'VODBOUNDING', 'MONTHLY']


def etv_tv_vod_batch(dag_name):
    dag =  DAG(dag_name, default_args=default_args, schedule_interval=None)


    if 'ESP' in process_list:
        etv_espresso_vod_intensity_adjusted = BashOperator(
                task_id= 'etv_espresso_vod_intensity_adjusted',
                bash_command= re.sub('\\s+',' ',"""snowsql --config {0}
                                -f {1}
                                -D WEEK_5={2}
                                -D WEEK_SCHEMA_5={3}
                                -D WEEK_4={4}
                                -D WEEK_SCHEMA_4={5}
                                -D WEEK_3={6}
                                -D WEEK_SCHEMA_3={7}
                                -D WEEK_2={8}
                                -D WEEK_SCHEMA_2={9}
                                -D WEEK_1={10}
                                -D WEEK_SCHEMA_1={11}
                                -D WEEK_0={12}
                                -D WEEK_SCHEMA_0={13}
                                -D month_id_min2={14}
                                -D month_id={15}"""\
                                .format(CONFIG_PATH, BASE_SQL_PATH + "vod_intensity_adjusted.sql"\
                                , WEEK_5, WEEK_SCHEMA_5\
                                , WEEK_4, WEEK_SCHEMA_4\
                                , WEEK_3, WEEK_SCHEMA_3\
                                , WEEK_2, WEEK_SCHEMA_2\
                                , WEEK_1, WEEK_SCHEMA_1\
                                , WEEK_0, WEEK_SCHEMA_0\
                                ,month_id_min2, month_id ))\
                                ,dag=dag)

        etv_espresso_vod_regcoef_apply_prob_reweight = BashOperator(
                task_id= 'etv_espresso_vod_regcoef_apply_prob_reweight',
                bash_command= re.sub('\\s+',' ',"""snowsql --config {0}
                                -f {1}
                                -D WEEK_SCHEMA_0={2}"""\
                                .format(CONFIG_PATH, BASE_SQL_PATH+"regcoef_apply_prob_reweight.sql"\
                                ,WEEK_SCHEMA_0))\
                                ,dag=dag)

        if DATA_QUALITY_CHECK == "TRUE":
            dqc_vod_reweight = BashOperator(
                task_id='dqc_vod_reweight',
                bash_command='{0}/dataqualitycheck.sh Espresso espresso_reweight {1} VOD \"@table_name={2}.etv_experian_vod_espresso_reweight @table_previous={3}.etv_experian_vod_espresso_reweight\" {0} {4}'.format(DQC_PATH, WEEK_ID, WEEK_SCHEMA_0, WEEK_SCHEMA_1, CONFIG_PATH)
            , dag=dag)


        truncate_dictionary_for_processing = BashOperator(
                task_id= 'truncate_dictionary_for_processing',
                bash_command= re.sub('\\s+',' ',"""snowsql --config {0}
                                -f {1}
                                -D WEEK_SCHEMA_0={2}"""\
                                .format(CONFIG_PATH, BASE_SQL_PATH+"VOD_dictionaries_subset.sql"\
                                ,WEEK_SCHEMA_0))\
                                ,dag=dag)


        etv_espresso_vod_agg = BashOperator(
                task_id= 'etv_espresso_vod_agg',
                bash_command= re.sub('\\s+',' ',"""snowsql --config {0}
                                -f {1}
                                -D WEEK_0={2}
                                -D WEEK_SCHEMA_0={3}"""\
                                .format(CONFIG_PATH, BASE_SQL_PATH+"vod_espresso_agg.sql"\
                                ,WEEK_0,WEEK_SCHEMA_0))\
                                ,dag=dag)

        if DATA_QUALITY_CHECK == "TRUE":
            dqc_vod_agg = BashOperator(
                task_id='dqc_vod_agg',
                bash_command='{0}/dataqualitycheck.sh Espresso espresso_agg {1} VOD \"@table_name={2}.etv_vod_espresso_agg_weights @table_previous={3}.etv_vod_espresso_agg_weights\" {0} {4}'.format(DQC_PATH, WEEK_ID, WEEK_SCHEMA_0, WEEK_SCHEMA_1, CONFIG_PATH)
            , dag=dag)

    #end 'ESP' in process_list

    if 'VODVIEW' in process_list:
        vod_viewership = BashOperator(
                task_id= 'vod_viewership',
                bash_command= re.sub('\\s+',' ',"""snowsql --config {0}
                                -f {1}
                                -D WEEK_0={2}
                                -D WEEK_SCHEMA_0={3}
                                -D start_time_id={4}
                                -D end_time_id={5}"""\
                                .format(CONFIG_PATH, BASE_SQL_PATH+"vod_viewership.sql"\
                                ,WEEK_0, WEEK_SCHEMA_0, start_time_id, end_time_id))\
                                ,dag=dag)

    #end 'VODVIEW' in process_list

    if 'VODINCSMALL' in process_list:
        Input_Prep_and_Output_Tables_Creation = BashOperator(
                task_id= 'Input_Prep_and_Output_Tables_Creation',
                bash_command= re.sub('\\s+',' ',"""snowsql --config {0}
                                -f {1}
                                -D WEEK_5={2}
                                -D WEEK_SCHEMA_5={3}
                                -D WEEK_4={4}
                                -D WEEK_SCHEMA_4={5}
                                -D WEEK_3={6}
                                -D WEEK_SCHEMA_3={7}
                                -D WEEK_2={8}
                                -D WEEK_SCHEMA_2={9}
                                -D WEEK_1={10}
                                -D WEEK_SCHEMA_1={11}
                                -D WEEK_0={12}
                                -D WEEK_SCHEMA_0={13}"""\
                                .format(CONFIG_PATH, BASE_SQL_PATH+"inclusive_small_prep.sql"\
                                , WEEK_5, WEEK_SCHEMA_5\
                                , WEEK_4, WEEK_SCHEMA_4\
                                , WEEK_3, WEEK_SCHEMA_3\
                                , WEEK_2, WEEK_SCHEMA_2\
                                , WEEK_1, WEEK_SCHEMA_1\
                                , WEEK_0, WEEK_SCHEMA_0))\
                                ,dag=dag)

        DP = [35,34,33,32,31,30,29,28,27,26,16,12,10,9,8,2,1,0]

        for value in DP:

            bashtask = """tsv_Incremental_Small_for_DP_{0} = BashOperator(
                    task_id= 'tsv_Incremental_Small_for_DP_{0}',
                    bash_command= re.sub('\\s+',' ','snowsql --config {1}\
                                    -f {2}\
                                    -D WEEK_SCHEMA_0={3}\
                                    -D DP={0}\
                                    -D WEEK_0={4}')\
                                    ,dag=dag)"""\
                                    .format(value, CONFIG_PATH, BASE_SQL_PATH+"inclusive_small_daypart_loop.sql"\
                                    ,WEEK_SCHEMA_0, WEEK_0)
            exec(bashtask)

        if DATA_QUALITY_CHECK == "TRUE":
            dqc_vod_small = BashOperator(
                task_id='dqc_vod_small',
                bash_command='{0}/dataqualitycheck.sh Incremental small {1} VOD \"@table_name={2}.vod_inclusive_small @table_previous={3}.vod_inclusive_small\" {0} {4}'.format(DQC_PATH, WEEK_ID, WEEK_SCHEMA_0, WEEK_SCHEMA_1, CONFIG_PATH)
            , dag=dag)

    #end 'VODINCSMALL' in process_list
    if 'VODLATTE' in process_list:
        vod_input_format = BashOperator(
                task_id= 'vod_input_format',
                bash_command= re.sub('\\s+',' ',"""snowsql --config {0}
                                -f {1}
                                -D WEEK_SCHEMA_0={2}"""\
                                .format(CONFIG_PATH, BASE_SQL_PATH+"latte_0_input_format_vod.sql"\
                                ,WEEK_SCHEMA_0))\
                                ,dag=dag)

        if DATA_QUALITY_CHECK == "TRUE":
            dqc_vod_venti = BashOperator(
                task_id='dqc_vod_venti',
                bash_command='{0}/dataqualitycheck.sh Viewership viewership {1} VOD \"@table_name={2}.etv_vod_venti @table_previous={3}.etv_vod_venti\" {0} {4}'.format(DQC_PATH, WEEK_ID, WEEK_SCHEMA_0, WEEK_SCHEMA_1, CONFIG_PATH)
            , dag=dag)


        Prepare_Latte_input_data = BashOperator(
                task_id= 'Prepare_Latte_input_data',
                bash_command= re.sub('\\s+',' ',"""snowsql --config {0}
                                -f {1}
                                -D WEEK_SCHEMA_0={2}"""\
                                .format(CONFIG_PATH, BASE_SQL_PATH+"latte_1_data_prep.sql"\
                                ,WEEK_SCHEMA_0))\
                                ,dag=dag)

        Latte_Merge_final_table_creation = BashOperator(
                task_id= 'Latte_Merge_final_table_creation',
                bash_command= re.sub('\\s+',' ',"""snowsql --config {0}
                                -f {1}
                                -D WEEK_SCHEMA_0={2}"""\
                                .format(CONFIG_PATH, BASE_SQL_PATH+"latte_3_merge_output.sql"\
                                ,WEEK_SCHEMA_0))\
                                ,dag=dag)


        DP = [35,34,33,32,31,30,29,28,27,26,16,12,10,9,8,2,1,0]

        if DATA_QUALITY_CHECK == "TRUE":
            dqc_vod_latte = BashOperator(
                task_id='dqc_vod_latte',
                bash_command='{0}/dataqualitycheck.sh Latte latte {1} VOD \"@table_name={2}.etv_experian_vod_latte @table_previous={3}.etv_experian_vod_latte\" {0} {4}'.format(DQC_PATH, WEEK_ID, WEEK_SCHEMA_0, WEEK_SCHEMA_1, CONFIG_PATH)
            , dag=dag)

        for value in DP:

            bashtask = """Demodist_VPVH_CVF_for_Daypart_{0} = BashOperator(
                    task_id= 'Demodist_VPVH_CVF_for_Daypart_{0}',
                    bash_command= re.sub('\\s+',' ','snowsql --config {1}\
                                    -f {2}\
                                    -D WEEK_SCHEMA_0={3}\
                                    -D DAYPART_ID={0}\
                                    -D WEEK_0={4}')\
                                    ,dag=dag)"""\
                                    .format(value, CONFIG_PATH, BASE_SQL_PATH+"latte_2a_demodist_vpvh_cvf.sql"\
                                    ,WEEK_SCHEMA_0, WEEK_0)
            exec(bashtask)

            bashtask = """Latte_daypart_loop_for_Daypart_part1_{0} = BashOperator(
                    task_id= 'Latte_daypart_loop_for_Daypart_part1_{0}',
                    bash_command= re.sub('\\s+',' ','snowsql --config {1}\
                                    -f {2}\
                                    -D WEEK_SCHEMA_0={3}\
                                    -D DP={0}')\
                                    ,dag=dag)"""\
                                    .format(value, CONFIG_PATH, BASE_SQL_PATH+"latte_2_daypart_loop_part1.sql"\
                                    ,WEEK_SCHEMA_0, WEEK_0)
            exec(bashtask)

            bashtask = """Latte_daypart_loop_for_Daypart_func_{0} = BashOperator(
                    task_id='Latte_daypart_loop_for_Daypart_func_{0}',
                    bash_command='python {1} {2} {0} {3}',
                    dag=dag)""".format(value, BASE_PYTHON_PATH+'latte_2_daypart_loop.py'\
                                    ,WEEK_0, WEEK_SCHEMA_0 +'.vod_latte_tv_hierarchy' )

            exec(bashtask)

            bashtask = """Latte_daypart_loop_for_Daypart_part2_{0} = BashOperator(
                    task_id= 'Latte_daypart_loop_for_Daypart_part2_{0}',
                    bash_command= re.sub('\\s+',' ','snowsql --config {1}\
                                    -f {2}\
                                    -D WEEK_SCHEMA_0={3}\
                                    -D DP={0}')\
                                    ,dag=dag)"""\
                                    .format(value, CONFIG_PATH, BASE_SQL_PATH+"latte_2_daypart_loop_part2.sql"\
                                    ,WEEK_SCHEMA_0, WEEK_0)
            exec(bashtask)

            bashtask = """DP{0}_Output_Insert_to_the_Master_Table = BashOperator(
                    task_id= 'DP{0}_Output_Insert_to_the_Master_Table',
                    bash_command= re.sub('\\s+',' ','snowsql --config {1}\
                                    -f {2}\
                                    -D WEEK_SCHEMA_0={3}\
                                    -D DP={0}')\
                                    ,dag=dag)"""\
                                    .format(value, CONFIG_PATH, BASE_SQL_PATH+"latte_4_output_loop.sql"\
                                    ,WEEK_SCHEMA_0, WEEK_0)
            exec(bashtask)

            if DATA_QUALITY_CHECK == "TRUE":
                dqc_temp="""dqc_vod_latte.set_upstream(DP{0}_Output_Insert_to_the_Master_Table)""".format(value)
                exec(dqc_temp)


    #end 'VODLATTE' in process_list
    if 'VODINC' in process_list:

        Output_Table_Creation = BashOperator(
                task_id= 'Output_Table_Creation',
                bash_command= re.sub('\\s+',' ',"""snowsql --config {0}
                                -f {1}
                                -D WEEK_SCHEMA_0={2}"""\
                                .format(CONFIG_PATH, BASE_SQL_PATH+"incremental_pt0.sql"\
                                ,WEEK_SCHEMA_0))\
                                ,dag=dag)

        Week_to_week_MLE = BashOperator(
                task_id= 'Week_to_week_MLE',
                bash_command= re.sub('\\s+',' ',"""snowsql --config {0}
                                -f {1}
                                -D WEEK_0={2}
                                -D WEEK_SCHEMA_0={3}
                                -D WEEK_SCHEMA_1={4}"""\
                                .format(CONFIG_PATH, BASE_SQL_PATH+"incremental_pt1.sql"\
                                ,WEEK_0,WEEK_SCHEMA_0, WEEK_SCHEMA_1))\
                                ,dag=dag)

        Four_plus_and_Eight_plus = BashOperator(
                task_id= 'Four_plus_and_Eight_plus',
                bash_command= re.sub('\\s+',' ',"""snowsql --config {0}
                                -f {1}
                                -D WEEK_SCHEMA_0={2}
                                -D WEEK_SCHEMA_1={3}"""\
                                .format(CONFIG_PATH, BASE_SQL_PATH+"incremental_pt2.sql"\
                                ,WEEK_SCHEMA_0, WEEK_SCHEMA_1))\
                                ,dag=dag)


        Within_the_week_Prep = BashOperator(
                task_id= 'Within_the_week_Prep',
                bash_command= re.sub('\\s+',' ',"""snowsql --config {0}
                                -f {1}
                                -D WEEK_0={2}
                                -D WEEK_SCHEMA_0={3}"""\
                                .format(CONFIG_PATH, BASE_SQL_PATH+"incremental_pt3.sql"\
                                ,WEEK_0,WEEK_SCHEMA_0))\
                                ,dag=dag)


        for value in range(1,11):

            bashtask = """Within_the_week_loop_{0} = BashOperator(
                    task_id= 'Within_the_week_loop_{0}',
                    bash_command= re.sub('\\s+',' ','snowsql --config {1}\
                                    -f {2}\
                                    -D WEEK_SCHEMA_0={3}\
                                    -D CURRENT={0}')\
                                    ,dag=dag)"""\
                                    .format(value, CONFIG_PATH, BASE_SQL_PATH+"incremental_pt4.sql"\
                                    ,WEEK_SCHEMA_0, value)

            exec(bashtask)

        Final_output = BashOperator(
                task_id= 'Final_output',
                bash_command= re.sub('\\s+',' ',"""snowsql --config {0}
                                -f {1}
                                -D WEEK_0={2}
                                -D WEEK_SCHEMA_0={3}"""\
                                .format(CONFIG_PATH, BASE_SQL_PATH+"incremental_pt5.sql"\
                                ,WEEK_0,WEEK_SCHEMA_0))\
                                ,dag=dag)

        Clean_up_intermediate_tables = BashOperator(
                task_id= 'Clean_up_intermediate_tables',
                bash_command= re.sub('\\s+',' ',"""snowsql --config {0}
                                -f {1}
                                -D WEEK_SCHEMA_0={2}"""\
                                .format(CONFIG_PATH, BASE_SQL_PATH+"incremental_pt6.sql"\
                                ,WEEK_SCHEMA_0))\
                                ,dag=dag)

        if DATA_QUALITY_CHECK == "TRUE":
            dqc_vod_inc_latte = BashOperator(
                task_id='dqc_vod_inc_latte',
                bash_command='{0}/dataqualitycheck.sh Incremental incremental {1} VOD \"@table_name={2}.etv_vod_latte_inclusive @table_previous={3}.etv_vod_latte_inclusive\" {0} {4}'.format(DQC_PATH, WEEK_ID, WEEK_SCHEMA_0, WEEK_SCHEMA_1, CONFIG_PATH)
            , dag=dag)


    #end 'VODINC' in process_list
    if 'VODBOUNDING' in process_list:

        start_vod_bounding = BashOperator(
                task_id= 'start_vod_bounding',
                bash_command= """{0} {1} {2} {3} {4} {5} {6} {7} {8} {9}"""\
                                .format(BASE_BASH_PATH+ "Execute_tv_bounding_step.sh"\
                                ,BASE_SQL_PATH, CONFIG_PATH, WEEK_SCHEMA_0, WEEK_SCHEMA_1,WEEK_0, WEEK_1, platform, start_time_id, end_time_id)\
                                ,dag=dag)


        VOD_Bounding_Fix = BashOperator(
                task_id= 'VOD_Bounding_Fix',
                bash_command= re.sub('\\s+',' ',"""snowsql --config {0}
                                -f {1}
                                -D WEEK_SCHEMA_0={2}"""\
                                .format(CONFIG_PATH, BASE_SQL_PATH+"vod_bounding_fix.sql"\
                                ,WEEK_SCHEMA_0))\
                                ,dag=dag)

        if DATA_QUALITY_CHECK == "TRUE":
            dqc_vod_bounding = BashOperator(
                task_id='dqc_vod_bounding',
                bash_command='{0}/dataqualitycheck.sh Bounding bounding {1} VOD \"@table_name={2}.etv_vod_latte_inclusive_bounded @table_previous={3}.etv_vod_latte_inclusive_bounded\" {0} {4}'.format(DQC_PATH, WEEK_ID, WEEK_SCHEMA_0, WEEK_SCHEMA_1, CONFIG_PATH)
            , dag=dag)

            dqc_vod_bounding.set_upstream(VOD_Bounding_Fix)

    #end 'VODBOUNDING' in process_list


    current_bmonth = str(csdatetools.week_id_to_broadcast_month(int(WEEKID_0)))
    first_week_of_bmonth = str(csdatetools.get_first_week_id_in_broadcast_month(int(current_bmonth)))

    if int(WEEKID_0) > int(first_week_of_bmonth) and 'MONTHLY' in process_list:

        if (int(first_week_of_bmonth) + 1 == int(WEEKID_0)):
            single_platform_small_inputs_prep = BashOperator(
                task_id= 'single_platform_small_inputs_prep',
                bash_command= re.sub('\\s+',' ',"""snowsql --config {0}
                                -f {1}
                                -D WEEK_SCHEMA_0={2}
                                -D WEEK_SCHEMA_1={3}"""\
                                .format(CONFIG_PATH, BASE_SQL_PATH+"01_single_platform_small_inputs_prep_2nd_week.sql"\
                                ,WEEK_SCHEMA_0,WEEK_SCHEMA_1))\
                                ,dag=dag)
        else:
            get_espresso_agg_a_weeks = BashOperator(
                    task_id='get_espresso_agg_a_weeks',
                    bash_command='python {0} {1} {2}'\
                    .format(BASE_PYTHON_PATH + "get_espresso_agg_a_weeks.py"\
                    ,WEEK_0, platform),\
                    dag=dag)

            single_platform_small_inputs_prep = BashOperator(
                task_id= 'single_platform_small_inputs_prep',
                bash_command= re.sub('\\s+',' ',"""snowsql --config {0}
                                -f {1}
                                -D WEEK_SCHEMA_0={2}"""\
                                .format(CONFIG_PATH, BASE_SQL_PATH+"01_single_platform_small_inputs_prep.sql"\
                                ,WEEK_SCHEMA_0))\
                                ,dag=dag)

            if 'VODBOUNDING' in process_list:
                if DATA_QUALITY_CHECK == "TRUE":
                    get_espresso_agg_a_weeks.set_upstream(dqc_vod_bounding)
                else:
                    get_espresso_agg_a_weeks.set_upstream(VOD_Bounding_Fix)
                single_platform_small_inputs_prep.set_upstream(get_espresso_agg_a_weeks)

        single_platform_small = BashOperator(
                task_id= 'single_platform_small',
                bash_command= re.sub('\\s+',' ',"""snowsql --config {0}
                                -f {1}
                                -D WEEK_SCHEMA_0={2}"""\
                                .format(CONFIG_PATH, BASE_SQL_PATH+"02_single_platform_small.sql"\
                                ,WEEK_SCHEMA_0))\
                                ,dag=dag)


        if (int(first_week_of_bmonth) + 1 == int(WEEKID_0)):
            single_platform_overlap_w_final_format_if_last_week = BashOperator(
                task_id= 'single_platform_overlap_w_final_format_if_last_week',
                bash_command=re.sub('\\s+',' ',"""snowsql --config {0}
                                -f {1}
                                -D WEEK_SCHEMA_0={2}
                                -D WEEK_SCHEMA_1={3}
                                -D WEEK_0={4}"""\
                                .format(CONFIG_PATH, BASE_SQL_PATH+"03_single_platform_overlap_2nd_week.sql"\
                                ,WEEK_SCHEMA_0, WEEK_SCHEMA_1, WEEK_0))\
                                ,dag=dag)

        else:
            single_platform_overlap_w_final_format_if_last_week = BashOperator(
                task_id= 'single_platform_overlap_w_final_format_if_last_week',
                bash_command=re.sub('\\s+',' ',"""snowsql --config {0}
                                -f {1}
                                -D WEEK_SCHEMA_0={2}
                                -D WEEK_SCHEMA_1={3}
                                -D WEEK_0={4}"""\
                                .format(CONFIG_PATH, BASE_SQL_PATH+"03_single_platform_overlap.sql"\
                                ,WEEK_SCHEMA_0, WEEK_SCHEMA_1, WEEK_0))\
                                ,dag=dag)


        lastdateofbmonth = str(csdatetools.broadcast_month_to_last_time_id(int(current_bmonth)))


        if (int(WEEKID_0) == int(csdatetools.time_id_to_week_id(int(lastdateofbmonth)))):
            single_platform_final_format = BashOperator(
                task_id= 'single_platform_final_format',
                bash_command=re.sub('\\s+',' ',"""snowsql --config {0}
                                -f {1}
                                -D WEEK_SCHEMA_0={2}
                                -D MONTH_ID={3}"""\
                                .format(CONFIG_PATH, BASE_SQL_PATH+"04_single_platform_final_format.sql"\
                                ,WEEK_SCHEMA_0, month_id))\
                                ,dag=dag)

        VOD_Monthly_Fix = BashOperator(
                task_id= 'VOD_Monthly_Fix',
                bash_command=re.sub('\\s+',' ',"""snowsql --config {0}
                                -f {1}
                                -D WEEK_SCHEMA_0={2}"""\
                                .format(CONFIG_PATH, BASE_SQL_PATH+"vod_monthly_partial_fix.sql"\
                                ,WEEK_SCHEMA_0))\
                                ,dag=dag)


    # DAG DIRECTION
    if 'ESP' in process_list:
        if DATA_QUALITY_CHECK == "TRUE":
            etv_espresso_vod_regcoef_apply_prob_reweight.set_upstream(etv_espresso_vod_intensity_adjusted)
            dqc_vod_reweight.set_upstream(etv_espresso_vod_regcoef_apply_prob_reweight)
            truncate_dictionary_for_processing.set_upstream(dqc_vod_reweight)
        else:
            etv_espresso_vod_regcoef_apply_prob_reweight.set_upstream(etv_espresso_vod_intensity_adjusted)
            truncate_dictionary_for_processing.set_upstream(etv_espresso_vod_regcoef_apply_prob_reweight)
        etv_espresso_vod_agg.set_upstream(truncate_dictionary_for_processing)

    if 'VODINCSMALL' in process_list:
        if 'ESP' in process_list:
            if DATA_QUALITY_CHECK == "TRUE":
                dqc_vod_agg.set_upstream(etv_espresso_vod_agg)
                Input_Prep_and_Output_Tables_Creation.set_upstream(dqc_vod_agg)
            else:
                Input_Prep_and_Output_Tables_Creation.set_upstream(etv_espresso_vod_agg)

            for value in DP:
                dagDir = """tsv_Incremental_Small_for_DP_{0}.set_upstream(Input_Prep_and_Output_Tables_Creation)"""\
                            .format(value)
                exec(dagDir)

    if 'VODLATTE' in process_list:
        if 'VODVIEW' in process_list:
            vod_input_format.set_upstream(vod_viewership)
        if 'ESP' in process_list:
            if DATA_QUALITY_CHECK == "TRUE":
                vod_input_format.set_upstream(dqc_vod_agg)
            else:
                vod_input_format.set_upstream(etv_espresso_vod_agg)
        if DATA_QUALITY_CHECK == "TRUE":
            dqc_vod_venti.set_upstream(vod_input_format)
            Prepare_Latte_input_data.set_upstream(dqc_vod_venti)
            Latte_Merge_final_table_creation.set_upstream(dqc_vod_venti)
        else:
            Prepare_Latte_input_data.set_upstream(vod_input_format)
            Latte_Merge_final_table_creation.set_upstream(vod_input_format)

        for value in DP:
            dagDir = """Demodist_VPVH_CVF_for_Daypart_{}.set_upstream(Prepare_Latte_input_data)"""\
                        .format(value)
            exec(dagDir)
            dagDir = """Latte_daypart_loop_for_Daypart_part1_{0}.set_upstream(Demodist_VPVH_CVF_for_Daypart_{0})"""\
                        .format(value)
            exec(dagDir)
            dagDir = """Latte_daypart_loop_for_Daypart_func_{0}.set_upstream(Latte_daypart_loop_for_Daypart_part1_{0})"""\
                        .format(value)
            exec(dagDir)
            dagDir = """Latte_daypart_loop_for_Daypart_part2_{0}.set_upstream(Latte_daypart_loop_for_Daypart_func_{0})"""\
                        .format(value)
            exec(dagDir)
            dagDir = """DP{0}_Output_Insert_to_the_Master_Table.set_upstream(
                        [Latte_Merge_final_table_creation,Latte_daypart_loop_for_Daypart_part2_{0}])"""\
                        .format(value)
            exec(dagDir)

    if 'VODINC' in process_list:
        if 'VODLATTE' in process_list:
            if DATA_QUALITY_CHECK == "TRUE":
                Output_Table_Creation.set_upstream(dqc_vod_latte)
            else:
                for value in DP:
                    dagDir = """Output_Table_Creation.set_upstream(DP{0}_Output_Insert_to_the_Master_Table)"""\
                                .format(value)
                    exec(dagDir)

        if 'VODINCSMALL' in process_list:

            if DATA_QUALITY_CHECK == "TRUE":
                for value in DP:
                    dagDir = """dqc_vod_small.set_upstream(tsv_Incremental_Small_for_DP_{0})""" \
                        .format(value)
                    exec(dagDir)
                Output_Table_Creation.set_upstream(dqc_vod_small)
            else:
                for value in DP:
                    dagDir = """Output_Table_Creation.set_upstream(tsv_Incremental_Small_for_DP_{0})"""\
                                .format(value)
                    exec(dagDir)

        Week_to_week_MLE.set_upstream(Output_Table_Creation)
        Four_plus_and_Eight_plus.set_upstream(Output_Table_Creation)
        Within_the_week_Prep.set_upstream(Week_to_week_MLE)

        for value in range(1,11):
            if value == 1:
                prev = ''
            else:
                prev = 'Within_the_week_loop_{}'.format(value-1)

            dagDir = """Within_the_week_loop_{0}.set_upstream([Within_the_week_Prep{1}])"""\
                    .format(value, ','+prev)
            exec(dagDir)

            dagDir = """Final_output.set_upstream(Within_the_week_loop_{0})"""\
                    .format(value,)
            exec(dagDir)


        Final_output.set_upstream(Four_plus_and_Eight_plus)
        Clean_up_intermediate_tables.set_upstream(Final_output)



    if 'VODBOUNDING' in process_list:
        if 'VODINC' in process_list:
            if DATA_QUALITY_CHECK == "TRUE":
                dqc_vod_inc_latte.set_upstream(Clean_up_intermediate_tables)
                start_vod_bounding.set_upstream(dqc_vod_inc_latte)
            else:
                start_vod_bounding.set_upstream(Clean_up_intermediate_tables)
            VOD_Bounding_Fix.set_upstream(start_vod_bounding)


    if int(WEEKID_0) > int(first_week_of_bmonth) and 'MONTHLY' in process_list:
        single_platform_small.set_upstream(single_platform_small_inputs_prep)
        single_platform_overlap_w_final_format_if_last_week.set_upstream(single_platform_small)
        if (int(WEEKID_0) == int(csdatetools.time_id_to_week_id(int(lastdateofbmonth)))):
            single_platform_final_format.set_upstream(single_platform_overlap_w_final_format_if_last_week)
            VOD_Monthly_Fix.set_upstream(single_platform_final_format)
        else:
            VOD_Monthly_Fix.set_upstream(single_platform_overlap_w_final_format_if_last_week)

    return dag

etv_tv_vod_batch_dag = etv_tv_vod_batch('etv_tv_vod_batch')
