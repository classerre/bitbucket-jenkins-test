from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from datetime import datetime, timedelta  
from airflow.models import Variable
from operators.dfm import DFMOperator
from ecs_operator import ECSOperator
from csdatetools.csdatetools import get_last_time_id_in_week_id, get_first_time_id_in_week_id, time_id_to_yyyymmdd, \
		time_id_to_month_id, time_id_to_week_id, get_first_week_id_in_broadcast_month, month_id_to_yyyymmdd
from comscoreEmail import notify_email

default_args = {
    'owner': 'etv',
    'depends_on_past': False,
    'start_date': datetime(2019, 1, 31),
    'email': ['sgude@comscore.com, vkomuravelli@comscore.com, skamidi@comscore.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=1),
    'on_failure_callback': notify_email
}    
 
def etv_tv_shared_pre_batch(dag_name): 
	dag = DAG(dag_name, default_args=default_args, schedule_interval=None)

	# Variables    
	BASE_SQL_PATH = Variable.get("base_sql_path")
	WEEK_ID =  Variable.get("week_id")
	DQC_PATH = Variable.get("data_quality_check_path")
	DATA_QUALITY_CHECK = Variable.get("data_quality_check")
	Tasks =   Variable.get("shared_pre_batch_taskList")             # 'POSTTVD,HWE,MR,PESP,LOOKUP,ESP'
	CONFIG_PATH =  Variable.get("config_path")              # '/mapr/ri1.comscore.com/test_data/census/dags/snowsql/snowflake.config' #
	VCEXP_RUN = Variable.get("shared_pre_batch_vcxep_run") # 'false'
	BASE_SQL_PATH = BASE_SQL_PATH + 'etv_tv_shared_pre_batch/'

	#Derived Variables
	Week = 'W' + WEEK_ID;
	PREVIOUS_WEEK_1 = 'W' + str(int(WEEK_ID) - 1)
	PREVIOUS_WEEK_2 = 'W' + str(int(WEEK_ID) - 2)
	PREVIOUS_WEEK_3 = 'W' + str(int(WEEK_ID) - 3)
	PREVIOUS_WEEK_4 = 'W' + str(int(WEEK_ID) - 4)
	PREVIOUS_WEEK_5 = 'W' + str(int(WEEK_ID) - 5)

	PREVIOUS_WEEK_ID_1 = str(int(WEEK_ID) - 1)
	PREVIOUS_WEEK_ID_2 = str(int(WEEK_ID) - 2)
	PREVIOUS_WEEK_ID_3 = str(int(WEEK_ID) - 3)
	PREVIOUS_WEEK_ID_4 = str(int(WEEK_ID) - 4)
	PREVIOUS_WEEK_ID_5 = str(int(WEEK_ID) - 5)
	WEEK_NUM_MIN = WEEK_ID 
	WEEK_NUM_MAX = WEEK_ID

	TaskList = Tasks.split(",")	
	# Function calls
	START_DATE = time_id_to_yyyymmdd(get_first_time_id_in_week_id(int(WEEK_ID)));    # '2018-12-10'
	END_DATE =  time_id_to_yyyymmdd(get_last_time_id_in_week_id(int(WEEK_ID)));  # '2018-12-16'
	START_TIME = str(get_first_time_id_in_week_id(int(WEEK_ID)));  # '6919'
	END_TIME =  str(get_last_time_id_in_week_id(int(WEEK_ID)));    # '6925'
	WEEK_PERIOD_START_TIME_ID = str(get_first_time_id_in_week_id(int(PREVIOUS_WEEK_ID_5)));   # '6884'
	MONTH_ID = str(time_id_to_month_id(get_last_time_id_in_week_id(int(WEEK_ID)))); # '228'
	WEEK_NUM_MIN_MONTH = str(get_first_week_id_in_broadcast_month(int(MONTH_ID)));   # '987'

	YEAR_MONTH = month_id_to_yyyymmdd(int(MONTH_ID));		
	#Remove separator if any		
	YEAR_MONTH = YEAR_MONTH.replace('-','')		
	MONTH_DICT = {'01':'January',		
			'02':'February',		
			'03':'March',		
			'04':'April',		
			'05':'May',		
			'06':'June',		
			'07':'July',		
			'08':'August',		
			'09':'September',		
			'10':'October',		
			'11':'November',		
			'12':'December'}		
	#Extract Month short name from YYYYMMM		
	MONTH_MM = YEAR_MONTH[4:6]		
	MONTH_YEAR = '{0} {1}'.format(MONTH_DICT[MONTH_MM], YEAR_MONTH[0:4])

	PREVIOUS_WEEK_ID_1_MONTH_ID = str(time_id_to_month_id(get_last_time_id_in_week_id(int(PREVIOUS_WEEK_ID_1))));
	PREVIOUS_WEEK_ID_2_MONTH_ID = str(time_id_to_month_id(get_last_time_id_in_week_id(int(PREVIOUS_WEEK_ID_2))));
	PREVIOUS_WEEK_ID_3_MONTH_ID = str(time_id_to_month_id(get_last_time_id_in_week_id(int(PREVIOUS_WEEK_ID_3))));
	PREVIOUS_WEEK_ID_4_MONTH_ID = str(time_id_to_month_id(get_last_time_id_in_week_id(int(PREVIOUS_WEEK_ID_4))));
	PREVIOUS_WEEK_ID_5_MONTH_ID = str(time_id_to_month_id(get_last_time_id_in_week_id(int(PREVIOUS_WEEK_ID_5))));

	PREVIOUS_MONTH_ID_2 = str(int(MONTH_ID) - 2)

	S3_DATA_PATH = Variable.get("base_s3_loc")+ '/'
	S3_DATA_PATH_WEEK = S3_DATA_PATH + WEEK_ID + 'w/'
	S3_R_INPUT_FILE_NAME = 'rtk_ppm_hwe_' + WEEK_ID +'w.csv'
	SNOWFLAKE_DATA_PATH = Variable.get("base_s3_path")+ '/' + WEEK_ID+ 'w/'


	if "POSTTVD" in TaskList:
		POSTTVD_Execute_cfd_main_plus = BashOperator(task_id='POSTTVD_Execute_cfd_main_plus',
											bash_command='snowsql --config ' + CONFIG_PATH + ''
											  ' -f '+ BASE_SQL_PATH + 'tv_cfd_main_plus.sql'
											  ' -D  WEEK_SCHEMA_0=' + Week  + ''
											  ' -D  WEEK_SCHEMA_1=' + PREVIOUS_WEEK_1  + ''
											  ' -D  WEEK_SCHEMA_2=' + PREVIOUS_WEEK_2  + ''
											  ' -D  WEEK_SCHEMA_3=' + PREVIOUS_WEEK_3  + ''
											  ' -D  WEEK_SCHEMA_4=' + PREVIOUS_WEEK_4  + ''
											  ' -D  WEEK_SCHEMA_5=' + PREVIOUS_WEEK_5  + ''
											  ' -D  START_TIME_ID=' + WEEK_PERIOD_START_TIME_ID + '', 
											dag=dag                                        
											)
		Import_Data_TV_Hierarchy_Porcessing    = BashOperator(task_id='Import_Data_TV_Hierarchy_Porcessing',
													bash_command='snowsql --config ' + CONFIG_PATH + ''
														' -f '+ BASE_SQL_PATH + 'exportData.sql'
																					' -D s3Path=' + SNOWFLAKE_DATA_PATH + 'static_etv_tv_parentage_hierarchy.txt' + ''
																					' -D sourceTable='+Week+'.static_etv_tv_parentage_hierarchy'
																					' -D outputFormat=my_csv_format_uncompressed'
																					' -D exportType=true'
																					' -D withHeader=false', 
													dag=dag                                        
													)
		
		# EXECUTE SYNDICATED JAR
		TV_Hierarchy_Processing = ECSOperator(
			task_id="TV_Hierarchy_Processing",
			task_definition="syndicated-task",
			launch_type="FARGATE",
			cluster="Fargate-Cluster",
			network_configuration={
				'awsvpcConfiguration': {
					'subnets': [
						'subnet-0d6d29c0d1dfe14f1',
					],
					'securityGroups': [
						'sg-02b1cb2a987877ff1',
					],
					'assignPublicIp': 'ENABLED'
				}
			},
			overrides={
				'containerOverrides': [
					{
						'name': 'syndicated',
						'command': [
							# DON'T CHANGE THIS
							'syndicated-1.0-SNAPSHOT.py',
							'/usr/bin/java',
							'-Xmx8g',
							'-cp',
							'/syndicated-1.0-SNAPSHOT.jar',
							'com.comscore.dictionary.Main',
							# HARDCODED BUCKET S3 csxpdev-xp-east
							# INPUT
							'' + S3_DATA_PATH_WEEK + 'static_etv_tv_parentage_hierarchy.txt',
							# OUTPUT
							'' + S3_DATA_PATH_WEEK + 'tv_hierarchy.txt'
						]
					}
				]
			},
			dag=dag
		)   
													
		Copy_S3_Data_To_Table_TV_Hierarchy_Porcessing = BashOperator(task_id='Copy_S3_Data_To_Table_TV_Hierarchy_Porcessing',
													bash_command='snowsql --config ' + CONFIG_PATH + ''
														  ' -f '+ BASE_SQL_PATH + 'load_tv_hierarchy.sql'
														  ' -D  WEEK_SCHEMA_0=' + Week  + ''
														  ' -D  WEEK_0=' + WEEK_ID  + '', 
													dag=dag                                        
													)    

		Genre_Assignment = BashOperator(task_id='Genre_Assignment',
											bash_command='snowsql --config ' + CONFIG_PATH + ''
											  ' -f '+ BASE_SQL_PATH + 'genre_assignment.sql'
											  ' -D  WEEK_SCHEMA_0=' + Week  + '',
											dag=dag                                        
											)

		MetadataStore_TSV_Lookup= BashOperator(task_id='MetadataStore_TSV_Lookup',
											bash_command='snowsql --config ' + CONFIG_PATH + ''
											  ' -f '+ BASE_SQL_PATH + 'metadata_store_tsv_lookup.sql'
											  ' -D  WEEK_0=' + WEEK_ID  + ''
											  ' -D  WEEK_SCHEMA_0=' + Week  + ''
											  ' -D  WEEK_SCHEMA_1=' + PREVIOUS_WEEK_1  + '',
											dag=dag
											)

		TV_Parentage = BashOperator(task_id='TV_Parentage',
										bash_command='snowsql --config ' + CONFIG_PATH + ''
											  ' -f '+ BASE_SQL_PATH + 'tv_parentage.sql'
											  ' -D  WEEK_0=' + WEEK_ID  + ''
											  ' -D  WEEK_SCHEMA_0=' + Week  + ''
											  ' -D  WEEK_SCHEMA_1=' + PREVIOUS_WEEK_1  + ''
											  ' -D  WEEK_SCHEMA_2=' + PREVIOUS_WEEK_2  + ''
											  ' -D  WEEK_SCHEMA_3=' + PREVIOUS_WEEK_3  + ''
											  ' -D  WEEK_SCHEMA_4=' + PREVIOUS_WEEK_4  + ''
											  ' -D  WEEK_SCHEMA_5=' + PREVIOUS_WEEK_5  + '',
										dag=dag)

		TV_Parentage_Daypart = BashOperator(task_id='TV_Parentage_Daypart',
											bash_command='snowsql --config ' + CONFIG_PATH + ''
											  ' -f '+ BASE_SQL_PATH + 'tv_parentage_daypart.sql'
											  ' -D  WEEK_0=' + WEEK_ID  + ''
											  ' -D  WEEK_SCHEMA_0=' + Week  + ''
											  ' -D  WEEK_SCHEMA_1=' + PREVIOUS_WEEK_1  + ''
											  ' -D  WEEK_SCHEMA_2=' + PREVIOUS_WEEK_2  + ''
											  ' -D  WEEK_SCHEMA_3=' + PREVIOUS_WEEK_3  + ''
											  ' -D  WEEK_SCHEMA_4=' + PREVIOUS_WEEK_4  + ''
											  ' -D  WEEK_SCHEMA_5=' + PREVIOUS_WEEK_5  + '',
											dag=dag
											)    

		Web_Genre_Lookup = BashOperator(task_id='Web_Genre_Lookup',
											bash_command='snowsql --config ' + CONFIG_PATH + ''
											  ' -f '+ BASE_SQL_PATH + 'web_genre_lookup.sql'
											  ' -D  WEEK_0=' + WEEK_ID  + ''
											  ' -D  WEEK_SCHEMA_0=' + Week + '',
											dag=dag
											)

	# Logical Break
	if "HWE" in TaskList:
		HHNotinTab_Data_Generation = BashOperator(task_id='HHNotinTab_Data_Generation',
												bash_command='snowsql --config ' + CONFIG_PATH + ''
												  ' -f '+ BASE_SQL_PATH + 'hh_not_in_tab.sql'
												  ' -D  WEEK_0=' + WEEK_ID  + ''
												  ' -D  WEEK_SCHEMA_0=' + Week + '',
												dag=dag
												)
		
		Run_Live_HWE = BashOperator(task_id='Run_Live_HWE',
									bash_command='snowsql --config ' + CONFIG_PATH + ''
										' -f '+ BASE_SQL_PATH + 'live_hwe.sql'
										' -D WEEK_SCHEMA_0=' + Week  + ''
										' -D start_date=' + START_DATE  + ''
										' -D end_date=' + END_DATE  + ''
										' -D VCEXP_RUN=' + VCEXP_RUN + '',
									dag=dag
									)
									
		if int(WEEK_ID) >= 964 and int(WEEK_ID) < 974:
			Live_HWE_TWC_Exclusion = BashOperator(task_id='Live_HWE_TWC_Exclusion',
										bash_command='snowsql --config ' + CONFIG_PATH + ''
											' -f '+ BASE_SQL_PATH + 'TWC_Exclusion.sql'
											' -D WEEK_SCHEMA_0=' + Week  + ''
											' -D ETV_HWE_PLATFORM=etv_esp_live_tv_hwe',
										dag=dag
										)
		if DATA_QUALITY_CHECK == "TRUE":
			dqc_live_hwe = BashOperator(
				task_id='dqc_live_hwe',
				bash_command='{0}/dataqualitycheck.sh HWE hwe {1} LIVE \"@table_name={2}.etv_esp_live_tv_hwe @table_previous={3}.etv_esp_live_tv_hwe\" {0} {4}'.format(
					DQC_PATH, WEEK_ID, Week, PREVIOUS_WEEK_1, CONFIG_PATH)
				, dag=dag)
				

	#Logical Break
	if "MR" in TaskList:
		if int(WEEK_ID) > 899:
			MasterRoster_1 = BashOperator(task_id='MasterRoster_LIVE',
											bash_command='snowsql --config ' + CONFIG_PATH + ''
												' -f '+ BASE_SQL_PATH + 'master_roster_w_activity_flag_Live.sql'
												' -D WEEK_SCHEMA_0=' + Week  + ''
												' -D platform=live'
												' -D startTime=' + START_TIME +''
												' -D endTime=' + END_TIME +''
												' -D VCEXP_RUN=' + VCEXP_RUN + '',
											dag=dag)

			MasterRoster_2 = BashOperator(task_id='MasterRoster_Live_View_Creation',
														bash_command='snowsql --config ' + CONFIG_PATH + ''
															' -f '+ BASE_SQL_PATH + 'master_roster_w_activity_flag_view_creation.sql'
															' -D WEEK_SCHEMA_0=' + Week  + '',
															dag=dag)
		
		else:
			MasterRoster_1 = BashOperator(task_id='MasterRoster_LIVE_PDG',
											bash_command='snowsql --config ' + CONFIG_PATH + ''
												' -f '+ BASE_SQL_PATH + 'master_roster_pdg_live.sql'
												' -D WEEK_SCHEMA_0=' + Week  + ''
												' -D platform=live'
												' -D startTime=' + START_TIME +''
												' -D endTime=' + END_TIME +''
												' -D VCEXP_RUN=' + VCEXP_RUN + '',
											dag=dag)
			
			MasterRoster_2 = BashOperator(task_id='MasterRoster_LIVE_PDG_View_Creation',
														bash_command='snowsql --config ' + CONFIG_PATH + ''
															' -f '+ BASE_SQL_PATH + 'master_roster_w_activity_flag_pdg.sql'
															' -D WEEK_SCHEMA_0=' + Week  + '',
															dag=dag)

		if int(WEEK_ID) >= 964 and int(WEEK_ID) < 974:
			MasterRoster_LIVE_TWC = BashOperator(task_id='MasterRoster_LIVE_TWC',bash_command='snowsql --config ' + CONFIG_PATH + ''
							' -f '+ BASE_SQL_PATH + 'master_roster_w_activity_flag_TWC_exclusion.sql'
							' -D WEEK_SCHEMA_0=' + Week  + ''
							' -D platform=live',
							dag=dag)

		if DATA_QUALITY_CHECK == "TRUE":
			dqc_Master_Roster = BashOperator(
				task_id='dqc_Master_Roster',
				bash_command='{0}/dataqualitycheck.sh Pre_Espresso master_roster_active {1} TV \"@table_name={2}.master_roster_ext_tv @table_previous={3}.master_roster_ext_tv\" {0} {4}'.format(
					DQC_PATH, WEEK_ID, Week, PREVIOUS_WEEK_1, CONFIG_PATH)
				, dag=dag)

	#LOGICAL BREAK
	if "PESP" in TaskList:
		PPM_FRH_weights = BashOperator(task_id='PPM_FRH_weights', 
										bash_command='snowsql --config ' + CONFIG_PATH + ''
												' -f '+ BASE_SQL_PATH + 'frh_weights_hisp.sql'
												' -D  WEEK_0=' + WEEK_ID  + ''
												' -D  WEEK_SCHEMA_0=' + Week + ''
												' -D  MONTH_0=' + MONTH_ID + '',
										dag=dag
										)                
		

		Export_weights_strata_info_Pre_Espresso_Weights_SnowSQL_S3 = BashOperator(task_id='Export_weights_strata_info_Pre_Espresso_Weights_SnowSQL_S3',
														bash_command='snowsql --config ' + CONFIG_PATH + ''
														' -f '+ BASE_SQL_PATH + 'exportData.sql'
																					' -D s3Path=' + SNOWFLAKE_DATA_PATH + 'frh_strata_info_hisp.csv' + ''
																					' -D sourceTable='+Week+'.xmssystem_frh_weights_strata_info_hisp'
																					' -D outputFormat=csv_format_comma_separated_uncompressed'
																					' -D exportType=true'
																					' -D withHeader=true'
														,dag=dag                                        
														)

		Export_sample_roster_Pre_Espresso_Weights_SnowSQL_S3 = BashOperator(task_id='Export_sample_roster_Pre_Espresso_Weights_SnowSQL_S3',
														bash_command='snowsql --config ' + CONFIG_PATH + ''
														' -f '+ BASE_SQL_PATH + 'exportData.sql'
																					' -D s3Path=' + SNOWFLAKE_DATA_PATH + 'frh_sample_roster_hisp.csv' + ''
																					' -D sourceTable='+Week+'.xmssystem_frh_weights_sample_roster_hisp'
																					' -D outputFormat=csv_format_comma_separated_uncompressed'
																					' -D exportType=true'
																					' -D withHeader=true'
														,dag=dag                                        
														)

		Export_pop_estimate_Pre_Espresso_Weights_SnowSQL_S3 = BashOperator(task_id='Export_pop_estimate_Pre_Espresso_Weights_SnowSQL_S3',
														bash_command='snowsql --config ' + CONFIG_PATH + ''
														' -f '+ BASE_SQL_PATH + 'exportData.sql'
																					' -D s3Path=' + SNOWFLAKE_DATA_PATH + 'frh_pop_estimate_hisp.csv' + ''
																					' -D sourceTable='+Week+'.xmssystem_frh_weights_pop_estimate_hisp'
																					' -D outputFormat=csv_format_comma_separated_uncompressed'
																					' -D exportType=true'
																					' -D withHeader=true'
														,dag=dag                                        
														) 
							
																												
		Pre_Espresso_Weights = ECSOperator(
			task_id="Pre_Espresso_Weights",
			task_definition="weightssystem-linux_task",
			launch_type="FARGATE",
			cluster="Fargate-Cluster",
			network_configuration={
				'awsvpcConfiguration': {
					'subnets': [
						'subnet-0d6d29c0d1dfe14f1',
					],
					'securityGroups': [
						'sg-02b1cb2a987877ff1',
					],
					'assignPublicIp': 'ENABLED'
				}
			},
			overrides={
				 'containerOverrides': [
					 {
						 'name': 'weightssystem-linux',
						 'command': [
							'-s3_sample_path', S3_DATA_PATH_WEEK + 'frh_sample_roster_hisp.csv',
							'-s3_strata_info_path', S3_DATA_PATH_WEEK + 'frh_strata_info_hisp.csv',
							'-s3_population_estimates_path', S3_DATA_PATH_WEEK + 'frh_pop_estimate_hisp.csv',
							'-s3_threshold_cap_path','s3://csxpdev-xp-east/etv/etv-ecs/weights/iss_parms_gsma_weights.csv', #STATIC FILE
							'-s3_output_folder', S3_DATA_PATH_WEEK + 'frh_weights',
							'-country_num','840', #STATIC VAR
							'-location','All_loc', #STATIC VAR
							'-email_address','jdaly@comscore.com,ETVProduction@comscore.com',
							'-max_iterations','50',
							'-max_threads','100',
							'-weights_output_name','frh_weights',
							'-weights_distribution_output_name','frh_weights_distribution',
							'-weights_info_name','frh_weights_info',
							'-weights_log_name','frh_weights_log'
						 ]
					 }
				 ]
			},
			dag=dag
			)     
		
		Export_frh_weights_Pre_Espresso_Weights_S3_SnowSQL = BashOperator(task_id='Export_frh_weights_Pre_Espresso_Weights_S3_SnowSQL',
															bash_command='snowsql --config ' + CONFIG_PATH + ''
															' -f '+ BASE_SQL_PATH + 'Load_frh_weights_from_s3_to_snowflake.sql'
															' -D  WEEK_SCHEMA_0=' + Week  +''
															' -D  WEEK_ID=' + WEEK_ID  +''
															,dag=dag)
								

		ETV_PPM_FRH_HWE = BashOperator(task_id='ETV_PPM_FRH_HWE',
										bash_command='snowsql --config ' + CONFIG_PATH + ''
												' -f '+ BASE_SQL_PATH + 'frh_glm_training_set.sql'
												' -D  WEEK_0=' + WEEK_ID  + ''
												' -D  WEEK_1=' + PREVIOUS_WEEK_ID_1  + ''
												' -D  WEEK_2=' + PREVIOUS_WEEK_ID_2 + ''
												' -D  WEEK_3=' + PREVIOUS_WEEK_ID_3  + ''
												' -D  WEEK_4=' + PREVIOUS_WEEK_ID_4  + ''
												' -D  WEEK_5=' + PREVIOUS_WEEK_ID_5  + ''
												' -D  MONTH_0=' + MONTH_ID  + ''
												' -D  MONTH_1=' + PREVIOUS_WEEK_ID_1_MONTH_ID  + ''
												' -D  MONTH_2=' + PREVIOUS_WEEK_ID_2_MONTH_ID + ''
												' -D  MONTH_3=' + PREVIOUS_WEEK_ID_3_MONTH_ID  + ''
												' -D  MONTH_4=' + PREVIOUS_WEEK_ID_4_MONTH_ID  + ''
												' -D  MONTH_5=' + PREVIOUS_WEEK_ID_5_MONTH_ID  + ''
												' -D  WEEK_SCHEMA_0=' + Week  + ''
												' -D  WEEK_SCHEMA_1=' + PREVIOUS_WEEK_1  + ''
												' -D  WEEK_SCHEMA_2=' + PREVIOUS_WEEK_2  + ''
												' -D  WEEK_SCHEMA_3=' + PREVIOUS_WEEK_3  + ''
												' -D  WEEK_SCHEMA_4=' + PREVIOUS_WEEK_4  + ''
												' -D  WEEK_SCHEMA_5=' + PREVIOUS_WEEK_5  + '',
										dag=dag                                    
										)
										
		Export_PPM_FRH_HWE_Snowflake_To_S3 = BashOperator(task_id = "Export_PPM_FRH_HWE_Snowflake_To_S3",
																			bash_command='snowsql --config ' + CONFIG_PATH + ''
												' -f '+ BASE_SQL_PATH + 'exportData.sql'
																			' -D s3Path=' + SNOWFLAKE_DATA_PATH + 'rtk_ppm_hwe/' + S3_R_INPUT_FILE_NAME+ ''
																			' -D sourceTable='+Week+'.rtk_ppm_hwe '
																			' -D outputFormat=my_csv_format_uncompressed '
																			' -D exportType=True'
																			' -D withHeader=False',
																	dag = dag)

		Espresso_Live_Regression_R_Script = ECSOperator(
			task_definition="regression_frh_task",
			launch_type="FARGATE",
			cluster="Fargate-Cluster",
			network_configuration={
				'awsvpcConfiguration': {
					'subnets': [
						'subnet-0d6d29c0d1dfe14f1',
					],
					'securityGroups': [
						'sg-02b1cb2a987877ff1',
					],
					'assignPublicIp': 'ENABLED'
				}
			},
			overrides={
				'containerOverrides': [
					{
						'name': 'regression_frh',
						'command': [
							'-weekId', WEEK_ID,
							# EMBEDED BUCKER S3 csxpdev-xp-east
							# INPUT S3
							'-s3InputFile', S3_DATA_PATH_WEEK + 'rtk_ppm_hwe/' + S3_R_INPUT_FILE_NAME,
							# OUTPUT FOLDER S3
							'-s3OutputFolder', S3_DATA_PATH_WEEK + 'rtk_ppm_hwe/pre_shared_batch_r_output_' + WEEK_ID + 'w'
						]
					}
				]
			},
			task_id="Espresso_Live_Regression_R_Script",
			dag=dag)
											
		Export_Espresso_Live_Regression_FRH_S3_To_SnowSQL = BashOperator(task_id='Export_Espresso_Live_Regression_FRH_S3_To_SnowSQL',
															bash_command='snowsql --config ' + CONFIG_PATH + ''
															' -f '+ BASE_SQL_PATH + 'load_espresso_live_regression_frh_S3_to_SnowSQL.sql'
															' -D  WEEK_SCHEMA_0=' + Week  +''
															' -D  WEEK_ID=' + WEEK_ID  +''
															,dag=dag)        

	#LOGICAL BREAK
	if "LOOKUP" in TaskList:
		Run_Singular_Key_Lookup = BashOperator(task_id='Run_Singular_Key_Lookup',
											bash_command='snowsql --config ' + CONFIG_PATH + ''
												' -f '+ BASE_SQL_PATH + 'lookup_sk_master.sql'
												' -D  WEEK_SCHEMA_0=' + Week  + ''
												' -D  WEEK_SCHEMA_1=' + PREVIOUS_WEEK_1 + ''
												' -D  month_num=' + MONTH_ID  + ''
												' -D  week_num_min=' + WEEK_NUM_MIN  + ''
												' -D  week_num_max=' + WEEK_NUM_MAX  + ''
												' -D  week_num_min_month=' + WEEK_NUM_MIN_MONTH  + ''
												" -D  MONTH_YEAR='" + MONTH_YEAR + "'",
											dag=dag
											)    

		ETV_DictionaryLookup = BashOperator(task_id='ETV_DictionaryLookup',
											bash_command='snowsql --config ' + CONFIG_PATH + ''
												' -f '+ BASE_SQL_PATH + 'lookup_dictionary.sql'
												' -D  WEEK_0=' + WEEK_ID  + ''
												' -D  WEEK_SCHEMA_0=' + Week  + '',
											dag=dag
											)    

		ETV_DemoGroupLookup = BashOperator(task_id='ETV_DemoGroupLookup',
										   bash_command='snowsql --config ' + CONFIG_PATH + ''
												' -f '+ BASE_SQL_PATH + 'lookup_demogroups.sql'
												' -D  WEEK_0=' + WEEK_ID  + ''
												' -D  WEEK_SCHEMA_0=' + Week  + '',
										   dag=dag
										   )    

	#LOGICAL BREAK
	if "ESP" in TaskList:
		ETV_espresso_cvf_vpvh_live =BashOperator(task_id='ETV_espresso_cvf_vpvh_live',
												bash_command='snowsql --config ' + CONFIG_PATH + ''
													' -f '+ BASE_SQL_PATH + 'live_intensity_adjusted.sql'
													' -D  WEEK_0=' + WEEK_ID  + ''
													' -D  WEEK_1=' + PREVIOUS_WEEK_ID_1  + ''
													' -D  WEEK_2=' + PREVIOUS_WEEK_ID_2 + ''
													' -D  WEEK_3=' + PREVIOUS_WEEK_ID_3  + ''
													' -D  WEEK_4=' + PREVIOUS_WEEK_ID_4  + ''
													' -D  WEEK_5=' + PREVIOUS_WEEK_ID_5  + ''
													' -D  MONTH_ID_1=' + PREVIOUS_MONTH_ID_2  + ''                                                  
													' -D  MONTH_ID_3=' + MONTH_ID  + ''                                         
													' -D  WEEK_SCHEMA_0=' + Week  + ''
													' -D  WEEK_SCHEMA_1=' + PREVIOUS_WEEK_1  + ''
													' -D  WEEK_SCHEMA_2=' + PREVIOUS_WEEK_2  + ''
													' -D  WEEK_SCHEMA_3=' + PREVIOUS_WEEK_3  + ''
													' -D  WEEK_SCHEMA_4=' + PREVIOUS_WEEK_4  + ''
													' -D  WEEK_SCHEMA_5=' + PREVIOUS_WEEK_5  + '',
												dag=dag
												)     

		ETV_batch_live_coeff_Apply = BashOperator(task_id='ETV_batch_live_coeff_Apply',
												bash_command='snowsql --config ' + CONFIG_PATH + ''
													' -f '+ BASE_SQL_PATH + 'regcoef_apply_prob_reweight.sql'
													' -D  WEEK_SCHEMA_0=' + Week  + '',
												dag=dag
												)
		if DATA_QUALITY_CHECK == "TRUE":
			dqc_live_reweight = BashOperator(
				task_id='dqc_live_reweight',
				bash_command='{0}/dataqualitycheck.sh Espresso espresso_reweight {1} LIVE \"@table_name={2}.etv_experian_live_espresso_reweight @table_previous={3}.etv_experian_live_espresso_reweight\" {0} {4}'.format(
					DQC_PATH, WEEK_ID, Week, PREVIOUS_WEEK_1, CONFIG_PATH)
				, dag=dag)
			dqc_live_reweight.set_upstream(ETV_batch_live_coeff_Apply)

	if "POSTTVD" in TaskList:
		Import_Data_TV_Hierarchy_Porcessing.set_upstream(POSTTVD_Execute_cfd_main_plus)
		TV_Hierarchy_Processing.set_upstream(Import_Data_TV_Hierarchy_Porcessing)    
		Copy_S3_Data_To_Table_TV_Hierarchy_Porcessing.set_upstream(TV_Hierarchy_Processing)

		Genre_Assignment.set_upstream(POSTTVD_Execute_cfd_main_plus)
		MetadataStore_TSV_Lookup.set_upstream(Genre_Assignment)
		TV_Parentage_Daypart.set_upstream(POSTTVD_Execute_cfd_main_plus)
		TV_Parentage.set_upstream(POSTTVD_Execute_cfd_main_plus)
		Web_Genre_Lookup.set_upstream([TV_Parentage_Daypart, TV_Parentage, MetadataStore_TSV_Lookup, Copy_S3_Data_To_Table_TV_Hierarchy_Porcessing])

	#Logical Break
	if "HWE" in TaskList:
		Run_Live_HWE.set_upstream(HHNotinTab_Data_Generation)
		if "POSTTVD" in TaskList:
			HHNotinTab_Data_Generation.set_upstream([TV_Parentage_Daypart, TV_Parentage, MetadataStore_TSV_Lookup, Copy_S3_Data_To_Table_TV_Hierarchy_Porcessing])
		if int(WEEK_ID) >= 964 and int(WEEK_ID) < 974:
			if DATA_QUALITY_CHECK == "TRUE":
				Live_HWE_TWC_Exclusion.set_upstream(Run_Live_HWE)
				dqc_live_hwe.set_upstream(Live_HWE_TWC_Exclusion)
			else:
				Live_HWE_TWC_Exclusion.set_upstream(Run_Live_HWE)

	#Logical Break
	if "MR" in TaskList:
		MasterRoster_2.set_upstream(MasterRoster_1)
		if int(WEEK_ID) >= 964 and int(WEEK_ID) < 974:
			if DATA_QUALITY_CHECK == "TRUE":
				MasterRoster_LIVE_TWC.set_upstream(MasterRoster_2)
				dqc_Master_Roster.set_upstream(MasterRoster_LIVE_TWC)
			else:
				MasterRoster_LIVE_TWC.set_upstream(MasterRoster_2)
		if "HWE" in TaskList:        
			if int(WEEK_ID) >= 964 and int(WEEK_ID) < 974:
				if DATA_QUALITY_CHECK == "TRUE":
					MasterRoster_1.set_upstream(dqc_live_hwe)
				else:
					MasterRoster_1.set_upstream(Live_HWE_TWC_Exclusion)
			else:
				if DATA_QUALITY_CHECK == "TRUE":
					dqc_live_hwe.set_upstream(Run_Live_HWE)
					MasterRoster_1.set_upstream(dqc_live_hwe)
				else:
					MasterRoster_1.set_upstream(Run_Live_HWE)

	#Logical Break
	if "PESP" in TaskList:
		if "MR" in TaskList:
			if int(WEEK_ID) >= 964 and int(WEEK_ID) < 974:
				if DATA_QUALITY_CHECK == "TRUE":
					PPM_FRH_weights.set_upstream(dqc_Master_Roster)
				else:
					PPM_FRH_weights.set_upstream(MasterRoster_LIVE_TWC)
			else:
				if DATA_QUALITY_CHECK == "TRUE":
					dqc_Master_Roster.set_upstream(MasterRoster_2)
					PPM_FRH_weights.set_upstream(dqc_Master_Roster)
				else:
					PPM_FRH_weights.set_upstream(MasterRoster_2)
		Export_weights_strata_info_Pre_Espresso_Weights_SnowSQL_S3.set_upstream(PPM_FRH_weights)
		Export_sample_roster_Pre_Espresso_Weights_SnowSQL_S3.set_upstream(PPM_FRH_weights)
		Export_pop_estimate_Pre_Espresso_Weights_SnowSQL_S3.set_upstream(PPM_FRH_weights)
		Pre_Espresso_Weights.set_upstream([Export_weights_strata_info_Pre_Espresso_Weights_SnowSQL_S3, Export_sample_roster_Pre_Espresso_Weights_SnowSQL_S3, Export_pop_estimate_Pre_Espresso_Weights_SnowSQL_S3])
		Export_frh_weights_Pre_Espresso_Weights_S3_SnowSQL.set_upstream(Pre_Espresso_Weights)
		ETV_PPM_FRH_HWE.set_upstream(Export_frh_weights_Pre_Espresso_Weights_S3_SnowSQL) # might have to change this to PPM_FRH_weights
		Export_PPM_FRH_HWE_Snowflake_To_S3.set_upstream(ETV_PPM_FRH_HWE)
		Espresso_Live_Regression_R_Script.set_upstream(Export_PPM_FRH_HWE_Snowflake_To_S3)
		Export_Espresso_Live_Regression_FRH_S3_To_SnowSQL.set_upstream(Espresso_Live_Regression_R_Script)

	if "LOOKUP" in TaskList:
		if "POSTTVD" in TaskList:        
			Run_Singular_Key_Lookup.set_upstream([TV_Parentage_Daypart, TV_Parentage, MetadataStore_TSV_Lookup, Copy_S3_Data_To_Table_TV_Hierarchy_Porcessing])        
			ETV_DictionaryLookup.set_upstream([TV_Parentage_Daypart, TV_Parentage, MetadataStore_TSV_Lookup, Copy_S3_Data_To_Table_TV_Hierarchy_Porcessing])        
			ETV_DemoGroupLookup.set_upstream([TV_Parentage_Daypart, TV_Parentage, MetadataStore_TSV_Lookup, Copy_S3_Data_To_Table_TV_Hierarchy_Porcessing])

	#Logical Break
	if "ESP" in TaskList:
		if "PESP" in TaskList:
			ETV_espresso_cvf_vpvh_live.set_upstream(Export_Espresso_Live_Regression_FRH_S3_To_SnowSQL)
		ETV_batch_live_coeff_Apply.set_upstream(ETV_espresso_cvf_vpvh_live)
		
	return dag

etv_tv_shared_pre_batch_dag = etv_tv_shared_pre_batch('etv_tv_shared_pre_batch')