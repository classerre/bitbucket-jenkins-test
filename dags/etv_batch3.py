__author__ = " Vidyan Komuravelli "

"""  
       importing dependencies for etv_batch3
"""
from etv_digital_tvd_mapping_parts import etv_digital_tvd_mapping_parts
from etv_daily_duration_tvd_apply import etv_daily_duration_tvd_apply

from airflow import DAG
from airflow.models import Variable
from airflow.operators.subdag_operator import SubDagOperator
from datetime import datetime, timedelta
from airflow.executors.celery_executor import CeleryExecutor
from comscoreEmail import returnEmailOperator

BATCH_NAME = 'etv_batch3'
WEEK_ID = Variable.get("week_id")

args = {
    'owner': 'etv',
    'depends_on_past': False,
    'start_date': datetime(2019, 1, 24),
    'email': ['sgude@comscore.com', 'vkomuravelli@comscore.com', 'skamidi@comscore.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=1)
}

def etv_batch3(dag_name):
    dag = DAG(dag_name, default_args=args, schedule_interval=None)

    etv_digital_tvd_mapping_parts_dag = SubDagOperator(
        executor=CeleryExecutor(),
        task_id='etv_digital_tvd_mapping_parts',
        subdag=etv_digital_tvd_mapping_parts(dag_name + '.etv_digital_tvd_mapping_parts'),
        default_args=args,
        dag=dag)

    etv_daily_duration_tvd_apply_dag = SubDagOperator(
        executor=CeleryExecutor(),
        task_id='etv_daily_duration_tvd_apply',
        subdag=etv_daily_duration_tvd_apply(dag_name + '.etv_daily_duration_tvd_apply'),
        default_args=args,
        dag=dag)

    # Dependencies :
    etv_daily_duration_tvd_apply_dag.set_upstream(etv_digital_tvd_mapping_parts_dag)

    notify_success = returnEmailOperator(dag, BATCH_NAME, WEEK_ID)
    notify_success.set_upstream(etv_daily_duration_tvd_apply_dag)
    return dag

etv_batch3_dag = etv_batch3(BATCH_NAME)