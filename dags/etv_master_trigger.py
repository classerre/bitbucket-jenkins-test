import airflow

from etv_batch0 import etv_batch0
from etv_batch1 import etv_batch1
from etv_batch2 import etv_batch2
from etv_batch3 import etv_batch3
from etv_batch4 import etv_batch4
from etv_batch5 import etv_batch5

from airflow.models import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.dagrun_operator import TriggerDagRunOperator
from datetime import datetime, timedelta
from airflow.executors.celery_executor import CeleryExecutor


DAG_NAME = 'etv_master_trigger'

args = {
    'owner': 'etv',
    'depends_on_past': False,
    'start_date': datetime(2019, 1, 24),
    'email': ['sgude@comscore.com', 'vkomuravelli@comscore.com', 'skamidi@comscore.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=1)
}

dag = DAG(DAG_NAME, default_args=args, schedule_interval=None)

etv_batch0_dag = TriggerDagRunOperator(
    task_id='etv_batch0',
    trigger_dag_id='etv_batch0',
    default_args=args,
    dag=dag)

etv_batch1_dag = TriggerDagRunOperator(
    task_id='etv_batch1',
    trigger_dag_id='etv_batch1',
    default_args=args,
    dag=dag)

etv_batch2_dag = TriggerDagRunOperator(
    task_id='etv_batch2',
    trigger_dag_id='etv_batch2',
    default_args=args,
    dag=dag)

etv_batch3_dag = TriggerDagRunOperator(
    task_id='etv_batch3',
    trigger_dag_id='etv_batch3',
    default_args=args,
    dag=dag)

etv_batch4_dag = TriggerDagRunOperator(
    task_id='etv_batch4',
    trigger_dag_id='etv_batch4',
    default_args=args,
    dag=dag)

etv_batch5_dag = TriggerDagRunOperator(
    task_id='etv_batch5',
    trigger_dag_id='etv_batch5',
    default_args=args,
    dag=dag)

# Dependencies :
etv_batch1_dag.set_upstream(etv_batch0_dag)
etv_batch3_dag.set_upstream(etv_batch0_dag)
etv_batch2_dag.set_upstream(etv_batch1_dag)
etv_batch4_dag.set_upstream(etv_batch3_dag)
etv_batch5_dag.set_upstream([etv_batch2_dag, etv_batch4_dag])