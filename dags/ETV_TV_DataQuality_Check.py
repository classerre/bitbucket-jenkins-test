from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from datetime import datetime, timedelta  
from airflow.models import Variable
from CSUtils import runCSUtilsMethod
from airflow.utils.email import send_email
from comscoreEmail import notify_email

default_args = {
    'owner': 'vkomuravelli',
    'depends_on_past': False,
    'start_date': datetime(2019, 1, 31),
    'email': ['sgude@comscore.com, vkomuravelli@comscore.com, skamidi@comscore.com, rbhallamudi@comscore.com, gmuthyala@comscore.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=1),
    'on_failure_callback': notify_email
}	
   
dag = DAG('ETV_TV_DataQuality_Check', default_args=default_args, schedule_interval=None)

# Variables	
BASE_SQL_PATH = Variable.get("base_sql_path")	 # '/mapr/ri1.comscore.com/test_data/census/dags/SnowflakeSQL/' # 
WEEK_ID =  Variable.get("week_id")	 # '970' #

DataQuality_Check_Bounding	= BashOperator(task_id='DataQuality_Check_Bounding',
												bash_command='/home/vkomuravelli/datachecksdf/dataqualitycheck.sh bounding 989 DVR  \"@table_name=W989.ETV_DVR_LATTE_INCLUSIVE_BOUNDED @table_previous=W989.ETV_DVR_LATTE_INCLUSIVE_BOUNDED\"', 
												dag=dag										
												)
