from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.dummy_operator import DummyOperator
from datetime import datetime, timedelta
from airflow.models import Variable
from operators.dfm import DFMOperator
from ecs_operator import ECSOperator
from csdatetools import csdatetools

default_args = {
    'owner': 'etv',
    'depends_on_past': False,
    'start_date': datetime(2019, 1, 31),
    'email': ['sgude@comscore.com, vkomuravelli@comscore.com, skamidi@comscore.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 8,
    'retry_delay': timedelta(minutes=1)
}

def etv_tv_overlap_batch(dag_name):
    dag = DAG(dag_name, default_args=default_args, schedule_interval=None)

    # DAG Specific constants
    PLATFORM_LIVE = 'live'
    PLATFORM_DVR = 'DVR'
    PLATFORM_VOD = 'VOD'

    # Variables
    BASE_SQL_PATH = Variable.get("base_sql_path")
    WEEK_ID = Variable.get("week_id")
    Tasks = Variable.get("Overlap_Batch_TaskList")
    CONFIG_PATH = Variable.get("config_path")
    UTILITY_PATH = Variable.get("base_sql_path") + 'utilities'
    DQC_PATH = Variable.get("data_quality_check_path")
    DATA_QUALITY_CHECK = Variable.get("data_quality_check")
    S3_THRESHOLD_OVERLAP_CAP_PATH = Variable.get("s3_threshold_overlap_cap_path")


    MONTH_ID = str(csdatetools.time_id_to_month_id( csdatetools.get_last_time_id_in_week_id(int(WEEK_ID))))
    FIRST_TIME_ID_IN_RANGE = str(csdatetools.get_first_time_id_in_week_id(int(WEEK_ID)))
    LAST_TIME_ID_IN_RANGE = str(csdatetools.get_last_time_id_in_week_id(int(WEEK_ID)))

    __BM_OF_WEEKID = str(csdatetools.time_id_to_broadcast_month(csdatetools.get_last_time_id_in_week_id(int(WEEK_ID))))
    __TIMEID_OF_LASTWEEK_OF_MONTH = str(csdatetools.datetime_to_time_id(csdatetools.get_last_day_in_gregorian_month(
                                                     csdatetools.time_id_to_month_id(
                                                                      csdatetools.get_last_time_id_in_week_id(int(WEEK_ID))))))
    __BM_LAST_WEEKID_OF_MONTH = str(csdatetools.time_id_to_broadcast_month(int(__TIMEID_OF_LASTWEEK_OF_MONTH)))
    __LASTWEEK_OF_MONTH = str(csdatetools.time_id_to_week_id(int(__TIMEID_OF_LASTWEEK_OF_MONTH)))
    __TIMEID_OF_FIRSTWEEK_OF_MONTH = str(csdatetools.datetime_to_time_id(csdatetools.get_first_day_in_gregorian_month(
                                                      csdatetools.time_id_to_month_id(
                                                                       csdatetools.get_first_time_id_in_week_id(int(WEEK_ID))))))
    __FIRSTWEEK_OF_MONTH = str(csdatetools.time_id_to_week_id(int(__TIMEID_OF_FIRSTWEEK_OF_MONTH)))

    if __BM_OF_WEEKID == __BM_LAST_WEEKID_OF_MONTH:
        LAST_WEEKID_OF_BROADCAST_MONTH = __LASTWEEK_OF_MONTH
    else:
        LAST_WEEKID_OF_BROADCAST_MONTH = int(__LASTWEEK_OF_MONTH) - 1

    START_WEEK = __FIRSTWEEK_OF_MONTH
    END_WEEK = LAST_WEEKID_OF_BROADCAST_MONTH

    START_TIME_ID = str(csdatetools.get_first_time_id_in_week_id(int(WEEK_ID)))
    END_TIME_ID = str(csdatetools.get_last_time_id_in_week_id(int(WEEK_ID)))

    # Derived Variables
    Week = 'W' + WEEK_ID
    PREVIOUS_WEEK_1 = 'W' + str(int(WEEK_ID) - 1)
    PREVIOUS_WEEK_ID_1 = str(int(WEEK_ID) - 1)
    PREVIOUS_WEEK_ID_2 = str(int(WEEK_ID) - 2)
    PREVIOUS_WEEK_ID_3 = str(int(WEEK_ID) - 3)
    PREVIOUS_WEEK_ID_4 = str(int(WEEK_ID) - 4)
    PREVIOUS_WEEK_ID_5 = str(int(WEEK_ID) - 5)
    TaskList = Tasks.split(",")
    DPs = [35, 34, 33, 32, 31, 30, 29, 28, 27, 26, 16, 12, 10, 9, 8, 2, 1, 0]
    DMAs = [501, 504, 505, 506, 508, 510, 511, 512, 517, 524, 527, 528, 534, 539, 560, 602, 609, 613, 618, 623, 659, 751,
            753, 770, 803, 807, 819, 820, 825, 862, 8400001, 8400002, 8400003, 8400004, 8400005, 8400006, 8400007, 8400008,
            8400009]

    borg_level_sql_path = BASE_SQL_PATH + 'etv_tv_overlap_batch/'

    if "OLWEIGHTS" in TaskList:
        etv_vod_live_overlap_weights_input = BashOperator(
            task_id='etv_vod_live_overlap_weights_input',
            bash_command='snowsql --config ' + CONFIG_PATH + '' +
                         ' -f ' + borg_level_sql_path + 'vod_live_overlap.sql' +
                         ' -D  WEEK_SCHEMA_0=' + Week + '' +
                         ' -D  MONTH_ID=' + MONTH_ID + '' +
                         ' -D  WEEK_0=' + WEEK_ID + '',
            dag=dag
        )

        # on premise task  generate_vod_live_overlap_weights.ps1  start
        TEST_PREFIX = '' # Not required
        S3_DATA_PATH = Variable.get('base_s3_path') + '/export/'
        S3_DATA_WEEK_PATH = Variable.get('base_s3_loc') + '/'
        WEEK_SCHEMA_0 = 'W' + WEEK_ID

        delete_Export_weights_strata_info_hisp_vod_live_overlap_SnowSQL_S3 = BashOperator(
            task_id='delete_Export_weights_strata_info_hisp_vod_live_overlap_SnowSQL_S3'
            , bash_command='{}/deleteS3Object.sh '.format(UTILITY_PATH) +
                           '--s3Path {0}{1}.etv_hwe_weights_strata_info_hisp_vod_live_overlap'.format(S3_DATA_PATH,WEEK_SCHEMA_0)
            , dag=dag
        )

        Export_weights_strata_info_hisp_vod_live_overlap_SnowSQL_S3 = \
        BashOperator(task_id='Export_weights_strata_info_hisp_vod_live_overlap_SnowSQL_S3',
                     bash_command='snowsql --config ' + CONFIG_PATH + ''
                     ' -f ' + borg_level_sql_path + 'exportData.sql'
                     ' -D s3Path=' + S3_DATA_PATH + WEEK_SCHEMA_0 +
                     '.etv_hwe_weights_strata_info_hisp_vod_live_overlap' + ''
                     ' -D sourceTable='+WEEK_SCHEMA_0 +
                     '.etv_hwe_weights_strata_info_hisp_vod_live_overlap'
                     ' -D outputFormat=csv_format_comma_separated_uncompressed'
                     ' -D exportType=true'
                     ' -D withHeader=true', dag=dag)

        Export_weights_strata_info_hisp_vod_live_overlap_SnowSQL_S3.set_upstream(delete_Export_weights_strata_info_hisp_vod_live_overlap_SnowSQL_S3)
        delete_Export_sample_roster_Pre_hisp_vod_live_overlap_SnowSQL_S3 = BashOperator(
            task_id='delete_Export_sample_roster_Pre_hisp_vod_live_overlap_SnowSQL_S3'
            , bash_command='{}/deleteS3Object.sh '.format(UTILITY_PATH) +
                           '--s3Path {0}{1}.etv_hwe_weights_sample_roster_hisp_vod_live_overlap'.format(S3_DATA_PATH,WEEK_SCHEMA_0)
            , dag=dag
        )

        Export_sample_roster_Pre_hisp_vod_live_overlap_SnowSQL_S3 = \
        BashOperator(task_id='Export_sample_roster_Pre_hisp_vod_live_overlap_SnowSQL_S3',
                     bash_command='snowsql --config ' + CONFIG_PATH + ''
                     ' -f ' + borg_level_sql_path + 'exportData.sql'
                     ' -D s3Path=' + S3_DATA_PATH + WEEK_SCHEMA_0 +
                     '.etv_hwe_weights_sample_roster_hisp_vod_live_overlap' + ''
                     ' -D sourceTable='+WEEK_SCHEMA_0 +
                     '.etv_hwe_weights_sample_roster_hisp_vod_live_overlap'
                     ' -D outputFormat=csv_format_comma_separated_uncompressed'
                     ' -D exportType=true'
                     ' -D withHeader=true', dag=dag)

        Export_sample_roster_Pre_hisp_vod_live_overlap_SnowSQL_S3.set_upstream(delete_Export_sample_roster_Pre_hisp_vod_live_overlap_SnowSQL_S3)


        delete_Export_pop_estimate_hisp_vod_live_overlap_SnowSQL_S3 = BashOperator(
            task_id='delete_Export_pop_estimate_hisp_vod_live_overlap_SnowSQL_S3'
            , bash_command='{}/deleteS3Object.sh '.format(UTILITY_PATH) +
                           '--s3Path {0}{1}.etv_hwe_weights_pop_estimate_hisp_vod_live_overlap'.format(S3_DATA_PATH,WEEK_SCHEMA_0)
            , dag=dag
        )

        Export_pop_estimate_hisp_vod_live_overlap_SnowSQL_S3 = \
        BashOperator(task_id='Export_pop_estimate_hisp_vod_live_overlap_SnowSQL_S3',
                     bash_command='snowsql --config ' + CONFIG_PATH + ''
                     ' -f ' + borg_level_sql_path + 'exportData.sql'
                     ' -D s3Path=' + S3_DATA_PATH + WEEK_SCHEMA_0 +
                     '.etv_hwe_weights_pop_estimate_hisp_vod_live_overlap' + ''
                     ' -D sourceTable='+WEEK_SCHEMA_0 +
                     '.etv_hwe_weights_pop_estimate_hisp_vod_live_overlap'
                     ' -D outputFormat=csv_format_comma_separated_uncompressed'
                     ' -D exportType=true'
                     ' -D withHeader=true', dag=dag)

        Export_pop_estimate_hisp_vod_live_overlap_SnowSQL_S3.set_upstream(delete_Export_pop_estimate_hisp_vod_live_overlap_SnowSQL_S3)

        generate_vod_live_overlap_weights = ECSOperator(
            task_id="generate_vod_live_overlap_weights",
            task_definition="weightssystem-linux_task",
            launch_type="FARGATE",
            cluster="Fargate-Cluster",
            network_configuration={
                'awsvpcConfiguration': {
                    'subnets': [
                        'subnet-0d6d29c0d1dfe14f1',
                    ],
                    'securityGroups': [
                        'sg-02b1cb2a987877ff1',
                    ],
                    'assignPublicIp': 'ENABLED'
                }
            },
            overrides={
                'containerOverrides': [
                    {
                        'name': 'weightssystem-linux',
                        'command': [
                            '-s3_sample_path', S3_DATA_WEEK_PATH + 'export/' +
                                WEEK_SCHEMA_0 + '.etv_hwe_weights_sample_roster_hisp_vod_live_overlap',
                            '-s3_strata_info_path', S3_DATA_WEEK_PATH + 'export/' +
                                WEEK_SCHEMA_0 + '.etv_hwe_weights_strata_info_hisp_vod_live_overlap',
                            '-s3_population_estimates_path', S3_DATA_WEEK_PATH + 'export/' +
                                WEEK_SCHEMA_0 + '.etv_hwe_weights_pop_estimate_hisp_vod_live_overlap',
                            '-s3_threshold_cap_path', S3_THRESHOLD_OVERLAP_CAP_PATH + 'vod_live_overlap_weights_threshold_cap.csv',  # STATIC FILE
                            '-s3_output_folder', S3_DATA_WEEK_PATH + TEST_PREFIX + WEEK_SCHEMA_0,
                            '-country_num', '840',  # STATIC VAR
                            '-location', 'All_loc',  # STATIC VAR
                            '-email_address', 'adas@comscore.com',
                            '-max_iterations', '50',
                            '-max_threads', '100',
                            '-weights_output_name', 'vod_live_overlap_weights',
                            '-weights_distribution_output_name', 'vod_live_overlap_weights_distribution',
                            '-weights_info_name', 'vod_live_overlap_weights_info',
                            '-weights_log_name', 'vod_live_overlap_weights_log'
                        ]
                    }
                ]
            },
            dag=dag
        )

        Export_vod_live_overlap_weights_S3_SnowSQL = BashOperator(
            task_id='Export_vod_live_overlap_weights_S3_SnowSQL',
            bash_command='snowsql --config ' + CONFIG_PATH + '' +
                         ' -f ' + borg_level_sql_path + 'load_vod_live_weights_gp.sql' +
                         ' -D  WEEK_SCHEMA_0=' + WEEK_SCHEMA_0 + '' ,
                        # ' -D  TEST_PREFIX=' + TEST_PREFIX + '',
            dag=dag)

        etv_dvr_live_overlap_weights_input = BashOperator(
            task_id='etv_dvr_live_overlap_weights_input',
            bash_command='snowsql --config ' + CONFIG_PATH + '' +
                         ' -f ' + borg_level_sql_path + 'dvr_live_overlap.sql' +
                         ' -D  WEEK_SCHEMA_0=' + Week + '' +
                         ' -D  MONTH_ID=' + MONTH_ID + '' +
                         ' -D  WEEK_0=' + WEEK_ID + '',
            dag=dag)

        delete_Export_etv_hwe_weights_strata_info_hisp_dvr_live_overlap_SnowSQL_S3 = BashOperator(
            task_id='delete_Export_etv_hwe_weights_strata_info_hisp_dvr_live_overlap_SnowSQL_S3'
            , bash_command='{}/deleteS3Object.sh '.format(UTILITY_PATH) +
                           '--s3Path {0}{1}.etv_hwe_weights_strata_info_hisp_dvr_live_overlap'.format(S3_DATA_PATH,WEEK_SCHEMA_0)
            , dag=dag
        )

        Export_etv_hwe_weights_strata_info_hisp_dvr_live_overlap_SnowSQL_S3 = \
        BashOperator(task_id='Export_etv_hwe_weights_strata_info_hisp_dvr_live_overlap_SnowSQL_S3',
                     bash_command='snowsql --config ' + CONFIG_PATH + ''
                     ' -f ' + borg_level_sql_path + 'exportData.sql'
                     ' -D s3Path=' + S3_DATA_PATH + WEEK_SCHEMA_0 +
                     '.etv_hwe_weights_strata_info_hisp_dvr_live_overlap' + ''
                     ' -D sourceTable='+WEEK_SCHEMA_0 +
                     '.etv_hwe_weights_strata_info_hisp_dvr_live_overlap'
                     ' -D outputFormat=csv_format_comma_separated_uncompressed'
                     ' -D exportType=true'
                     ' -D withHeader=true', dag=dag)

        Export_etv_hwe_weights_strata_info_hisp_dvr_live_overlap_SnowSQL_S3.set_upstream(delete_Export_etv_hwe_weights_strata_info_hisp_dvr_live_overlap_SnowSQL_S3)


        delete_Export_etv_hwe_weights_sample_roster_hisp_dvr_live_overlap_SnowSQL_S3 = BashOperator(
            task_id='delete_Export_etv_hwe_weights_sample_roster_hisp_dvr_live_overlap_SnowSQL_S3'
            , bash_command='{}/deleteS3Object.sh '.format(UTILITY_PATH) +
                           '--s3Path {0}{1}.etv_hwe_weights_sample_roster_hisp_dvr_live_overlap'.format(S3_DATA_PATH,WEEK_SCHEMA_0)
            , dag=dag
        )

        Export_etv_hwe_weights_sample_roster_hisp_dvr_live_overlap_SnowSQL_S3 = \
        BashOperator(task_id='Export_etv_hwe_weights_sample_roster_hisp_dvr_live_overlap_SnowSQL_S3',
                     bash_command='snowsql --config ' + CONFIG_PATH + ''
                     ' -f ' + borg_level_sql_path + 'exportData.sql'
                     ' -D s3Path=' + S3_DATA_PATH + WEEK_SCHEMA_0 +
                     '.etv_hwe_weights_sample_roster_hisp_dvr_live_overlap' + ''
                     ' -D sourceTable='+WEEK_SCHEMA_0 +
                     '.etv_hwe_weights_sample_roster_hisp_dvr_live_overlap'
                     ' -D outputFormat=csv_format_comma_separated_uncompressed'
                     ' -D exportType=true'
                     ' -D withHeader=true', dag=dag)

        Export_etv_hwe_weights_sample_roster_hisp_dvr_live_overlap_SnowSQL_S3.set_upstream(delete_Export_etv_hwe_weights_sample_roster_hisp_dvr_live_overlap_SnowSQL_S3)

        delete_Export_etv_hwe_weights_pop_estimate_hisp_dvr_live_overlap_SnowSQL_S3 = BashOperator(
            task_id='delete_Export_etv_hwe_weights_pop_estimate_hisp_dvr_live_overlap_SnowSQL_S3'
            , bash_command='{}/deleteS3Object.sh '.format(UTILITY_PATH) +
                           '--s3Path {0}{1}.etv_hwe_weights_pop_estimate_hisp_dvr_live_overlap'.format(S3_DATA_PATH,WEEK_SCHEMA_0)
            , dag=dag
        )

        Export_etv_hwe_weights_pop_estimate_hisp_dvr_live_overlap_SnowSQL_S3 = \
        BashOperator(task_id='Export_etv_hwe_weights_pop_estimate_hisp_dvr_live_overlap_SnowSQL_S3',
                     bash_command='snowsql --config ' + CONFIG_PATH + ''
                     ' -f ' + borg_level_sql_path + 'exportData.sql'
                     ' -D s3Path=' + S3_DATA_PATH + WEEK_SCHEMA_0 +
                     '.etv_hwe_weights_pop_estimate_hisp_dvr_live_overlap' + ''
                     ' -D sourceTable='+WEEK_SCHEMA_0 +
                     '.etv_hwe_weights_pop_estimate_hisp_dvr_live_overlap'
                     ' -D outputFormat=csv_format_comma_separated_uncompressed'
                     ' -D exportType=true'
                     ' -D withHeader=true', dag=dag)

        Export_etv_hwe_weights_pop_estimate_hisp_dvr_live_overlap_SnowSQL_S3.set_upstream(delete_Export_etv_hwe_weights_pop_estimate_hisp_dvr_live_overlap_SnowSQL_S3)

        Dma_weights = []
        for DMA in DMAs:
            Dma_weights.append(ECSOperator(
                task_id="Dma_weights_" + str(DMA),
                task_definition="weightssystem-linux_task",
                launch_type="FARGATE",
                cluster="Fargate-Cluster",
                network_configuration={
                    'awsvpcConfiguration': {
                        'subnets': [
                            'subnet-0d6d29c0d1dfe14f1',
                        ],
                        'securityGroups': [
                            'sg-02b1cb2a987877ff1',
                        ],
                        'assignPublicIp': 'ENABLED'
                    }
                },
                overrides={
                    'containerOverrides': [
                        {
                            'name': 'weightssystem-linux',
                            'command': [
                                '-s3_sample_path', S3_DATA_WEEK_PATH + 'export/' +
                                    WEEK_SCHEMA_0 + '.etv_hwe_weights_sample_roster_hisp_dvr_live_overlap',
                                '-s3_strata_info_path', S3_DATA_WEEK_PATH + 'export/' +
                                    WEEK_SCHEMA_0 + '.etv_hwe_weights_strata_info_hisp_dvr_live_overlap',
                                '-s3_population_estimates_path', S3_DATA_WEEK_PATH + 'export/' +
                                    WEEK_SCHEMA_0 + '.etv_hwe_weights_pop_estimate_hisp_dvr_live_overlap',
                                '-s3_threshold_cap_path', S3_THRESHOLD_OVERLAP_CAP_PATH + 'dvr_live_overlap_weights_threshold_cap.csv',  # STATIC FILE
                                '-s3_output_folder', S3_DATA_WEEK_PATH + \
                                    TEST_PREFIX + WEEK_SCHEMA_0 + \
                                '/' + str(DMA),
                                '-country_num', str(DMA),
                                '-location', 'All_loc',  # STATIC VAR
                                '-email_address', 'adas@comscore.com',
                                '-max_iterations', '50',
                                '-max_threads', '100',
                                '-weights_output_name', 'dvr_live_overlap_weights',
                                '-weights_distribution_output_name', 'dvr_live_overlap_weights_distribution',
                                '-weights_info_name', 'dvr_live_overlap_weights_info',
                                '-weights_log_name', 'dvr_live_overlap_weights_log'
                            ]
                        }
                    ]
                },
                dag=dag)
            )

        Export_dvr_live_overlap_weights_S3_SnowSQL = BashOperator(
            task_id='Export_dvr_live_overlap_weights_S3_SnowSQL',
            bash_command='snowsql --config ' + CONFIG_PATH + '' +
                         ' -f ' + borg_level_sql_path + 'load_dvr_live_weights_gp.sql' +
                         ' -D  WEEK_SCHEMA_0=' + WEEK_SCHEMA_0 + '' +
                         ' -D  TEST_PREFIX=' + TEST_PREFIX + '',
            dag=dag)

        Export_weights_strata_info_hisp_vod_live_overlap_SnowSQL_S3.set_upstream(etv_vod_live_overlap_weights_input)
        Export_sample_roster_Pre_hisp_vod_live_overlap_SnowSQL_S3.set_upstream(etv_vod_live_overlap_weights_input)
        Export_pop_estimate_hisp_vod_live_overlap_SnowSQL_S3.set_upstream(etv_vod_live_overlap_weights_input)
        generate_vod_live_overlap_weights.set_upstream([
            Export_weights_strata_info_hisp_vod_live_overlap_SnowSQL_S3,
            Export_sample_roster_Pre_hisp_vod_live_overlap_SnowSQL_S3,
            Export_pop_estimate_hisp_vod_live_overlap_SnowSQL_S3
        ])
        Export_vod_live_overlap_weights_S3_SnowSQL.set_upstream(generate_vod_live_overlap_weights)
        Export_etv_hwe_weights_strata_info_hisp_dvr_live_overlap_SnowSQL_S3.set_upstream(etv_dvr_live_overlap_weights_input)
        Export_etv_hwe_weights_sample_roster_hisp_dvr_live_overlap_SnowSQL_S3.set_upstream(etv_dvr_live_overlap_weights_input)
        Export_etv_hwe_weights_pop_estimate_hisp_dvr_live_overlap_SnowSQL_S3.set_upstream(etv_dvr_live_overlap_weights_input)

        for Dma_weight in Dma_weights:
            Dma_weight.set_upstream([Export_etv_hwe_weights_strata_info_hisp_dvr_live_overlap_SnowSQL_S3,
                                     Export_etv_hwe_weights_sample_roster_hisp_dvr_live_overlap_SnowSQL_S3,
                                     Export_etv_hwe_weights_pop_estimate_hisp_dvr_live_overlap_SnowSQL_S3])

            Export_dvr_live_overlap_weights_S3_SnowSQL.set_upstream(Dma_weight)

    # Logical Break
    if "OLSMALL" in TaskList:

        Live_DVR_small_output_creation = BashOperator(
            task_id='Live_DVR_small_output_creation',
            bash_command='snowsql --config ' + CONFIG_PATH + '' +
                         ' -f ' + borg_level_sql_path + 'live_dvr_small_pre.sql' +
                         ' -D  WEEK_SCHEMA_0=' + Week + '',
            dag=dag)

        for DP in DPs:
            live_dvr_small = """live_dvr_small_for_{0} = BashOperator(
                 task_id= 'DVRINCSMALL_dvr_Incremental_Small_for_DP_' + str({0})+ '',
                 bash_command='snowsql --config ' +CONFIG_PATH +''
                          ' -f '+borg_level_sql_path + 'live_dvr_small.sql' +''
                          ' -D  WEEK_SCHEMA_0=' + Week + ''
                          ' -D daypart_id=' + str({0})  +''
                          ' -D  WEEK_0='  +WEEK_ID  +''
                          ,dag=dag)""".format(DP)

            exec (live_dvr_small)

        prep_live_DVR_1 = BashOperator(
            task_id='prep_live_DVR_1',
            bash_command='snowsql --config ' + CONFIG_PATH + '' +
                         ' -f ' + borg_level_sql_path + 'live_vod_small_pt1.sql' +
                         ' -D  WEEK_SCHEMA_0=' + Week + '' +
                         ' -D  WEEK_0=' + WEEK_ID + '',
            dag=dag
        )

        prep_live_DVR_2 = BashOperator(
            task_id='prep_live_DVR_2',
            bash_command='snowsql --config ' + CONFIG_PATH + '' +
                         ' -f ' + borg_level_sql_path + 'live_vod_small_pt2.sql' +
                         ' -D  WEEK_SCHEMA_0=' + Week + '' +
                         ' -D  WEEK_0=' + WEEK_ID + '',
            dag=dag
        )

        prep_live_DVR_3 = BashOperator(
            task_id='prep_live_DVR_3',
            bash_command='snowsql --config ' + CONFIG_PATH + '' +
                         ' -f ' + borg_level_sql_path + 'live_vod_small_pt3.sql' +
                         ' -D  WEEK_SCHEMA_0=' + Week + '' +
                         ' -D  WEEK_0=' + WEEK_ID + '',
            dag=dag
        )

        prep_live_DVR_4 = BashOperator(
            task_id='prep_live_DVR_4',
            bash_command='snowsql --config ' + CONFIG_PATH + '' +
                         ' -f ' + borg_level_sql_path + 'live_vod_small_pt4.sql' +
                         ' -D  WEEK_SCHEMA_0=' + Week + '' +
                         ' -D  WEEK_0=' + WEEK_ID + '',
            dag=dag
        )

        prep_live_DVR_5 = BashOperator(
            task_id='prep_live_DVR_5',
            bash_command='snowsql --config ' + CONFIG_PATH + '' +
                         ' -f ' + borg_level_sql_path + 'live_vod_small_pt5.sql' +
                         ' -D  WEEK_SCHEMA_0=' + Week + '' +
                         ' -D  WEEK_0=' + WEEK_ID + '',
            dag=dag
        )

        prep_live_DVR_6 = BashOperator(
            task_id='prep_live_DVR_6',
            bash_command='snowsql --config ' + CONFIG_PATH + '' +
                         ' -f ' + borg_level_sql_path + 'live_vod_small_pt6.sql' +
                         ' -D  WEEK_SCHEMA_0=' + Week + '' +
                         ' -D  WEEK_0=' + WEEK_ID + '',
            dag=dag
        )

        if DATA_QUALITY_CHECK == "TRUE":
            dqc_threeway_small = BashOperator(
                task_id='dqc_threeway_small',
                bash_command='{0}/dataqualitycheck.sh Overlap live_vod_small {1} Live_VOD \"@table_name={2}.etv_live_dvr_vod_small @table_previous={3}.etv_live_dvr_vod_small\" {0} {4}'.format(DQC_PATH, WEEK_ID, WEEK_SCHEMA_0, PREVIOUS_WEEK_1,CONFIG_PATH)
            , dag=dag)


        if "OLWEIGHTS" in TaskList:
            Live_DVR_small_output_creation.set_upstream(
                [Export_dvr_live_overlap_weights_S3_SnowSQL, Export_vod_live_overlap_weights_S3_SnowSQL])
            prep_live_DVR_1.set_upstream(
                [Export_dvr_live_overlap_weights_S3_SnowSQL, Export_vod_live_overlap_weights_S3_SnowSQL])
            prep_live_DVR_3.set_upstream(
                [Export_dvr_live_overlap_weights_S3_SnowSQL, Export_vod_live_overlap_weights_S3_SnowSQL])

        for DP in DPs:
            live_dvr_small_creation_dependency = """live_dvr_small_for_{0}.set_upstream(Live_DVR_small_output_creation)""" \
                .format(DP)
            exec(live_dvr_small_creation_dependency)

        prep_live_DVR_2.set_upstream(prep_live_DVR_1)
        prep_live_DVR_4.set_upstream([prep_live_DVR_3, prep_live_DVR_2])
        prep_live_DVR_5.set_upstream(prep_live_DVR_4)
        prep_live_DVR_6.set_upstream(prep_live_DVR_5)
        if DATA_QUALITY_CHECK == "TRUE":
            dqc_threeway_small.set_upstream(prep_live_DVR_6)

    # LOGICAL BREAK
    if "OLTTV" in TaskList:
        # need to pass required parameters
        live_dvr_vod_overlap_1 = BashOperator(
            task_id='live_dvr_vod_overlap_1',
            bash_command='snowsql --config ' + CONFIG_PATH + '' +
                         ' -f ' + borg_level_sql_path + 'all_overlap_Part1.sql' +
                         ' -D  WEEK_SCHEMA_0=' + Week + '' +
                         ' -D  WEEK_ID=' + WEEK_ID + '' +
                         ' -D  START_TIME_ID=' + START_TIME_ID + '' +
                         ' -D  END_TIME_ID=' + END_TIME_ID + '',
            dag=dag)

        live_dvr_vod_overlap_2 = BashOperator(
            task_id='live_dvr_vod_overlap_2',
            bash_command='python ' + borg_level_sql_path +
            'all_overlap_loop.py ' + WEEK_ID + ' ' + BASE_SQL_PATH,
            dag=dag)

        live_dvr_vod_overlap_3 = BashOperator(
            task_id='live_dvr_vod_overlap_3',
            bash_command='snowsql --config ' + CONFIG_PATH + '' +
                         ' -f ' + borg_level_sql_path + 'all_overlap_Part2.sql' +
                         ' -D  WEEK_SCHEMA_0=' + Week + '' +
                         ' -D  WEEK_0=' + WEEK_ID + '' +
                         ' -D  START_TIME_ID=' + START_TIME_ID + '' +
                         ' -D  END_TIME_ID=' + END_TIME_ID + '',
            dag=dag)

        live_dvr_vod_overlap_4 = BashOperator(
            task_id='live_dvr_vod_overlap_4',
            bash_command='python ' + borg_level_sql_path + 'jlee_tsv_bounding_base.py ' +
            BASE_SQL_PATH,
            dag=dag)

        live_dvr_vod_overlap_5 = BashOperator(
            task_id='live_dvr_vod_overlap_5',
            bash_command='snowsql --config ' + CONFIG_PATH + '' +
                         ' -f ' + borg_level_sql_path + 'all_overlap_Part3.sql' +
                         ' -D  PREVIOUS_WEEK_1=' + PREVIOUS_WEEK_1 + '' +
                         ' -D  WEEK_0=' + WEEK_ID + '' +
                         ' -D  WEEK_SCHEMA_0=' + Week + '' +
                         ' -D  START_TIME_ID=' + START_TIME_ID + '' +
                         ' -D  END_TIME_ID=' + END_TIME_ID + '',
            dag=dag)

        live_dvr_vod_overlap_6 = BashOperator(
            task_id='live_dvr_vod_overlap_6',
            bash_command='python ' + borg_level_sql_path + 'tv_bounding_4.py ' +
            BASE_SQL_PATH,
            dag=dag)

        live_dvr_vod_overlap_7 = BashOperator(
            task_id='live_dvr_vod_overlap_7',
            bash_command='snowsql --config ' + CONFIG_PATH + '' +
                         ' -f ' + borg_level_sql_path + 'all_overlap_Part4.sql' +
                         ' -D  WEEK_SCHEMA_0=' + Week + '' +
                         ' -D  WEEK_0=' + WEEK_ID + '' +
                         ' -D  START_TIME_ID=' + START_TIME_ID + '' +
                         ' -D  END_TIME_ID=' + END_TIME_ID + '',
            dag=dag)

        if "OLSMALL" in TaskList:
            if DATA_QUALITY_CHECK == "TRUE":
                live_dvr_vod_overlap_1.set_upstream(dqc_threeway_small)
            else:
                live_dvr_vod_overlap_1.set_upstream(prep_live_DVR_6)

            for DP in DPs:
                live_dvr_vod_dependency = """live_dvr_vod_overlap_1.set_upstream(live_dvr_small_for_{0})""".format(
                    DP)
                exec (live_dvr_vod_dependency)

        live_dvr_vod_overlap_2.set_upstream(live_dvr_vod_overlap_1)
        live_dvr_vod_overlap_3.set_upstream(live_dvr_vod_overlap_2)
        live_dvr_vod_overlap_4.set_upstream(live_dvr_vod_overlap_3)
        live_dvr_vod_overlap_5.set_upstream(live_dvr_vod_overlap_4)
        live_dvr_vod_overlap_6.set_upstream(live_dvr_vod_overlap_5)
        live_dvr_vod_overlap_7.set_upstream(live_dvr_vod_overlap_6)

    if int(WEEK_ID) == LAST_WEEKID_OF_BROADCAST_MONTH:

        if "MONTHLY" in TaskList:

            total_tv_small_inputs_prep_1 = BashOperator(
                task_id='total_tv_small_inputs_prep_1',
                bash_command='python {0} {1} {2} {3}'.format(
                    borg_level_sql_path + "total_tv_small_inputs_prep_step5_part1.py",
                    START_WEEK, END_WEEK, MONTH_ID) + ' ' +
                    BASE_SQL_PATH,
                dag=dag)

            total_tv_small_inputs_prep_2 = BashOperator(
                task_id='total_tv_small_inputs_prep_2',
                bash_command='snowsql --config ' + CONFIG_PATH + '' +
                             ' -f ' + borg_level_sql_path + 'total_tv_small_inputs_prep_step5_part2.sql' +
                             ' -D  WEEK_SCHEMA_0=' + Week + '' +
                             ' -D  START_WEEK=' + START_WEEK + '' +
                             ' -D  END_WEEK={}'.format(END_WEEK) +
                             ' -D  MONTH_ID={}'.format(MONTH_ID),
                dag=dag
            )

            total_tv_small_dvr = BashOperator(
                task_id='total_tv_small_dvr',
                bash_command='snowsql --config ' + CONFIG_PATH + '' +
                             ' -f ' + borg_level_sql_path + 'total_tv_small_step6b.sql' +
                             ' -D  WEEK_SCHEMA_0=' + Week + '' +
                             ' -D  PLATFORM=' + PLATFORM_DVR + '' +
                             ' -D  END_WEEK={}'.format(END_WEEK) +
                             ' -D  MONTH_ID={}'.format(MONTH_ID),
                dag=dag
            )

            total_tv_small_vod = BashOperator(
                task_id='total_tv_small_vod',
                bash_command='snowsql --config ' + CONFIG_PATH + '' +
                             ' -f ' + borg_level_sql_path + 'total_tv_small_step6.sql' +
                             ' -D  WEEK_SCHEMA_0=' + Week + '' +
                             ' -D  PLATFORM=' + PLATFORM_VOD + '' +
                             ' -D  END_WEEK={}'.format(END_WEEK) +
                             ' -D  MONTH_ID={}'.format(MONTH_ID),
                dag=dag
            )

            total_tv_overlap_dvr = BashOperator(
                task_id='total_tv_overlap_dvr',
                bash_command='snowsql --config ' + CONFIG_PATH + '' +
                             ' -f ' + borg_level_sql_path + 'total_tv_overlap_step7.sql' +
                             ' -D  WEEK_SCHEMA_0=' + Week + '' +
                             ' -D  PLATFORM=' + PLATFORM_DVR + '' +
                             ' -D  END_WEEK={}'.format(END_WEEK) +
                             ' -D  MONTH_ID={}'.format(MONTH_ID),
                dag=dag
            )

            total_tv_overlap_3ways = BashOperator(
                task_id='total_tv_overlap_3ways',
                bash_command='snowsql --config ' + CONFIG_PATH + '' +
                             ' -f ' + borg_level_sql_path + 'total_tv_overlap_step7b.sql' +
                             ' -D  WEEK_SCHEMA_0=' + Week + '' +
                             ' -D  PLATFORM=' + PLATFORM_VOD + '' +
                             ' -D  END_WEEK={}'.format(END_WEEK) +
                             ' -D  MONTH_ID={}'.format(MONTH_ID),
                dag=dag
            )

            live_dvr_vod_and_format = BashOperator(
                task_id='live_dvr_vod_and_format',
                bash_command='snowsql --config ' + CONFIG_PATH + '' +
                             ' -f ' + borg_level_sql_path + 'total_tv_live_dvr_vod_and_format_step8.sql' +
                             ' -D  WEEK_SCHEMA_0=' + Week + '' +
                             ' -D  START_WEEK={}'.format(START_WEEK) +
                             ' -D  END_WEEK={}'.format(END_WEEK) +
                             ' -D  MONTH_ID={}'.format(MONTH_ID),
                dag=dag
            )

            if "OLTTV" in TaskList:
                total_tv_small_inputs_prep_1.set_upstream(live_dvr_vod_overlap_7)
            total_tv_small_inputs_prep_2.set_upstream(total_tv_small_inputs_prep_1)
            total_tv_small_dvr.set_upstream(total_tv_small_inputs_prep_2)
            total_tv_small_vod.set_upstream(total_tv_small_inputs_prep_2)
            total_tv_overlap_dvr.set_upstream(total_tv_small_dvr)
            total_tv_overlap_3ways.set_upstream([total_tv_overlap_dvr, total_tv_small_vod])
            live_dvr_vod_and_format.set_upstream([total_tv_overlap_dvr, total_tv_overlap_3ways])

    return dag

etv_tv_overlap_batch_dag = etv_tv_overlap_batch('etv_tv_overlap_batch')
