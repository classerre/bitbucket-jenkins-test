from airflow import DAG
from airflow.contrib.operators.ssh_operator import SSHOperator
from datetime import datetime, timedelta
from airflow.contrib.hooks.ssh_hook import SSHHook
from airflow.models import Variable
from emr_operations_spark import emr_tasks
from airflow.operators.bash_operator import BashOperator
#from emr_operations import emr_tasks
from airflow.utils.email import send_email
from comscoreEmail import notify_email
from csdatetools import csdatetools

#sshHook = SSHHook(ssh_conn_id='emr_test')

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime.utcnow(),
    'email': ['sgude@comscore.com, vkomuravelli@comscore.com, skamidi@comscore.com, rbhallamudi@comscore.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=1),
    'on_failure_callback': notify_email
}
PLATFORM = 'live'
#WEEK_ID = "{{var.value.week_id}}"
WEEK_ID = Variable.get("week_id")
BASE_PATH = "{{var.value.base_s3_loc}}"+'/'
BASE_OUTPUT_PATH=BASE_PATH+WEEK_ID+'w/'
ETV_TV_JAR = '/home/hadoop/etv-1.0.0.jar'
DATA_EXPORT_SCRIPT_PATH = '/mapr/ri1.comscore.com/test_data/census/dags/SnowflakeSQL/DataCopy/dataExport_aws.sh'
WEEK_SCHEMA_0 = 'W'+ WEEK_ID
CONFIG_PATH =  Variable.get("config_path_spark_weekly_monthly_batch")     # '/mapr/ri1.comscore.com/test_data/census/dags/snowsql/snowflake.config' #
BASE_SQL_PATH = Variable.get("base_sql_path")+'etv_tv_live_small_monthly_spark_alpha/'  # '/mapr/ri1.comscore.com/test_data/census/dags/SnowflakeSQL/' #
TEST_PREFIX = Variable.get("test_suffix")
S3_STAGE_PATH = Variable.get("base_s3_path")
RUN_WITH_BLANK_FILES = Variable.get("run_with_blank_files")
def etv_tv_live_smalls_monthly_spark_alpha(dag_name):
    dag = DAG(dag_name, default_args=default_args, schedule_interval=None, concurrency=15)

    emr=emr_tasks(dag)
    sshHook=emr.get_ssh_hook("emr_test")

    transform_GP_monthly_dag_list = []
    Run_Transpose_monthly_for_part =[]
    #Run_Transpose_Weekly for the given week for 0 to 25 partitions
    for i in range(25):
        temp = SSHOperator(task_id='Run_Transpose_monthly_for_part_'+str(i),
                           command='spark-submit --master yarn'
                                   ' --driver-memory 1g'
                                   ' --conf spark.yarn.executor.memoryOverhead=5g'
                                   ' --class com.comscore.xp.etv.drivers.Main'
                                   ' --num-executors 30'
                                   ' --executor-memory 20g'
                                   ' --executor-cores 6 '+ETV_TV_JAR+''
                                   ' --sparkSerializer org.apache.spark.serializer.KryoSerializer'
                                   ' --primaryStorageLevel MEMORY_AND_DISK_SER'
                                   ' --secondaryStorageLevel DISK_ONLY'
                                   ' --appName ETV_smallIncremental_monthly_transpose_live_'+WEEK_ID+'w_partition_'+str(i)+' '
                                   ' --weekId ' + WEEK_ID + ''
                                   ' --jobType transposeMonthly'
                                   ' --espressoAggBasePath '+ BASE_OUTPUT_PATH + PLATFORM + '/Small/Weekly/partitioned_espresso_Weekly'
                                   ' --partitionedEspressoAggPath '+ BASE_OUTPUT_PATH + PLATFORM + '/Small/Weekly/partitioned_espresso_Weekly/partitionId='+str(i)+''
                                   ' --espressoAggTransposedPath ' + BASE_OUTPUT_PATH + PLATFORM + '/Small/Monthly/Transpose_live_'+str(WEEK_ID)+'w/part'+str(i)+' ',
                           dag=dag,
                           ssh_hook=sshHook
                           )
        Run_Transpose_monthly_for_part.append(temp)

    LAST_TIME_ID = str(csdatetools.get_last_time_id_in_week_id(int(WEEK_ID)))
    BC_MONTH_ID = str(csdatetools.time_id_to_broadcast_month(int(LAST_TIME_ID)))
    FIRST_WEEK_OF_MONTH = str(csdatetools.get_first_week_id_in_broadcast_month(int(BC_MONTH_ID)))

    if int(FIRST_WEEK_OF_MONTH )!= int(WEEK_ID):

        Experian_filter_1 =[]
        Experian_filter_2 =[]
        Experian_filter_3 =[]
        Experian_filter_4 =[]
        Experian_filter_5 =[]
        Experian_filter_6 =[]

        #Experian_Filter_on_Transpose_Weekly for the given week
        for i in range(25):
            temp1 = SSHOperator(task_id='LTT-Experian_filter_on_transpose_weekly_part'+str(i),
                           command='spark-submit --master yarn --driver-memory 1g'
                                   ' --conf spark.yarn.executor.memoryOverhead=5g'
                                   ' --class com.comscore.xp.etv.drivers.Main'
                                   ' --num-executors 30'
                                   ' --executor-memory 20g'
                                   ' --executor-cores 6 '+ETV_TV_JAR+''
                                   ' --sparkSerializer org.apache.spark.serializer.KryoSerializer'
                                   ' --primaryStorageLevel MEMORY_AND_DISK_SER'
                                   ' --secondaryStorageLevel DISK_ONLY '
                                   ' --appName ETV_smallIncremental_monthly_ltt_experian_filter_transpose_'+WEEK_ID+'w_partition_'+str(i)+' '
                                   ' --weekId  '+WEEK_ID+''
                                   ' --jobType experianOnlyMonthly '
                                   ' --espressoAggTransposedPath ' + BASE_OUTPUT_PATH + PLATFORM + '/Small/Monthly/Transpose_live_'+str(WEEK_ID)+'w/part'+str(i)+' '
                                   ' --experianLookUpPath ' + BASE_OUTPUT_PATH + PLATFORM + '/Small/experian_only_roster_live_'+WEEK_ID+'w/incremental_small '
                                   ' --expOnlyTransposedPath '+ BASE_OUTPUT_PATH + PLATFORM + '/Small/Monthly/experian_transpose_'+WEEK_ID+'w_monthly/part'+str(i)+' ',
                           dag=dag,
                           ssh_hook=sshHook
                           )
            Experian_filter_1.append(temp1)
            temp1.set_upstream(Run_Transpose_monthly_for_part[i])
        if RUN_WITH_BLANK_FILES == "FALSE":
            temparoty_counter = 1
            #Experian_filter_on_transpose_weekly from 955 to 959(0 to 24 partitions)
            for i in range(int(WEEK_ID)-5, int(WEEK_ID)):
                #for i in range(955, 960):
                temparoty_counter = temparoty_counter+ 1
                for j in range(25):
                    Experian_filter =  SSHOperator(task_id='LTT-Experian_filter_on_transpose_weekly_'+str(i)+'w_for_part_'+str(j),
                               command='spark-submit --master yarn '
                                       ' --driver-memory 1g '
                                       ' --conf spark.yarn.executor.memoryOverhead=5g'
                                       ' --class com.comscore.xp.etv.drivers.Main'
                                       ' --num-executors 30 '
                                       ' --executor-memory 20g '
                                       ' --executor-cores 6 '+ETV_TV_JAR+''
                                       ' --sparkSerializer org.apache.spark.serializer.KryoSerializer'
                                       ' --primaryStorageLevel MEMORSER'
                                       ' --secondaryStorageLevel DISK_ONLY'
                                       ' --appName ETV_smallIncremental_monthly_ltt_experian_filter_transpose_'+str(i)+'w_partition_'+str(j)+'_for_'+WEEK_ID+'w '
                                       ' --weekId '+str(i)+''
                                       ' --jobType experianOnlyMonthly '
                                       ' --espressoAggTransposedPath ' + BASE_PATH+''+str(i)+'w/' + PLATFORM + '/Small/Monthly/Transpose_live_'+str(i)+'w/part'+str(j)+' '
                                       ' --experianLookUpPath ' + BASE_OUTPUT_PATH + PLATFORM + '/Small/experian_only_roster_live_'+WEEK_ID+'w/incremental_small '
                                       ' --expOnlyTransposedPath '+ BASE_OUTPUT_PATH + PLATFORM + '/Small/Monthly/experian_transpose_'+str(i)+'w_monthly/part'+str(j)+' ',
                               dag=dag,
                               ssh_hook=sshHook
                               )
                    if(temparoty_counter == 2):
                        Experian_filter_2.append(Experian_filter)
                    if(temparoty_counter == 3):
                        Experian_filter_3.append(Experian_filter)
                    if(temparoty_counter == 4):
                        Experian_filter_4.append(Experian_filter)
                    if(temparoty_counter == 5):
                        Experian_filter_5.append(Experian_filter)
                    if(temparoty_counter == 6):
                        Experian_filter_6.append(Experian_filter)
                    Experian_filter.set_upstream(Run_Transpose_monthly_for_part[j])

        Run_Matrix_JOBS = []
            #Run_Matrix_for_Partition from 0 to 24
        for i in range(25):
            Run_Matrix = SSHOperator(task_id='run_matrix_for_part_'+str(i),
                              command='spark-submit --master yarn '
                                      ' --driver-memory 1g'
                                      ' --conf spark.dynamicAllocation.minExecutors=5 '
                                      ' --conf spark.dynamicAllocation.maxExecutors=40 '
                                      ' --conf spark.dynamicAllocation.initialExecutors=10 '
                                      ' --class com.comscore.xp.etv.drivers.Main'
                                      ' --num-executors 40'
                                      ' --executor-memory 29g'
                                      ' --executor-cores 6 '+ETV_TV_JAR+''
                                      ' --sparkSerializer org.apache.spark.serializer.KryoSerializer '
                                      ' --primaryStorageLevel MEMORY_AND_DISK_SER '
                                      ' --secondaryStorageLevel DISK_ONLY '
                                      ' --appName ETV_smallIncremental_monthly_6WeeksMatrix_live_'+WEEK_ID+'w_partition_'+str(i)+' '
                                      ' --weekId '+WEEK_ID+''
                                      ' --jobType 6WeeksMatrixMonthly'
                                      ' --expOnlyTransposedCurrentWeekPath '+ BASE_OUTPUT_PATH + PLATFORM + '/Small/Monthly/experian_transpose_'+(WEEK_ID)+'w_monthly/part'+str(i)+' '
                                      ' --expOnlyTransposedPrevWeek1Path ' + BASE_OUTPUT_PATH + PLATFORM + '/Small/Monthly/experian_transpose_'+(str(int(WEEK_ID)-1))+'w_monthly/part'+str(i)+' '
                                      ' --expOnlyTransposedPrevWeek2Path '+ BASE_OUTPUT_PATH + PLATFORM + '/Small/Monthly/experian_transpose_'+str((int(WEEK_ID)-2))+'w_monthly/part'+str(i)+' '
                                      ' --expOnlyTransposedPrevWeek3Path ' + BASE_OUTPUT_PATH + PLATFORM + '/Small/Monthly/experian_transpose_'+str((int(WEEK_ID)-3))+'w_monthly/part'+str(i)+' '
                                      ' --expOnlyTransposedPrevWeek4Path '+BASE_OUTPUT_PATH + PLATFORM + '/Small/Monthly/experian_transpose_'+str((int(WEEK_ID)-4))+'w_monthly/part'+str(i)+' '
                                      ' --expOnlyTransposedPrevWeek5Path '+BASE_OUTPUT_PATH + PLATFORM + '/Small/Monthly/experian_transpose_'+str((int(WEEK_ID)-5))+'w_monthly/part'+str(i)+' '
                                      ' --espressoAggMatrix '+BASE_OUTPUT_PATH + PLATFORM + '/Small/Monthly/espresso_agg_matrix_monthly/part'+str(i)+' ',
                              dag=dag,
                              ssh_hook=sshHook
                              )
            Run_Matrix.set_upstream(Experian_filter_1[i])
            if RUN_WITH_BLANK_FILES == "FALSE":
                    Run_Matrix.set_upstream(Experian_filter_2[i])
                    Run_Matrix.set_upstream(Experian_filter_3[i])
                    Run_Matrix.set_upstream(Experian_filter_4[i])
                    Run_Matrix.set_upstream(Experian_filter_5[i])
                    Run_Matrix.set_upstream(Experian_filter_6[i])
            Run_Matrix_JOBS.append(Run_Matrix)
               
            FIRST_TIME_ID = str(csdatetools.broadcast_month_to_first_time_id(int(BC_MONTH_ID)))  
            SECOND_WEEK_OF_MONTH = str(csdatetools.time_id_to_week_id(int(FIRST_TIME_ID )+ 7))
            THIRD_WEEK_OF_MONTH = str(csdatetools.time_id_to_week_id(int(FIRST_TIME_ID) + 14))
            FOURTH_WEEK_OF_MONTH = str(csdatetools.time_id_to_week_id(int(FIRST_TIME_ID) + 21))
            FIFTH_WEEK_OF_MONTH = str(csdatetools.time_id_to_week_id(int(FIRST_TIME_ID) + 28))

            calcAWeeks =  'pw1' if int(WEEK_ID)== int(SECOND_WEEK_OF_MONTH) else ('pw1,pw2' if int(WEEK_ID) == int(THIRD_WEEK_OF_MONTH) else ('pw1,pw2,pw3' if int(WEEK_ID) == int(FOURTH_WEEK_OF_MONTH) else ('pw1,pw2,pw3,pw4' if int(WEEK_ID) == int(FIFTH_WEEK_OF_MONTH) else '')))


        small_Computer_Monthly_dag_list = []
        for i in range(25):
            Run_smalls_for_part = SSHOperator(task_id='Run_Smalls_For_Part_' + str(i),
                                 command='spark-submit --master yarn'
                                         ' --driver-memory 1g'
                                         ' --conf spark.sql.shuffle.partitions=1000'
                                         ' --conf spark.network.timeout=600'
                                         ' --class com.comscore.xp.etv.drivers.Main '
                                         ' --num-executors 30'
                                         ' --executor-memory 29g'
                                         ' --executor-cores 6  ' + ETV_TV_JAR + ''
                                         ' --sparkSerializer org.apache.spark.serializer.KryoSerializer'
                                         ' --primaryStorageLevel MEMORY_AND_DISK_SER'
                                         ' --secondaryStorageLevel DISK_ONLY'
                                         ' --appName ETV_TV_small_computer_monthly_' + PLATFORM + '_'+WEEK_ID+'w_partition_' + str(i) + ' '
                                         ' --weekId ' + WEEK_ID + ''
                                         ' --jobType smallComputerMonthly'
                                         ' --platform ' + PLATFORM + ''
                                         ' --espressoAggMatrix '+BASE_OUTPUT_PATH + PLATFORM + '/Small/Monthly/espresso_agg_matrix_monthly/part' + str(i) + ' '
                                         ' --lttDemoLookUpPath  '+ BASE_OUTPUT_PATH +PLATFORM+ '/Small/singular_key_demo_lookup_live_'+ WEEK_ID + 'w'
                                         ' --smallComputation '+BASE_OUTPUT_PATH + PLATFORM + '/Small/Monthly/Smalls_live_'+ WEEK_ID + 'w/part' + str(i)+' '
                                         '--calc1AWeeks "'+calcAWeeks +'" '
                                         '--calc2AWeeks "'+calcAWeeks +'" ' ,
                                 dag=dag,
                                 ssh_hook=sshHook)

            small_Computer_Monthly_dag_list.append(Run_smalls_for_part)
            Run_smalls_for_part.set_upstream(Run_Matrix_JOBS[i])

        transform_GP_monthly_dag_list = []
        for i in range(25):
            Run_final_incremental_report = SSHOperator(task_id='Run_Final_Incremental_Report_For_Part_' + str(i) + '',
                               command='spark-submit --master yarn'
                                       ' --driver-memory 1g'
                                       ' --class com.comscore.xp.etv.drivers.Main'
                                       ' --executor-memory 25g'
                                       ' --executor-cores 6 ' + ETV_TV_JAR + ' '
                                       ' --sparkSerializer org.apache.spark.serializer.KryoSerializer'
                                       ' --primaryStorageLevel MEMORY_AND_DISK_SER'
                                       ' --secondaryStorageLevel DISK_ONLY'
                                       ' --appName ETV_small_final_monthly_report_' + PLATFORM + '_'+WEEK_ID+'w_partition_' + str(i) + ' '
                                       ' --weekId ' + WEEK_ID + ''
                                       ' --jobType transformGPMonthly'
                                       ' --platform ' + PLATFORM + ''
                                       ' --smallComputerPath '+BASE_OUTPUT_PATH + PLATFORM + '/Small/Monthly/Smalls_live_'+ WEEK_ID + 'w/part' + str(i)+' '
                                       ' --smallComputerExpandedPath '+BASE_OUTPUT_PATH + PLATFORM + '/Small/Monthly/report_generation_monthly/part' + str(i)+'',
                               dag=dag,
                               ssh_hook=sshHook
                               )
            transform_GP_monthly_dag_list.append(Run_final_incremental_report)
            Run_final_incremental_report.set_upstream(small_Computer_Monthly_dag_list[i])
                #Export_etv_live_monthly_partial_small_S3_SnowSQL.set_upstream(Run_final_in)

        #Run_Final_Inc_Report = []
        #for i in range(25):
        Export_etv_live_monthly_partial_small_S3_SnowSQL = BashOperator(task_id='Export_etv_live_monthly_partial_small_S3_SnowSQL',
                                                                  bash_command='snowsql --config {} '.format(CONFIG_PATH) +
                                                                               '-f {}Load_etv_live_monthly_partial_small_s3_snowflake.sql '.format(BASE_SQL_PATH) +
                                                                               '-D WEEK_SCHEMA_0={} '.format(WEEK_SCHEMA_0) +
                                                                               '-D TEST_PREFIX={} '.format(TEST_PREFIX) +
                                                                               '-D PLATFORM={} '.format(PLATFORM) +
                                                                               '-D WEEK_ID={}'.format(str(WEEK_ID)+'w')
                                                                  ,dag=dag)

        Export_etv_live_monthly_partial_small_S3_SnowSQL.set_upstream(transform_GP_monthly_dag_list)
        emr.delete_cluster().set_upstream(Export_etv_live_monthly_partial_small_S3_SnowSQL)
    emr.delete_cluster().set_upstream(Run_Transpose_monthly_for_part)

    return dag

etv_tv_live_smalls_monthly_spark_alpha_dag = etv_tv_live_smalls_monthly_spark_alpha('etv_tv_live_smalls_monthly_spark_alpha')
