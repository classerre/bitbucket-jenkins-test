from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from datetime import datetime, timedelta
from airflow.models import Variable
from comscoreEmail import notify_email

from csdatetools.csdatetools import get_first_time_id_in_week_id, time_id_to_month_id

default_args = {
    'owner': 'etv',
    'depends_on_past': False,
    'start_date': datetime(2019, 1, 31),
    'email': ['sgude@comscore.com, vkomuravelli@comscore.com, skamidi@comscore.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=1),
    'on_failure_callback': notify_email
}


def etv_tv_latte_pig(dag_name):
    dag = DAG(dag_name, default_args=default_args, schedule_interval=None)

    # DAG Specific constants
    PRODUCT = 'tv'
    PLATFORM_COMMON = 'common'
    PLATFORM_LIVE = 'live'
    PLATFORM_LOOKUPS = 'lookups'

    # Variables
    BASE_SQL_PATH = Variable.get("base_sql_path") + 'etv_tv_latte_pig'
    WEEK_ID = Variable.get("week_id")  #
    Tasks = Variable.get("etv_tv_latte_pig_TaskList")
    CONFIG_PATH = Variable.get("config_path")
    DQC_PATH = Variable.get("data_quality_check_path")
    DATA_QUALITY_CHECK = Variable.get("data_quality_check")

    # Derived Variables
    WEEK_ID_0 = WEEK_ID
    WEEK_ID_1 = str(int(WEEK_ID) - 1)
    WEEK_ID_2 = str(int(WEEK_ID) - 2)
    WEEK_ID_3 = str(int(WEEK_ID) - 3)
    WEEK_ID_4 = str(int(WEEK_ID) - 4)
    WEEK_ID_5 = str(int(WEEK_ID) - 5)
    TaskList = Tasks.split(",")
    WEEK_SCHEMA_0 = 'W' + str(WEEK_ID_0)
    PREVIOUS_WEEK_1 = 'W' + str(int(WEEK_ID) - 1)


    # Function calls
    MONTH_ID = str(time_id_to_month_id(get_first_time_id_in_week_id(int(WEEK_ID))))  # '228'

    if "ETV_TV_LIVE_LATTE" in TaskList:
        Generate_Demo_distribution_and_VPVH_CVF_Lookup_table = BashOperator(
            task_id='Generate_Demo_distribution_and_VPVH_CVF_Lookup_table',
            bash_command='snowsql --config ' + CONFIG_PATH + '' +
                         ' -f ' + BASE_SQL_PATH + '/demo_dist_vpvh_cvf.sql' +
                         ' -D  WEEK_SCHEMA_0=w' + WEEK_ID_0 + '' +
                         ' -D  WEEK_0=' + WEEK_ID_0 + '',
            dag=dag
        )


        Prepare_Live_Viewership = BashOperator(
            task_id='Prepare_Live_Viewership',
            bash_command='snowsql --config ' + CONFIG_PATH + '' +
                         ' -f ' + BASE_SQL_PATH + '/prepare_live_viewership.sql' +
                         ' -D  WEEK_SCHEMA_0=w' + WEEK_ID_0 + '' +
                         ' -D  WEEK_0=' + WEEK_ID_0 + '',
            dag=dag
        )

        Generate_Assigned_Web_Agg = BashOperator(
            task_id='Generate_Assigned_Web_Agg_for',
            bash_command='snowsql --config ' + CONFIG_PATH + '' +
                         ' -f ' + BASE_SQL_PATH + '/assigned_web_agg.sql' +
                         ' -D  WEEK_SCHEMA_0=w' + WEEK_ID_0 + '' +
                         ' -D  WEEK_0=' + WEEK_ID_0 + '',
            dag=dag
        )

        Run_Hierarchy_Fix_To_generate_Demo_Web_Agg = BashOperator(
            task_id='Run_Hierarchy_Fix_To_generate_Demo_Web_Agg',
            bash_command='snowsql --config ' + CONFIG_PATH + '' +
                         ' -f ' + BASE_SQL_PATH + '/hierarchy_fix.sql' +
                         ' -D  WEEK_SCHEMA_0=w' + WEEK_ID_0 + '' +
                         ' -D  WEEK_0=' + WEEK_ID_0 + '',
            dag=dag)

        Generate_Marginal_Latte = BashOperator(
            task_id='Generate_Marginal_Latte',
            bash_command='snowsql --config ' + CONFIG_PATH + '' +
                         ' -f ' + BASE_SQL_PATH + '/marginal_latte.sql' +
                         ' -D  WEEK_SCHEMA_0=w' + WEEK_ID_0 + '' +
                         ' -D  month_id=' + MONTH_ID + '' +
                         ' -D  WEEK_0=' + WEEK_ID_0 + '',
            dag=dag
        )

	if DATA_QUALITY_CHECK == "TRUE":
			DQC_for_Latte	= BashOperator(task_id='DQC_for_Latte',
			bash_command = DQC_PATH+'/dataqualitycheck.sh Latte latte '+WEEK_ID+' LIVE \"@table_name='+WEEK_SCHEMA_0
			+'.etv_experian_live_latte @table_previous='+PREVIOUS_WEEK_1+'.etv_experian_live_latte\" '+DQC_PATH+' '+CONFIG_PATH,
			dag=dag
			)

    # Logical Break
    if "VENTI_AND_HOUSEHOLD_RANKING_STATS" in TaskList:
        Generate_Household_Ranking_stats = BashOperator(
            task_id='Generate_Household_Ranking_stats',
            bash_command='snowsql --config ' + CONFIG_PATH + '' +
                         ' -f ' + BASE_SQL_PATH + '/household_ranking.sql' +
                         ' -D  WEEK_SCHEMA_0=w' + WEEK_ID_0 + '' +
                         ' -D  WEEK_0=' + WEEK_ID_0 + '' +
                         ' -D  PLATFORM=' + PLATFORM_LIVE + '',
            dag=dag
        )

        Run_Generic_Stats_For_Latte_Viewership = BashOperator(
            task_id='Run_Generic_Stats_For_Latte_Viewership',
            bash_command='snowsql --config ' + CONFIG_PATH + '' +
                         ' -f ' + BASE_SQL_PATH + '/genericVentiStats.sql' +
                         ' -D  WEEK_SCHEMA_0=w' + WEEK_ID_0 + '' +
                         ' -D  WEEK_0=' + WEEK_ID_0 + '',
            dag=dag
        )

	if DATA_QUALITY_CHECK == "TRUE":
			DQC_for_Venti	= BashOperator(task_id='DQC_for_Venti',
			bash_command = DQC_PATH+'/dataqualitycheck.sh Viewership viewership '+WEEK_ID+' LIVE \"@table_name='+WEEK_SCHEMA_0
			+'.etv_live_venti_stats @table_previous='+PREVIOUS_WEEK_1+'.etv_live_venti_stats\" '+DQC_PATH+' '+CONFIG_PATH,
			dag=dag
			)


    if "ETV_TV_LIVE_LATTE" in TaskList:
        Generate_Assigned_Web_Agg.set_upstream(
            [Generate_Demo_distribution_and_VPVH_CVF_Lookup_table, Prepare_Live_Viewership])
        Run_Hierarchy_Fix_To_generate_Demo_Web_Agg.set_upstream(Generate_Assigned_Web_Agg)
        Generate_Marginal_Latte.set_upstream(Run_Hierarchy_Fix_To_generate_Demo_Web_Agg)

	if DATA_QUALITY_CHECK == "TRUE":
		DQC_for_Latte.set_upstream(Generate_Marginal_Latte)

    # Logical Break
    if "VENTI_AND_HOUSEHOLD_RANKING_STATS" in TaskList:
        if "ETV_TV_LIVE_LATTE" in TaskList:
            Generate_Household_Ranking_stats.set_upstream(Prepare_Live_Viewership)
            Run_Generic_Stats_For_Latte_Viewership.set_upstream(Prepare_Live_Viewership)

	if DATA_QUALITY_CHECK == "TRUE":
		DQC_for_Venti.set_upstream(Run_Generic_Stats_For_Latte_Viewership)

    return dag


etv_tv_latte_pig_dag = etv_tv_latte_pig('etv_tv_latte_pig')
