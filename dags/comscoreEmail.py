from airflow.utils.email import send_email
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.email_operator import EmailOperator
from datetime import datetime, timedelta  
from airflow.models import Variable

def notify_email(contextDict, **kwargs):
    """Send custom email alerts."""
    # Email Distibution List
    to_email = 'comscoreetvmigration@ggktech.com,xmedia-engineering@comScore.onmicrosoft.com,xmediasupport@comscore.com,tnaganuma@comscore.com'
    # email title.
    task = "{task_instance_key_str}".format(**contextDict)
    title = "ETV Data Loads: Failure in ETL processing at " + task
    # email contents
    body = """
    Hello Everyone, <br>
    <br>
    ETV Data loads encountered a failure at """ + task + """<br>
    <br>
    Please take necessary actions. More Details at http://cviadpetl01:8080/admin/<br>
    <br>
    Thanks,<br>
    ETV Operations <br>
    """.format(**contextDict)
    send_email(to_email, title, body)


def returnEmailOperator(dag_obj, week_id, batch_name):
    mail_content = """
    Hello Everyone,<br><br>
    ETV Data loads is successful for week- {0} for {1}<br><br>
    More Details at http://cviadpetl01:8080/admin/<br>
    <br>
    Cheers,<br>
    ETV Operations<br>
    """.format(week_id, batch_name)

    return EmailOperator(
        task_id='success_notification',
        to='comscoreetvmigration@ggktech.com,xmedia-engineering@comScore.onmicrosoft.com,xmediasupport@comscore.com,tnaganuma@comscore.com',
        subject='ETV Data Loads: {0} {1}w completed'.format(
            week_id, batch_name),
        html_content=mail_content,
        dag=dag_obj)
