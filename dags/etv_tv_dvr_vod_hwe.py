from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from datetime import datetime, timedelta
from airflow.models import Variable
from comscoreEmail import notify_email
from csdatetools.csdatetools import get_first_time_id_in_week_id, get_last_time_id_in_week_id, time_id_to_month_id, \
    time_id_to_yyyymmdd

default_args = {
    'owner': 'etv',
    'depends_on_past': False,
    'start_date': datetime(2019, 1, 31),
    'email': ['sgude@comscore.com, vkomuravelli@comscore.com, skamidi@comscore.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=1),
    'on_failure_callback': notify_email
}

# Global Variables
CONFIG_PATH = Variable.get("config_path")
BASE_SQL_PATH = Variable.get("base_sql_path") + 'etv_tv_dvr_vod_hwe'
DQC_PATH = Variable.get("data_quality_check_path")
DATA_QUALITY_CHECK = Variable.get("data_quality_check")

# Local Variables
WEEK_ID = Variable.get("week_id")
PREVIOUS_WEEK_1 = 'W' + str(int(WEEK_ID) - 1)
Tasks = Variable.get("ETV_TV_DVR_VOD_TaskList")

FIRST_TIME_ID_THRESHOLD = Variable.get("first_time_id_threshold")
FIRST_TIME_ID_IN_RANGE = str(get_first_time_id_in_week_id(int(WEEK_ID)))
LAST_TIME_ID_IN_RANGE = str(get_last_time_id_in_week_id(int(WEEK_ID)))
MONTH_ID = str(time_id_to_month_id(get_last_time_id_in_week_id(int(WEEK_ID))))
START_DATE_ID = str(time_id_to_yyyymmdd(int(FIRST_TIME_ID_IN_RANGE) - int(FIRST_TIME_ID_THRESHOLD)))
END_DATE_ID = str(time_id_to_yyyymmdd(int(LAST_TIME_ID_IN_RANGE) + 1))
START_DATE = str(time_id_to_yyyymmdd(get_first_time_id_in_week_id(int(WEEK_ID))))
END_DATE = str(time_id_to_yyyymmdd(get_last_time_id_in_week_id(int(WEEK_ID))))
START_TIME = str(get_first_time_id_in_week_id(int(WEEK_ID)))
END_TIME = str(get_last_time_id_in_week_id(int(WEEK_ID)))

# Derived Variables
WEEK_SCHEMA_0 = 'W' + WEEK_ID
TaskList = Tasks.split(",")


def etv_tv_dvr_vod_hwe(dag_name):

    dag = DAG(dag_name, default_args=default_args, schedule_interval=None)

    if "HWEVOD" in TaskList:
        PLATFORM = 'vod'
        VOD_HWE_SQL = ''
        if WEEK_ID >= 964 and WEEK_ID < 974:
            VOD_HWE_SQL = 'vod_hwe_TWC_exclusion.sql'
        else:
            VOD_HWE_SQL = 'vod_hwe.sql'

        vod_to_tvd_title_and_owner_mapping = BashOperator(
            task_id='vod_to_tvd_title_and_owner_mapping',
            bash_command='snowsql --config {}'.format(CONFIG_PATH) +
                         ' -f {}/VOD_title_and_owner_mapping_production_psql.sql '.format(BASE_SQL_PATH) +
                         ' -D WEEK_SCHEMA_0={} '.format(WEEK_SCHEMA_0) +
                         ' -D MONTH_ID={} '.format(MONTH_ID) +
                         ' -D WEEK_0={} '.format(WEEK_ID) +
                         ' -D START_DATE={} '.format(START_DATE) +
                         ' -D END_DATE={} '.format(END_DATE)
            , dag=dag)

        vod_hwe = BashOperator(
            task_id='vod_hwe',
            bash_command='snowsql --config {}'.format(CONFIG_PATH) +
                         ' -f {0}/{1} '.format(BASE_SQL_PATH, VOD_HWE_SQL) +
                         ' -D WEEK_SCHEMA_0={} '.format(WEEK_SCHEMA_0) +
                         ' -D WEEK_0={} '.format(WEEK_ID) +
                         ' -D START_DATE={} '.format(START_DATE) +
                         ' -D END_DATE={} '.format(END_DATE)
            , dag=dag)

        if DATA_QUALITY_CHECK == "TRUE":
            dqc_hwe_vod = BashOperator(
                task_id='dqc_hwe_vod',
                bash_command='{0}/dataqualitycheck.sh HWE hwe {1} VOD \"@table_name={2}.etv_esp_vod_tv_hwe @table_previous={3}.etv_esp_vod_tv_hwe\" {0} {4}'.format(DQC_PATH, WEEK_ID, WEEK_SCHEMA_0, PREVIOUS_WEEK_1, CONFIG_PATH)
            , dag=dag)

    if "MRVOD" in TaskList:
        PLATFORM = 'vod'
        if WEEK_ID > 899:
            master_roster_vod = BashOperator(
                task_id='master_roster_vod',
                bash_command='snowsql --config {}'.format(CONFIG_PATH) +
                             ' -f {}/master_roster_w_activity_flag_VOD.sql '.format(BASE_SQL_PATH) +
                             ' -D WEEK_SCHEMA_0={} '.format(WEEK_SCHEMA_0) +
                             ' -D platform={} '.format(PLATFORM) +
                             ' -D startTime={} '.format(START_TIME) +
                             ' -D endTime={} '.format(END_TIME)
                , dag=dag)

            master_roster_vod_view_creation = BashOperator(
                task_id='master_roster_vod_view_creation',
                bash_command='snowsql --config {}'.format(CONFIG_PATH) +
                             ' -f {}/master_roster_w_activity_flag_view_creation.sql '.format(BASE_SQL_PATH) +
                             ' -D WEEK_SCHEMA_0={} '.format(WEEK_SCHEMA_0)
                , dag=dag)

            if DATA_QUALITY_CHECK == "TRUE":
                dqc_mr_vod = BashOperator(
                    task_id='dqc_mr_vod',
                    bash_command='{0}/dataqualitycheck.sh Pre_Espresso master_roster_active {1} TV \"@table_name={2}.master_roster_ext_tv @table_previous={3}.master_roster_ext_tv\" {0} {4}'.format(DQC_PATH, WEEK_ID, WEEK_SCHEMA_0, PREVIOUS_WEEK_1, CONFIG_PATH)
                , dag=dag)

                dqc_mr_vod.set_upstream(master_roster_vod_view_creation)

        else:
            master_roster_vod_pdg = BashOperator(
                task_id='master_roster_vod_pdg',
                bash_command='snowsql --config {}'.format(CONFIG_PATH) +
                             ' -f {}/master_roster_pdg_vod.sql '.format(BASE_SQL_PATH) +
                             ' -D WEEK_SCHEMA_0={} '.format(WEEK_SCHEMA_0) +
                             ' -D platform={} '.format(PLATFORM) +
                             ' -D startTime={} '.format(START_TIME) +
                             ' -D endTime={} '.format(END_TIME)
                , dag=dag)

            master_roster_vod_pdg_view_creation = BashOperator(
                task_id='master_roster_vod_pdg_view_creation',
                bash_command='snowsql --config {}'.format(CONFIG_PATH) +
                             ' -f {}/master_roster_w_activity_flag_pdg.sql '.format(BASE_SQL_PATH) +
                             ' -D WEEK_SCHEMA_0={} '.format(WEEK_SCHEMA_0)
                , dag=dag)


            if DATA_QUALITY_CHECK == "TRUE":
                dqc_mr_vod = BashOperator(
                    task_id='dqc_mr_vod',
                    bash_command='{0}/dataqualitycheck.sh Pre_Espresso master_roster_active {1} TV \"@table_name={2}.master_roster_ext_tv @table_previous={3}.master_roster_ext_tv\" {0} {4}'.format(DQC_PATH, WEEK_ID, WEEK_SCHEMA_0, PREVIOUS_WEEK_1, CONFIG_PATH)
                , dag=dag)

                dqc_mr_vod.set_upstream(master_roster_vod_view_creation)

        if WEEK_ID >= 964 and WEEK_ID < 974:
            master_roster_vod_twc = BashOperator(
                task_id='master_roster_vod_twc',
                bash_command='snowsql --config {}'.format(CONFIG_PATH) +
                             ' -f {}/master_roster_w_activity_flag_TWC_exclusion.sql '.format(BASE_SQL_PATH) +
                             ' -D WEEK_SCHEMA_0={} '.format(WEEK_SCHEMA_0) +
                             ' -D platform={} '.format(PLATFORM)
                , dag=dag)


            if DATA_QUALITY_CHECK == "TRUE":
                dqc_mr_vod = BashOperator(
                    task_id='dqc_mr_vod',
                    bash_command='{0}/dataqualitycheck.sh Pre_Espresso master_roster_active {1} VOD \"@table_name={2}.master_roster_ext_tv @table_previous={3}.master_roster_ext_tv\" {0} {4}'.format(DQC_PATH, WEEK_ID, WEEK_SCHEMA_0, PREVIOUS_WEEK_1, CONFIG_PATH)
                , dag=dag)

                dqc_mr_vod.set_upstream(master_roster_vod_view_creation)

    if "HWEDVR" in TaskList:
        PLATFORM = 'dvr'
        DVR_HWE_SQL = ''
        if WEEK_ID >= 964 and WEEK_ID < 974:
            DVR_HWE_SQL = 'dvr_hwe_TWC_exclusion.sql'
        else:
            DVR_HWE_SQL = 'dvr_hwe.sql'
        dvr_hwe = BashOperator(
            task_id='dvr_hwe',
            bash_command='snowsql --config {}'.format(CONFIG_PATH) +
                         ' -f {0}/{1} '.format(BASE_SQL_PATH, DVR_HWE_SQL) +
                         ' -D WEEK_SCHEMA_0={} '.format(WEEK_SCHEMA_0) +
                         ' -D WEEK_0={} '.format(WEEK_ID) +
                         ' -D platform={} '.format(PLATFORM) +
                         ' -D START_DATE={} '.format(time_id_to_yyyymmdd(get_first_time_id_in_week_id(int(WEEK_ID)) - 2)) +
                         ' -D END_DATE={} '.format(time_id_to_yyyymmdd((get_last_time_id_in_week_id(int(WEEK_ID))) + 2)) +
                         ' -D ACTUAL_START_DATE="{}" '.format(time_id_to_yyyymmdd(get_first_time_id_in_week_id(int(WEEK_ID))) + ' 06:00:00') +
                         ' -D ACTUAL_END_DATE="{}" '.format(time_id_to_yyyymmdd(get_last_time_id_in_week_id(int(WEEK_ID)) + 1) + ' 05:59:59') +
                         ' -D START_DATE_ID={} '.format(START_DATE_ID) +
                         ' -D END_DATE_ID={} '.format(END_DATE_ID)
            , dag=dag)

        if DATA_QUALITY_CHECK == "TRUE":
            dqc_hwe_dvr = BashOperator(
                task_id='dqc_hwe_dvr',
                bash_command='{0}/dataqualitycheck.sh HWE hwe {1} DVR \"@table_name={2}.etv_esp_dvr_tv_hwe @table_previous={3}.etv_esp_dvr_tv_hwe\" {0} {4}'.format(DQC_PATH, WEEK_ID, WEEK_SCHEMA_0, PREVIOUS_WEEK_1, CONFIG_PATH)
            , dag=dag)

    if "MRDVR" in TaskList:
        PLATFORM = 'dvr'
        if WEEK_ID > 899:
            master_roster_dvr = BashOperator(
                task_id='master_roster_dvr',
                bash_command='snowsql --config {}'.format(CONFIG_PATH) +
                             ' -f {}/master_roster_w_activity_flag_DVR.sql '.format(BASE_SQL_PATH) +
                             ' -D WEEK_SCHEMA_0={} '.format(WEEK_SCHEMA_0) +
                             ' -D platform={} '.format(PLATFORM) +
                             ' -D startTime={} '.format(START_TIME) +
                             ' -D endTime={} '.format(END_TIME)
                , dag=dag)

            master_roster_dvr_view_creation = BashOperator(
                task_id='master_roster_dvr_view_creation',
                bash_command='snowsql --config {}'.format(CONFIG_PATH) +
                             ' -f {}/master_roster_w_activity_flag_view_creation.sql '.format(BASE_SQL_PATH) +
                             ' -D WEEK_SCHEMA_0={} '.format(WEEK_SCHEMA_0)
                , dag=dag)

            if DATA_QUALITY_CHECK == "TRUE":
                dqc_mr_dvr = BashOperator(
                    task_id='dqc_mr_dvr',
                    bash_command='{0}/dataqualitycheck.sh Pre_Espresso master_roster_active {1} TV \"@table_name={2}.master_roster_ext_tv @table_previous={3}.master_roster_ext_tv\" {0} {4}'.format(DQC_PATH, WEEK_ID, WEEK_SCHEMA_0, PREVIOUS_WEEK_1, CONFIG_PATH)
                , dag=dag)

                dqc_mr_dvr.set_upstream(master_roster_dvr_view_creation)

        else:
            master_roster_dvr_pdg = BashOperator(
                task_id='master_roster_dvr_pdg',
                bash_command='snowsql --config {}'.format(CONFIG_PATH) +
                             ' -f {}/master_roster_pdg_dvr.sql '.format(BASE_SQL_PATH) +
                             ' -D WEEK_SCHEMA_0={} '.format(WEEK_SCHEMA_0) +
                             ' -D platform={} '.format(PLATFORM) +
                             ' -D startTime={} '.format(START_TIME) +
                             ' -D endTime={} '.format(END_TIME)
                , dag=dag)

            master_roster_dvr_pdg_view_creation = BashOperator(
                task_id='master_roster_dvr_pdg_view_creation',
                bash_command='snowsql --config {}'.format(CONFIG_PATH) +
                             ' -f {}/master_roster_w_activity_flag_pdg.sql '.format(BASE_SQL_PATH) +
                             ' -D WEEK_SCHEMA_0={} '.format(WEEK_SCHEMA_0)
                , dag=dag)

            if DATA_QUALITY_CHECK == "TRUE":
                dqc_mr_dvr = BashOperator(
                    task_id='dqc_mr_dvr',
                    bash_command='{0}/dataqualitycheck.sh Pre_Espresso master_roster_active {1} TV \"@table_name={2}.master_roster_ext_tv @table_previous={3}.master_roster_ext_tv\" {0} {4}'.format(DQC_PATH, WEEK_ID, WEEK_SCHEMA_0, PREVIOUS_WEEK_1, CONFIG_PATH)
                , dag=dag)

                dqc_mr_dvr.set_upstream(master_roster_dvr_view_creation)

        if WEEK_ID >= 964 and WEEK_ID < 974:
            master_roster_dvr_twc = BashOperator(
                task_id='master_roster_dvr_twc',
                bash_command='snowsql --config {}'.format(CONFIG_PATH) +
                             ' -f {}/master_roster_w_activity_flag_TWC_exclusion.sql '.format(BASE_SQL_PATH) +
                             ' -D WEEK_SCHEMA_0={} '.format(WEEK_SCHEMA_0) +
                             ' -D platform={} '.format(PLATFORM)
                , dag=dag)

            if DATA_QUALITY_CHECK == "TRUE":
                dqc_mr_dvr = BashOperator(
                    task_id='dqc_mr_dvr',
                    bash_command='{0}/dataqualitycheck.sh Pre_Espresso master_roster_active {1} TV \"@table_name={2}.master_roster_ext_tv @table_previous={3}.master_roster_ext_tv\" {0} {4}'.format(DQC_PATH, WEEK_ID, WEEK_SCHEMA_0, PREVIOUS_WEEK_1, CONFIG_PATH)
                , dag=dag)

                dqc_mr_dvr.set_upstream(master_roster_dvr_view_creation)


    if "HWEVOD" in TaskList:
        vod_hwe.set_upstream(vod_to_tvd_title_and_owner_mapping)
    if "HWEVOD" in TaskList:
        if "MRVOD" in TaskList:
            if WEEK_ID > 899:
                if DATA_QUALITY_CHECK == "TRUE":
                    dqc_hwe_vod.set_upstream(vod_hwe)
                    master_roster_vod.set_upstream(dqc_hwe_vod)
                else:
                    master_roster_vod.set_upstream(vod_hwe)
                master_roster_vod_view_creation.set_upstream(master_roster_vod)
            else:
                master_roster_vod_pdg.set_upstream(vod_hwe)
                master_roster_vod_pdg_view_creation.set_upstream(master_roster_vod_pdg)
            if WEEK_ID >= 964 and WEEK_ID < 974:
                master_roster_vod_twc.set_upstream(master_roster_vod_view_creation)
    if "HWEDVR" in TaskList:
        if "MRDVR" in TaskList:
            if WEEK_ID > 899:
                if DATA_QUALITY_CHECK == "TRUE":
                    dqc_hwe_dvr.set_upstream(dvr_hwe)
                    master_roster_dvr.set_upstream(dqc_hwe_dvr)
                else:
                    master_roster_dvr.set_upstream(dvr_hwe)
                master_roster_dvr_view_creation.set_upstream(master_roster_dvr)
            else:
                master_roster_dvr_pdg.set_upstream(dvr_hwe)
                master_roster_dvr_pdg_view_creation.set_upstream(master_roster_dvr_pdg)
            if WEEK_ID >= 964 and WEEK_ID < 974:
                master_roster_dvr_twc.set_upstream(master_roster_dvr_view_creation)

    return dag


etv_tv_dvr_vod_hwe_dag = etv_tv_dvr_vod_hwe('etv_tv_dvr_vod_hwe')
